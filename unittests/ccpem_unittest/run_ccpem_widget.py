#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Simple template to display widget.  To be expanded to full unit test.
'''

import sys
import os
from PyQt4 import QtGui, QtCore
import ccpem_core.gui.icons as icons
from ccpem_core.gui.text_browser import text_browser

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.setWindowTitle('CCP-EM | Test widget')
        tb = text_browser.TextBrowser(filepath='run_ccpem_widget.py')
        self.setCentralWidget(tb)

def main():
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
    mw = MainWindow()
    mw.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()
