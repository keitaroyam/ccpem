#!/bin/bash

# ccpem-python
#
# Runs python with a custom PYTHONPATH so ccpem can find all required packages
#
# To use your own copy of the CCP-EM code (for example, a checkout of the
# ccp-em Git repository), set the CCPEM_DEV_SRC environment variable to point to
# your ccpem directory path.
#
# If modeller is installed, set the MODELLER_DIR environment variable so it can
# be used for Flex-EM.

# ccpem-python can be used before the setup is sourced (e.g. during build)
CCPEM="$(cd "$(dirname "$0")/.." && pwd)"

# Old PYTHONPATH is ignored -- it may be set for different Python version.
# Special case when Windows Python is called from MSYS/MinGW shell is handled
# near to "python.exe" below (";" must be used as a separator in that case).

# Choose the correct ccpem path to use

# First define the default ccpem location for convenience
DEFAULT_CCPEM_SRC_DIR=$CCPEM/lib/py2/ccpem

# Unset QT_PLUGIN_PATH as this can cause conflicts on some systems
unset QT_PLUGIN_PATH

# Check if the user has set CCPEM_DIR
if [ -n "$CCPEM_DIR" ]; then
    echo "Error: CCPEM_DIR is set, but this is no longer used. Please set CCPEM_DEV_SRC instead."
    exit 1
fi

# Check if the user has set CCPEM_DEV_SRC
if [ -n "$CCPEM_DEV_SRC" ]; then
    # CCPEM_DEV_SRC is set; check if the directory exists
    if [ ! -d "$CCPEM_DEV_SRC" ]; then
        echo "  Warning: CCPEM_DEV_SRC is set to \"$CCPEM_DEV_SRC\" but this is not a valid path."
        echo "  Falling back to default CCP-EM location: \"$DEFAULT_CCPEM_SRC_DIR\""
        CCPEM_DEV_SRC=$DEFAULT_CCPEM_SRC_DIR
    fi
    CCPEM_SRC_DIR=$CCPEM_DEV_SRC
else
    # CCPEM_DEV_SRC is not set; use the default
    CCPEM_SRC_DIR=$DEFAULT_CCPEM_SRC_DIR
fi

# Set python paths
export PYTHONPATH="$CCPEM_SRC_DIR/unittests:$CCPEM_SRC_DIR/src:$CCPEM_SRC_DIR/src/ccpem_core:$CCPEM_SRC_DIR/src/ccpem_progs/emdb_sfftk/sff:$CCPEM/lib/python2.7/site-packages:$CCPEM/lib/py2:$CCPEM/lib/py2/site-packages"

### Modeller path setup - only required for linux... mac found automatically

# Autoset modeller path if installed
MODELLER_PATH=$CCPEM/lib/modeller/install/lib/x86_64-intel8
if [ -d $MODELLER_PATH ]; then
    export PYTHONPATH="$PYTHONPATH:$CCPEM/lib/modeller/install/modlib:$CCPEM/lib/modeller/install/lib/x86_64-intel8/python2.5"
    export LD_LIBRARY_PATH="$MODELLER_PATH:$LD_LIBRARY_PATH"
fi

# Append modeller to python path if set via env var
if  [ -n "$MODELLER_DIR" ]; then
    export PYTHONPATH="$PYTHONPATH:$MODELLER_DIR"
fi

# Find modeller path for Mac
if [[ "$OSTYPE" = "darwin"* ]]; then
  for VER in 29 28 27 26 25 24 23 22 21 20 19 18 17 16
  do
    MAC_MOD1=/Library/modeller-9.$VER/lib/mac10v4
    if test -d $MAC_MOD1; then
      export PYTHONPATH="$PYTHONPATH:$MAC_MOD1"
    fi
    MAC_MOD2=/Library/modeller-9.$VER/modlib
    if test -d $MAC_MOD2; then
      export PYTHONPATH="$PYTHONPATH:$MAC_MOD2"
    fi
  done
fi

if test -d $CCPEM/Frameworks/Python.framework; then
    export DYLD_FALLBACK_LIBRARY_PATH=$CCPEM/lib:$DYLD_FALLBACK_LIBRARY_PATH
    # Hide Qt warnings for Apple
    export QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT=1
    exec $CCPEM/Frameworks/Python.framework/Versions/2.7/bin/python2.7 "$@"
elif test -e $CCPEM/libexec/python2.7; then
    export QT_NO_DEBUG_OUTPUT QT_NO_WARNING_OUTPUT=1
    exec $CCPEM/libexec/python2.7 "$@"   # location in dev. build
elif test -e $CCPEM/../python2.7/python.exe; then
    exec $CCPEM/../python2.7/python.exe "$@"   # on Windows python is not in bin/
else
    # $CCPEM/libexec/python has RPATH set to $CCPEM/lib. LD_LIBRARY_PATH is
    # needed only for system Python, because extension modules on its own
    # don't have rpaths pointing to $CCPEM/lib (yet)
    export LD_LIBRARY_PATH=$CCPEM/lib:$LD_LIBRARY_PATH
    exec python2.7 "$@"   # any python 2.7 should also be fine
fi
