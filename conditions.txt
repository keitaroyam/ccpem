 NOTICE TO THE LICENSEE

 Please read the following Conditions of Use and the appropriate Licence
 Agreement carefully before completing the installation process and using the
 software. These Conditions and the Licence Agreement are legal documents, and
 together contain information on allowed usage of the software suite, warranty
 information and liability disclaimers. If you do not agree with these
 Conditions, then do not complete the installation and do not use the software.

 These conditions have been updated for release 1.0.0 of the CCP-EM software
 suite, and all sites are required to agree to these conditions of use. You
 should only be prompted for this agreement once.

 The Conditions are as follows:

 a. For commercial users and those doing commercial work:
    Please contact CCP-EM at ccpemlicensing@stfc.ac.uk to arrange a commercial
    licence. If you have arranged or renewed a commercial licence in the last
    12 months, then no action is required. By agreeing to these conditions,
    you confirm that you have a valid and current CCP-EM commercial licence.

 b. For users at non-profit sites and not engaged in commercial work:
    Please print and complete an Academic licence, sign it and send it by
    email to ccpemlicensing@stfc.ac.uk or by post to CCP-EM at the address
    given on the licence. This licence is valid for 1.0.0 and subsequent
    versions until further notice. A copy of the licence can be found in PDF
    format in the 'licence' directory of this CCP-EM distribution
    (CCP-EM_academic_software_licence.pdf) and on the CCP-EM web site at
    www.ccpem.ac.uk. By agreeing to these conditions, you confirm that you
    have returned a licence for CCP-EM version 1.0.0. It is sufficient to
    send the licence - there will be no confirmation that it has been
    received.

 I agree to the above conditions and have done the actions required.
