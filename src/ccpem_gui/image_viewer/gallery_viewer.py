#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Image gallery viewer.
'''

import sys
import os
import collections
from PIL import ImageQt
from PyQt4 import QtGui, QtCore
from ccpem_gui.icons import icon_utils
from ccpem_gui.image_viewer import image_utils


class CCPEMGalleryWindow(QtGui.QMainWindow):
    '''
    CCPEM gallery viewer for display multiple images.
    '''
    def __init__(self,
                 filenames=[],
                 parent=None):
        super(CCPEMGalleryWindow, self).__init__(parent)
        # Set size
        self.setWindowTitle('CCPEM | Gallery Viewer')
        avail_geo = QtGui.QDesktopWidget().availableGeometry(self).size()
        self.resize(QtCore.QSize(avail_geo.width() * 0.45,
                                 avail_geo.height() * 0.45 * 1.618))
        self.gallery_widget = CCPEMGalleryWidget(filenames=filenames,
                                                 parent=self)
        self.splitter = QtGui.QSplitter(QtCore.Qt.Vertical)
        self.splitter.addWidget(self.gallery_widget)
        self.splitter.setStretchFactor(0, 2)
        self.metadata_widget = CCPEMStackedMetaDataWidget(parent=self)
        self.gallery_widget.metadata_signal.connect(
            self.metadata_widget.set_metadata)
        scroll = QtGui.QScrollArea()
        scroll.setWidget(self.metadata_widget)
        scroll.setWidgetResizable(True)
        self.splitter.addWidget(scroll)
        self.setCentralWidget(self.splitter)
        # Set toolbar
        self.set_toolbar()

    def set_toolbar(self):
        self.toolbar = QtGui.QToolBar()
        self.status = 'ready'
        # Icons from http://p.yusukekamiyamane.com/
        # Open / close
        open_image_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'folder-open.png')
        self.tb_open = self.toolbar.addAction(
            QtGui.QIcon(open_image_icon),
            'Open images',
            self.gallery_widget.open_images)
        close_images_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'bomb.png')
        self.tb_close = self.toolbar.addAction(
            QtGui.QIcon(close_images_icon),
            'Close all images',
            self.gallery_widget.clear_images)
        self.toolbar.addSeparator()
        # Zoom in / out
        zoom_out_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'minus.png')
        self.tb_zoom_in = self.toolbar.addAction(
            QtGui.QIcon(zoom_out_icon),
            'Decrease size',
            self.gallery_widget.decrease_image_size)
        zoom_in_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'plus.png')
        self.tb_zoom_out = self.toolbar.addAction(
            QtGui.QIcon(zoom_in_icon),
            'Increase size',
            self.gallery_widget.increase_image_size)
        self.toolbar.addSeparator()
        # Brightness up / down
        brightness_down = os.path.join(
            icon_utils.get_image_icons_path(),
            'brightness-control.png')
        brightness_up = os.path.join(
            icon_utils.get_image_icons_path(),
            'brightness-control-up.png')
        self.tb_constrast_up = self.toolbar.addAction(
            QtGui.QIcon(brightness_down),
            'Decrease brightness',
            self.gallery_widget.decrease_brightness)
        self.tb_constrast_down = self.toolbar.addAction(
            QtGui.QIcon(brightness_up),
            'Increase brightness',
            self.gallery_widget.increase_brightness)
        self.toolbar.addSeparator()
        # Contrast up / down
        contrast_down = os.path.join(
            icon_utils.get_image_icons_path(),
            'contrast-control.png')
        contrast_up = os.path.join(
            icon_utils.get_image_icons_path(),
            'contrast-control-up.png')
        self.tb_constrast_down = self.toolbar.addAction(
            QtGui.QIcon(contrast_down),
            'Decrease contrast',
            self.gallery_widget.decrease_contrast)
        self.tb_constrast_up = self.toolbar.addAction(
            QtGui.QIcon(contrast_up),
            'Increase contrast',
            self.gallery_widget.increase_contrast)
        # Auto contrast / Equalise
        self.toolbar.addSeparator()
        auto_contrast_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'contrast.png')
        self.tb_auto_contrast = self.toolbar.addAction(
            QtGui.QIcon(auto_contrast_icon),
            'Auto contrast',
            self.gallery_widget.auto_contrast)
        equalize_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'equalizer-flat.png')
        self.tb_equalize = self.toolbar.addAction(
            QtGui.QIcon(equalize_icon),
            'Equalize',
            self.gallery_widget.equalize)
        # Reset / export images
        self.toolbar.addSeparator()
        redo_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'arrow-circle.png')
        self.tb_reset = self.toolbar.addAction(
            QtGui.QIcon(redo_icon),
            'Reset images',
            self.gallery_widget.reset_images)
        self.addToolBar(self.toolbar)
        #
        redo_icon = os.path.join(
            icon_utils.get_image_icons_path(),
            'disk--arrow.png')
        self.tb_export = self.toolbar.addAction(
            QtGui.QIcon(redo_icon),
            'Export selected images',
            self.gallery_widget.export_images)
        self.addToolBar(self.toolbar)


class CCPEMStackedMetaDataWidget(QtGui.QStackedWidget):
    '''
    Stacked widget for displaying metadata.
    '''
    def __init__(self, metadata=None, parent=None):
        super(CCPEMStackedMetaDataWidget, self).__init__(parent)
        self.image_widget = CCPEMImageMetaDataWidget(title='Image metadata',
                                                     parent=self)
        self.addWidget(self.image_widget)
        self.mrc_widget = CCPEMMRCMetaDataWidget(title='MRC metadata',
                                                 parent=self)
        self.addWidget(self.mrc_widget)
        self.setCurrentWidget(self.image_widget)

    @QtCore.pyqtSlot(dict)
    def set_metadata(self, metadata):
        self.metadata = metadata
        if 'format' in self.metadata:
            self.setCurrentWidget(self.mrc_widget)
        else:
            self.setCurrentWidget(self.image_widget)
        self.currentWidget().set_metadata(self.metadata)


class CCPEMImageMetaDataLabel(object):
    '''
    Data label object
    '''
    def __init__(self,
                 name,
                 label,
                 tooltip=''):
        self.name = name
        self.label = label
        self.tooltip = tooltip

class CCPEMMetaDataWidget(QtGui.QGroupBox):
    '''
    Metadata widget base class.
    '''
    display_items = [collections.OrderedDict()]

    def __init__(self, title=None, metadata=None, parent=None):
        super(CCPEMMetaDataWidget, self).__init__(parent=parent)
        if title is not None:
            self.setTitle(title)
        self.metadata = metadata
        self.image_layout = QtGui.QGridLayout()
        self.set_image_form()
        self.setLayout(self.image_layout)

    def set_image_form(self):
        # Find maximum row size
        max_row_size = max(len(row) for row in self.display_items)
        max_row_size = (max_row_size*2)-1
        for row, items in enumerate(self.display_items):
            for col, item in enumerate(items):
                label = QtGui.QLabel(item.label, self)
                if item.tooltip != '':
                    label.setToolTip(item.tooltip)
                edit = QtGui.QLineEdit(self)
                edit.setReadOnly(True)
                edit.setObjectName(item.name)
                col_adj = col * 2
                self.image_layout.addWidget(label, row, col_adj)
                if len(items) == 1:
                    self.image_layout.addWidget(edit, row, col_adj+1, 1, max_row_size)
                else:
                    self.image_layout.addWidget(edit, row, col_adj+1, 1, 1)

    @QtCore.pyqtSlot(dict)
    def set_metadata(self, metadata):
        self.metadata = metadata
        for row in self.display_items:
            for item in row:
                line_edit = self.findChild(QtGui.QLineEdit, item.name)
                if line_edit is not None:
                    if item.name in self.metadata:
                        val = str(self.metadata[item.name])
                    else:
                        val = ''
                    line_edit.setText(val)


class CCPEMImageMetaDataWidget(CCPEMMetaDataWidget):
    '''
    Image metadata class for standard images.
    '''
    display_items = [[CCPEMImageMetaDataLabel('filename', 'Filename', 'File path'),
                      CCPEMImageMetaDataLabel('mode', 'Mode', 'Image mode')], 
                     #
                     [CCPEMImageMetaDataLabel('height', 'Height (px)', 'Height in pixels'),
                      CCPEMImageMetaDataLabel('width', 'Width (px)', 'Width in pixels')]]

    def __init__(self, title=None, metadata=None, parent=None):
        super(CCPEMImageMetaDataWidget, self).__init__(parent=parent,
                                                       title=title)


class CCPEMMRCMetaDataWidget(CCPEMMetaDataWidget):
    '''
    Image metadata class for mrc format.
    '''
    display_items = [[CCPEMImageMetaDataLabel('filename', 'Filename', 'File path')],
                     #
                     [CCPEMImageMetaDataLabel('nx', 'Cols', 'Number of columns (fastest changing in map)'), 
                      CCPEMImageMetaDataLabel('ny', 'Rows', 'Number of rows'),
                      CCPEMImageMetaDataLabel('nz', 'Sections', 'Number of sections (slowest changing in map)')],
                     #
                     [CCPEMImageMetaDataLabel('xlen', 'X size', 'X cell size'), 
                      CCPEMImageMetaDataLabel('ylen', 'Y size', 'Y cell size'),
                      CCPEMImageMetaDataLabel('zlen', 'Z size', 'Z cell size')],
                     #
                     [CCPEMImageMetaDataLabel('xorg', 'X org', 'X origin'), 
                      CCPEMImageMetaDataLabel('yorg', 'Y org', 'Y origin'),
                      CCPEMImageMetaDataLabel('zorg', 'Z org', 'Z origin')],
                     #
                     [CCPEMImageMetaDataLabel('mapc', 'Col axis', 'Axis corresponding to columns (1,2,3 for X,Y,Z)'), 
                      CCPEMImageMetaDataLabel('mapr', 'Row axis', 'Axis corresponding to rows (1,2,3 for X,Y,Z)'),
                      CCPEMImageMetaDataLabel('maps', 'Sec axis', 'Axis corresponding to sections (1,2,3 for X,Y,Z)')]]

    def __init__(self, title=None, metadata=None, parent=None):
        super(CCPEMMRCMetaDataWidget, self).__init__(parent=parent,
                                                     title=title)


class FormatSelectionDialog(QtGui.QDialog):
    '''
    Dialog to request range of images.
    '''
    def __init__(self, parent=None):
        QtGui.QLabel.__init__(self, parent)
        self.setWindowTitle('Image export format')
        layout = QtGui.QGridLayout()
        self.format = '.png'
        self.setLayout(layout)
        #
        label = QtGui.QLabel()
        text = ('Select image export format')
        label.setText(text)
        layout.addWidget(label, 0, 0, 1, 3)
        #
        label = QtGui.QLabel('Export format')
        self.select_box = QtGui.QComboBox()
        self.pil_formats = image_utils.pil_ext
        self.select_box.addItems(image_utils.pil_ext)
        self.select_box.currentIndexChanged.connect(
            self.set_format)
        self.select_box.setCurrentIndex(self.pil_formats.index(self.format))
        layout.addWidget(label, 1, 0)
        layout.addWidget(self.select_box, 1, 1)
        #
        button_box = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok)
        button_box.accepted.connect(self.accept)
        layout.addWidget(button_box, 2, 0, 1, 2)

    def set_format(self):
        self.format = self.pil_formats[self.select_box.currentIndex()]


class CCPEMGalleryWidget(QtGui.QListWidget):
    '''
    CCPEM images gallery.
    '''
    icon_sizes = [50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600,
                  650, 700, 750, 800, 850, 900, 950, 1000]
    metadata_signal = QtCore.pyqtSignal(dict)

    def __init__(self,
                 filenames=[],
                 parent=None):
        super(CCPEMGalleryWidget, self).__init__(parent)
        # Setup preferences
        self.setViewMode(self.IconMode)
        self.setResizeMode(self.Adjust)
        self.setSpacing(20)
        self.setMovement(self.Static)
        self.setLayoutMode(self.Batched)
        self.setSelectionMode(self.ExtendedSelection)
        self.setUniformItemSizes(True)
        self.setTextElideMode(QtCore.Qt.ElideRight)
        layout = QtGui.QGridLayout()
        layout.setAlignment(QtCore.Qt.AlignHCenter)
        self.setLayout(layout)
        # Set on-selection actions
        self.itemClicked.connect(self.on_item_clicked)
        # Get images
        self.images = []
        self.filenames = filenames
        if not isinstance(self.filenames, list):
            self.filenames = [self.filenames]
        self.get_images(self.filenames)
        # Set image size
        self.icon_size = self.icon_sizes[2]
        self.set_image_size()
        # Display images
        self.set_display()

    def get_images(self, filenames):
        '''
        Open give files.  Filenames can be list or single string.
        '''
        if not isinstance(filenames, list):
            filenames = [filenames]
        if len(filenames) > 0:
            new_images = image_utils.import_images(filenames)
            if new_images is not None:
                self.images += new_images

    def reset_images(self):
        '''
        Reload images and display.
        '''
        self.images = image_utils.import_images(self.filenames)
        self.set_display()

    def clear_images(self):
        '''
        Clear images.  If no images selected all removed, else remove selected.
        '''
        self.images = []
        self.filenames = []
        self.clear()

    def open_images(self):
        '''
        Open desktop browser to select images.
        '''
        # Allowed extensions
        image_ext = ''
        mrc_ext = ''
        for ext in image_utils.mrc_ext:
            mrc_ext += ('*'+ext+' ')
        for ext in image_utils.pil_ext:
            image_ext += ('*'+ext+' ')
        allowed_ext = 'MRC ({0});;Images ({1});;All files (*.*)'.format(
            mrc_ext, image_ext)
        q_settings = QtCore.QSettings()
        # Get previous path
        dialog_path = q_settings.value('IMAGE_DIR_KEY').toString()
        if not os.path.exists(dialog_path):
            dialog_path = QtCore.QDir.currentPath()
        file_list = QtGui.QFileDialog.getOpenFileNames(
            None,
            'Open a image file',
            dialog_path,
            allowed_ext)
        filenames = map(str, file_list)
        # Save path for next opening
        if len(filenames) > 0:
            if os.path.exists(filenames[0]):
                q_settings.setValue('IMAGE_DIR_KEY',
                                    os.path.dirname(filenames[0]))
        self.get_images(filenames)
        self.filenames += filenames
        #
        self.set_display()

    def export_images(self):
        '''
        Export selected images to selected format.
        '''
        format_dialog = FormatSelectionDialog()
        if format_dialog.exec_():
            image_format = format_dialog.format
        if len(self.selectedItems()) == 0:
            self.selectAll()
        for sel in self.selectedItems():
            fileout = os.path.splitext(sel.metadata['filename'])[0]
            fileout += image_format
            sel.image.pil_image.save(fileout)

    def set_brightness(self, factor=1.25):
        '''
        Set brightness.
        '''
        if len(self.selectedItems()) == 0:
            self.selectAll()
        for item in self.selectedItems():
            item.image.pil_image = image_utils.set_brightness_pil_image(
                item.image.pil_image,
                factor)
            pixmap = self.pixmap_from_pil_image(pil_image=item.image.pil_image)
            self.set_item_icon_from_pixmap(item=item, pixmap=pixmap)

    def increase_brightness(self):
        self.set_brightness(factor=1.25)

    def decrease_brightness(self):
        self.set_brightness(factor=0.8)

    def set_contrast(self, factor):
        '''
        Set contrast.
        '''
        if len(self.selectedItems()) == 0:
            self.selectAll()
        for item in self.selectedItems():
            item.image.pil_image = image_utils.set_constrast_pil_image(
                item.image.pil_image,
                factor)
            pixmap = self.pixmap_from_pil_image(pil_image=item.image.pil_image)
            self.set_item_icon_from_pixmap(item=item, pixmap=pixmap)

    def increase_contrast(self):
        self.set_contrast(factor=1.25)

    def decrease_contrast(self):
        self.set_contrast(factor=0.8)

    def auto_contrast(self):
        '''
        Maximize (normalize) image contrast. This function calculates a
        histogram of the input image, removes cutoff percent of the lightest
        and darkest pixels from the histogram, and remaps the image so that
        the darkest pixel becomes black (0), and the lightest becomes white
        (255).
        '''
        if len(self.selectedItems()) == 0:
            self.selectAll()
        for item in self.selectedItems():
            item.image.pil_image = image_utils.set_auto_contrast_pil_image(
                item.image.pil_image)
            pixmap = self.pixmap_from_pil_image(pil_image=item.image.pil_image)
            self.set_item_icon_from_pixmap(item=item, pixmap=pixmap)

    def equalize(self):
        '''
        Equalize the image histogram. This function applies a non-linear
        mapping to the input image, in order to create a uniform distribution
        of grayscale values in the output image.
        '''
        if len(self.selectedItems()) == 0:
            self.selectAll()
        for item in self.selectedItems():
            item.image.pil_image = image_utils.set_equalize_pil_image(
                item.image.pil_image)
            pixmap = self.pixmap_from_pil_image(pil_image=item.image.pil_image)
            self.set_item_icon_from_pixmap(item=item, pixmap=pixmap)

    def pixmap_from_pil_image(self, pil_image):
        # Convert pil to qimage.  N.B. imageqt must be cast as if
        # is used image distortions occur.
        imageqt = ImageQt.ImageQt(pil_image)
        qimage = QtGui.QImage(imageqt)
        pixmap = QtGui.QPixmap.fromImage(qimage)
        size = QtCore.QSize(
            self.icon_sizes[-1],
            self.icon_sizes[-1])
        #
        pixmap = pixmap.scaled(
            size,
            QtCore.Qt.KeepAspectRatio,
            QtCore.Qt.SmoothTransformation)
        return pixmap

    def set_item_icon_from_pixmap(self, item, pixmap):
        icon = QtGui.QIcon()
        icon.addPixmap(pixmap, icon.Normal)
        icon.addPixmap(pixmap, icon.Selected)
        item.setIcon(icon)

    def set_display(self):
        '''
        Display images.
        '''
        self.clear()
        if self.images is not None:
            for image in self.images:
                pixmap = self.pixmap_from_pil_image(pil_image=image.pil_image)
                icon = QtGui.QIcon()
                icon.addPixmap(pixmap, icon.Normal)
                icon.addPixmap(pixmap, icon.Selected)
                #
                filename = image.metadata['filename']
                text = os.path.basename(filename)
                item = QtGui.QListWidgetItem(icon,
                                             text)
                item.filename = filename
                item.setTextAlignment(QtCore.Qt.AlignCenter)
                item.metadata = image.metadata
                item.image = image
                self.addItem(item)

    def set_image_size(self):
        '''
        Set icon size for image display.
        '''
        self.setIconSize(QtCore.QSize(self.icon_size, self.icon_size))

    def increase_image_size(self):
        n = self.icon_sizes.index(self.icon_size)
        n = min(n+1, len(self.icon_sizes)-1)
        self.icon_size = self.icon_sizes[n]
        self.set_image_size()

    def decrease_image_size(self):
        n = self.icon_sizes.index(self.icon_size)
        n = max(n-1, 0)
        self.icon_size = self.icon_sizes[n]
        self.set_image_size()

    def on_item_clicked(self, item):
        '''
        On item click send metadata signal.
        '''
        self.metadata_signal.emit(item.metadata)


def main():
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icon_utils.get_ccpem_icon()))
    from ccpem_core import test_data
    test_image = os.path.join(test_data.get_test_data_path(),
                              'images/mrc/Falcon_2012_06_12-14_33_35_0.mrc')
    assert os.path.exists(test_image)
    mw = CCPEMGalleryWindow(
        filenames=test_image)
    mw.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()
