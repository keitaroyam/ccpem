#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import sys
import os
import os.path

from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4 import QtSql

from ccpem_gui.project_database import sqlite_project_database
from ccpem_gui.project_database import project_manager
from ccpem_core import process_manager

class DateTimeFormatDelegate(QtGui.QStyledItemDelegate):
    '''
    Subclass of QStyledItemDelegate for custom date formatting.
    '''
    def __init__(self, date_format, parent=None):
        super(DateTimeFormatDelegate, self).__init__(parent)
        self.date_format = date_format

    def displayText(self, value, locale):
        return value.toDateTime().toString(self.date_format)


class PathFormatDelegate(QtGui.QStyledItemDelegate):
    '''
    Subclass of QStyledItemDelegate for custom path formatting.
    '''
    def __init__(self, basename=False, parent=None):
        super(PathFormatDelegate, self).__init__(parent)
        self.parent = parent
        self.basename = basename

    def displayText(self, value, locale):
        path = str(value.toString())
        if self.basename:
            return os.path.basename(path)
        else:
            return path


class CCPEMProjectTableView(QtGui.QDialog):
    '''
    Shows summary table of jobs in project.
    '''
    def __init__(self, parent=None, database=None):
        super(CCPEMProjectTableView, self).__init__(parent=parent)
        # Set window
        self.setWindowTitle('CCP-EM | job information')
        if parent is not None:
            self.db = parent.database.database
            self.db_manager = parent.database
        else:
            self.db = database.database
            self.db_manager = database
        self.set_model_view()

        # Set layout
        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.view)
        self.setLayout(layout)

        # Set project infomation
        self.status_bar = QtGui.QStatusBar()
        self.set_status_bar()
        self.status_bar.setToolTip('Project name | project user | project path')
        layout.addWidget(self.status_bar)

        # Update table
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.set_model_select)
        self.timer.start(500)

    def set_status_bar(self):
        project = project_manager.get_project_from_db_path(
            self.db_manager.database_path)
        if project is not None:
            message = project.name + ' | ' + project.user + ' | ' + project.path
            self.status_bar.showMessage(message)

    def set_model_view(self):
        # Set model/view
        self.model = QtSql.QSqlTableModel(self,
                                          self.db)
        self.model.setTable('jobs')
        # Set names
        pretty_name = {'id': 'Job',
                       'program': 'Program',
                       'title': 'Title',
                       'status': 'Status',
                       'starttime': 'Start',
                       'endtime': 'Finish',
                       'dirpath': 'Directory'}
        for name in pretty_name.keys():
            index = self.model.fieldIndex(name)
            self.model.setHeaderData(index,
                                     QtCore.Qt.Horizontal,
                                     QtCore.QVariant(pretty_name[name]))
        self.model.setSort(self.model.fieldIndex('id'),
                           QtCore.Qt.DescendingOrder)
        self.set_model_select()
        # Set view and options
        self.view = QtGui.QTableView()
        self.view.setSortingEnabled(True)
        self.view.setShowGrid(False)
        self.view.verticalHeader().setVisible(False)
        self.view.setModel(self.model)
        self.view.resizeColumnsToContents()
        self.view.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.view.setSelectionMode(QtGui.QAbstractItemView.NoSelection)
        self.view.setToolTip('Click to view job, right click for options')
        self.view.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.view.customContextMenuRequested.connect(self.custom_menu)
        self.view.setTextElideMode(QtCore.Qt.ElideLeft)
        # Set custom delegates
        dt_delegate = DateTimeFormatDelegate(parent=self,
                                             date_format='hh:mm:ss dd/MM/yyyy')
        self.view.setItemDelegateForColumn(
            self.model.fieldIndex('starttime'),
            dt_delegate)
        self.view.setItemDelegateForColumn(
            self.model.fieldIndex('endtime'),
            dt_delegate)
        path_delegate = PathFormatDelegate(parent=self)
        self.view.setItemDelegateForColumn(
            self.model.fieldIndex('dirpath'),
            path_delegate)
        # Hide columns
        for col in ['mode', 'user', 'readytime']:
            index = self.model.fieldIndex(col)
            self.view.hideColumn(index)
        # Move columns
        self.view.horizontalHeader().moveSection(
            self.model.fieldIndex('dirpath'),
            self.view.horizontalHeader().count()-1)
        # Mouse click options
        self.view.clicked.connect(self.on_click)
        # Resize
        self.view.resizeColumnsToContents()
        self.view.horizontalHeader().setStretchLastSection(True)

    def custom_menu(self, pos):
        menu = QtGui.QMenu()
        menu.addAction('Delete job', self.delete_job_action)
        menu.addAction('Clone job', self.clone_job_action)
        menu.addAction('Set status running', self.set_job_status_running)
        menu.addAction('Set status finished', self.set_job_status_finished)
        index = self.view.indexAt(pos)
        id_index = index.sibling(index.row(),
                                 self.model.fieldIndex('id'))
        self.sel_job_id = str(self.model.data(id_index).toString())
        path_index = index.sibling(index.row(),
                                   self.model.fieldIndex('dirpath'))
        self.sel_job_path = str(self.model.data(path_index).toString())
        status = index.sibling(index.row(),
                               self.model.fieldIndex('status'))
        self.sel_job_status = str(self.model.data(status).toString())
        program_index = index.sibling(index.row(),
                                      self.model.fieldIndex('program'))
        self.program = str(self.model.data(program_index).toString())
        menu.exec_(self.view.viewport().mapToGlobal(pos))

    def delete_job_action(self):
        if self.sel_job_status not in ['finished', 'failed']:
            msg = QtGui.QMessageBox()
            msg.setIcon(msg.Warning)
            msg.setText('Job must be finished or terminated before deletion')
            msg.setStandardButtons(msg.Ok)
            retval = msg.exec_()
        else:
            msg = QtGui.QMessageBox()
            msg.setIcon(msg.Warning)
            msg.setText('Delete job {}?'.format(os.path.basename(self.sel_job_path)))
            msg.setInformativeText('Remove job and all job data?')
            msg.setWindowTitle('Delete job?')
            detailed_text = 'Job folder to be removed:\n{0}'.format(
                self.sel_job_path)
            msg.setDetailedText(detailed_text)
            msg.setStandardButtons(msg.Ok | msg.Cancel)
            retval = msg.exec_()
            if retval == msg.Ok:
                self.db_manager.delete_job(job_id=self.sel_job_id,
                                           remove_data=True)

    def clone_job_action(self):
        args_json = os.path.join(self.sel_job_path, 'args.json')
        self.clone_custom(args_json=args_json)

    def clone_custom(self, args_path):
        raise NotImplementedError('Parent should implement this')

    def set_job_status_running(self):
        self.db_manager.update_job_start(job_id=self.sel_job_id)
        path = os.path.join(self.sel_job_path, 'task.ccpem')
        if os.path.exists(path=path):
            process_manager.set_status(path, status='running')

    def set_job_status_finished(self):
        self.db_manager.update_job_end(job_id=self.sel_job_id)
        path = os.path.join(self.sel_job_path, 'task.ccpem')
        if os.path.exists(path=path):
            process_manager.set_status(path, status='finished')

    def set_model_select(self):
        self.model.select()
        while self.model.canFetchMore():
            self.model.fetchMore()

    def on_click(self, index):
        '''
        Return task window with task loaded, except if path field clicked in
        which case return job folder.
        '''
        path_index = index.sibling(index.row(),
                                   self.model.fieldIndex('dirpath'))
        path = str(self.model.data(path_index).toString())
        if index.column() == self.model.fieldIndex('dirpath'):
            if os.path.exists(path):
                QtGui.QDesktopServices.openUrl(QtCore.QUrl('file:///' + path))
        else:
            self.on_click_custom(index, path)

    def on_click_custom(self, index, path):
        raise NotImplementedError('Parent should implement this')

    def closeEvent(self, event):
        self.timer.stop()


def main():
    '''
    For testing.
    '''
    app = QtGui.QApplication(sys.argv)
    db = sqlite_project_database.CCPEMDatabase(
        database_path='./test_data/pyqt4_projectmanager.sqlite')
    project_view = CCPEMProjectTableView(
        database=db)
    project_view.show()
    app.exec_()

if __name__ == '__main__':
    main()
