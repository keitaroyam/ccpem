#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Runs molecular viewer widget.  N.B. not a true unit test, just for debugging.
'''

import sys
import os
import threading
from PyQt4 import QtGui, QtCore
import ccpem_core.gui_ext as icons
from ccpem_core.gui_ext.jsmol_viewer import molecular_viewer
from ccpem_core.ccpem_utils import ccpem_widgets
from ccpem_core.ccpem_utils import ccpem_server
from ccpem_progs import flex_em

class MainWindow(QtGui.QMainWindow):
    def __init__(self, program, use_server=False, parent=None):
        super(MainWindow, self).__init__(parent)
        self.program = program
        assert self.program in ['preview', 'flexem', 'ribfind']
        self.setWindowTitle('CCP-EM | Test molecular browser')
        if use_server:
            # Run with server to show maps (very slow)
            self.add_molecular_browser()
        else:
            # Run without server
            self.add_molecular_browser_no_server()

    def add_molecular_browser_no_server(self):
        self.pdbPreview = molecular_viewer.MolecularBrowserNoServer(parent=self)
        self.pdbPreview.setMinimumSize(QtCore.QSize(800, 800))
        self.pdbPreview.setMaximumSize(QtCore.QSize(800, 800))
        self.setCentralWidget(self.pdbPreview)
        test_pdb_path = os.path.dirname(flex_em.__file__)
        test_pdb_path2 = test_pdb_path
        ribfind_xml_path = test_pdb_path
        test_pdb_path += '/test_data/1akeA.pdb'
        test_pdb_path2 += '/test_data/mdl1.pdb'
        if self.program == 'preview':
            self.pdbPreview.load_file(model1=test_pdb_path2,
                                      model2=test_pdb_path)
        elif self.program == 'flexem':
            self.pdbPreview.load_file(program=self.program,
                                      model1=test_pdb_path2,
                                      model2=test_pdb_path)
        elif self.program == 'ribfind':
            ribfind_xml_path += '/test_data/ribfind.xml'
            self.pdbPreview.load_file(program=self.program,
                                      ribfind_xml=ribfind_xml_path,
                                      model1=test_pdb_path2)

    def add_molecular_browser(self):
        self.project_directory = '/home/tom/test_ccpem_project'
        self.pdbPreview = molecular_viewer.MolecularBrowser(parent=self)
        self.pdbPreview.setMinimumSize(QtCore.QSize(800, 800))
        self.pdbPreview.setMaximumSize(QtCore.QSize(800, 800))
        self.pdbPreview.setUrl(QtCore.QUrl(ccpem_widgets._fromUtf8("about:blank")))
        self.pdbPreview.setObjectName(ccpem_widgets._fromUtf8("pdbPreview"))
        self.setCentralWidget(self.pdbPreview)
        # Set up server
        self.setup_local_port()
        # Get pdb file, symbolic link to project directory
        test_pdb_path = os.path.dirname(flex_em.__file__)
        test_pdb_path += '/test_data/1akeA.pdb'
        test_mrc_path = os.path.dirname(flex_em.__file__)
        test_mrc_path += '/test_data/1akeA_10A.mrc'
        assert os.path.isfile(test_pdb_path)
        pdb_symlink = self.project_directory + '/1akeA.pdb'
        mrc_symlink = self.project_directory + '/1akeA.mrc'
        if os.path.isfile(pdb_symlink):
            os.remove(pdb_symlink)
        os.symlink(test_pdb_path, pdb_symlink)
        if os.path.isfile(mrc_symlink):
            os.remove(mrc_symlink)
        os.symlink(test_mrc_path, mrc_symlink)
        # Send symbolic link and port to pdbpreview
        hosted_pdb = "http://localhost:"+str(self.portnumber)+'/1akeA.pdb'
        hosted_map = "http://localhost:"+str(self.portnumber)+'/1akeA.mrc'
        self.pdbPreview.load_file(programme="preview",
                                  model1=hosted_pdb,
                                  port=self.portnumber)

    def setup_local_port(self):
        self.internal_server = ccpem_server.CCPEMServer()
        self.internal_server.server_root = self.project_directory
        self.server = self.internal_server.serversetup()
        httpdthread = threading.Thread(target=self.server.serve_forever)
        httpdthread.start()
        self.portnumber = self.server.server_port

def main():
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))
#     mw = MainWindow(program='preview')
#     mw.show()
#     mw_rib = MainWindow(program='ribfind')
#     mw_rib.show()
#     mw_flex = MainWindow(program='flexem')
#     mw_flex.show()
    mw_server = MainWindow(program='preview', use_server=True)
    mw_server.show()
    app.exec_()
    sys.exit()

if __name__ == '__main__':
    main()
