import sys
import os
import subprocess

from ccpem_core import ccpem_utils
from ccpem_core import settings


def get_linux_terminal_command():
    command = settings.which('gnome-terminal')
    if command is None:
        command = settings.which('konsole')
    if command is None:
        command = settings.which('xterm')
    return command


def run_terminal(working_directory=None):
    if sys.platform == 'linux2':
        command = get_linux_terminal_command()
        find_wd = False
        if working_directory is not None:
            if os.path.exists(working_directory):
                find_wd = True
        if command is not None:
            args = []
            if find_wd:
                if 'gnome-terminal' in command:
                    arg = '--working-directory={0}'.format(working_directory)
                    args = [arg]
                elif 'konsole' in command:
                    args = '--workdir {0}'.format(working_directory)
                elif 'xterm' in command:
                    print 'Xterm not terminal launch surported'
                    # Can't reliably pass working directory to xterm
                    pass
            run_detatched_process(command=command,
                                  args=args)
    elif sys.platform == 'darwin':
        if working_directory is None:
                working_directory = ''
        osascript = '''osascript -e 'tell application "Terminal" \
                       to do script "cd {0}"' '''.format(
            working_directory)
        os.system(osascript)
    else:
        print 'Window terminal launch not supported yet...'


def run_coot(args=None):
    '''
    Run coot with optional command line args e.g. :
    coot --pdb=foo.pdb --map=foo.mrc

    args must be a list or None
    '''
    coot_command = settings.which('coot')
    if coot_command is None:
        text = 'Warning: Coot not found'
        ccpem_utils.print_error(message=text)
    else:
        # Run Coot in CCP4 mode, which puts the output in a separate window
        # rather than on the console
        full_args = ['--ccp4-mode']
        if args is not None:
            full_args += args
        run_detatched_process(command=coot_command,
                              args=full_args)


def run_ccp4mg(args=None):
    '''
    Run ccp4mg with optional command line args e.g. :
    ccp4mg -pdb foo.pdb -map foo.map
    N.B. ccp4mg doesn't currently automatically recognise .mrc extension.

    args must be a list or None
    '''
    ccp4mg_command = settings.which('ccp4mg')
    if ccp4mg_command is None:
        text = 'Warning: CCP4MG not found'
        ccpem_utils.print_error(message=text)
    else:
        if args is None:
            args = []
        run_detatched_process(command=ccp4mg_command,
                              args=args)


def run_chimera(args=None):
    '''
    Run chimera with optional command line args e.g. :
    chimera foo.pdb foo.map

    args must be a list or None
    '''
    chimera_command = settings.which('chimera')
    if chimera_command is None:
        text = 'Warning: Chimera not found'
        ccpem_utils.print_error(message=text)
    else:
        if args is None:
            args = []
        run_detatched_process(command=chimera_command,
                              args=args)

def run_chimera_x(args=None):
    '''
    Run ChimeraX with optional command line args e.g. :
    chimera foo.pdb foo.map

    args must be a list or None
    '''
    chimera_command = settings.which('ChimeraX')
    if chimera_command is None:
        text = 'Warning: ChimeraX not found'
        ccpem_utils.print_error(message=text)
    else:
        if args is None:
            args = []
        run_detatched_process(command=chimera_command,
                              args=args)

def run_pymol(args=None):
    '''
    Run PyMOL with optional command line args
    '''
    pymol_command = settings.which('pymol')
    if pymol_command is None:
        text = 'Warning: PyMOL not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=pymol_command,
                              args=args)


def run_relion_display():
    '''
    Run "relion_display --gui" to view map files
    '''
    relion_display_command = settings.which('relion_display')
    if relion_display_command is None:
        text = 'Warning: relion_display not found'
        ccpem_utils.print_error(message=text)
    else:
        run_detatched_process(command=relion_display_command, args=['--gui'])


def run_detatched_process(command, args=None, cwd=None):
    if args is None:
        args = []
    assert command is not None
    assert type(args) is type([])

    # Run with python subprocess to allow environment to be set.  This is
    # required to reset PYTHONPATH and LD_LIBRARY_PATH to remove ccpem python
    # and modeller libraries which can cause conflicts.
    env = os.environ.copy()
    env.pop('PYTHONPATH', None)
    env.pop('LD_LIBRARY_PATH', None)

    command = [command] + args
    subprocess.Popen(command, env=env, cwd=cwd)
