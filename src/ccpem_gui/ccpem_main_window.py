import sys
import os
from PyQt4 import QtGui
from PyQt4 import QtCore
from ccpem_gui import icons
from ccpem_core import settings


'''
Launch script for ccpem main window.  Separated from ccpem_mw module as imports
are time consuming and best done whilst splash screen is visible.
'''

def create_splash():
    # Create and display the splash screen
    image_path = os.path.join(
        icons.icon_utils.get_icons_path(),
        'splash/splash_v1.png')
    splash_pix = QtGui.QPixmap(image_path)
    splash = QtGui.QSplashScreen(splash_pix,
                                 QtCore.Qt.WindowStaysOnTopHint)
    splash.setMask(splash_pix.mask())
    splash.show()
    splash.repaint()
    splash.showMessage(' ')
    return splash


def main():
    # Start main Qt app
    app = QtGui.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon(icons.icon_utils.get_ccpem_icon()))

    # Set splash
    splash = create_splash()

    # Set styles
    args = settings.get_ccpem_settings()
    if args.style.value == 'default':
        if sys.platform == 'linux' or sys.platform == 'linux2':
            args.style.value = 'plastique'
    app.setStyle(args.style.value)
    app.setStyleSheet('''QToolTip {background-color: black;
                                   color: white;
                                   border: black solid 1px
                                   }''')

    # Set locale
    QtCore.QLocale.setDefault(QtCore.QLocale('en_UK'))

    # This seems to be necessary to make sure the splash image is shown
    # properly every time. Otherwise the splash is sometimes left blank.
    splash.repaint()
    app.processEvents()

    # Launch main GUI
    # This import triggers multiple other imports and is slow. Therefore
    # imported in function not globally so delay occurs whilst splash
    # screen is visible.
    # Specifically import ccpem_tasks is major time usage.

    from ccpem_gui import ccpem_mw
    window = ccpem_mw.CCPEMMainWindow(splash=splash, args=args)
    window.show()
    # Active and raise window (required for apple)
    window.activateWindow()
    window.raise_()

    # Close splash after main window rendered
    splash.finish(window)
    window.set_tasks_projects()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
