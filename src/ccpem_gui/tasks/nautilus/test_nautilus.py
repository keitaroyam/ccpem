#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Test nautilus task
'''

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.nautilus import nautilus_task
from ccpem_gui.tasks.nautilus import nautilus_window
from ccpem_core.test_data.tasks import nautilus as nautilus_test
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

app = QtGui.QApplication(sys.argv)


class NautilusTest(unittest.TestCase):
    '''
    Unit test for Nautilus (invokes GUI).
    '''

    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(nautilus_test.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    def test_nautilus_window_integration(self):
        '''
        Test Nautilus auto building pipeline via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Nautilus')
        # Unit test args contain relative paths, must change to this directory
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = nautilus_task.Nautilus(
            job_location=self.test_output,
            args_json=args_path)
        print(run_task.args.output_args_as_text())
        # Run w/out gui
#         run_task.run_task()
        # Run w/ gui
        window = nautilus_window.NautilusWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            window.tool_bar.widgetForAction(window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        job_completed = False
        xmlout_found = False
        timeout = 0
        # Global refine, nautilus, and libg stdout (i.e. last job in pipeline)
        stdout_ref = run_task.pipeline.pipeline[-1][-1].stdout
        stdout_libg = run_task.pipeline.pipeline[-2][-1].stdout
        stdout_nau = run_task.pipeline.pipeline[-3][-1].stdout
        stdout_suffix = run_task.args.ncycle.value
        assert os.path.basename(
            stdout_ref) == 'refmacrefineglobal' + str(stdout_suffix) + '_stdout.txt'
        assert os.path.basename(stdout_nau) == 'nautilusbuild' + \
            str(stdout_suffix) + '_stdout.txt'
        assert os.path.basename(stdout_libg) == 'dnaorrnabasepairrestraints' + \
            str(stdout_suffix) + '_stdout.txt'
        delay = 5.0
        timeout = 500
        run_time = 0

        while not job_completed and run_time < timeout:
            print('Nautilus running for {0} secs (timeout = {1})'.format(
                run_time, timeout))
            time.sleep(delay)
            timeout += delay
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            # Check job is finished
            if status == 'finished':
                if os.path.isfile(stdout_nau) and os.path.isfile(stdout_ref):
                    tail = ccpem_utils.tail(stdout_ref, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        # Check Nautilus output exists
                        nautilus_pdb = \
                            run_task.process_nautilus_pipeline.pdbout
                        if os.path.exists(nautilus_pdb):
                            job_completed = True
                        # Get result summary
                        getResultLines_nau(stdout_nau)
                        xmlfilename = 'program' + str(stdout_suffix) + '.xml'
                        if os.path.isfile(xmlfilename):
                            xmlout_found = True
                            print('Found {0}'.format(xmlfilename))
                            getXML_nau(xmlfilename)

        # Check timeout
        assert run_time < timeout
        # Check job completed
        assert job_completed
        # Check program.xml produced
        assert xmlout_found, 'xml output not found'
        # Check fragment length
        #assert longest_fragment >= 4


def getResultLines_nau(filename):
    found_summary = False
    found_naustart = False
    count = 1
    with open(filename, 'r') as openfile:
        for line in openfile:
            if ' cnautilus' in line:
                print('Nautilus Pipeline Cycle {0}' .format(count))
                found_naustart = True
                cycle_line = ''
                built_line = ''
            if found_naustart:
                if '--SUMMARY_BEGIN--' in line:
                    found_summary = True
            if found_summary:
                if 'Internal cycle' in line:
                    cycle_line = line.strip('\n')
                if 'built' in line:
                    built_line = line.strip('\n')
                if '--SUMMARY_END--' in line:
                    if built_line:
                        print('{0}, {1}' .format(cycle_line, built_line))
                        found_summary = False
                    else:
                        found_summary = False
            if 'cnautilus: Normal termination' in line:
                found_summary = False
                found_naustart = False
                count = count + 1


def getXML_nau(filename):
    from lxml import etree
    '''
    Get XML output
    '''
    tree = etree.parse(filename)
    for child in tree.findall('Final'):
        FragBuilt = child.find('FragmentsBuilt')
        ResBuilt = child.find('ResiduesBuilt')
        ResSeq = child.find('ResiduesSequenced')
        ResLongFrag = child.find('ResiduesLongestFragment')

    print("")
    print("{0} : {1}" .format(FragBuilt.tag, FragBuilt.text))
    print("{0} : {1}" .format(ResBuilt.tag, ResBuilt.text))
    print("{0} : {1}" .format(ResSeq.tag, ResSeq.text))
    print("{0} : {1}" .format(ResLongFrag.tag, ResLongFrag.text))
    print("")


if __name__ == '__main__':
    unittest.main()
