#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
'''
Opens map and solutions of DockEM (passed as a string) in Chimera
'''

from sys import argv
from chimera import runCommand as rc
import VolumeViewer as VV

def set_number_volume_histograms(n=5):
    vd = VV.volumedialog
    d = vd.Volume_Dialog()
    dop = d.display_options_panel
    dop.max_histograms.set(5)

# Increase default number of histograms displayed
set_number_volume_histograms()

# Expected imports from command line
search_object_mask = argv[1]
target_map_filtered = argv[2]
search_object_filtered = argv[3]
search_object = argv[4]
mask_threshold = argv[5]
roi_map = argv[6]
job_location = argv[7]

#Sets the transparency and level for mask file
rc("open %s"%(search_object_mask))
v = VV.active_volume()
if len(argv) > 5:
    level = float(mask_threshold)
else:
    level = v.data.full_matrix().std()
rc("volume #0 level %f transparency 0.5"%(level))

#Sets the transparency and level for filtered target map
rc("open %s"%(target_map_filtered))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc("volume #1 level %f transparency 0.5"%(stdev))

#Sets the transparency and level for filtered search object map
rc("open %s"%(search_object_filtered))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc("volume #2 level %f transparency 0.5"%(stdev))

#Opens the input search object pdb
rc("open %s"%(search_object))

#Sets the transparency and level for roi map
rc("open %s"%(roi_map))
v = VV.active_volume()
stdev = v.data.full_matrix().std()
rc("volume #4 level %f transparency 0.5 showOutlineBox True"%(stdev))

#Change working directory
if len(argv) > 7:
    rc("cd %s"%(job_location))
