#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import glob, os, re

import numpy as np
from PyQt4 import QtGui, QtCore

from ccpem_gui.utils import window_utils
from ccpem_core.tasks.ribfind import ribfind_task
from ccpem_core.settings import which
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import ribfind as test_data


class RibfindWindow(window_utils.CCPEMTaskWindow):
    '''
    Ribfind window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(RibfindWindow, self).__init__(task=task,
                                            parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Contact distance
        contact_distance_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='contact_distance',
            args=self.args)
        self.args_widget.args_layout.addWidget(contact_distance_input)

        # Input pdb
        input_pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_pdb',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_pdb_input)

        # Set launcher file
        self.launcher.add_file(
            arg_name='input_pdb',
            file_type='pdb',
            description=self.args.input_pdb.help,
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.
        '''
        outdir = os.path.splitext(os.path.basename(self.task.args.input_pdb.value))[0]
        self.ribfind_outdir = os.path.join(self.task.job_location,outdir,'protein')
        if os.path.isdir(self.ribfind_outdir):
            # Add rigid body files to launcher
            clust_files_dict = {}
            for filename in os.listdir(self.ribfind_outdir):
                match = re.search("_denclust_([0-9]+)\.txt", filename)
                if match is not None:
                    clust_files_dict[filename] = int(match.group(1))

            self.num_clust_files = len(clust_files_dict)

            for filename in sorted(clust_files_dict, key=clust_files_dict.get):
                path = os.path.join(self.ribfind_outdir,filename)
                basename = os.path.basename(path)
                if re.search('denclust_', basename) is not None:
                    self.launcher.add_file(
                        path=path,
                        description='Rigid body file (cluster cut-off {}%)'.format(clust_files_dict[filename]),
                        selected=True)
            # Set Chimera results
            self.set_results_chimera()

    def handle_chimera_view(self):
        button = self.sender()
        index = self.ribfind_results_widget.indexAt(button.parent().pos())
        chim_script_rbs = os.path.join(
                os.path.dirname(os.path.realpath(__file__)),
                'chimera_assign_attribute.py')
        chimera_command = '%s %s %s' % (
            str(chim_script_rbs),
            str(self.args.input_pdb.value),
            str(self.list_attributefiles[index.row()]))
#         arg_list_chimera = QtCore.QStringList()
#         arg_list_chimera.append('--script')
#         arg_list_chimera.append('''"%s"''' % (chimera_command))
        arg_list_chimera = ['--script', '%s' % (chimera_command)]
        
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else: 
            process.startDetached(chimera_bin, arg_list_chimera)

    def set_results_chimera(self):
        '''
        Show results with chimera
        '''
        pdb_outdir = '.'.join(os.path.basename(self.task.args.input_pdb.value).split('.')[:-1])
        self.ribfind_outdir = os.path.join(self.task.job_location,pdb_outdir,'protein')
        # Set dock and layout
        pdb_dock_layout = QtGui.QVBoxLayout()
        pdb_dock_widget = QtGui.QWidget()
        pdb_dock_widget.setLayout(pdb_dock_layout)
        self.pdb_dock = QtGui.QDockWidget('Results',
                                          self,
                                          QtCore.Qt.Widget)
        self.pdb_dock.setWidget(pdb_dock_widget)
        self.tabifyDockWidget(self.pipeline_dock, self.pdb_dock)

        #add table with rigid body results
        self.ribfind_results_widget = QtGui.QTableWidget()
        #header font
        hfont = QtGui.QFont()
        hfont.setPointSize(14)
        #table layout
        self.ribfind_results_widget.setColumnCount(4)
        self.ribfind_results_widget.setRowCount(self.num_clust_files)
        self.ribfind_results_widget.verticalHeader().setVisible(False)
        horizHeader = self.ribfind_results_widget.horizontalHeader()
        vertHeader = self.ribfind_results_widget.verticalHeader()
        #row and colum sizes
        #horizHeader.setStretchLastSection(True)
        horizHeader.setResizeMode(1,QtGui.QHeaderView.Stretch)
        horizHeader.setResizeMode(2,QtGui.QHeaderView.Stretch)
        vertHeader.setResizeMode(QtGui.QHeaderView.ResizeToContents)
        #header labels
        self.ribfind_results_widget.setHorizontalHeaderLabels(\
                        QtCore.QString("Cluster cutoff;Number of Rigid segments"\
                        ";Rigid segments;View\n(Chimera)").split(";"))
        self.ribfind_results_widget.horizontalHeaderItem(0).setFont(hfont)
        self.ribfind_results_widget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)
        #color using chimera attribute files
        self.list_attributefiles = []
        r = 0
        for cutoff in range(0,101):
            #if cutoff == 0: cutoff += 1
            #cluster cutoff
            #add rigid body details
            rigid_file = os.path.join(self.ribfind_outdir,pdb_outdir+'_denclust_{}.txt'.format(cutoff))
            
            if not os.path.isfile(rigid_file): continue
            chimera_attribute_file = os.path.splitext(rigid_file)[0] +'_attr.txt'
            
            item_cutoff = QtGui.QTableWidgetItem("{}".format(cutoff))
            item_cutoff.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ribfind_results_widget.setItem(r,0,item_cutoff)
            
            self.list_attributefiles.append(chimera_attribute_file)
            
            outfp = open(chimera_attribute_file,'w')
            dict_rigid_bodies = write_attribute(rigid_file,outfp)
            outfp.close()
            ##rigid_bodies_widget = CollapsibleBox("rigid segments")
            self.combo = QtGui.QComboBox()
#             self.combo.setSizePolicy(QtGui.QSizePolicy.Preferred, 
#                                 QtGui.QSizePolicy.Expanding)
            
            #self.combo.currentIndexChanged.connect(self.combo.setCurrentIndex)
            combo_model = self.combo.model()
            ##rigid_bodies_layout = QtGui.QVBoxLayout()
            #number rigid bodies
            number_rigid = QtGui.QTableWidgetItem("{}".format(len(dict_rigid_bodies)))
            number_rigid.setTextAlignment(QtCore.Qt.AlignCenter)
            self.ribfind_results_widget.setItem(r,1,number_rigid)
            
            list_qlabels = []
            i = 0
            item = QtGui.QStandardItem("                 Expand/Close      ")
            font = item.font()
            font.setPointSize(20)
            item.setFont(font)
            combo_model.appendRow(item)
            self.combo.addItem(" ")
            for k in dict_rigid_bodies:
#                 list_qlabels.append(QtGui.QLabel())
#                 list_qlabels[i].setText(dict_rigid_bodies[k])
#                 list_qlabels[i].setStyleSheet("color: rgb({}, {}, {});".\
#                                               format(k[0],k[1],k[2]))
                item = QtGui.QStandardItem(str(dict_rigid_bodies[k]))
                item.setForeground(QtGui.QColor(k[0],k[1],k[2]))
                item.setSelectable(False)
                combo_model.appendRow(item)
                ##rigid_bodies_layout.addWidget(list_qlabels[i])
                i += 1
            ##rigid_bodies_widget.setContentLayout(rigid_bodies_layout)
            self.combo.setView(QtGui.QListView())
            
            #self.combo.setSizeAdjustPolicy(QtGui.QComboBox.AdjustToContentsOnFirstShow)
#             cb_height = self.combo.minimumSizeHint().height()
#             self.combo.view().setMinimumHeight(cb_height)
            self.combo.setStyleSheet('''
                                    QComboBox { max-width: 600px;}
                                    QComboBox QAbstractItemView 
                                        {
                                        max-width: 500px;
                                        }
                                    ''')
            self.combo.view().setMaximumWidth(500)
            self.ribfind_results_widget.setCellWidget(r,2,self.combo)
            #view in chimera
            chimera_button_widget = QtGui.QWidget()
            chimera_button_layout = QtGui.QVBoxLayout()
            chimera_button_widget.setContentsMargins(3, 3, 3, 3);
            chimera_button_layout.setContentsMargins(3, 3, 3, 3)
            
            chimera_view_button = QtGui.QPushButton('View')
            chimera_view_button.clicked.connect(self.handle_chimera_view)
            chimera_button_layout.addWidget(chimera_view_button)
            chimera_button_layout.setAlignment(QtCore.Qt.AlignCenter)
            chimera_button_widget.setLayout(chimera_button_layout)
            self.ribfind_results_widget.setCellWidget(r,3,chimera_button_widget)
            r += 1
        pdb_dock_layout.addWidget(self.ribfind_results_widget)

    def indexchanged(self):
        self.combo.setCurrentIndex(0)

def write_attribute(rigid_body_file,outfp,rigidbody_file_tag=None):
    #TODO : add check and exit
    #if outfp.closed:
    # if not rigidbody_file_tag is None:
    outfp.write("attribute: ribbonColor\n")
    outfp.write("match mode: 1-to-1\n") #any match
    outfp.write("recipient: residues\n")
    
    #read from rigid body file and assign color
    assert os.path.isfile(rigid_body_file)
    rf = open(rigid_body_file,'r')
    
    #number of lines in rigid_body_file
    #is there a better way to get an approximate count?
    num_lines = 0
    for l in rf:
        if not l[0] == '#': num_lines += 1    
    #make list of colors
    col_len = np.ceil(np.power(num_lines,1./3))
    
    G = np.arange(0.0,0.95,0.9/col_len)
    B = np.arange(0.0,0.9,0.9/col_len)
    if not col_len == 1: col_len -= 1
    R = np.append(np.arange(0.9,0.0,-0.9/col_len),0.0)
    RGB = np.meshgrid(R,G,B)[0].ravel(),np.meshgrid(R,G,B)[1].ravel(),\
            np.meshgrid(R,G,B)[2].ravel()
    dict_rb = {}
    ct_rb = 0
    rf.seek(0)
    for line in rf:
        if line.startswith("#"):
            pass
        else:
            if len(line) < 3: continue
            tokens = line.split()
            if len(tokens) < 2: continue
            chainID = ''
            list_rb = []
            for i in range(int(len(tokens)/2)):
                start = int(tokens[i*2].split(':')[0])
                end = int(tokens[i*2+1].split(':')[0])
                if ':' in tokens[i*2]:
                    chainID = tokens[i*2].split(':')[1]
                    if not tokens[i*2+1].split(':')[1] == chainID:
                        print 'Check chain IDs in rigid body file', tokens[i*2]
                        continue
                for j in range(start,end+1):
                    #If just one model is opened in chimera
                    if len(chainID) != 0: atom_spec_chimera = "#0:{}.{}".format(j,chainID)
                    else: atom_spec_chimera = "#0:{}".format(j)
                    col_assign = "\t{}\t{} {} {}". \
                                format(atom_spec_chimera,RGB[0][ct_rb],RGB[1][ct_rb],RGB[2][ct_rb])
                    outfp.write("{}\n".format(col_assign))
                list_rb.extend([str(tokens[i*2]),str(tokens[i*2+1])])
                chainID = ''
            str_rb = ''
            str_len = 0
            for nr in range(1,len(list_rb)+1):
                str_rb += list_rb[nr-1]
                if nr >= 10 and nr % 10 == 0 and nr != len(list_rb): 
                    str_rb += '\n'
                elif nr >= 2 and nr % 2 == 0:
                    str_rb += '\t'
                else: str_rb += ' '
                
            dict_rb[(int(255*RGB[0][ct_rb]),int(255*RGB[1][ct_rb]),int(255*RGB[2][ct_rb]))] = str_rb
            if len(line) > 3: ct_rb += 1
            
    return dict_rb

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=ribfind_task.Ribfind,
        window_class=RibfindWindow)

if __name__ == '__main__':
    main()
