#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


'''
Task window for Buccaneer.
'''
import os

from PyQt4 import QtGui, QtCore, QtWebKit

from ccpem_core.tasks.buccaneer import buccaneer_task
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import buccaneer as test_data


class BuccaneerWindow(window_utils.CCPEMTaskWindow):
    '''
    Buccaneer window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(BuccaneerWindow, self).__init__(task=task,
                                              parent=parent)
        self.rv_timer = QtCore.QTimer()
        self.rv_timer.timeout.connect(self.set_rv_ui)
        self.rv_timer.start(1500)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_map',
            args=self.args,
            label='Input map',
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(map_input)

        # Resolution
        resolution_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.1,
            arg_name='resolution',
            args=self.args,
            required=True)
        self.args_widget.args_layout.addWidget(resolution_input)

        # Map sharpen
        map_sharpen_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            minimum=-1000,
            maximum=1000,
            arg_name='map_sharpen',
            args=self.args)
        self.args_widget.args_layout.addWidget(map_sharpen_input)

        # Input seq
        self.seq_input = window_utils.FileArgInput(
            parent=self,
            arg_name='input_seq',
            args=self.args,
            label='Input sequence',
            required=True)
        self.args_widget.args_layout.addWidget(self.seq_input)

        # Initial model input
        extend_model_input = window_utils.FileArgInput(
            parent=self,
            arg_name='extend_pdb',
            args=self.args,
            label='Extend model',
            file_types=ccpem_file_types.pdb_ext,
            required=False)
        self.args_widget.args_layout.addWidget(extend_model_input)

        # Nonprotein-radius option (preserve things not protein)
        self.nonprotein_radius_on_input = window_utils.CheckArgInput(
            parent=self,
            arg_name='nonprotein_radius_on',
            label='Preserve non-protein',
            args=self.args)
        self.args_widget.args_layout.addWidget(
            self.nonprotein_radius_on_input)

        # Radius value for nonprotein-radius
        self.npradius_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.5,
            minimum=0,
            required=False,
            arg_name='nonprotein_radius',
            label='Non-protein mask radius',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.npradius_input)

        # Number of Buccaneer pipeline cycles
        ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle',
            args=self.args)
        self.args_widget.args_layout.addWidget(ncycle_input)

        # Advance pipeline controls
        pipeline_control_frame = window_utils.CCPEMExtensionFrame(
            button_name='Advance pipeline controls',
            button_tooltip='Show advance pipeline controls')
        self.args_widget.args_layout.addLayout(pipeline_control_frame)

        # Set number of internal Buccaneer cycles in 1st buccaneer cycle
        buc1st_ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle_buc1st',
            args=self.args)
        pipeline_control_frame.add_extension_widget(buc1st_ncycle_input)

        # Set number of internal Buccaneer cycles in nth cycles
        bucnth_ncycle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle_bucnth',
            args=self.args)
        pipeline_control_frame.add_extension_widget(bucnth_ncycle_input)

        # Set number of cpus to use in Buccaneer
        ncpus_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncpus',
            args=self.args)
        pipeline_control_frame.add_extension_widget(ncpus_input)

        # Set number of Refmac cycles in pipeline
        ref_ncyle_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='ncycle_refmac',
            args=self.args)
        pipeline_control_frame.add_extension_widget(ref_ncyle_input)

        # Local refinement option
        self.local_refinement_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='local_refinement_on',
            args=self.args)
        pipeline_control_frame.add_extension_widget(
            self.local_refinement_input)

        # Local refinement on, mask radius
        self.mask_radius_input = window_utils.NumberArgInput(
            parent=self,
            decimals=1,
            step=0.5,
            minimum=0,
            required=False,
            arg_name='mask_radius',
            label='Mask radius',
            args=self.args)
        pipeline_control_frame.add_extension_widget(self.mask_radius_input)

        # Ligand library input for Refmac5
        lib_input = window_utils.FileArgInput(
            parent=self,
            label='Input ligand',
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        pipeline_control_frame.add_extension_widget(lib_input)

        # -> LIBG restraints for nucleic acids
        self.libg_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='libg',
            args=self.args)
        pipeline_control_frame.add_extension_widget(
            self.libg_input)

        # -> Keyword entry for libg nucleotide range selection
        self.libg_selection_input = window_utils.KeywordArgInput(
            parent=self,
            arg_name='libg_selection',
            args=self.args,
            label='Nucleotide range')
        pipeline_control_frame.add_extension_widget(
            self.libg_selection_input)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # buccaneer Keywords
        self.keyword_entry = window_utils.KeywordArgInput(
            parent=self,
            arg_name='keywords',
            args=self.args)
        extended_options_frame.add_extension_widget(self.keyword_entry)

        # refmac Keywords
        self.keyword_entry2 = window_utils.KeywordArgInput(
            parent=self,
            arg_name='refmac_keywords',
            args=self.args)
        extended_options_frame.add_extension_widget(self.keyword_entry2)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='input_map',
            file_type='map',
            description=self.args.input_map.help,
            selected=True)
        self.launcher.add_file(
            arg_name='input_seq',
            file_type='standard',
            description=self.args.input_seq.help,
            selected=False)
        self.launcher.add_file(
            arg_name='extend_pdb',
            file_type='pdb',
            description=self.args.extend_pdb.help,
            selected=True)

        # Set dependencies
        extend_model_input.value_line.textChanged.connect(
            self.show_nonprotein_radius_option)
        self.nonprotein_radius_on_input.value_line.stateChanged.connect(
            self.set_nonprotein_radius)
        self.show_nonprotein_radius_option()
        self.set_nonprotein_radius()
        self.local_refinement_input.value_line.currentIndexChanged.connect(
            self.set_mask_radius)
        self.set_mask_radius()
        self.libg_input.value_line.currentIndexChanged.connect(
            self.set_libg_selection)
        self.set_libg_selection()

    def show_nonprotein_radius_option(self):
        if self.args.extend_pdb.value not in ['', None, 'None']:
            self.nonprotein_radius_on_input.show()
        else:
            self.nonprotein_radius_on_input.hide()

    def set_nonprotein_radius(self):
        if self.args.nonprotein_radius_on.value:
            self.npradius_input.show()
        else:
            self.npradius_input.hide()

    def set_mask_radius(self):
        if self.args.local_refinement_on.value:
            self.mask_radius_input.show()
        else:
            self.mask_radius_input.hide()

    def set_libg_selection(self):
        if self.args.libg.value:
            self.libg_selection_input.show()
        else:
            self.libg_selection_input.hide()

    def set_rv_ui(self):
        '''
        RVAPI results viewer.
        '''
        if hasattr(self.task, 'job_location'):
            if self.task.job_location is not None:
                report = os.path.join(self.task.job_location,
                                      'report/index.html')
                if os.path.exists(report):
                    self.rv_view = QtWebKit.QWebView()
                    self.rv_view.load(QtCore.QUrl(report))
                    self.results_dock = QtGui.QDockWidget('Results',
                                                          self,
                                                          QtCore.Qt.Widget)
                    self.results_dock.setToolTip('Results overview')
                    self.results_dock.setWidget(self.rv_view)
                    self.tabifyDockWidget(self.setup_dock, self.results_dock)
                    self.results_dock.show()
                    self.results_dock.raise_()
                    self.rv_timer.stop()

    def set_on_job_running_custom(self):
        # Structure factor from input map
        if hasattr(self.task, 'process_maptomtz'):
            self.launcher.add_file(
                arg_name=None,
                path=self.task.process_maptomtz.hklout_path,
                file_type='mtz',
                description='Structure factors from input map',
                selected=False)
        # Add PDB from last Buccaneer cycle
        for i in range(1, (self.args.ncycle.value)):
            path = os.path.dirname(self.task.process_buccaneer_pipeline.pdbout)
            fname = os.path.join(path, 'build' + str(i) + '.pdb')
            self.launcher.add_file(
                path=fname,
                file_type='pdb',
                description='Model built from Buccaneer cycle #' + str(i),
                selected=False)
            path = os.path.dirname(self.task.process_refine.pdbout_path)
            fname = os.path.join(path, 'refined' + str(i) + '.pdb')
            self.launcher.add_file(
                path=fname,
                file_type='pdb',
                description='Model built and refined from Buccaneer cycle #' +
                str(i),
                selected=False)
        # needed this line to refresh the file launcher view
        self.launcher.set_tree_view()

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  For now show starting, refined
        pdb and experimental map.
        '''
        if hasattr(self.task, 'process_buccaneer_pipeline'):
            if hasattr(self.task, 'process_refine'):
                self.launcher.add_file(
                    path=self.task.process_buccaneer_pipeline.pdbout,
                    file_type='pdb',
                    description='Model built from final Buccaneer cycle',
                    selected=False)
                self.launcher.add_file(
                    path=self.task.process_refine.pdbout_path,
                    file_type='pdb',
                    description='Final Buccaneer built and refined model',
                    selected=True)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=buccaneer_task.Buccaneer,
        window_class=BuccaneerWindow)


if __name__ == '__main__':
    main()
