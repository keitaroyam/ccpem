#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
from ccpem_gui.utils import window_utils
from ccpem_core.tasks.haruspex import haruspex_task
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import haruspex as test_data

class HaruspexWindow(window_utils.CCPEMTaskWindow):
    '''
    Haruspex window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(HaruspexWindow, self).__init__(task=task,
                                           parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Input maps
        input_target_map = window_utils.FileArgInput(
            parent=self,
            arg_name='target_map',
            required=True,
            file_types=ccpem_file_types.mrc_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_target_map)


        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='target_map',
            file_type='map',
            description=self.args.target_map.help,
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  Show output pdb.
        '''
        # Set launcher
        # Add expected output map names based on input target
        prefix = os.path.join(
            self.task.job_location,
            os.path.splitext(os.path.basename(self.args.target_map()))[0]
            )
        sheet_map = prefix + '_sheet.mrc'
        helix_map = prefix + '_helix.mrc'
        npair_map = prefix + '_npair.mrc'

        self.launcher.add_file(
            arg_name=None,
            path=sheet_map,
            file_type='map',
            description='Beta sheet segmented map',
            selected=True)
        self.launcher.add_file(
            arg_name=None,
            path=helix_map,
            file_type='map',
            description='Alpha helix segmented map',
            selected=True)
        self.launcher.add_file(
            arg_name=None,
            path=npair_map,
            file_type='map',
            description='Nucleic acid segmented map',
            selected=True)
        self.launcher.set_tree_view()

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=haruspex_task.Haruspex,
        window_class=HaruspexWindow)

if __name__ == '__main__':
    main()

