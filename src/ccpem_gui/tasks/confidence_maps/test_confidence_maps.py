#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import sys
import shutil
import time
import tempfile

from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest

from ccpem_core.tasks.confidence_maps import confidence_maps_task
from ccpem_gui.tasks.confidence_maps import confidence_maps_window
from ccpem_core.test_data.tasks import confidence_maps as confidence_maps_test_data
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

app = QtGui.QApplication(sys.argv)


class Test(unittest.TestCase):
    '''
    Unit test for Confidence Maps (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(confidence_maps_test_data.__file__)
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_confidence_maps_args_exist(self):
        assert os.path.isfile(confidence_maps_window.ConfidenceMapsWindow.gui_test_args)

    def test_confidence_maps_window_integration(self):
        '''
        Test Confidence Maps task via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Confidence Maps')
        # Load args
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        run_task = confidence_maps_task.ConfidenceMapsTask(job_location=self.test_output,
                                                           args_json=args_path)
        # Setup GUI
        self.window = confidence_maps_window.ConfidenceMapsWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(self.window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        self.job_completed = False
        timeout = 0
        stdout = run_task.pipeline.pipeline[-1][0].stdout
        while not self.job_completed and timeout < 50:
            print 'Confidence Maps running for {0} secs (timeout = 50)'.format(timeout)
            time.sleep(5.0)
            timeout += 5
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        self.job_completed = True
        # Check timeout
        assert timeout < 50
        # Check job completed
        assert self.job_completed
        # Check output files created
        assert os.path.exists(os.path.join(self.window.task.job_location,
                                           'diag_image.pdf'))
        assert os.path.exists(os.path.join(self.window.task.job_location,
                                           'emd_3488_confidenceMap.mrc'))

    def test_confidence_maps_window_with_many_args(self):
        '''
        Test Confidence Maps task via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - Confidence Maps')
        # Load args
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'fast_test_args.json')
        run_task = confidence_maps_task.ConfidenceMapsTask(job_location=self.test_output,
                                                           args_json=args_path)
        # Setup GUI
        self.window = confidence_maps_window.ConfidenceMapsWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(self.window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        self.job_completed = False
        timeout = 0
        stdout = run_task.pipeline.pipeline[-1][0].stdout
        while not self.job_completed and timeout < 50:
            print 'Confidence Maps running for {0} secs (timeout = 50)'.format(timeout)
            time.sleep(5.0)
            timeout += 5
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        self.job_completed = True
        # Check timeout
        assert timeout < 50
        # Check job completed
        assert self.job_completed
        # Check output files created
        assert os.path.exists(os.path.join(self.window.task.job_location,
                                           'diag_image.pdf'))
        assert os.path.exists(os.path.join(self.window.task.job_location,
                                           '5ni1_B_31-39_confidenceMap.mrc'))

if __name__ == '__main__':
    unittest.main()
