#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Task window for Confidence Maps
'''
import glob
import os

from confidenceMapUtil import mapUtil
import mrcfile
from PyQt4 import QtGui

from ccpem_gui.utils import command_line_launch
from ccpem_gui.utils import window_utils
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.tasks.confidence_maps import confidence_maps_task
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import confidence_maps as test_data


class ConfidenceMapsWindow(window_utils.CCPEMTaskWindow):
    '''
    Confidence Maps window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='em_map',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext,
            required=True)
        self.args_widget.args_layout.addWidget(self.map_input)

        # Noise box controls in their own layout
        noisebox_layout = QtGui.QHBoxLayout()
        self.args_widget.args_layout.addLayout(noisebox_layout)

        # Noise window size
        self.window_size_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='window_size',
            args=self.args,
            required=True)
        noisebox_layout.addWidget(self.window_size_input)

        # Noise window coordinates
        self.noise_box_input = window_utils.ListArgInput(
            parent=self,
            arg_name='noise_box',
            args=self.args,
            required=False,
            element_type=int)
        noisebox_layout.addWidget(self.noise_box_input)
        self.noise_box_input.value_line.setPlaceholderText('Default')

        check_noise_box_button = QtGui.QPushButton('Check noise box')
        check_noise_box_button.clicked.connect(self.check_noise_box)
        check_noise_box_button.setToolTip("Show the noise box size and location.\n\nYou should check that the box does"
                                          " not overlap the molecule or any masked-out areas; it\nshould contain only "
                                          "background noise. If the box size is not set, a default value will be used.")
        noisebox_layout.addWidget(check_noise_box_button)

        # Extended options
        extended_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Show extended options')
        self.args_widget.args_layout.addLayout(extended_options_frame)

        # Map pixel size
        apix_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='apix',
            args=self.args,
            decimals=3,
            step=0.01)
        extended_options_frame.add_extension_widget(apix_input)

        # Local resolution map
        locresmap_input = window_utils.FileArgInput(
            parent=self,
            arg_name='locResMap',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext)
        extended_options_frame.add_extension_widget(locresmap_input)

        # Correction method
        method_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='method',
            args=self.args,
            second_width=None)  # None for auto-width for the drop-down box
        extended_options_frame.add_extension_widget(method_input)

        # Mean map
        meanmap_input = window_utils.FileArgInput(
            parent=self,
            arg_name='meanMap',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext)
        extended_options_frame.add_extension_widget(meanmap_input)

        # Variance map
        variancemap_input = window_utils.FileArgInput(
            parent=self,
            arg_name='varianceMap',
            args=self.args,
            file_types=ccpem_file_types.mrc_ext)
        extended_options_frame.add_extension_widget(variancemap_input)

        # Test procedure
        testproc_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='test_proc',
            args=self.args)
        extended_options_frame.add_extension_widget(testproc_input)

        # Low pass filter
        lowpass_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='lowPassFilter',
            args=self.args)
        extended_options_frame.add_extension_widget(lowpass_input)

        # Empirical CDF?
        ecdf_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='ecdf',
            args=self.args)
        extended_options_frame.add_extension_widget(ecdf_input)

        # Set inputs and defaults for launcher
        self.launcher.set_mg_default('Chimera')
        self.launcher.add_file(
            arg_name='em_map',
            file_type='map',
            description=self.args.em_map.help,
            selected=True)

    def set_on_job_finish_custom(self):
        diag_image = os.path.join(self.pipeline.location, 'diag_image.pdf')
        if os.path.isfile(diag_image):
            self.launcher.add_file(
                arg_name=None,
                path=diag_image,
                file_type='standard',
                description='Noise box diagnostic image',
                selected=False)

        for output_map in glob.glob(os.path.join(self.pipeline.location, '*_confidenceMap.mrc')):
            self.launcher.add_file(
                arg_name=None,
                path=output_map,
                file_type='map',
                description="Confidence map",
                selected=True)

    def check_noise_box(self):
        # Get map file name from GUI
        self.map_input.on_edit_finished()
        map_file = self.map_input.action.value

        if map_file is None or not os.path.isfile(map_file):
            QtGui.QMessageBox.warning(self, 'Error', "Map file '{}' not found".format(map_file))
            return

        # Get window size from GUI
        self.window_size_input.on_edit_finished()
        window_size = self.window_size_input.action.value

        # Get box coordinates from GUI
        self.noise_box_input.on_edit_finished()
        box_coords = self.noise_box_input.action.value
        assert len(box_coords) == 0 or len(box_coords) == 3
        if len(box_coords) == 0:
            box_coords = 0

        # Generate and show the diagnostic plot
        with mrcfile.mmap(map_file) as map:

            # This duplicates the calculation done in confidenceMapMain.calculateConfidenceMap()
            # (It would be better to rearrange Max's code slightly so we don't have to just copy the calculation.)
            map_size = map.data.shape
            if window_size is None or window_size == 0:
                window_size = max(int(0.05 * map_size[0]), 10)
                self.window_size_input.set_arg_value(window_size)

            if window_size < 20:
                result = QtGui.QMessageBox.warning(self, 'Warning',
                                                   "Noise box size ({}) is quite small. Please think about potential "
                                                   "inaccuracies of your noise estimates!".format(window_size),
                                                   buttons=QtGui.QMessageBox.Ok | QtGui.QMessageBox.Cancel)
                if result == QtGui.QMessageBox.Cancel:
                    return

            import matplotlib.pyplot as plt
            plt.rcdefaults()
            plt.ion()
            mapUtil.makeDiagnosticPlot(map.data, window_size, False, box_coords)
            plt.show()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=confidence_maps_task.ConfidenceMapsTask,
        window_class=ConfidenceMapsWindow)


if __name__ == '__main__':
    main()
