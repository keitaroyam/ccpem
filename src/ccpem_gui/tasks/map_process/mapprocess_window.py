#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os

from PyQt4 import QtCore, QtGui

from ccpem_gui.utils import window_utils
from ccpem_core.tasks.map_process import mapprocess_task
from ccpem_core.settings import which
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import map_process as test_data

list_methods = ['shiftpeak_to_zero','pad','lowpass_filter','crop',
                'threshold','mask','dust_filter','downsample','softmask',
                'shift_origin']
list_methods_noargs = ['shiftpeak_to_zero','threshold','crop','dust_filter','softmask',]

dict_tooltips = {
    'shiftpeak_to_zero': 'Move background peak to zero',
    'lowpass_filter': 'Lowpass filter at given resolution',
    'threshold': 'Threshold map at this contour level',
    'mask':'Apply a mask(mrc file)',
    'crop': 'Crop map at a given contour',
    'pad':'Pad map given (X Y Z or X,Y,Z) padding',
    'downsample': 'Downsample map to a larger voxel size',
    'dust_filter': 'Remove small isolated densities at given contour',
    'softmask': 'Softmask the map edges',
    'shift_origin': 'Shift map origin to (X Y Z or X,Y,Z)'}


class MapProcessWindow(window_utils.CCPEMTaskWindow):
    '''
    TEMPy Map Process window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(MapProcessWindow, self).__init__(task=task,
                                                  parent=parent)
    
    def set_args(self):
        '''
        Set input arguments
        '''
        # Input map
        self.map_input = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input)

        self.map_contour = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_contour',
            decimals=5,
            args=self.args,
            required=False)
        self.args_widget.args_layout.addWidget(self.map_contour)
        
        # Create ListWidget
        self.list_widget = QtGui.QListWidget()
        total_height = 0
        self.dict_method_buttons = {}
        self.list_selected_methods = []
        for i in range(len(list_methods)):
            method_name = list_methods[i]
            item = QtGui.QListWidgetItem() 
            itemwidget = QtGui.QWidget()
            # Method
            if method_name in list_methods_noargs:
                options_frame = QtGui.QVBoxLayout()
                button = QtGui.QRadioButton(str(method_name))
                button.setToolTip(dict_tooltips[method_name])
                options_frame.addWidget(button)
                self.dict_method_buttons[method_name] = button
            else:
                options_frame = window_utils.CCPEMExtensionFrame(
                    button_name=str(method_name),
                    button_tooltip=dict_tooltips[method_name])
                self.dict_method_buttons[method_name] = options_frame.button
                options_frame.setAlignment(QtCore.Qt.AlignTop)
            
            self.dict_method_buttons[method_name].clicked.connect(
                                lambda checked: self.handle_button(
                                                        checked))
            #options_frame.setSizeConstraint(QtGui.QLayout.SetFixedSize)
            #add method args
            self.add_method_args(options_frame, method_name)
            #if options_frame.button.isChecked():
            itemwidget.setLayout(options_frame)
            s = itemwidget.sizeHint()
            if not method_name in list_methods_noargs:
                item_height = s.height()+options_frame.frame.sizeHint().height()
                s.setHeight(item_height)
            total_height += s.height()
            item.setSizeHint(s)
            item.setWhatsThis(method_name)
            self.list_widget.addItem(item)
            self.list_widget.setItemWidget(item, itemwidget)
        # Enable drag & drop ordering of items.
        self.list_widget.setDragDropMode(QtGui.QAbstractItemView.InternalMove)
        self.list_widget.model().layoutChanged.connect(self.check_method_order)
        # Make sure list widget is big enough for all items
        self.list_widget.setMinimumHeight(int(total_height*0.5))
        #self.list_widget.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.list_widget.setSizePolicy(
            QtGui.QSizePolicy.Expanding, 
            QtGui.QSizePolicy.Expanding)
        self.args_widget.args_layout.addWidget(self.list_widget)
        #edit tooltip for setup window
        self.setup_dock.setToolTip('Drag and drop to rearrange \n Select to choose operation')
        #set initial states
        if self.task.args.list_process.value != None:
            for method_name in self.task.args.list_process.value:
                if method_name in self.dict_method_buttons:
                    method_button = self.dict_method_buttons[method_name]
                    method_button.setChecked(True)
        
    def add_method_args(self,options_frame, method_name):
        self.contour_added = False
        if method_name == 'lowpass_filter':
            self.map_resolution = window_utils.NumberArgInput(
                parent=self,
                arg_name='map_resolution',
                decimals=3,
                args=self.args)
            options_frame.add_extension_widget(self.map_resolution)
        elif method_name == 'mask':
            self.mask_path = window_utils.FileArgInput(
                parent=self,
                arg_name='mask_path',
                args=self.args)
            options_frame.add_extension_widget(self.mask_path)
        elif method_name == 'pad':
            self.map_pad = window_utils.ListArgInput(
                parent=self,
                arg_name='map_pad',
                args=self.args,
                element_type=int)
            options_frame.add_extension_widget(self.map_pad)
        elif method_name == 'shift_origin':
            self.map_origin = window_utils.ListArgInput(
                parent=self,
                arg_name='map_origin',
                args=self.args,
                element_type=float)
            options_frame.add_extension_widget(self.map_origin)
        elif method_name == 'downsample':
            self.map_newapix = window_utils.NumberArgInput(
                parent=self,
                arg_name='map_apix',
                decimals=3,
                args=self.args)
            options_frame.add_extension_widget(self.map_newapix)


    def handle_button(self,checked):
        sender_button = self.sender()
        method_name = str(sender_button.text())
        if checked:
            if not method_name in self.list_selected_methods:
                self.list_selected_methods.append(method_name)
            self.task.args.list_process.value = self.list_selected_methods
            
            if method_name in ['threshold','crop','dust_filter']:
                self.map_contour.set_required(True)
        else:
            if method_name in self.list_selected_methods:
                self.list_selected_methods.remove(method_name)
            self.task.args.list_process.value = self.list_selected_methods
            if method_name in ['threshold','crop','dust_filter']:
                if 'threshold' not in self.list_selected_methods and \
                'crop' not in self.list_selected_methods and \
                'dust_filter' not in self.list_selected_methods:
                    self.map_contour.set_required(False)
            
        
    
    def check_method_order(self):
        for i in range(self.list_widget.count()):
            list_item = self.list_widget.item(i)
            item_str = str(list_item.whatsThis())
            try: 
                button = self.dict_method_buttons[item_str]
                if button.isChecked(): 
                    self.list_selected_methods.append(item_str)
            except: continue
        self.task.args.list_process.value = self.list_selected_methods
        
    def set_on_job_finish_custom(self):
        if os.path.isfile(self.args.map_path.value):
            self.launcher.add_file(
                arg_name='map_path',
                file_type='map',
                description=self.args.map_path.help,
                selected=True)

        processed_mapfile = os.path.splitext(self.args.map_path.value)[0]+\
                            '_processed.mrc'
        if os.path.isfile(processed_mapfile):
            self.launcher.add_file(
                arg_name=None,
                path=processed_mapfile,
                file_type='map',
                description='Processed map file',
                selected=True)

        # Add density histogram plots
        ini_histogram_plot = os.path.join(
            self.task.job_location,
            'initial_histogram.png')
        if os.path.exists(ini_histogram_plot):
            self.launcher.add_file(
                arg_name=None,
                path=ini_histogram_plot,
                description='Density histogram of input map',
                selected=False)
        final_histogram_plot = os.path.join(
            self.task.job_location,
            'processed_histogram.png')
        if os.path.exists(final_histogram_plot):
            self.launcher.add_file(
                arg_name=None,
                path=final_histogram_plot,
                description='Density histogram of processed map',
                selected=False)

        # Add power spectra plot
        ini_power_spectra_plot = os.path.join(
            self.task.job_location,
            'initial_powspectra.png')
        if os.path.exists(ini_power_spectra_plot):
            self.launcher.add_file(
                arg_name=None,
                path=ini_power_spectra_plot,
                description='Power spectra of input map',
                selected=False)
        final_power_spectra_plot = os.path.join(
            self.task.job_location,
            'processed_powspectra.png')
        if os.path.exists(final_power_spectra_plot):
            self.launcher.add_file(
                arg_name=None,
                path=final_power_spectra_plot,
                description='Power spectra of processed map',
                selected=False)
        self.launcher.set_tree_view()
        self.launcher_dock.raise_()
        self.launcher_dock.show()



def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=mapprocess_task.MapProcess,
        window_class=MapProcessWindow)

if __name__ == '__main__':
    main()
