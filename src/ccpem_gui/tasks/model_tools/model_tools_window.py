#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from ccpem_gui.utils import window_utils
from ccpem_core.tasks.model_tools import model_tools_task
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.test_data.tasks import model_tools as test_data


class ModelToolsWindow(window_utils.CCPEMTaskWindow):
    '''
    Model Tools CCP-EM GUI window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(ModelToolsWindow, self).__init__(
            task=task,
            parent=parent)

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)

        # Input pdb
        input_model = window_utils.FileArgInput(
            parent=self,
            arg_name='input_model',
            required=True,
            file_types=ccpem_file_types.pdb_ext,
            args=self.args)
        self.args_widget.args_layout.addWidget(input_model)

        # 
        output_mmcif = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='output_mmcif',
            args=self.args)
        self.args_widget.args_layout.addWidget(output_mmcif)
        # 
        output_pdb = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='output_pdb',
            args=self.args)
        self.args_widget.args_layout.addWidget(output_pdb)

        # Set inputs for launcher
        self.launcher.add_file(
            arg_name='input_model',
            file_type='pdb',
            description=self.args.input_model.help,
            selected=True)

    def set_on_job_finish_custom(self):
        '''
        Actions to run on job completion.  Show output pdb.
        '''
        # Set launcher
        if self.task.output_pdb_path is not None:
            if os.path.exists(path=self.task.output_pdb_path):
                self.launcher.add_file(
                    arg_name=None,
                    path=self.task.output_pdb_path,
                    file_type='pdb',
                    description='Output PDB file',
                    selected=True)
                self.launcher.set_tree_view()
        if self.task.output_mmcif_path is not None:
            if os.path.exists(path=self.task.output_mmcif_path):
                self.launcher.add_file(
                    arg_name=None,
                    path=self.task.output_mmcif_path,
                    file_type='pdb',
                    description='Output mmCIF file',
                    selected=True)
                self.launcher.set_tree_view()


def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=model_tools_task.ModelTools,
        window_class=ModelToolsWindow)

if __name__ == '__main__':
    main()
