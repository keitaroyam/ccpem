#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os

from PyQt4 import QtCore, QtGui

from ccpem_gui.utils import window_utils
from ccpem_core.tasks.tempy.difference_map import difference_map_task,\
    difference_map_chimera
from ccpem_core.settings import which
from ccpem_gui.utils import command_line_launch
from ccpem_core.ccpem_utils import get_test_data_path
from ccpem_core.ccpem_utils import ccpem_file_types
from ccpem_core.test_data.tasks.tempy import difference_map as test_data

from ccpem_core.process_manager import job_register
from ccpem_gui.project_database import sqlite_project_database
from ccpem_core.tasks.map_process import mapprocess_task
from ccpem_gui.tasks.map_process import mapprocess_window

# To Do
# Add spectra plot
# Options to add - to be uncomment below when Agnel adds command line arg:
#    -> Model vs map
#    -> Map alignment option (slow)

class DifferenceMapWindow(window_utils.CCPEMTaskWindow):
    '''
    TEMPy Difference map window.
    '''
    gui_test_args = get_test_data_path(test_data, 'unittest_args.json')

    def __init__(self,
                 task,
                 parent=None):
        super(DifferenceMapWindow, self).__init__(task=task,
                                                  parent=parent)
        self.output_pdb = None

    def set_args(self):
        '''
        Set input arguments
        '''
        # Job title
        self.title_input = window_utils.TitleArgInput(
            parent=self,
            arg_name='job_title',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.title_input)
        self.title_input.value_line.editingFinished.connect(
            self.handle_title_set)

        # Input map 1
        self.map_input_1 = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path_1',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input_1)
        #add mapprocess button
        self.task.mapprocess_task1 = None
        self.mapprocess_window1 = None
        self.mapprocess_list1 = [None,None]
        self.mp_button1 = QtGui.QRadioButton('Process input map1')
        self.mp_button1.setToolTip('Process input map1')
        self.args_widget.args_layout.addWidget(self.mp_button1)
        self.mp_button1.clicked.connect(lambda: self.handle_mapprocess_task(
                                                    self.mapprocess_window1))
        #self.mp_button1.setEnabled(False)
        #self.set_mapprocess_button(self.mp_button1)
        self.map_input_1.select_button.clicked.connect(
                lambda: self.enable_mapprocess_button(self.mp_button1, 
                                                      self.args.map_path_1.value,
                                                      self.mapprocess_list1
                                                      ))
        
        self.map_input_1.value_line.editingFinished.connect(
                lambda: self.enable_mapprocess_button(self.mp_button1, 
                                                      self.args.map_path_1.value,
                                                      self.mapprocess_list1
                                                      ))
        
        # Map 1 resolution
        self.map_resolution_1 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution_1',
            required=True,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution_1)
        #check for processed map
        self.map_resolution_1.value_line.editingFinished.connect(
            self.check_processed_maps)
        
        # Input map or PDB
        self.map_or_pdb = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='map_or_pdb_selection',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_or_pdb)
        self.map_or_pdb.value_line.currentIndexChanged.connect(
            self.set_pdb_or_map_input)

        # Input map 2
        self.map_input_2 = window_utils.FileArgInput(
            parent=self,
            arg_name='map_path_2',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_input_2)
        #add mapprocess button
        self.task.mapprocess_task2 = None
        self.mapprocess_window2 = None
        self.mapprocess_list2 = [None,None]
        self.mp_button2 = QtGui.QRadioButton('Process input map2')
        self.mp_button2.setToolTip('Process input map2')
        self.args_widget.args_layout.addWidget(self.mp_button2)
        self.mp_button2.clicked.connect(lambda: self.handle_mapprocess_task(
                                                    self.mapprocess_window2))
        #self.mp_button2.setEnabled(False)
        #self.set_mapprocess_button(self.mp_button1)
        self.map_input_2.select_button.clicked.connect(
                lambda: self.enable_mapprocess_button(self.mp_button2, 
                                                      self.args.map_path_2.value,
                                                      self.mapprocess_list2
                                                      ))
        self.map_input_2.value_line.editingFinished.connect(
                lambda: self.enable_mapprocess_button(self.mp_button2, 
                                                      self.args.map_path_2.value,
                                                      self.mapprocess_list2
                                                      ))
        # Map 2 resolution
        self.map_resolution_2 = window_utils.NumberArgInput(
            parent=self,
            arg_name='map_resolution_2',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.map_resolution_2)
        #check for processed map
        self.map_resolution_2.value_line.editingFinished.connect(
            self.check_processed_maps)
        # Input pdb
        self.pdb_input = window_utils.FileArgInput(
            parent=self,
            arg_name='pdb_path',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.pdb_input)
        # -> Use Refmac for model-map conversion
        self.use_refmac = window_utils.CheckArgInput(
            parent=self,
            arg_name='use_refmac',
            args=self.args,
            label_width=245)
        self.args_widget.args_layout.addWidget(self.use_refmac)
        self.use_refmac.value_line.stateChanged.connect(self.set_modelmap)
        # Ligand library input for Refmac5
        self.lib_input = window_utils.FileArgInput(
            parent=self,
            arg_name='lib_in',
            args=self.args,
            file_types=ccpem_file_types.lib_ext,
            required=False)
        self.args_widget.args_layout.addWidget(self.lib_input)

        # Disable low pass filter of input maps?
        self.no_lowpass_filter = window_utils.CheckArgInput(
            parent=self,
            arg_name='nofilt',
            args=self.args)
        self.args_widget.args_layout.addWidget(self.no_lowpass_filter)
        #self.no_lowpass_filter.value_line.stateChanged.connect(self.set_lowpass_filter)
        
        # global or local mode
        self.global_or_local = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='mode_selection',
            required=False,
            args=self.args)
        self.args_widget.args_layout.addWidget(self.global_or_local)
        self.global_or_local.value_line.currentIndexChanged.connect(
            self.set_mode_options)
        
        self.local_options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Local mode options',
            button_tooltip='Options specific for local mode')
        self.args_widget.args_layout.addLayout(self.local_options_frame)
        
        self.maskmap = window_utils.FileArgInput(
            parent=self,
            arg_name='maskfile',
            required=False,
            args=self.args)
        self.local_options_frame.add_extension_widget(self.maskmap)
        
        self.use_mpi_input = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='use_mpi',
            required=False,
            args=self.args)
        self.local_options_frame.add_extension_widget(self.use_mpi_input)
        self.use_mpi_input.value_line.currentIndexChanged.connect(
            self.set_n_mpi_visible)
         
        if self.args.n_mpi.value == 1:
            self.args.n_mpi.value = QtCore.QThread.idealThreadCount() / 2
        self.n_mpi_input = window_utils.NumberArgInput(
            parent=self,
            arg_name='n_mpi',
            required=False,
            args=self.args)
        self.local_options_frame.add_extension_widget(self.n_mpi_input)

        self.window_size = window_utils.NumberArgInput(
            parent=self,
            arg_name='window_size',
            required=False,
            args=self.args)
        self.local_options_frame.add_extension_widget(self.window_size)

        # Extended options
        options_frame = window_utils.CCPEMExtensionFrame(
            button_name='Extended options',
            button_tooltip='Extended difference map options')
        self.args_widget.args_layout.addLayout(options_frame)
        #add advanced options
        self.add_diffmap_advanced_args(options_frame)

#         # XXX TODO: placement holder until Agnel finishes map alignment code
#         # -> Map alignement
#         map_alignment = window_utils.ChoiceArgInput(
#             parent=self,
#             arg_name='map_alignment',
#             args=self.args)
#         options_frame.add_extension_widget(map_alignment)
        self.set_pdb_or_map_input(autoreset=False)
        self.set_mode_options()
        self.set_dust_prob()
        self.enable_mapprocess_button(self.mp_button1, 
                                     self.args.map_path_1.value,
                                     self.mapprocess_list1
                                    )
        self.enable_mapprocess_button(self.mp_button2, 
                                     self.args.map_path_2.value,
                                     self.mapprocess_list2
                                    )
        self.set_n_mpi_visible()
        #add files to launcher
        self.add_diffmap_input_files_to_launcher()


    def add_diffmap_advanced_args(self,options_frame):
        # -> Use second map as reference for scaling
        self.reference_scaling = window_utils.CheckArgInput(
            parent=self,
            arg_name='refscale',
            args=self.args)
        options_frame.add_extension_widget(self.reference_scaling)
        self.reference_scaling.value_line.stateChanged.connect(self.set_ref_scale)
        # -> Threshold for fractional difference
        self.threshold_difference = window_utils.NumberArgInput(
            parent=self,
            arg_name='threshold_fraction',
            args=self.args)
        options_frame.add_extension_widget(self.threshold_difference)
        # -> Dust filter
        self.dust_filter = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='dust_filter',
            args=self.args)
        options_frame.add_extension_widget(self.dust_filter)
        self.dust_filter.value_line.currentIndexChanged.connect(
            self.set_dust_prob)
        self.dust_prob = window_utils.NumberArgInput(
            parent=self,
            arg_name='dustprob',
            args=self.args)
        options_frame.add_extension_widget(self.dust_prob)
        # -> Fractional difference
        self.frac_map = window_utils.ChoiceArgInput(
            parent=self,
            arg_name='save_fracmap',
            args=self.args)
        options_frame.add_extension_widget(self.frac_map)

    def add_diffmap_input_files_to_launcher(self):
                # Add files to launcher
        self.launcher.add_file(
            arg_name='map_path_1',
            file_type='map',
            description=self.args.map_path_1.help,
            selected=True)
        if self.args.map_or_pdb_selection.value == 'Map':
            self.launcher.add_file(
                arg_name='map_path_2',
                file_type='map',
                description=self.args.map_path_2.help,
                selected=True)
        else:
            self.launcher.add_file(
                arg_name='pdb_path',
                file_type='pdb',
                description=self.args.pdb_path.help,
                selected=True)


    def set_pdb_or_map_input(self,autoreset=True):
        #index = self.args.map_or_pdb_selection.choices.index(
        #    self.args.map_or_pdb_selection.value)
        #sel = self.args.map_or_pdb_selection.choices[index]
        sel = self.args.map_or_pdb_selection.value
        if sel == 'Model':
            #self.task.args.map_or_pdb_selection.value = 'Model'
            self.map_input_2.required=False
            self.map_input_2.set_required(False)
            self.map_input_2.hide()
            self.mp_button2.hide()
            self.map_resolution_2.required=False
            self.map_resolution_2.set_required(False)
            self.map_resolution_2.hide()
            self.pdb_input.set_required(True)
            self.pdb_input.show()
            self.use_refmac.show()
            self.lib_input.show()
            self.task.args.map_path_2.value = None
            if autoreset:
                self.reference_scaling.value_line.setChecked(True)
                self.task.args.refscale.value = True
                self.no_lowpass_filter.value_line.setChecked(True)
                self.task.args.nofilt.value = True
        
        elif sel == 'Map':
            self.map_input_2.set_required(True)
            self.map_input_2.show()
            self.mp_button2.show()
            self.map_resolution_2.show()
            self.map_resolution_2.set_required(True)
            self.pdb_input.required=False
            self.pdb_input.set_required(False)
            self.pdb_input.hide()
            self.use_refmac.hide()
            self.lib_input.hide()
            self.task.args.pdb_path.value = None
            if autoreset:
                self.reference_scaling.value_line.setChecked(False)
                self.task.args.refscale.value = False
                self.no_lowpass_filter.value_line.setChecked(False)
                self.task.args.nofilt.value = False
            
#     def set_lowpass_filter(self):
#         if self.no_lowpass_filter.value_line.isChecked():
#             self.task.args.nofilt.value = True
#         else:
#             self.task.args.nofilt.value = False
    
    def set_modelmap(self):
        if self.use_refmac.value_line.isChecked():
            self.task.args.use_refmac.value = True
            self.no_lowpass_filter.value_line.setChecked(False)
            self.task.args.nofilt.value = False
        else:
            self.task.args.use_refmac.value = False
    
    def set_ref_scale(self):
        if self.reference_scaling.value_line.isChecked():
            self.task.args.refscale.value = True
        else:
            self.task.args.refscale.value = False
    
    def set_mode_options(self):
        sel = self.args.mode_selection.value
        if sel == 'local':
            self.local_options_frame.show()
            self.maskmap.show()
            self.window_size.show()
            #self.use_mpi_input.show()
        elif sel == 'global':
            self.local_options_frame.hide()
            self.maskmap.hide()
            self.window_size.hide()
            #self.use_mpi_input.hide()
        elif sel == 'None':
            self.local_options_frame.hide()
            self.maskmap.hide()
            self.window_size.hide()
            #self.use_mpi_input.hide()
            self.task.args.noscale.value = True
        #check for processed maps
        self.check_processed_maps()
    
    def set_dust_prob(self):
        if self.args.dust_filter.value:
            self.dust_prob.show()
        else:
            self.dust_prob.hide()
    
    def set_n_mpi_visible(self):
        if self.args.use_mpi():
            self.n_mpi_input.show()
        else:
            self.n_mpi_input.hide()
        

    def set_on_job_running_custom(self):
        if self.args.map_or_pdb_selection.value == 'Model':
            self.launcher.add_file(
                arg_name='pdb_path',
                file_type='pdb',
                description=self.args.pdb_path.help,
                selected=True)

    def set_on_job_finish_custom(self):
        # Set expected output files as implemented in TEMPy/difference_map
        diff_map1, diff_map2 = self.get_difference_map_names()
        # Add output files to launcher
        if self.args.map_or_pdb_selection.value == 'Model':
            self.launcher.add_file(
                arg_name=None,
                path=self.get_synthetic_map_name(),
                file_type='map',
                description='Synthetic map from PDB',
                selected=True)
        self.launcher.add_file(
            arg_name=None,
            path=diff_map1,
            file_type='map',
            description='Difference map 1 (map1 - map2)',
            selected=True)
        self.launcher.add_file(
            arg_name=None,
            path=diff_map2,
            file_type='map',
            description='Difference map 2 (map2 - map1)',
            selected=True)
        
        if self.args.save_fracmap.value:
            diff_fracmap1 = os.path.join(self.task.job_location,
                                 'diff_frac1.mrc')
            if os.path.exists(diff_fracmap1):
                self.launcher.add_file(
                    arg_name=None,
                    path=diff_fracmap1,
                    file_type='map',
                    description='Fractional difference map 1 (map1-map2)/map1',
                    selected=False)
            diff_fracmap2 = os.path.join(self.task.job_location,
                                 'diff_frac2.mrc')
            
            if os.path.exists(diff_fracmap2):
                self.launcher.add_file(
                    arg_name=None,
                    path=diff_fracmap2,
                    file_type='map',
                    description='Fractional difference map 2 (map2-map1)/map2',
                    selected=False)
        # Add power spectra plot
        power_spectra_plot = os.path.join(
            self.task.job_location,
            'spectra.png')
        if os.path.exists(power_spectra_plot):
            self.launcher.add_file(
                arg_name=None,
                path=power_spectra_plot,
                description='Matched power spectra of input maps',
                selected=False)

        message = ('Chimera will launch maps and automatically set input maps '
                   'to 2.0 sigma and difference maps to 1.0 sigma')
        self.launcher.set_message_label(message=message)

    def get_difference_map_names(self):
        m1_name =  os.path.basename(self.args.map_path_1.value).split('.')[0]
        if self.args.map_or_pdb_selection.value == 'Model':
            m2_name = self.args.pdb_path.value
            m2_name =  os.path.basename(m2_name).split('.')[0]+'_syn'
        else:
            m2_name = self.args.map_path_2.value
            m2_name =  os.path.basename(m2_name).split('.')[0]
            
        diff_map1 = os.path.join(self.task.job_location,
                                 (m1_name + '-' + m2_name+'_diff.mrc'))
        diff_map2 = os.path.join(self.task.job_location,
                                 (m2_name + '-' + m1_name+'_diff.mrc'))
        return diff_map1, diff_map2

    def get_synthetic_map_name(self):
        syn_map_path = os.path.basename(self.args.pdb_path.value).split('.')[0]
        syn_map_path = os.path.join(self.task.job_location,
                                    syn_map_path + '_syn.mrc')
        return syn_map_path

    def run_chimera_custom(self):
        chimera_script = difference_map_chimera.__file__
        chimera_script = chimera_script.replace('.pyc', '.py')
        # Chimera script expects 2 input maps, followed by 2 difference maps
        if self.args.map_or_pdb_selection.value == 'Model':
            map2 = self.get_synthetic_map_name()
        else:
            map2 = self.args.map_path_2.value
        diff_map1, diff_map2 = self.get_difference_map_names()
        maps = [self.args.map_path_1.value,
                map2,
                diff_map1,
                diff_map2]
        map_args = chimera_script
        # Check map exists else pass 'None'
        for map_ in maps:
            map_path = 'None'
            if map_ is not None:
                if os.path.exists(map_):
                    map_path = map_
            map_args += ' ' + map_path
        if self.args.map_or_pdb_selection.value == 'Model':
            map_args += (' ' + self.args.pdb_path.value)
        else:
            map_args += (' no_pdb')
        run_args = ['--script']
        run_args.append(map_args)
        process = QtCore.QProcess()
        chimera_bin = which('chimera')
        if chimera_bin is None:
            print 'Chimera executable not found (add executable to system PATH)'
        else:
            process.startDetached(chimera_bin, run_args)
        
    def enable_mapprocess_button(self,mp_button,map_path,list_mapprocess):
        if map_path is not None and \
            os.path.isfile(map_path):
                mp_button.setEnabled(True)
                list_mapprocess[0], list_mapprocess[1] = \
                    self.generate_mapprocess_task(map_path)
                self.task.mapprocess_task1,self.mapprocess_window1 = \
                    self.mapprocess_list1[0],self.mapprocess_list1[1]
                self.task.mapprocess_task2,self.mapprocess_window2 = \
                    self.mapprocess_list2[0],self.mapprocess_list2[1]
        else:
            mp_button.setEnabled(False)
    
    def check_mapprocess_button(self,mp_button,map_path):
        if map_path is not None and \
            os.path.isfile(map_path):
                mp_button.setEnabled(True)
        else:
            mp_button.setEnabled(False)
        
    def generate_mapprocess_task(self,map_path,
                                 db_inject=None, job_title=None):
        if self.task.database_path is not None:
            path = os.path.dirname(self.task.database_path)
            db_inject = sqlite_project_database.CCPEMInjector(
                database_path=self.task.database_path)
        # Set task args
        args = mapprocess_task.MapProcess().args
        #args.job_title.value = job_title
        args.map_path.value = map_path
        #args_json = args.output_args_as_json()
        task = mapprocess_task.MapProcess(
            parent=self,
            args=args,
            database_path=self.task.database_path)
            #job_location=job_location)
        window = mapprocess_window.MapProcessWindow(
            parent=self,
            task=task)
        return task, window
    
    def handle_mapprocess_task(self,window):
        if not window is None: window.show()
        
    def check_processed_maps(self):
        if self.args.map_path_1.value is not None:
            processed_map1_path = os.path.splitext(
                                os.path.basename(self.args.map_path_1.value))[0] \
                                +'_processed.mrc'
            
            if os.path.isfile(processed_map1_path) and \
                self.task.mapprocess_task1 is not None:
                self.map_input_1.value_line.setText(processed_map1_path)
        
        if self.args.map_path_2.value is not None:
            processed_map2_path = os.path.splitext(
                            os.path.basename(self.args.map_path_2.value))[0] \
                            +'_processed.mrc'
            if os.path.isfile(processed_map2_path) and \
                self.task.mapprocess_task2 is not None:
                self.map_input_2.value_line.setText(processed_map2_path)

def main():
    '''
    Launch standalone task runner.
    '''
    command_line_launch.ccpem_task_launch(
        task_class=difference_map_task.DifferenceMap,
        window_class=DifferenceMapWindow)

if __name__ == '__main__':
    main()
