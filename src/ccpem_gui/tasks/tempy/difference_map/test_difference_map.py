#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import sys
import shutil
import time
import tempfile
from PyQt4 import QtGui, QtCore
from PyQt4.QtTest import QTest
from ccpem_core.tasks.tempy.difference_map import difference_map_task
from ccpem_gui.tasks.tempy.difference_map import difference_map_window
from ccpem_core.test_data.tasks.tempy \
    import difference_map as difference_map_test
from ccpem_core import test_data
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
import numpy as np
import mrcfile

app = QtGui.QApplication(sys.argv)

class Test(unittest.TestCase):
    '''
    Unit test for TEMPy Difference Map (invokes GUI).
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.dirname(difference_map_test.__file__)
        self.test_output = tempfile.mkdtemp()
        self.testdata_dir = os.path.dirname(test_data.__file__)

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_difference_map_window_integration(self):
        '''
        Test TEMPy difference map via GUI.
        '''
        ccpem_utils.print_header(message='Unit test - TEMPy: Difference Map')
        os.chdir(os.path.dirname(os.path.realpath(__file__)))
        args_path = os.path.join(self.test_data, 'unittest_args.json')
        self.check_differencemap_input()
        run_task = difference_map_task.DifferenceMap(
            job_location=self.test_output,
            args_json=args_path)
        # Run w/out gui
#         run_task.run_task()
        # Setup GUI
        self.window = difference_map_window.DifferenceMapWindow(task=run_task)
        # Mouse click run
        QTest.mouseClick(
            self.window.tool_bar.widgetForAction(self.window.tb_run_button),
            QtCore.Qt.LeftButton)
        # Wait for run to complete
        self.job_completed = False
        timeout = 0
        stdout = run_task.pipeline.pipeline[0][0].stdout
        while not self.job_completed and timeout < 500:
            print '{0} running for {1} secs (timeout = 500)'.format(
                run_task.task_info.name,
                timeout)
            time.sleep(5.0)
            timeout += 5
            status =\
                process_manager.get_process_status(run_task.pipeline.json)
            if status == 'finished':
                if os.path.isfile(stdout):
                    tail = ccpem_utils.tail(stdout, maxlen=10)
                    if tail.find('CCP-EM process finished') != -1:
                        self.job_completed = True
        # Check timeout
        assert timeout < 500
        # Check job completed
        assert self.job_completed
        self.check_differencemap_output()
        # Time to check path exists before tear down
        time.sleep(0.1)
    
    def check_differencemap_input(self):
        mapfile1 = os.path.join(self.testdata_dir,'map','mrc',
                                'adpalfx_3622.mrc')
        mapfile2 = os.path.join(self.testdata_dir,'map','mrc',
                                'no_nuc_3621.mrc')
        map1 = mrcfile.open(mapfile1,mode='r')
        map2 = mrcfile.open(mapfile2,mode='r')
        self.assertGreater(np.corrcoef(map1.data.ravel(),
                                       map2.data.ravel())[0][1],
                                       0.5)
    def check_differencemap_output(self):
        diffmap_file = os.path.join(self.window.task.job_location,
                                'adpalfx_3622-no_nuc_3621_diff.mrc')
        assert os.path.exists(diffmap_file)
        nucleotide_map_file = os.path.join(self.testdata_dir,'map','mrc',
                                       'adpalfxonly_modmap.mrc')
        if os.path.exists(nucleotide_map_file):
            nucmap = mrcfile.open(nucleotide_map_file,mode='r')
            diffmap = mrcfile.open(diffmap_file,mode='r')
            mask_indices = np.nonzero(nucmap.data)
            diffmap_values = diffmap.data[mask_indices]
            nucmap_values = nucmap.data[mask_indices]
            self.assertAlmostEqual(np.corrcoef(diffmap_values,
                                        nucmap_values)[0][1],
                                           0.5, 1)
            nucmap.close()
            diffmap.close()
            
        

        

if __name__ == '__main__':
    unittest.main()
