#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Store location of gui icons.
'''
import os


def get_icons_path():
    '''
    Return directory where this script and associated icons are located.
    '''
    return os.path.dirname(__file__)


def get_ccpem_icon():
    '''
    Get small ccpem logo e.g. for gui windows
    '''
    path = os.path.join(get_icons_path(),
                       'ccpem_icon_96x96.png')
    if os.path.exists(path):
        return path
    else:
        print 'Warning: ccpem_icon.png not found'
        return ''


def get_other_icons_path():
    '''
    Get other program icons path.
    '''
    path = os.path.join(get_icons_path(),
                        'other_icons')
    if os.path.exists(path):
        return path
    else:
        print 'Warning: ccpem other icons path not found'
        return ''


def get_image_icons_path():
    '''
    Get image manipulation icons.  N.B. image icons provided by:
    http://p.yusukekamiyamane.com/icons/attribution/
    '''
    path = os.path.join(get_icons_path(),
                        'image_icons')
    if os.path.exists(path):
        return path
    else:
        print 'Warning: ccpem other icons path not found'
        return ''
