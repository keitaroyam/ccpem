C     ====================================================
      SUBROUTINE LKYASN(MINDX,NLPRGI,LSPRGI,CTPRGI,LOOKUP)
C     ====================================================
C
C---- There follows a jiffy subroutine to do column assignments, bypassing
C     the need for keyworded input. This is useful in writing little mtz
C     programs, without using Parser in the main program.          PRE
C
C     Read column assignments and make them, for input MTZ file 
C     open for read on index MINDX
C
C     It expects to read from stream 5 a line of the form
C       LABIN  program_label=file_label program_label=file_label . . .
C
C     This routine is useful for simple jiffy programs that don't want 
C     full keyworded input
C
C     MINDX	(I)	INTEGER		file index number for opened MTZ input file
C
C     NLPRGI    (I)	INTEGER		number of input program labels
C
C     LSPRGI	(I)	CHARACTER*30	array of dimension at least NLPRGI
C                               	containing the program label strings
C
C     CTPRGI	(I)	CHAR*1		array of column types for each column: 
C					these will be checked to see that they 
C					match the actual column types in the file. 
C					If you don't want to check column types, 
C					provide blank types here
C					(dimension at least NLPRGI)
C
C     LOOKUP	(O)	INTEGER		array of dimension at least NLPRGI
C					containing column numbers for each 
C					assigned label
C
C     .. Parameters ..
      INTEGER MFILES
      PARAMETER (MFILES=4)
      INTEGER MAXTOK
      PARAMETER (MAXTOK=100)
C     ..
C     .. Scalar Arguments ..
      INTEGER MINDX,NLPRGI
C     ..
C     .. Array Arguments ..
      INTEGER LOOKUP(*)
      CHARACTER*1  CTPRGI(*)
      CHARACTER*30 LSPRGI(*)
C     ..
C     .. Local Scalars ..
      CHARACTER KEY*4,LINE*400,LINE2*400
      INTEGER NTOK,ISTAT,IFAIL
      LOGICAL LEND
C     ..
C     .. Local Arrays ..
      CHARACTER CVALUE(MAXTOK)*4
      INTEGER IBEG(MAXTOK),IEND(MAXTOK),ITYP(MAXTOK),IDEC(MAXTOK)
      REAL    FVALUE(MAXTOK)
C     ..
C     .. External Subroutines ..
      EXTERNAL PARSER,LKYIN,LRASSN,LERROR
C
C---- First check that the MINDX is valid
C
      IF ((MINDX.LE.0) .OR. (MINDX.GT.MFILES)) THEN
        WRITE (LINE2,FMT='(A,I3,A,1X,I1,1X,A)')
     +    'From LKYASN : Index',MINDX,
     +    ' is out of range (allowed 1..',MFILES,')'
        ISTAT = 2
        IFAIL = -1
C
C            ************************
        CALL LERROR(ISTAT,IFAIL,LINE2)
C            ************************
C
      ELSE
C
C---- loop to read control input
C
 1      LINE=' '
        NTOK=MAXTOK
C
C            *********************************************************
        CALL PARSER(
     +    KEY,LINE,IBEG,IEND,ITYP,FVALUE,CVALUE,IDEC,NTOK,LEND,.TRUE.)
C            *********************************************************
C
        IF(LEND) THEN
          WRITE (LINE2,FMT='(A)')
     +    'From LKYASN : *** End of file found in column assignment ***'
          ISTAT = 1
C
C              ************************
          CALL LERROR(ISTAT,IFAIL,LINE2)
C              ************************
C
          RETURN
        END IF
C
        IF(NTOK.EQ.0) GO TO 1
C
        IF (KEY .EQ. 'LABI') THEN
C
C---- Labin column assignments
C
C              **********************************************
          CALL LKYIN(MINDX,LSPRGI,NLPRGI,NTOK,LINE,IBEG,IEND)
C              **********************************************
C
C---- Column assignments, set lookup
C
C              ****************************************
          CALL LRASSN(MINDX,LSPRGI,NLPRGI,LOOKUP,CTPRGI)
C              ****************************************
C
        ELSE
C
          WRITE (LINE2,FMT='(A,A)')
     +    'From LKYASN:  *** Column assignment should begin',
     +    ' with keyword LABIN ***'
          ISTAT = 1
C
C              ************************
          CALL LERROR(ISTAT,IFAIL,LINE2)
C              ************************
C
        END IF
C
      END IF
C
      END
