C*HLXAVG.FOR*************************************************************
C     AVERAGING F'S OF DIFFERENT HELICAL PARTICLES, TURNING THEM ALL THE
C     SAME WAY UP ETC
C     Version 1.0 adapted from EMHLAVG for VAX    RAC    18NOV82
C     Compile  +LLGRAPH    Then PIMLINK
C     Version 2.0 change to imsubs2000/p2k        RAC    13NOV00
C     Version 2.1 dimension increase		  jms	 19JUL12
C
	integer*4	nrmax
	parameter	(nrmax = 2000)
C*** jms 19.07.2012      DIMENSION HEAD(10),NN(40),NL(40),A(40,200),B(40,200)
      DIMENSION HEAD(10),NN(40),NL(40),A(40,nrmax),B(40,nrmax)
      DIMENSION FSCAL(40),IRMAX(40),TITLE(10)
C*** jms 19.07.2012      DIMENSION AT(200),RT(200),BT(200)
      DIMENSION AT(nrmax),RT(nrmax),BT(nrmax)
C*** jms 19.07.2012      DIMENSION ROUT(200),FOUT(200),PHIOUT(200)
      DIMENSION ROUT(nrmax),FOUT(nrmax),PHIOUT(nrmax)
      REAL*8 HEAD
      CHARACTER FILIN*40
      LOGICAL LAST
C*** jms 19.07.2012      NRMAX=200
      NOUT=1
      NSP=2
C
      WRITE(6,990)
      READ(5,991) TITLE
      WRITE(6,992) TITLE
      READ(5,*) LLMAX,NSIDE,CANG,DELBR,IPRINT
      WRITE(6,1020) LLMAX,NSIDE,CANG,DELBR,IPRINT
      RECC=1./CANG
      WRITE(6,1030) NRMAX
C
C     INITIALISATION
C     Open output file for averaged data
      WRITE(6,993)
      CALL CCPDPN(NOUT,'LLOUT','UNKNOWN','F',0,0)
      PI=3.14159
      TWOPI=2.*PI
      RADIAN=PI/180.0
      DEGREE=180.0/PI
      DO 20 L=1,LLMAX
      IRMAX(L)=0
      DO 20 IR=1,NRMAX
      A(L,IR)=0.0
   20 B(L,IR)=0.0
C
C     READING IN TRANSFORM DATA FOR EACH SIDE
      DO 100 I=1,NSIDE
      READ(5,1100) FILIN
 1100 FORMAT(A)
      WRITE(6,1019) I
C     Open input file
      CALL CCPDPN(NSP,FILIN,'READONLY','F',0,0)
      READ(5,*) ISIDE,IPOLE,PHISHF,ZSHIFT,RSCALE
      WRITE(6,1000) ISIDE,IPOLE,PHISHF,ZSHIFT,RSCALE
      READ(5,*) (FSCAL(L),L=1,LLMAX)
      WRITE(6,1002) (FSCAL(L),L=1,LLMAX)
      ZSHIFT=TWOPI*ZSHIFT*RECC
      PHISHF=PHISHF*RADIAN
      L=0
      WRITE(6,1003) I
C
    1 L=L+1
      DO 25 IP=1,NRMAX
      AT(IP)=0.0
   25 BT(IP)=0.0
      IP=0
      IF(L.GT.LLMAX) GO TO 98
      READ(NSP,1004,END=110) HEAD,SCALE,NN(L),NL(L)
      WRITE(6,1006) HEAD,SCALE,NN(L),NL(L),FSCAL(L)
      PHASH=NN(L)*PHISHF-ZSHIFT*NL(L)
      MODN=IABS(NN(L))
      MODN=MOD(MODN,2)
C
    3 READ(NSP,1005,END=99) RR,F,PHI
      IF(IPRINT.EQ.1) WRITE(6,1007) RR,F,PHI
      IF(RR.EQ.0.0.AND.IP.NE.0) GO TO 99
C*** jms 19.07.2012      IF(IP.EQ.NRMAX) GO TO 3
      IF(IP.EQ.NRMAX) then
	 write(6,'(''*** too many points, increase maxpoints'')')
	GO TO 3
	end if
      IP=IP+1
      RT(IP)=RR*RSCALE
      PHI=PHI*RADIAN
      IF(IPOLE.EQ.1) PHI=-PHI
      IF(ISIDE.EQ.1) PHI=PHI+PI*MODN
      PHI=PHI-PHASH
      AT(IP)=F*COS(PHI)*FSCAL(L)*SCALE
      BT(IP)=F*SIN(PHI)*FSCAL(L)*SCALE
      GO TO 3
C
   99 IPMAX=IP
      NR=RT(IP)/DELBR+1.1
      IF(NR.GT.NRMAX) NR=NRMAX
      IF(NR.GT.IRMAX(L)) IRMAX(L)=NR
      DO 101 IR=1,NR
      RR=(IR-1)*DELBR
      IF(RR.LT.RT(1)) GO TO 101
      DO 102 JP=2,IPMAX
      IF(RR.GE.RT(JP)) GO TO 102
      IP=JP
      GO TO 103
  102 CONTINUE
      GO TO 101
  103 RDF=RT(IP)-RT(IP-1)
      RBIT=(RR-RT(IP-1))/RDF
      RBAR=1.0-RBIT
      A(L,IR)=A(L,IR)+AT(IP-1)*RBAR+AT(IP)*RBIT
      B(L,IR)=B(L,IR)+BT(IP-1)*RBAR+BT(IP)*RBIT
  101 CONTINUE
      GO TO 1
   98 READ(NSP,991,END=110) SPARE
      GO TO 98
C     Close this input file so stream can be used for next file
  110 CLOSE(NSP)
  100 CONTINUE
C
C     OUTPUT OF AVERAGE DATA
      SCALE =1.0
      DO 200 L=1,LLMAX
      FMAX=0.0
      NR=IRMAX(L)
      IF(NOUT.GT.0) WRITE(NOUT,1008) TITLE,NL(L),SCALE,NN(L),NL(L)
C***  IF(IPRINT.GT.O) WRITE(6,1009) TITLE,NL(L),SCALE,NN(L),NL(L)
      IF(IPRINT.GT.0) WRITE(6,1009) TITLE,NL(L),SCALE,NN(L),NL(L)
      IP=0
C
      DO 150 IR=1,NR
      IF(A(L,IR).EQ.0.0.AND.B(L,IR).EQ.0.0) GO TO 150
      IP=IP+1
      ROUT(IP)=(IR-1)*DELBR
      FOUT(IP)=SQRT(A(L,IR)*A(L,IR)+B(L,IR)*B(L,IR))/NSIDE
      PHIOUT(IP)=ATAN2(B(L,IR),A(L,IR))*DEGREE
      IF(FOUT(IP).GT.FMAX) FMAX=FOUT(IP)
      IF(NOUT.GT.0) WRITE(NOUT,1005) ROUT(IP),FOUT(IP),PHIOUT(IP)
      IF(IPRINT.GT.0) WRITE(6,1007) ROUT(IP),FOUT(IP),PHIOUT(IP)
  150 CONTINUE
      RR=0.0
      IF(NOUT.GT.0) WRITE(NOUT,1005) RR,RR,RR
      IF(IPRINT.GT.0) WRITE(6,1007) RR,RR,RR
      IF(IP.GT.0) GO TO 199
      IF(NOUT.GT.0) WRITE(NOUT,1005) RR,RR,RR
      IF(IPRINT.GT.0) WRITE(6,1007) RR,RR,RR
      GO TO 200
C
  199 LAST=.FALSE.
      IF(L.EQ.LLMAX) LAST=.TRUE.
      SPLL=NL(L)*RECC
      CALL LLGRAPH(FOUT,PHIOUT,ROUT,0.,0.05,FMAX,NL(L),SPLL,NN(L),
     1    IP,TITLE,LAST)
C
  200 CONTINUE
C
   15 STOP
C
  990 FORMAT('1'///'  HLXAVG : Average layer line data')
  991 FORMAT(10A4)
  992 FORMAT(///'  Title for averaged data  :  ',10A4)
  993 FORMAT(///'  Output file for averaged data  :')
 1000 FORMAT(///'  Side  near/far 0/1  ',I10,
     1         /'  Pole invert Y/N 1/0 ',I10,
     2         /'  Phi shift           ',F10.2,
     3         /'  Z shift             ',F10.2,
     4         /'  Radial scale        ',F10.4)
 1020 FORMAT(///'  No of layer lines   ',I10,
     1         /'  No of sides         ',I10,
     2         /'  Helical repeat      ',F10.1,
     3         /'  Layer line sampling ',F10.5,
     4         /'  Print control       ',I10)
 1002 FORMAT(//' Amplitude scale factors',4(/1X,10F10.3))
 1003 FORMAT('0LIST OF LAYER LINES, SIDE ',I5)
 1019 FORMAT('1'//'  Input file for side',I5)
 1004 FORMAT(10A5,F10.3,2I5)
 1005 FORMAT(3E10.3)
 1006 FORMAT('0',10A5,F10.3,2I5,5X,F10.3)
 1007 FORMAT(1X,3E10.3)
 1008 FORMAT(10A4,'LL',I3,5X,F10.3,2I5)
 1009 FORMAT('1',10A4,'LL',I3,5X,F10.3,2I5)
 1030 FORMAT(//' POINTS MORE THAN',I5,' STEPS OF DELBR AWAY FROM',
     .' THE MERIDIAN WILL BE IGNORED')
      END
