C****HEADER*******************************************************
C
C	VX1.00		11-JUN-87		RH
C	VX1.10		23-FEB-00		JMS
C
C	This program simply writes out the normal header records.
C	If the extra records contain anything it is also printed.	
C
      INTEGER NXYZ(3),MXYZ(3),IEXT(25)
      ISTREAM = 1
      CALL  IMOPEN(ISTREAM,'IN','READONLY')
      CALL  IRDHDR(ISTREAM,NXYZ,MXYZ,MODE,DMIN,DMAX,DMEAN)
      CALL  IRTEXT(ISTREAM,IEXT,1,25)
C
C
C  extra header data
	IOUT=0
	DO 100 J=1,25
	IF(IEXT(J).NE.0) IOUT=IOUT+1
100	CONTINUE
	IF(IOUT.NE.0) THEN
		WRITE(6,201)
		DO 200 J=1,25
		IF(IEXT(J).NE.0)WRITE(6,202)J,IEXT(J)
200		CONTINUE
	ENDIF
C
C
      CALL  IMCLOSE(ISTREAM)
201   FORMAT(' Extra information in header'/'    N    contents')
202   FORMAT(I5,I10)
      END
