C*** hlxdyad - program to read Layer line data and set phases to 0 or 180
C*** requires input file name and output file name
C*** 	Version 1.0		JMS
C***
	parameter (maxzerolines = 100)
	character*80  filin
	character*80  filout
	character*80  outfile
	character*80  dataline
	logical  first
	real*4   farray(3)
C***
	write(6,'(''Input file ?'')')
	read(5,'(a)') filin
	write(6,'(''Output file ?'')')
	read(5,'(a)') filout
        open(unit=1,file=filin,status='old')
	open(unit=2,file=filout,status='unknown')
        read(1,'(a)') dataline
  100   write(2,'(a)')dataline
  200   read(1,'(a)',end=500) dataline
C*** check if headerline
	if(dataline(41:42) .eq. 'LL') go to 100
C*** unpack 3 numbers
	read(dataline,'(3e10.3)') position, amplitude, phase
	if (abs(phase) .gt. 90.0) then
	 phase = sign(180., phase)
	else
	 phase = 0.
	end if
	write(dataline,'(3e10.3)') position, amplitude, phase
	write(2,'(a)') dataline
	go to 200
  500   close(1)
	close(2)
	stop
	end
