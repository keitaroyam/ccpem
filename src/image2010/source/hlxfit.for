C*HLXFIT.FOR**********************************************************
C     REFINING RELATIVE ORIENTATIONS OF TWO HELICAL PARTICLES
C     ALSO FIND RELATIVE RADIAL AND AMPLITUDE SCALES
C
C     Version 1.0 adapted from EMHLFIT for VAX system    RAC   17NOV82
C     Version 1.1 formats tidied up   RAC  07FEB83
C     Version 2.0 imsubs2000          RAC  12NOV00
C
      DIMENSION F1(40,150),PSI1(40,150),NN1(40),NL1(40),F0SUM(40)
      DIMENSION TITLE(20),RESID(50,13,10),F1SUM(40,10),F2SUM(40,10)
      DIMENSION POINTS(10),DELZ(13),XINC(10),RESID2(50,13,10),WT(40)
      REAL*8 HEAD(10)
      DIMENSION IRMAX(41),IRN(10)
      CHARACTER FILREF*40,FILIN*40
      NRMAX=150
      NSP1=1
      NSP2=2
C
C     Open file of reference data
      WRITE(6,1100)
      READ(5,1110) FILREF
      CALL CCPDPN(NSP1,FILREF,'READONLY','F',0,0)
      READ(5,*) LLMAX,CANG,DELBR,IPRINT,IMISS
      WRITE(6,1000)   LLMAX,CANG,DELBR,IPRINT,IMISS
      RECC=1/CANG
      IF(LLMAX.GT.40) LLMAX=40
      READ(5,*) (WT(L),L=1,LLMAX)
      WRITE(6,1021) (WT(L),L=1,LLMAX)
      WRITE(6,1030) NRMAX
      DEGREE=180.0/3.14159
      RADIAN=3.14159/180.0
      DO 40 I=1,40
      NN1(I)=0
   40 NL1(I)=0
      L=1
      SUMF1=0.0
      ALLPTS=0.0
      GO TO 2
C
C     READ IN DATA FOR WHOLE OF ONE SIDE & FIND MEAN MOD.F FOR EACH LAYE
C
    1 SUMF1=SUMF1+F0SUM(L)
      ALLPTS=ALLPTS+PTS
      IF(PTS.GT.0.0) F0SUM(L)=F0SUM(L)/PTS
      IRMAX(L)=IR
      L=L+1
    2 IR=0
      IF(L.GT.LLMAX) GO TO 98
      READ(NSP1,1004,END=100) HEAD,SCALE,NN1(L),NL1(L)
      WRITE(6,1006) HEAD,SCALE,NN1(L),NL1(L)
      F0SUM(L)=0.0
      PTS=0.0
      DO 5 JR=1,NRMAX
    5 F1(L,JR)=0.0
C
    3 READ(NSP1,1005,END=100) RR,FF,PSI
      IF(RR.EQ.0.0.AND.IR.GT.0) GO TO 1
      IF(IPRINT.EQ.1) WRITE(6,1007) RR,FF,PSI
      IR=RR/DELBR+1.1
      IF(IR.GT.NRMAX) GO TO 3
      F1(L,IR)=FF*SCALE*WT(L)
      F0SUM(L)=F0SUM(L)+F1(L,IR)
      PTS=PTS+WT(L)
      PSI1(L,IR)=PSI
      GO TO 3
C
   98 IF(NSP1.EQ.5) GO TO 100
      READ(NSP1,999,END=100) SPARE
      GO TO 98
C
C
C     READ IN CONTROL DATA FOR SECOND SIDE & INITIALISE RESIDUAL ARRAY E
C
  100 READ(5,1110,END=500) FILIN
      READ(5,1002) TITLE
      WRITE(6,1003) TITLE
C     Open file of data to be fitted
      CALL CCPDPN(NSP2,FILIN,'READONLY','F',0,0)
      READ(5,*) ISIDE,IPOLE
      WRITE(6,1012) ISIDE,IPOLE
      READ(5,*) PHIMIN,PHIMAX,DPHI
      WRITE(6,1120) PHIMIN,PHIMAX,DPHI
      READ(5,*) ZMIN,ZMAX,DZED
      WRITE(6,1121) ZMIN,ZMAX,DZED
      READ(5,*) RSCMIN,RSCMAX,DRSC
      WRITE(6,1122) RSCMIN,RSCMAX,DRSC
   80 NPH=1.1+(PHIMAX-PHIMIN)/DPHI
      NZ=1.1+(ZMAX-ZMIN)/DZED
      NRS=1.1+(RSCMAX-RSCMIN)/DRSC
      IF(NPH.GT.50) NPH=50
      IF(NZ.GT.13) NZ=13
      IF(NRS.GT.10) NRS=10
      DO 60 IZ=1,NZ
      DO 60 IPH=1,NPH
      DO 60 IRS=1,NRS
      RESID(IPH,IZ,IRS)=0.0
      RESID2(IPH,IZ,IRS)=0.0
   60 CONTINUE
      ZCMIN=ZMIN*360.0*RECC
      DZC=DZED*360.0*RECC
      FMEAN=SUMF1/ALLPTS
      DO 78 IRS=1,NRS
      XINC(IRS)=0.0
   78 POINTS(IRS)=0.0
C
C     READ IN LAYER-LINE DATA FOR SECOND SIDE
      L=0
   10 L=L+1
	IR=0
      IF(L.GT.LLMAX) GO TO 99
      READ(NSP2,1004,END=200) HEAD,SCALE,NN,NL
C
C     CHECK FOR SAME LAYER-LINE IN DATA 1
      LL=L
      IF(NN1(L).NE.NN) LL=-1
      IF(NL1(L).NE.NL) LL=-1
C
   21 WRITE(6,1006) HEAD,SCALE,NN,NL
      DO 20 IRS=1,NRS
      F1SUM(L,IRS)=0.0
      F2SUM(L,IRS)=0.0
   20 CONTINUE
C
   22 READ(NSP2,1005,END=200) RR,FF,PSI
      IF(RR.EQ.0.0.AND.IR.NE.0) GO TO 12
      IF(IPRINT.EQ.1) WRITE(6,1007) RR,FF,PSI
      IR=RR/DELBR+1.1
      IF(IR.GT.NRMAX) GO TO 22
      IF(LL.EQ.-1) GO TO 22
      IF(IPOLE.EQ.1) PSI=-PSI
      IF(ISIDE.EQ.1) PSI=PSI+NN*180.
      FF=FF*SCALE*WT(L)
      A2=FF*COS(PSI*RADIAN)
      B2=FF*SIN(PSI*RADIAN)
      DO 50 IRS=1,NRS
      RRS=RR*(RSCMIN+(IRS-1)*DRSC)
C     INTERPOLATE IN TRANSFORM1
      RRS=RRS/DELBR+1.0
      IR=RRS
	IRN(IRS)=IR
      IF(IR.GT.NRMAX-1) GO TO 50
      IF(F1(L,IR).EQ.0.0.OR.F1(L,IR+1).EQ.0.0) GO TO 50
      RBIT=RRS-IR
      RBAR=1.0-RBIT
      A1=F1(L,IR)*COS(PSI1(L,IR)*RADIAN)*RBAR
     1  +F1(L,IR+1)*COS(PSI1(L,IR+1)*RADIAN)*RBIT
      B1=F1(L,IR)*SIN(PSI1(L,IR)*RADIAN)*RBAR
     1  +F1(L,IR+1)*SIN(PSI1(L,IR+1)*RADIAN)*RBIT
      FF1=SQRT(A1*A1+B1*B1)
      PSI11=ATAN2(B1,A1)*DEGREE
   23 F1SUM(L,IRS)=F1SUM(L,IRS)+FF1
      F2SUM(L,IRS)=F2SUM(L,IRS)+FF
      DELPSI=PSI-PSI11
      FWT=(FF1+FF)/(2.0*FMEAN)
      POINTS(IRS)=POINTS(IRS)+FWT
      XINC(IRS)=XINC(IRS)+WT(L)
C
      DO 51 IPH=1,NPH
      DELPHI=(PHIMIN+(IPH-1)*DPHI)*NN
      DO 51 IZ=1,NZ
      DELZED=(ZCMIN+(IZ-1)*DZC)*NL
      DEL=ABS(DELPSI+DELZED-DELPHI)
      DEL=AMOD(DEL,360.0)
      IF(DEL.GT.180.0) DEL=360.0-DEL
      RESID(IPH,IZ,IRS)=RESID(IPH,IZ,IRS)+DEL*DEL*FWT
      THETA=(DELZED-DELPHI)*RADIAN
      CTH=COS(THETA)
      STH=SIN(THETA)
      A3=A2*CTH-B2*STH
      B3=A2*STH+B2*CTH
      RESID2(IPH,IZ,IRS)=RESID2(IPH,IZ,IRS)+(A1-A3)**2+(B1-B3)**2
   51 CONTINUE
C
   50 CONTINUE
      GO TO 22
   99 IF(NSP2.EQ.5) GO TO 200
      READ(NSP2,999,END=200) SPARE
      GO TO 99
C
C     INCLUDE LAYER-LINE DATA MISSING FOR SECOND SIDE
C           (IF IMISS=1)
12	CONTINUE
	IF(IMISS.NE.1)GO TO 10
      JRM2=IRMAX(L)
      IF(JRM2.GT.NRMAX) JRM2=NRMAX
      DO 180 IRS=1,NRS
      JRM1=(IRN(IRS)-1)*(RSCMIN+(IRS-1)*DRSC)+2.5
      IF(JRM1.GT.JRM2) GO TO 180
      RESIDX=0.0
      RESIDY=0.0
      DO 25 IR=JRM1,JRM2
      IF(F1(L,IR).EQ.0.0) GO TO 25
      A1=F1(L,IR)*COS(PSI1(L,IR)*RADIAN)
      B1=F1(L,IR)*SIN(PSI1(L,IR)*RADIAN)
      FF1=A1*A1+B1*B1
      RESIDX=RESIDX+FF1
      FWT=SQRT(FF1)/FMEAN
      POINTS(IRS)=POINTS(IRS)+FWT
      XINC(IRS)=XINC(IRS)+WT(L)
      RESIDY=RESIDY+8100.0*FWT
   25 CONTINUE
      DO 30 IPH=1,NPH
      DO 30 IZ=1,NZ
      RESID(IPH,IZ,IRS)=RESID(IPH,IZ,IRS)+RESIDY
      RESID2(IPH,IZ,IRS)=RESID2(IPH,IZ,IRS)+RESIDX
   30 CONTINUE
  180 CONTINUE
      GO TO 10
C
C     SEARCH FOR MINIMUM VALUE, NORMALISE AND OUTPUT
  200 RESMIN=1.0E20
      RESMN2=1.0E20
      DO 45 IZ=1,NZ
   45 DELZ(IZ)=ZMIN+(IZ-1)*DZED
      DO 250 IRS=1,NRS
        RSC=RSCMIN+(IRS-1)*DRSC
        IINC=XINC(IRS)+0.5
        WRITE(6,1001) RSC,IINC
        IF(POINTS(IRS).EQ.0.0) GO TO 250
        WRITE(6,1022) (DELZ(IZ),IZ=1,NZ)
        WRITE(6,1023)
        DO 255 IPH=1,NPH
          PHI=PHIMIN+(IPH-1)*DPHI
          DO 260 IZ=1,NZ
           RESID2(IPH,IZ,IRS)=SQRT(RESID2(IPH,IZ,IRS)/XINC(IRS))
           IF(RESID2(IPH,IZ,IRS).GE.RESMN2) GO TO 261
           RESMN2=RESID2(IPH,IZ,IRS)
           Z20=DELZ(IZ)
           PHI20=PHI
           RSC20=RSC
           IR20=IRS
  261      RESID(IPH,IZ,IRS)=SQRT(RESID(IPH,IZ,IRS)/POINTS(IRS))
           IF(RESID(IPH,IZ,IRS).GE.RESMIN) GO TO 260
           RESMIN=RESID(IPH,IZ,IRS)
           Z0=DELZ(IZ)
           PHI0=PHI
           RSC0=RSC
           IR0=IRS
  260     CONTINUE
          WRITE(6,1008) PHI,(RESID2(IPH,IZ,IRS),IZ=1,NZ)
          WRITE(6,1014) (RESID(IPH,IZ,IRS),IZ=1,NZ)
  255   CONTINUE
  250 CONTINUE
C
      WRITE(6,1015) RESMN2,PHI20,Z20,RSC20
      WRITE(6,1011) RESMIN,PHI0,Z0,RSC0
      DO 120 L=1,LLMAX
  120 IF(F2SUM(L,IR20).GT.0.0) F2SUM(L,IR20)=F1SUM(L,IR20)/F2SUM(L,IR20)
      WRITE(6,1013) (F2SUM(L,IR20),L=1,LLMAX)
      WRITE(6,1016)
      CLOSE(NSP2)
      GO TO 100
  500 STOP
C
  999 FORMAT(10A5)
 1000 FORMAT(///'  No. of layer lines      ',I10,
     1         /'  Helical repeat (c)      ',F10.2,' Angstroms',
     2         /'  Layer line sampling     ',F10.6,' rec. Ang.',
     3         /'  Print or not (1/0)      ',I10,
     4         /'  Missing data penalty    ',I10)
 1001 FORMAT('1'//' RESIDUAL WHEN RADIAL DISTANCES IN TRANSFORM 2 ARE
     1SCALED BY ',F10.3/
     21X,I10,' PAIRS OF DATA POINTS COMPARED')
 1002 FORMAT(20A4)
 1003 FORMAT('1'//'  Data for fitting :'/5X,20A4//'  From file :'/)
 1004 FORMAT(10A5,F10.3,2I5)
 1005 FORMAT(3E10.3)
 1006 FORMAT('0',10A5,F10.3,2I5)
 1007 FORMAT(1X,3E10.3)
 1008 FORMAT(1X,F8.2,2X,12E10.3)
 1011 FORMAT('0PHASE MINIMUM  ',E10.3,' AT PHI= ',F10.1,'   Z= ',F10.1,
     1'   RSCALE =  ',F10.3)
 1012 FORMAT(///'  Same/different sides (0/1)  ',I10,
     1         /'  IPOLE invert or not  (1/0)  ',I10)
 1013 FORMAT('0RATIO OF MEAN AMPLITUDES FOR EACH LAYERLINE',5X,
     1'DATA1/DATA2'/(1X,12E10.3))
 1014 FORMAT(T12,12F10.1)
 1015 FORMAT(///' VECTOR MINIMUM ',E10.3,' AT PHI= ',F10.1,'   Z= ',
     1 F10.1,'   RSCALE =  ',F10.3)
 1016 FORMAT(///)
 1021 FORMAT(//'  Layer line weights',//2(1X,15F8.2))
 1022 FORMAT('0',T50,'ZSHIFT'/(11X,12F10.1))
 1023 FORMAT(' PHISHIFT')
 1030 FORMAT(//' POINTS MORE THAN',I5,' STEPS OF DELBR AWAY FROM',
     1' THE MERIDIAN WILL BE IGNORED'//
     2' Reference layer line data :'/)
 1100 FORMAT('1',//'  HLXFIT : Helix fitting program'//
     1 '   Reference data from file :'/)
 1110 FORMAT(A)
 1120 FORMAT(//'   Initial phi     ',F10.1,' degrees',
     1        /'   Final phi       ',F10.1,' degrees',
     2        /'   Phi step        ',F10.1,' degrees')
 1121 FORMAT(//'   Initial Z       ',F10.1,' Angstroms',
     1        /'   Final Z         ',F10.1,' Angstroms',
     2        /'   Z step          ',F10.1,' Angstroms')
 1122 FORMAT(//'   Initial radial scale  ',F10.3,
     1        /'   Final radial scale    ',F10.3,
     2        /'   Radial scale step     ',F10.3,
     3       ///'  Layer line data for fitting : '/)
      END
