C*HLXLLOUT.FOR**********************************************************
C     Program to select and graph standard format layer line data either
C     with autoscaling for each layer line or on constant amplitude scale
C      Layer line input on stream 1 (IN)
C       TITLE    Title for plots (up to 40 chars)
C       LLMAX    No. of layer lines to be input
C       FSCA     Multiplying amplitude scale factor applied when data read
C                from disc. Allows data set to be put on absolute scale.
C       FMAX     Maximum amplitude overall on layer lines to be plotted,
C                after FSCA has been applied.
C                If set to zero then autoscaling used.
C       RMIN,RMAX Radial range for plotting box (rec Ang.)
C       CANG     Helical repeat (Angstroms) to set ZSTAR
C       IPLOT(I) I=1,LLMAX  Plot particular layer line if 1, not if 0
C
C        Compile , then PIMLINK
C            RAC    FOR VAX      7SEP83
C            RAC    CONVERT TO IMSUBS2000  11NOV00
C
      DIMENSION TITLE(10),IPLOT(30),JUNK(15),F(200),PHI(200),RAD(200)
      LOGICAL LAST
C
      LAST=.FALSE.
      WRITE(6,1000)
1000  FORMAT('1',///'  HLXLLOUT : Graph layer line data from file :'//)
      CALL CCPDPN(1,'LLIN','READONLY','F',0,0)
      READ(5,1001) TITLE
1001  FORMAT(10A4)
      WRITE(6,1002) TITLE
1002  FORMAT(//' Title for plots :  ',10A4)
      READ(5,*) LLMAX,FSCA,FMAX,RMIN,RMAX,CANG
      WRITE(6,1003) LLMAX,FSCA,FMAX,RMIN,RMAX,CANG
1003  FORMAT(///'  No of layer lines   ',I10,
     1         /'  Amp scale factor    ',F10.4,
     2         /'  Max amp for plot    ',F10.1
     3         /'  Plotting range      ',2F10.3,
     4         /'  Helical repeat      ',F10.1)
      READ(5,*) (IPLOT(I),I=1,LLMAX)
      WRITE(6,1004) (IPLOT(I),I=1,LLMAX)
1004  FORMAT(//' Plot flags',//30I3)
      DO 100 I=1,LLMAX
      NPT=0
      AMPMAX=0.
      READ(1,1010) JUNK,IORD,LLNO
1010  FORMAT(15A4,2I5)
105   NPT=NPT+1
      READ(1,1020) RAD(NPT),F(NPT),PHI(NPT)
1020  FORMAT(3E10.3)
      IF(RAD(NPT).EQ.0..AND.NPT.NE.1) GO TO 110
      F(NPT)=FSCA*F(NPT)
      IF(F(NPT).GT.AMPMAX) AMPMAX=F(NPT)
      IF(PHI(NPT).GT.180.) PHI(NPT)=PHI(NPT)-360.
      GO TO 105
110   NPT=NPT-1
      IF(IPLOT(I).EQ.0) GO TO 100
      SPLL=LLNO/CANG
      FPLOT=FMAX
      IF(FMAX.EQ.0.) FPLOT=AMPMAX
      IF(I.EQ.LLMAX) LAST=.TRUE.
      CALL LLGRAPH(F,PHI,RAD,RMIN,RMAX,FPLOT,LLNO,SPLL,IORD,NPT,
     1 TITLE,LAST)
100   CONTINUE
      STOP
      END
