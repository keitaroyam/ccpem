
C*ROTAV.FOR******************************************************
C       Rotational averaging in density space. Adapted from INTERPO.
C       Version 1.02    RAC    3-JUN-82
C       Version 1.03    RAC    27-APR-83   Rewrite INTERP
C       Version 2.0     RAC    15-NOV-00   Linked with imsubs2000
C       Will average 3-D map section by section
C       NB  Rot. axis position measured from bottom LH corner
C        of TONE image as (0,0).
C
C	This is a general 2-D interpolation program that allows
C	the user to perform Rotations, Translations, Size
C	alterations and Re-sampling on a skewed or distorted
C	coordinate system.
C	The rotation from OLD axis to NEW axis is positive for
C	an anti-clockwise rotation.
C
C
	COMMON//NXA,NYA,NZA,NXB,NYB,NZB
	DIMENSION ARRAY(90000),BRAY(90000),AMAT(2,2)
	DIMENSION NXYZA(3),MXYZ(3),TITLE(20),NXYZB(3),AMAT2(2,2)
	DIMENSION CELL(6),NXYZST(3),ROT(2,2),T1(2,2),T2(2,2)
	EQUIVALENCE (NXA,NXYZA), (NXB,NXYZB)
CTSH++
	CHARACTER*80 TMPTITLE
	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
	DATA CNV/ .017453291/, NXYZST/3*0/
C
	WRITE(6,1000)
1000	FORMAT(//,' ROTAV V1.03 : Rotational averaging program',/)
	CALL IMOPEN(1,'IN','RO')
	CALL IMOPEN(2,'OUT','NEW')
	CALL IRDHDR(1,NXYZA,MXYZ,MODE,DMIN,DMAX,DMEAN)
	CALL ITRHDR(2,1)
	CALL IRTCEL(1,CELL)
C
	NZB = NZA
	ROTX = 0.0
	AXANG = CELL(6)
	XT = 0.
	YT = 0.
	XCEN = (NXA-1)/2.
	YCEN = (NYA-1)/2.
	SCALE = 1.0
	TMIN =  1.E10
	TMAX = -1.E10
	TMEAN = 0.0
	AMAGX = 1.0
	AMAGY = 1.0
        ICELL=0
C
	WRITE(6,1200)
1200	FORMAT('$Enter rotation angle for X axis [0]: ')
	READ(5,*) ROTX
	WRITE(6,1300)
1300	FORMAT('$Enter rotational symmetry number:')
	READ(5,*) NSYM
        AXANG=90.
	WRITE(6,1400)
1400	FORMAT('$Enter magnification factor[1]: ')
	READ(5,*) AMAGX
        AMAGY=AMAGX
	WRITE(6,1500) XCEN,YCEN
1500	FORMAT('$Enter X,Y center for transformation [',2F7.1,' ]: ')
	NXB = NXA*AMAGX
	NYB = NYA*AMAGY
        IRAD=NXB/2
	READ(5,*) XCEN,YCEN
	WRITE(6,1600) NXB,NYB,IRAD
1600	FORMAT('$Enter output file size NX,NY and box radius
     1   [',3I4,' ]: ')
	READ(5,*) NXB,NYB,IRAD
        WRITE(6,1700)
1700    FORMAT('$Use original (0) or new (1) cell dimensions [0] : ')
        READ(5,*) ICELL
        WRITE(6,1900) ROTX,NSYM,AMAGX,XCEN,YCEN,NXB,NYB,IRAD,ICELL
1900    FORMAT(///' Rotn angle for x axis      ',F10.1,/
     1            ' Rotational symmetry        ',I10,/
     2            ' Magnification              ',F10.3,/
     3            ' Centre x,y                 ',2F10.3,/
     4            ' Image dimensions, radius   ',3I10,/
     5            ' Old(0) or new(1) cell dim. ',I10)
        NXYBT=NXB*NYB
CTSH	ENCODE (80,2000,TITLE) NSYM,ROTX,XCEN,YCEN,AMAGX,IRAD,ICELL
CTSH++
	WRITE (TMPTITLE,2000) NSYM,ROTX,XCEN,YCEN,AMAGX,IRAD,ICELL
CTSH--
2000	FORMAT('ROTAV: Nsym,Rot,XYcen,XYmag,Rad,Cell: ',
     1    I3,3F6.1,F6.3,I4,I2)
C
C  NEW HEADER
C
	DX = CELL(1)/MXYZ(1)
	DY = CELL(2)/MXYZ(2)
	DXP = DX/AMAGX
	DYP = DY/AMAGY
        IF (ICELL.EQ.1) THEN
         CELL(1) = DXP*NXB
         CELL(2) = DYP*NYB
        END IF
	CELL(6) = AXANG
	CALL IALSIZ(2,NXYZB,NXYZST)
	CALL IALCEL(2,CELL)
	CALL IWRHDR(2,TITLE,1,DMIN,DMAX,DMEAN)
C
C  SET UP CONVERSION MATRICES
C
        DELANG=6.283185/NSYM
	ROTX0 = CNV*ROTX
	AXANG = AXANG*CNV
	GAMMA = CELL(6)*CNV
        NBYTES=4*NXYBT
C       Sections loop
        DO 200 NZ=1,NZA
C
	  CALL IRDSEC(1,ARRAY,*99)
          CALL CCPZBI(BRAY,NBYTES)
C
C       Interpolate for each symmetry position
        DO 300 NS=1,NSYM
        ROTX=ROTX0+(NS-1)*DELANG
	CR = COS(ROTX)
	SR = SIN(ROTX)
	CA = COS(AXANG)
	SA = SIN(AXANG)
	CG = COS(GAMMA)
	SG = SIN(GAMMA)
	ROT(1,1) = CR
	ROT(1,2) = SR
	ROT(2,1) = -SR
	ROT(2,2) = CR
	T1(1,1) = DX
	T1(1,2) = -DY*CG
	T1(2,1) = 0.0
	T1(2,2) = DY*SG
	T2(1,1) = 1./DXP
	T2(1,2) = CA/(DXP*SA)
	T2(2,1) = 0.0
	T2(2,2) = 1./(DYP*SA)
C
	CALL MM(AMAT,ROT,T1)
	CALL MM(AMAT2,T2,AMAT)
C
C
	  CALL INTERP1(ARRAY,BRAY,NXA,NYA,NXB,NYB,AMAT2,XCEN,YCEN,
     .	  XT,YT,SCALE)
C
300     CONTINUE
C
C       Renormalize,apply circular box,find density limits
        RADSQ=IRAD*IRAD
        DMIN=1.E7
        DMAX=-1.E7
        DMEAN=0.
        NPT=0
        XC=(NXB-1)/2
        YC=(NYB-1)/2
        DO 500 NXY=1,NXYBT
        IY=(NXY-1)/NXB
        IX=NXY-IY*NXB
        RSQ=(IX-XC)**2+(IY-YC)**2
        IF(RSQ.GT.RADSQ) GO TO 510
C       Inside circular box
        BRAY(NXY)=BRAY(NXY)/NSYM
        IF(BRAY(NXY).GT.DMAX) DMAX=BRAY(NXY)
        IF(BRAY(NXY).LT.DMIN) DMIN=BRAY(NXY)
        DMEAN=DMEAN+BRAY(NXY)
        NPT=NPT+1
        GO TO 500
C       Outside circular box
510     BRAY(NXY)=0.
C
500     CONTINUE
        DMEAN=DMEAN/NPT
        WRITE(6,2100) DMIN,DMAX,DMEAN,NPT
2100    FORMAT(///'  Min max and mean density and no. of points inside
     1  circular box'/10X,3F10.1,I10)
C
	  CALL IWRSEC(2,BRAY)
        IF(DMIN.LT.TMIN) TMIN=DMIN
        IF(DMAX.GT.TMAX) TMAX=DMAX
        TMEAN=TMEAN+DMEAN
200     CONTINUE
C
        TMEAN=TMEAN/NZA
        IF(NZA.GT.1) WRITE(6,2200) TMIN,TMAX,TMEAN
2200    FORMAT(///' Overall min max and mean density',3F10.1)
	CALL IWRHDR(2,TITLE,-1,TMIN,TMAX,TMEAN)
99	CALL IMCLOSE(1)
	CALL IMCLOSE(2)
	STOP
	END
C
C*MM  MULTIPLY 2X2 MATRICES
C
	SUBROUTINE MM(CMAT,AMAT,BMAT)
	DIMENSION AMAT(2,2),BMAT(2,2),CMAT(2,2)
C
	CMAT(1,1) = AMAT(1,1)*BMAT(1,1) + AMAT(1,2)*BMAT(2,1)
	CMAT(1,2) = AMAT(1,1)*BMAT(1,2) + AMAT(1,2)*BMAT(2,2)
	CMAT(2,1) = AMAT(2,1)*BMAT(1,1) + AMAT(2,2)*BMAT(2,1)
	CMAT(2,2) = AMAT(2,1)*BMAT(1,2) + AMAT(2,2)*BMAT(2,2)
	RETURN
	END
C
C*INTERP1.FOR***************************************************
C       Adapted from INTERP for use with ROTAV
C	This subroutine will perform coordinate transformations
C	(rotations,translations, etc.) .
C
C	Accumulate sum of successively rotated versions of ARRAY
C       in BRAY
C
C	ARRAY	- The input image array
C	BRAY	- The output image array
C	NXA,NYA	- The dimensions of ARRAY
C	NXB,NYB	- The dimensions of BRAY
C	AMAT	- A 2x2 matrix to specify rotation,scaling,skewing
C	XCEN,YCEN- The coordinates of the Center of ARRAY
C	XT,YT	- The translation to add to the final image. The
C		  center of the output array is normally taken as
C		  (NXB-1)/2, (NYB-1)/2
C	SCALE	- A multiplicative scale actor for the intensities
C	
C	Xo = a11(Xi - Xc) + a12(Yi - Yc) + (NXB-1)/2. + XT
C	Yo = a21(Xi - Xc) + a22(Yi - Yc) + (NYB/-1)2. + YT
C
	SUBROUTINE INTERP1(ARRAY,BRAY,NXA,NYA,NXB,NYB,AMAT,
     .	XCEN,YCEN,XT,YT,SCALE)
	DIMENSION ARRAY(NXA,NYA),BRAY(NXB,NYB),AMAT(2,2)
C
C   Calc inverse transformation
C
	XC = (NXB-1)/2. + XT
	YC = (NYB-1)/2. + YT
	XCEN1 = XCEN + 1
	YCEN1 = YCEN + 1
	DENOM = AMAT(1,1)*AMAT(2,2) - AMAT(1,2)*AMAT(2,1)
	A11 =  AMAT(2,2)/DENOM
	A12 = -AMAT(1,2)/DENOM
	A21 = -AMAT(2,1)/DENOM
	A22 =  AMAT(1,1)/DENOM
C
C Loop over output image
C
	DO 200 IY = 1,NYB
	  DYO = IY - YC - 1
	  DO 100 IX = 1,NXB
	    DXO = IX - XC - 1
	    XP = A11*DXO + A12*DYO + XCEN1
	    YP = A21*DXO + A22*DYO + YCEN1
	    IXP = NINT(XP)
	    IYP = NINT(YP)
	    IF (IXP .LT. 1 .OR. IXP .GT. NXA) GOTO 100
	    IF (IYP .LT. 1 .OR. IYP .GT. NYA) GOTO 100
C
C   Do bilinear interpolation
C
	    DX = XP - IXP
	    DY = YP - IYP
	    DXBAR=1.-DX
	    DYBAR=1.-DY
	    IXPP1 = IXP + 1
	    IYPP1 = IYP + 1
	    IF (IXPP1 .GT. NXA) GO TO 100
	    IF (IYPP1 .GT. NYA) GO TO 100
C
         BRAY(IX,IY) = BRAY(IX,IY)+SCALE*(DXBAR*DYBAR*ARRAY(IXP,IYP)+
     .                                 DXBAR*DY*ARRAY(IXP,IYPP1)+
     .                                 DX*DYBAR*ARRAY(IXPP1,IYP)+
     .                                 DX*DY*ARRAY(IXPP1,IYPP1))
C
100	  CONTINUE
200	CONTINUE
C
	RETURN
	END
