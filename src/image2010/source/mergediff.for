C****MERGEDIFF -  FOR MERGING HIGH RESOLUTION ELECTRON DIFFRACTION
C                 DATA IN THREE DIMENSIONS.
C
C  FIRST VERSION OF MERGEDIFF EXTENSIVELY MODIFIED AND REWRITTEN, TAC 24.3.84
C	VX 1.0	24.3.84	TAC
C	VX 1.1  30.4.86	TAC
C	VX 1.2  15.4.92	RH	HKREV bug removed in PCF output.
C	VX 1.3  22.5.96	RH	array bounds in MA21 to allow check_bounds
C	VX 1.4 28.11.98	RH	debug of detwinning when MASK(3) is set to 0
C	VX 1.5  4.12.98	RH	option to subtract N0000 for duplicate films
C
C       VAX PROGRAM FOR ALL SEVENTEEN TWO-SIDED PLANE GROUPS.
C	ORIGINAL PROGRAM (SYMMETRY ETC.) S.D.FULLER 10.5.80
C	USED AFTER MUCH DEBUGGING IN EMBO COURSE 22.9.80
C	CONVERTED TO INTENSITIES AND S.D.'S ONLY (NO PHASES) RH 13.2.81
C	---- A USED TO STORE INTENSITY, P USED TO STORE DELTA-INTENSITY
C	NEW SORTING ROUTINE, SHLSRT, JMB1 14.7.82
C	TIDIED UP AND GENERALISED TO DO EVERYTHING NEEDED RH +JMB 7.9.82
C       L.S.CURVE FITTING AND S.D. CALCULATION JMB1 1982.
C       L.S.REFINEMENT OF TILTAXIS,TILTANGLE RH 14.9.82
C
C IMPORTANT NOTE ########################################
C            1. THIS PROGRAM DOES DIFFERENCE COEFFS AND TWIN DETERMINATION
C                IN P3.
C            2. THE MATRICES AND ONE STATEMENT IN ASYM HAVE BEEN CHANGED
C                SO THAT THE STANDARD ASYMMETRIC UNIT IN P4,P3,P6 SPACE
C                GROUPS HAS LATTICE LINES H,0 AND NOT 0,K PRESENT.
C
C
C   THIS VERSION IS SUPPOSED TO CARRY OUT ALL ACTIVITIES RELATED TO HANDLING
C   ELECTRON DIFFRACTION INTENSITY DATA FROM 3D (TILTED) E.D. PATTERNS IN
C   ANY TWO-DIMENSIONAL SPACE GROUP.  IT CAN :-
C       1.  READ IN AND PRODUCE A SORTED, MERGED LIST OF ROUGHLY SCALED DATA
C           FROM ANY NUMBER OF FILMS, EITHER INDIVIDUALLY AND/OR FROM A
C           PREVIOUSLY MERGED LIST.
C       2.  FIT CURVES TO THE MERGED DATA AND PLOT THE WHOLE THING LATTICE
C           LINE BY LINE, WITH THE POINTS,THE FITTED CURVE AND ITS S.D.
C           THIS OPTION IS CURRENTLY DONE BY SEPARATE PROGRAM 'SYNCFIT'
C           TAKING AS INPUT THE DATASET PRODUCED BY MERGHIGH ON UNIT 3
C       3.  PERFORM PROPER SCALING (S.F. AND TEMPF) AGAINST A PREVIOUSLY
C           CALCULATED SET OF CURVES.
C       4.  THE INPUT IS TYPICALLY EXPECTED TO COME FROM PICKTILT (E.D. PATT.
C           PROCESSING PROGRAM) AND OUTPUT TO GO STRAIGHT INTO THE NORMAL
C           CRYSTALLOGRAPHIC SYSTEM.
C
C#####################################################################
C
C                  CARD INPUT
C
C   0  NSER,TITLER (SER NO. AND TITLE FOR THIS JOB)          (I10,10A4)
C
C   1  LIST,SORT,NFILE3,NFILE4,IGUIDE,LSUMMARY (IO CONTROL)      (*)
C
C   2  LREF,LCURVES,BCURVES (REFERENCE DATA SPECS)	         (*)
C
C   3  SCFDET,TA_REFMT,DETWIN,MASK,LMODMSK (OPERATIONAL OPTIONS) (*)
C
C   4  ISPGRP,ALNG,BLNG,CLNG,WIDTH,ANG,RRESMAX (STR PARMS)       (*)
C
C   5  LCF,LMODSIG,ORESMIN,ORESMAX,
C                    AIMIN,AIMAX,DCUTOFF,LPRINT,CCRMAX           (*)
C   5a  TITLE IF LCF IS TRUE
C
C FOR EACH FILM TO BE PROCESSED...
C
C   (i)    IFILM, ISUBTRACT, TITLE                               (*)
C
C   (ii)   NIN,TAXA,TANGL				         (*)
C
C   (iii)  SCALE,LBISO,TFPAR,TFPERP,
C	    SGNXCH,ROT180,HKREV,WIN,RESIN		         (*)
C
C   (iv)   MASK(1),MASK(2),MASK(3),MASK(4),TP1,TP2,TP3,TP4       (*)
C
C        (     LFILM,TITLET                                   (I5,18A4)
C        (
C ON NIN (     IH,IK,I,DELI                                   (2I5,2I6)
C        (
C        (     IH>=900  ---> ENDS THE SET OF REFLECTIONS.
C
C    IFILM<0  ---> ENDS DATA INPUT
C
C##########################################################################
C
C
C                NSER   - SERIAL NUMBER AND TITLE OF THIS RUN, APPLIED TO
C                TITLER - OUTPUT ON UNITS 3,4,6, AND 8,10(WHEN WRITTEN).
C
C  FLAG MATRIX:
C		FLAG/ SCFDET  DETWIN TA_REFMT LCURVES  LREF
C
C	0		F	F	F	F	F
C	1		F	F	F	F	T
C	2		T	F	F	T	T
C	3		T	F	T	T	T
C	4		T	T	F	T	T
C	5		T	T	T	T	T
C
C	FLAGS: (T/F)
C               LIST    : Program lists each reflection on input
C                          otherwise only merged output on unit 3,4 is generated.
C               SORT    : If F, no sorting and suppression of all outputs.
C			   Note that this flag supercedes all others.
C               NFILE3  : Sorted reflections output to FOR003.
C		NFILE4	: Final list of combined reflections output to FOR004.
C		IGUIDE  : Add guide points to output where curves extend to
C			   higher resolution than data to be merged.
C		LSUMMARY: Produce a summary file of merged film data.
C
C		SCFDET	: Scale and temperature factor determination
C		TA_REFMT: Tilt axis/angle refinement
C		DETWIN	: Detwinning treatment (P3)
C		MASK	: Global mask for determining twin proportions
C		LMODMSK	: Alter mask if one twin type < 0.0
C
C		LREF	: Use reference data for comparisons
C		LCURVES	: Data read in from curve list vs merged list
C		BCURVES : Use binary curves as opposed to ASCII encoded ones
C
C                ISPGRP - NUMBER OF SPACE GROUP AS BELOW
C                ALNG   -  A AXIS IN ANGSTROMS
C                BLNG   -  B AXIS IN ANGSTROMS
C		 CLNG	-  C THICKNESS IN ANGSTROMS
C                WIDTH  -  WIDTH OF UNIT CELL (ZSTAR COMPARISONS FOR SCALING)
C                ANG    -  ANGLE BETWEEN A AND B - ONLY FOR P1
C		 RRESMAX-  MAXIMUM RESOLUTION CUTOFF FOR REFLECTION
C			     CONSIDERATION (Angstroms)
C
C		 LCF	- Produce pre-LCF file for difference Fourier on
C			  channel 8 (T/F)
C		 LMODSIG- Modify SIG values for output (increases lower limit)
C		 ORESMIN- Associated minumum resolution cutoff
C		 ORESMAX- Associated maximum resolution cutoff
C		 AIMIN	- Associated minimum intensity
C		 AIMAX	- Associated maximum intensity
C		 DCUTOFF- Difference amplitude cutoff
C		 LPRINT - Print Delta F's for low angle correl calc
C		 CCRMAX - Correlation calc max resolution limit
C
C	Then for each input film:
C
C                IFILM  - INTEGER FILM IDENTIFIER
C                ISUBTRACT - new option 4.12.98 to allow possibility of
C                            subtraction of N0000 from specified film numbers
C                            to avoid duplicated films being confused as one
C                            film in later steps of processing
C                TITLE  - DESCRIPTION OF FILM
C
C		 NIN	- UNIT NUMBER
C                TAXA   - ANGLE BETWEEN TILT AXIS AND A AXIS
C                         MEASURED IN DIRECTION OF A TO B BEING POSITIVE
C                         I.E. IF TAXA>0 THEN THE TILT AXIS IS CLOCKWISE
C                         FROM A.
C                TANGL  - TILT ANGLE IN DEGREES
C
C                SCALE  - MULTIPLIED BY AMPLITUDES BEFORE COMBINATION
C                         IF EQUAL TO 0 SCALING IS AUTOMATIC.
C		 LBISO	- (T/F) flag for iostropic temperature factors.
C                TFPAR  - Temperature factor to be applied to input data.
C                TFPERP - Temperature factor to be applied to input data.
C			   PAR is parallel to tilt axis, PERP is
C			   perpendicular.  If isotropic temp factor
C			   desired, only TFPAR used.
C                SGNXCH - IF T, FLIP AROUND A AXIS(T/F). USEFUL IN P121.
C                ROT180 - IF T, ROTATE 180 DEGREES ABOUT Z-AXIS(T/F).
C			  USEFUL IN P1,P3 -- ONLY APPROPRIATE FOR IMAGES.
C                HKREV  - IF T, EXCHANGE H AND K AXES(T/F). THIS IS A COSMETIC
C                         FEATURE TO FACILITATE INDEXING OF DIFFICULT TILTED
C                         FILMS. NOTE THAT TLTAXA AND TLTANG MUST BE CORRECT
C 			  W.R.T. THE ORIGINAL DEFINITIONS OF THE
C			  DIRECTIONS OF H AND K ON THE FILM.
C                WIN    - ZSTAR RANGE WITHIN WHICH SPOTS ARE COMPARED
C                         FOR SCALING
C
C		 MASK	- Four values for detwinning mask.
C		 BBIN	- Four values to specify twin proportions.
C
C	Then in each reflection file:
C
C                LFILM  - FILM NUMBER
C                TITLET - TITLE ON UNIT NIN (DATA). IF LFILM IS NOT EQUAL TO
C                         IFILM, THE SEARCH CONTINUES THROUGHOUT UNIT NIN.
C
C                IH     - H INDEX OF REFLECTION
C                IK     - K INDEX OF REFLECTION
C                I      - INTENSITY OF REFLECTION
C                DELI   - DELTA(S.D.) OF REFLECTION
C
C##########################################################################
C
C   INPUT IS MADE VIA
C	UNIT  1 - IF REQUESTED, PREVIOUSLY MERGED DATA LIST
C	UNIT NIN - NEW FILMS TO BE MERGED, NORMALLY UNIT 2
C	UNIT  5 - CONTROL DATA
C	UNIT  7 - PREVIOUSLY FITTED CURVES, STORED IN ASCII, IF REQUESTED
C	UNIT  9 - (logical unit BCURVES) PREVIOUSLY FITTED CURVES IN BINARY FORM
C
C   OUTPUT IS MADE VIA
C	UNIT  3 - MERGED DATA POINTS IF REQUESTED (H,K,Z,INTENS,DELTA,FILMN)
C	UNIT  4 - FINAL LIST OF COMBINED REFLECTIONS (FOR EXAMINATION)
C                       (SIMILAR TO UNIT 3, BUT CLEARER FORMAT)
C	UNIT  6 - LINEPRINTER
C	UNIT  8 - PRE-LCF FILE IF DESIRED
C	UNIT 10 - DELTA F VS S FORMATTED DATA, PRODUCED IF LCF=T
C	UNIT 11 - Summary of film data, produced if LCF=T
C	UNIT 12 - Summary of merged film data, produced if LSUMMARY=T
C
C##############################################################################
C
C     NUMBER   SPACEGROUP    ASYMMETRIC UNIT        REAL   IMAGINARY
C
C          1          P1         H>=0
C
C          2         P21         H,Z>=0              Z=0
C
C          3         P12         H,K>=0              K=0
C
C          4        P121         H,K>=0              K=0
C
C          5         C12         H,K>=0              K=0
C
C          6        P222         H,K,Z>=0            H=0
C                                                    K=0
C                                                    Z=0
C
C          7       P2221         H,K,Z>=0          (0,2N,Z)  (0,2N+1,Z)
C                                                    (H,K,0)
C                                                    (H,0,Z)
C
C          8      P22121         H,K,Z>=0            (H,K,0)
C                                                   (2N,0,Z)  (2N+1,0,Z)
C                                                   (0,2N,Z)  (0,2N+1,Z)
C
C          9        C222         H,K,Z>=0            (H,K,0)
C                                                    (H,0,Z)
C                                                    (0,K,Z)
C
C         10          P4         H,K,Z>=0            (H,K,0)
C
C         11        P422         H,K,Z>=0            (H,K,0)
C                                K>=H                (H,0,Z)
C                                                    (0,K,Z)
C                                                    (H,H,Z)
C
C         12       P4212         H,K,Z>=0            (H,K,0)
C                                K>=H                (H,H,Z)
C                                                   (2N,0,Z)   (2N+1,0,Z)
C                                                   (0,2N,Z)   (0,2N+1,Z)
C
C         13          P3         H,K>=0
C
C         14        P312         H,K>=0              (H,H,Z)
C                                K>=H
C
C         15        P321         H,K>=0              (H,0,Z)
C                                 K>H                (0,K,Z)
C
C         16          P6       H,K,Z>=0             (H,K,0)
C
C         17        P622         H,K,Z>=0            (H,K,0)
C                                K>=H                (H,H,Z)
C
C#################################################################
C
      INCLUDE 'merge.inc'
      REAL*4 KEY(MAXOBS) !SORT KEY
      REAL*4 TITLER(10)
C
C      ANGLE BETWEEN A AND B FOR ALL SPACEGROUPS BUT P1 AND P2.
C
      REAL*4 STANG(17)/0.0,11*90.0,5*120.0/
C
      PARAMETER (MAXTWINS=4)
      DIMENSION MASK(MAXTWINS),BBIN(MAXTWINS)
      REAL*8  BB(MAXTWINS)
C
      LOGICAL LIST,LCF,DETWIN,SCFDET,TA_REFMT,LCURVES,LREF,TA_DETD
      LOGICAL NO_DATA,SORT,BCURVES,NFILE3,NFILE4,IGUIDE,TPIN
      LOGICAL HKREV,LPRINT,LSUMMARY,LBISO,LMODSIG,LCYCLE,LMODMSK
C
      TDIF=0.
      TDIFS=0.
      TDIFW=0.
      TSUM=0.
      TSUMW=0.
      TSUMS=0.
      NNEGT=0
      NT=0
      NTSUMW=0
      NTSUMS=0
C
C        READ SPACE GROUP NUMBER, LIST PARAMETER, UNIT CELL AXES AND IF
C              SPACE GROUP IS P1 THE INTER AXIS ANGLE
C
      READ (5,120) NSER,TITLER
120   FORMAT(I10,10A4)
C
      READ (5,*) LIST,SORT,NFILE3,NFILE4,IGUIDE,LSUMMARY
C
      READ (5,*) LREF,LCURVES,BCURVES
C
      READ (5,*) SCFDET,TA_REFMT,DETWIN,MASK(1),
     $MASK(2),MASK(3),MASK(4),LMODMSK
C
      READ (5,*) ISPGRP,ALNG,BLNG,CLNG,WIDTH,ANG,RRESMAX
C
      READ (5,*) LCF,LMODSIG,ORESMIN,ORESMAX,AIMIN,AIMAX
     $          ,DCUTOFF,LPRINT,CCRMAX
C
      IF(LCF) READ(5,121) ITITLE
121   FORMAT(20A4)
C
      WRITE(6,110)
110   FORMAT(/'  MERGEDIFF - VX 1.5(4.12.98)',
     .	 ' Three dimensional intensity merging program '/)
C
      WRITE(6,106) NSER,TITLER
106   FORMAT(' Description of this run applied to output',I5,5X,10A4/)
C
      WRITE(6,101)LIST,SORT,NFILE3,NFILE4,IGUIDE,LSUMMARY,LCF,RRESMAX
101   FORMAT(' CONTROL DATA FOR THIS JOB AS READ IN'/
     1' LIST..........................................',L5/
     2' SORT..........................................',L5/
     3' NFILE3........................................',L5/
     4' NFILE4........................................',L5/
     5' IGUIDE........................................',L5/
     6' LSUMMARY......................................',L5/
     7' LCF...........................................',L5/
     8' RRESMAX.(Angstroms)...........................',F7.1/)
C
      WRITE(6,122) LREF
122   FORMAT(' Use reference data............................',L5)
      IF(LREF) THEN
      	WRITE(6,123) LCURVES,BCURVES
123     FORMAT(' Use previously fitted curves..................',L5/
     $         ' Use binary encoded curves.....................',L5)
      ENDIF
C
      WRITE(6,124) SCFDET,TA_REFMT
124   FORMAT(' Scale and temperature factor determination....',L5/
     $       ' Tilt axis/angle refinement....................',L5)
C
      IF(SCFDET) THEN
      	IF(.NOT.(LCURVES)) THEN
      	  WRITE(6,154)
154	  FORMAT(' Scale factor determination not possible',
     $	 ' under set conditions'/
     $	 ' Reference curves must be used')
      	  STOP
      	ENDIF
      ENDIF
C
      IF(DETWIN) THEN
      	IF(MASK(2).EQ.0.AND.
     $     MASK(3).EQ.0.AND.
     $     MASK(4).EQ.0) THEN
      	DETWIN=.FALSE.
        ENDIF
      ENDIF
C
      IF(.NOT.DETWIN) THEN   !MAKE SURE MASK IS CORRECT
      	MASK(1)=1
      	MASK(2)=0
      	MASK(3)=0
      	MASK(4)=0
      ENDIF
C
      IF(DETWIN) THEN
      	WRITE(6,125) DETWIN,(MASK(I), I=1,MAXTWINS),LMODMSK
125     FORMAT(' Detwin in p3; detwin mask.....................',L5,4I3/
     $         ' Modify mask if a twin < 0.....................',L5)
      ENDIF
C
      WRITE(6,115)ISPGRP
115   FORMAT(/'  TWO SIDED PLANE GROUP ',I3,//)
C
      IF(DETWIN.AND.ISPGRP.NE.13) THEN
      WRITE(6,103)
103   FORMAT(' Detwinning has only been implemented for space '
     $'group p3.')
      STOP
      ENDIF
C
      ABANG=STANG(ISPGRP)
      IF(ISPGRP.LE.2) ABANG=ANG
      IF(ISPGRP.GT.9) BLNG=ALNG
      WRITE(6,117)ALNG,BLNG,CLNG,WIDTH,ABANG
117   FORMAT('  CELLAXES:  A ',F7.2,'   B ',F7.2,'   C ',F7.1,
     $'   THICKNESS ',F7.1,
     $  '   AB ANGLE ',F7.2,' DEGREES.'/)
C
      ASTAR=1.0/(ALNG*SIN(DRAD*ABANG))
      BSTAR=1.0/(BLNG*SIN(DRAD*ABANG))
      WSTAR=1.0/WIDTH
      ABANG=180.-ABANG
      RESMAXF=1.0/(RRESMAX*RRESMAX)/4.0
C
      IF(LSUMMARY) THEN
      	WRITE(12,175) NSER,TITLER,ISPGRP,ALNG,BLNG,CLNG,RRESMAX
175	FORMAT(' Columns of data in summary file defined as:',17X,
     .	 'Input parameters defining this run:'//
     $'  1 - Film number',44X,
     .	 'Serial number.........................',I6/
     $'  2 - HKREV reverse h and k axes',29X,'Title:'/
     $'  3 - Number of reflections in input file',20X,3X,10A4/
     $'  4 - Number of reflections used in refinements'/
     $'  5 - Resolution limit of reflections used',19X,
     .	 'Space group number...................',I5/
     $'  6 - Friedel R factor after scaling',25X,
     .	 'Cell lengths A,B,C...................',3F8.2/
     $'  7 - Scale factor',43X,
     .	 'Limiting input resolution (A)........',F8.2)
      	WRITE(12,177) NFILE3,NFILE4,IGUIDE,SCFDET,TA_REFMT,
     $DETWIN,(MASK(I),I=1,MAXTWINS),LMODMSK
177     FORMAT('  8 - LBISO flag for isotropic B factors',21X,
     .	 'NFILE3,NFILE4,IGUIDE.................',3L3/
     $'  9 - Temperature factor (parallel to tilt axis)',13X,
     .	 'Scale factor determination...........',L3/
     $' 10 - Temperature factor (anisotropic perpendicular)',9X,
     .	 'Tilt axis refinement.................',L3/
     $' 11 - Merging R factor',39X,
     .	'p3 Twin detn, Mask, LMODMSK..........',L3,1X,4I2,L4/
     $' 12 - Tilt axis'/
     $' 13 - Tilt angle'/
     $' 14 - Twin proportions (four in total)'//
     $7X,'1',3X,'2',2X,'3',3X,'4',6X,'5',9X,'6',8X,'7',4X,'8',8X,'9',
     $8X,'10',7X,'11',6X,'12',7X,'13',5X,'------------14------------')
      ENDIF
C
      WRITE(6,129)
129   FORMAT(' *********************************************************
     1**************************************************************'//)
C
      NREWND=0
C
      ZMIN=0.0
      ZMAX=0.0
      AMAX=0.0
      JREFL=0
      NINOLD=0
C
      IF(LREF) THEN
C
C   INPUT OF PREVIOUSLY FITTED CURVES FOR USE IN PROPER S.F. AND TEMPF FITTING.
C
      IF(LCURVES) THEN
C      Can read either ASCII or binary curves
C
      	IF(BCURVES) THEN
      	  CALL RDBCURVES
      	ELSE
	  CALL RDCURVES
      	ENDIF
C
C  CHECK FOR CONSISTENT CELL PARAMETERS
C
      	IF((CELL(1).NE.ALNG).OR.(CELL(2).NE.BLNG)
     $.OR.(CELL(3).NE.CLNG)) THEN
      	  WRITE(6,130) (CELL(I),I=1,3)
130	  FORMAT(' Cell parameters from curves not consistent with',
     $' those in job deck. CELL(1->3)=',3F8.2)
      	  STOP
      	ENDIF
      ELSE !LCURVES
C
C   INPUT OF PREVIOUSLY MERGED DATA IN FORM OF LIST INCLUDING ZSTAR.
C
C3000  OPEN (UNIT=1, READONLY, STATUS='OLD')
3000  OPEN (UNIT=1, STATUS='OLD')
      READ(1,120) IRUN,TITLE
      WRITE(6,3001) IRUN,TITLE
3001  FORMAT(' OLD DATA READ IN FIRST',I10,10A4)
      DO 3010 I=1,MAXOBS+1
        READ(1,*,END=3020)JH(I),JK(I),ZSTAR(I),
     $	       RINT(I),DIFF(I),JFILM(I)
        IF(JH(I).EQ.100) GO TO 3020
        IF(ZSTAR(I).GT.ZMAX) ZMAX=ZSTAR(I)
        IF(ZSTAR(I).LT.ZMIN) ZMIN=ZSTAR(I)
        IF(RINT(I).GT.AMAX) AMAX=RINT(I)
3010  CONTINUE
      WRITE(6,149) MAXOBS
149   FORMAT(//' ***Error*** Total number of reflections > ',I8)
      STOP
C
3020  JREFL=I-1
      WRITE(6,3021) JREFL,ZMIN,ZMAX,AMAX
3021  FORMAT(I10,'  REFLECTION IN PREVIOUSLY MERGED LIST FROM UNIT 1'/
     1' ZMIN=',F10.6,'   ZMAX=',F10.6,'    AMAX=',F10.1)
      ENDIF !LCURVES
      ENDIF !LREF
C
C     READ INPUT DATA, ONE FILM AT A TIME.
C
      NO_DATA=.FALSE.
      NUMFILM=0
220   CALL READ_FILM(NO_DATA,ISPGRP,NUMFILM,IN,JREFL,LIST,RESMAXF,
     $WSTAR,ZMIN,ZMAX,XISUMCUR,XISUMPRE,NCOMP,SCALEIN,LBISO,
     $TFPAR,TFPERP,TAXA,TANGL,TPIN,BBIN,
     $HKREV,DETWIN,MASK,NINOLD,IFILM,RMAXIN,INRFLTOT,INLT0)
C
      IF(NO_DATA) GOTO 500
C
C  To avoid error in tilt angle refinement...
      IF(TA_REFMT.AND.(TANGL.EQ.0.0)) TANGL=1.0
C
C  Check for foolish use conditions.
C
      IF(SCALEIN.GT.0.0.AND.SCFDET) THEN
      	WRITE(6,151)
151	FORMAT(' *****Input scale factor overrides scale factor',
     $	 ' determination*****'/' SCFDET set to .FALSE.'/)
      	SCFDET=.FALSE.
      ENDIF
      IF(DETWIN) THEN
      	IF(.NOT.(SCFDET.AND.LCURVES)) THEN
      	  WRITE(6,152)
152	  FORMAT(' *****Twin determination not possible',
     $	 ' under set conditions*****'/
     $	 ' Reset SCFDET,LCURVES to .TRUE.'/)
      	  STOP
      	ENDIF
      ENDIF
      IF(TA_REFMT) THEN
      	IF(.NOT.(SCFDET.AND.LCURVES)) THEN
      	  WRITE(6,153)
153	  FORMAT(' *****Tilt axis refinement not possible',
     $	 ' under set conditions*****'/
     $	 ' Reset SCFDET,LCURVES to .TRUE.'/)
      	  STOP
      	ENDIF
      ENDIF
      IF(TPIN) THEN
      	IF(.NOT.(LCURVES)) THEN
      	  WRITE(6,155)
155	  FORMAT(' *****Detwinning not possible under set conditions*****'/
     $	 ' Reference curves must be used.'/)
      	  STOP
      	ENDIF
      ENDIF
C
C  If detwinning, then first time around do this...
C
C    Set twin proportions to .25 for each, then do scaling.
C
      IF(DETWIN) THEN
        DO 225 I=1,MAXTWINS
      	BB(I)=0.25
225     CONTINUE
C
C   Scale local data in local arrays
C	Regardless of selection, first do isotropic scaling
C
      	CALL SCALWTWN(IN,SCALE,TFPAR,TFPERP,DETWIN,BB,
     $	       RMAXIN,.TRUE.,TAXA,TANGL,TA_REFMT,WSTAR)
C   Apply on local data, put into global arrays
        CALL APPLY_SCALE_B(IN,JREFL,SCALE,.TRUE.,TFPAR,TFPERP,
     $	  LIST,TAXA,TANGL)
C
C  For tilt axis,angle refinement set twin proportions to 1,0,0,0
C
        IF(TA_REFMT) THEN
          BB(1)=1.0
      	  DO 230 I=2,MAXTWINS
      	    BB(I)=0.0
230	  CONTINUE
          CALL TILT(IN,JREFL,TAXA,TANGL,TA_DETD,DETWIN,BB,
     $	  ISPGRP,RFACT,WSTAR)
        ENDIF
      ENDIF
      IF(SCFDET) THEN
      	TA_DETD=.FALSE.
      	NCYCLE=0
      	LCYCLE=(.NOT.TA_DETD).AND.(NCYCLE.LT.4)
      	DO WHILE (LCYCLE)
      	  IF(DETWIN) THEN
      	    CALL DETTWIN(IN,JREFL,BB,MASK,DETWIN,TPIN)
      	    CALL RFACTOR(IN,JREFL,DETWIN,TPIN,BB,RFACT,TA_REFMT,WSTAR) !DO ALL 4 FOR NOW
      	  ELSE
      	    IF(TPIN) THEN
      	      DO 235 I=1,MAXTWINS
      	 BB(I)=BBIN(I)
235	      CONTINUE
      	    ELSE
      	      BB(1)=1.0
      	      BB(2)=0.0
      	      BB(3)=0.0
      	      BB(4)=0.0
      	    ENDIF
      	  ENDIF
C
C  FIND SCALE AND TEMPERATURE FACTORS VERSUS CURVES
C   FORMULA: ISCALED=IRAW*EXP(2.0*B*(SINTHETA/LAMBDA)**2)*SCALE
C
      	  CALL SCALWTWN(IN,SCALE,TFPAR,TFPERP,DETWIN.OR.TPIN,BB,
     $	  RMAXIN,LBISO,TAXA,TANGL,TA_REFMT,WSTAR)
          CALL APPLY_SCALE_B(IN,JREFL,SCALE,LBISO,TFPAR,TFPERP,
     $	   LIST,TAXA,TANGL)
          IF(TA_REFMT) THEN
      	    CALL TILT(IN,JREFL,TAXA,TANGL,TA_DETD,DETWIN.OR.TPIN,BB,
     $	  ISPGRP,RFACT,WSTAR)
      	  ELSE
      	    TA_DETD=.TRUE.
      	    CALL DETTWIN(IN,JREFL,BB,MASK,DETWIN,TPIN)
      	    CALL RFACTOR(IN,JREFL,DETWIN,TPIN,BB,RFACT,TA_REFMT,WSTAR) !DO ALL 4 FOR NOW
      	  ENDIF
C
      	  NCYCLE=NCYCLE+1
C      	  LCYCLE=(.NOT.TA_DETD).AND.(NCYCLE.LT.4)
      	  LCYCLE=(NCYCLE.LT.4)
C
C  REMOVE TWIN PROPORTIONS THAT ARE < 0 IF NECESSARY
      	  IF(DETWIN.AND.LMODMSK) THEN
      	    DO I=2,MAXTWINS
      	      IF(BB(I).LT.0.0) THEN
      	        MASK(I)=0
      	        WRITE(6,237) I
237	        FORMAT(' ***Twin proportion',I3,' set to zero.')
      	 LCYCLE=.TRUE.
      	      ENDIF
            END DO
      	  ENDIF    !(DETWIN.AND.LMODMSK)
C
      	END DO
C
      ELSE
C
      IF(TPIN) THEN
      DO 236 I=1,MAXTWINS
   	BB(I)=BBIN(I)
236   CONTINUE
      ENDIF
C
      IF(SCALEIN.GT.0.0001) THEN
      	SCALE=SCALEIN
      ELSE
      	IF(XISUMCUR.GT.0.0) THEN
      	  SCALE=XISUMPRE/XISUMCUR
      	  WRITE(6,170) SCALE,XISUMPRE,XISUMCUR,NCOMP
170	  FORMAT('   Scale factor vs. data read in so far =',
     1	   F10.5,10X,2F10.2,I10)
      	  TFPAR=0.0
      	  TFPERP=0.0
      	ELSE
      	  SCALE=1.0
      	  TFPAR=0.0
      	  TFPERP=0.0
      	ENDIF
      	LBISO=.TRUE.
      ENDIF
C
C  APPLICATION OF SCALE FACTOR,DONE IN ALL CASES, EVEN IF SCALE=1.,TEMPF=0.
C
      CALL APPLY_SCALE_B(IN,JREFL,SCALE,LBISO,TFPAR,TFPERP,LIST,
     $	  TAXA,TANGL)
      ENDIF
C
      JREFL=JREFL+IN
C
      IF(HKREV) THEN
      	TAXAONFILM=-TAXA-ABANG
      	TANGLONFILM=-TANGL
        WRITE(6,171) TAXAONFILM,TANGLONFILM
171	FORMAT(' TAXA, TANGL as on film =',2F10.3/)
      ELSE
      	TAXAONFILM=TAXA
      	TANGLONFILM=TANGL
      ENDIF
C
C NOW SOME STATISTICS ON EACH FILM AS IT IS INPUT (FILM BY FILM)
C
      CALL FRIEDELR(IN,JREFL,NRSUMS,RSUMS,RDIFS,
     $	       NRSUMW,RSUMW,RDIFW,NNEG,RFRIED)
C
C SUM ALL THIS STUFF
C
      NNEGT=NNEGT+NNEG
      TSUMS=TSUMS+RSUMS
      TDIFS=TDIFS+RDIFS
      TSUMW=TSUMW+RSUMW
      TDIFW=TDIFW+RDIFW
      NTSUMS=NTSUMS+NRSUMS
      NTSUMW=NTSUMW+NRSUMW
C
C Write out useful film characteristics if LCF is T
C
      RMAX=SQRT(1.0/(RMAXIN*4.0))
      IF(LCF) THEN
      	WRITE(11,172) IFILM,TITLE
172	FORMAT(/////,I10,5X,10A4/)
      	WRITE(11,173) HKREV,INRFLTOT,IN,INLT0,RMAX,SCALE,LBISO,
     $  TFPAR,TFPERP,RFACT,(BB(I),I=1,4),RFRIED,TAXAONFILM,TANGLONFILM
173	FORMAT( ' HKREV............................',L4/
     $	 ' Number of reflections.(in file)..',I4/
     $	 ' Number of reflections.(used).....',I4/
     $	 ' Number of reflections used < 0...',I4/
     $	 ' Resolution of input data.........',F8.2/
     $	 ' Scale factor.....................',F10.4/
     $	 ' Isotropic temperature factors....',L4/
     $	 ' Temperature factor.(par/perp)....',2F10.2/
     $	 ' Merging R factor.................',F10.4/
     $	 ' Twin proportions.................',4F8.4/
     $	 ' Friedel R factor.................',F10.4/
     $	 ' Tilt axis, angle.................',2F10.4)
      ENDIF
C
      IF(LSUMMARY) THEN
      	WRITE(12,174) NUMFILM,IFILM,HKREV,INRFLTOT,IN,RMAX,RFRIED,
     $   SCALE,LBISO,TFPAR,TFPERP,RFACT,TAXAONFILM,TANGLONFILM,
     $	 (BB(I),I=1,4)
174	FORMAT(I4,I6,L2,I4,I4,F8.2,F10.4,
     $	 F10.4,L2,2F10.2,F10.4,F8.2,F8.2,4F8.4)
      ENDIF
C
C Modify intensities (and deltas) to reflect desired output,
C  ie apply detwinning
C
C Also here put option for creating file of difference F's via
C  subroutine CRDIFS
C
      IF(.NOT.LCF) THEN
        IF(DETWIN.OR.TPIN) THEN
      	  IF(LREF.AND.LCURVES) THEN
      	    CALL MODINT(IN,JREFL,DETWIN.OR.TPIN,BB)
      	  ELSE
            WRITE(6,503)
503	    FORMAT(' ERROR - cannot do detwinning operations without',
     $	 ' reference curve data.')
      	    SORT=.FALSE.
      	  ENDIF
      	ENDIF
      ELSE
      	IF(NFILE3.OR.NFILE4) CALL CRDIFS(IN,JREFL,DETWIN,BB,
     $	   TA_REFMT,WSTAR)
      ENDIF
C
      GO TO 220
C
C NO MORE DATA TO ENTER, THEN
C
500   WRITE(6,129) !A BUNCH OF ASTERISKS
      IF(.NOT.SORT) THEN
        WRITE(6,502) SORT
502     FORMAT(' PROGRAM OUTPUT ABORTED, SORT=',L5)
        STOP
      ENDIF
C
      IF(NFILE3.OR.NFILE4) WRITE(6,180)
180   FORMAT(' Output generated, for scaled intensities and deltas. ')
C
      IF(NFILE3) THEN
        WRITE(3,120) NSER,TITLER
      	WRITE(6,181)
181	FORMAT('   Output list of intensities, deltas to unit FOR003.')
      ENDIF
C
      IF(NFILE4) THEN
        WRITE(4,120) NSER,TITLER
        WRITE(4,180)
      	WRITE(6,182)
182	FORMAT('   Output list of intensities, deltas to unit FOR004.')
      ENDIF
C
C        REFLECTIONS ARE SORTED INTO ASCENDING ORDER IN H,K AND ZSTAR
C
      INCDEC=1
      DO 510 J=1,JREFL
510   KEY(J)=100*JH(J)+JK(J)+ZSTAR(J)
      CALL SHLSRT(KEY,JREFL,JOUT,INCDEC)
C
      IF(LCF.AND.(NUMFILM.EQ.1)) THEN
      	WRITE(6,183)
183	FORMAT('  Output files:  a)  Pre-LCF file on channel 8'/
     $	       '                     (for entry into program EDLCF)'/
     $	       '                 b)  Delta F vs. s on channel 10'/)
      	IF(CCRMAX.EQ.0.0) THEN
      	  STHMAX=RMAXIN
      	ELSE
      	  STHMAX=1.0/(CCRMAX*CCRMAX)/4.0
      	ENDIF
      	CALL IOPRELCF(JREFL,DETWIN,BB,ORESMIN,ORESMAX,LMODSIG,IFILM,
     $	AIMIN,AIMAX,DCUTOFF,TAXA,TANGL,STHMAX,LPRINT,TA_REFMT,WSTAR)
      ENDIF
C
C IF NO OUTPUT DESIRED ON CHANNEL 3 OR 4 THEN STOP
C
      IF(NFILE3.OR.NFILE4) THEN
      	IF(IGUIDE) WRITE(6,184)
184	FORMAT(' Guide points added where curves extend beyond data.'/)
      	IF(NFILE3) THEN
      	  IFILE=3
      	ELSE
      	  IFILE=4
      	ENDIF
      	CALL OUT3OR4(JREFL,IFILE,IGUIDE,TA_REFMT,WSTAR,LSUMMARY)
        TDIF=TDIFW+TDIFS
        TSUM=TSUMW+TSUMS
        TOUT=TDIF/TSUM
        TOUTS=TDIFS/TSUMS
        TOUTW=TDIFW/TSUMW
        NT=NTSUMS+NTSUMW
        WRITE (6,1257)  NNEGT,JREFL
1257    FORMAT (//I5,' spots out of a total of',I8,' are negative.')
        WRITE (6,1258) NTSUMW,TOUTW
1258    FORMAT (' Friedel R-factor for',I8,' weak spots=   ',F6.4)
        WRITE (6,1259) NTSUMS,TOUTS
1259    FORMAT (' Friedel R-factor for',I8,' strong spots= ',F6.4)
        WRITE (6,1260) JREFL,TOUT
1260    FORMAT (' Friedel R-factor for all',I8,' spots=    ',F6.4/)
      ENDIF
C
      IF(LSUMMARY) WRITE(6,176)
176   FORMAT(' Summary of film data output on channel 12')
      STOP
      END
C*** commented out by JMS 29.03.2000      INCLUDE 'mergesubs.for'
