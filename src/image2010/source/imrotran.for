C
C*********************************************************************
C       IMROT
C       program to compare two 2-D density maps in IMAGE format by
C       rotating/ translating one relative to the other. Finally
C       the program averages the rotated / translated map with the
C       reference map and creates a new IMAGE file
C       A positive rotation angle will rotate the reference map
C       (map 2) clockwise wrt the reference map (map 1).
C       A +ve translation  in x will translate the relative map to
C       the right and in y vertically upwards.
C
C       Version 1.00    24-DEC-1985        JMS
C*********************************************************************
C
        PARAMETER (MAP_S=65536)
        DIMENSION REFMAP(MAP_S), RELMAP(MAP_S), WORKMAP(MAP_S),
     2            NXYZF(3),MXYZF(3),NXYZL(3),MXYZL(3),TITLE(20)
        CHARACTER*1 YES
        CHARACTER*40 FILREF,FILREL,FILOUT
        CHARACTER DAT*24
        EQUIVALENCE (NXREF,NXYZF),(NXREL,NXYZL)
        COMMON//NXREF,NYREF,NZREF,NXREL,NYREL,NZREL
CTSH++
	CHARACTER TMPTITLE*80
	EQUIVALENCE (TMPTITLE,TITLE)
CTSH--
        DATA NREF,NREL,NOUT/1,2,3/,YES/'Y'/
C
        CALL  FDATE(DAT)
C
CTSH        TYPE *,' '
CTSH        TYPE *,' ***************************************************'
CTSH        TYPE *,' '
CTSH        TYPE *,' IMROT v 1.00 - 2D rotation / translation program'
CTSH        TYPE *,' '
CTSH        TYPE *,' ***************************************************'
CTSH        TYPE *,' '
CTSH        TYPE *,' A positive rotation angle will rotate the reference'
CTSH        TYPE *,' map (map 2) clockwise wrt the reference map (map 1).'
CTSH        TYPE *,' A +ve translation  in x will translate the relative '
CTSH        TYPE *,' map to the right and in y vertically upwards.'
CTSH        TYPE *,' '
CTSH        TYPE *,' Type input filename for reference map'
CTSH++
        WRITE(6,*) ' '
        WRITE(6,*)
     *  ' ***************************************************'
        WRITE(6,*) ' '
        WRITE(6,*)
     *  ' IMROT v 1.00 - 2D rotation / translation program'
        WRITE(6,*) ' '
        WRITE(6,*)
     *  ' ***************************************************'
        WRITE(6,*) ' '
        WRITE(6,*)
     *  ' A positive rotation angle will rotate the reference'
        WRITE(6,*)
     *  ' map (map 2) clockwise wrt the reference map (map 1).'
        WRITE(6,*)
     *  ' A +ve translation  in x will translate the relative '
        WRITE(6,*)
     *  ' map to the right and in y vertically upwards.'
        WRITE(6,*) ' '
        WRITE(6,*)
     *  ' Type input filename for reference map'
CTSH--
        READ(5,10) FILREF
   10   FORMAT(A)
        WRITE(6,*) ' Type input filename for relative map'
        READ(5,10) FILREL
        WRITE(6,*) ' Type output filename for averaged map'
        READ(5,10) FILOUT
C
C       open IMAGE format files
C
        CALL  IMOPEN(NREF,FILREF,'RO')
        CALL  IMOPEN(NREL,FILREL,'RO')
        CALL  IMOPEN(NOUT,FILOUT,'NEW')
C
C       read header and section for reference map
C
        CALL  IRDHDR(NREF,NXYZF,MXYZF,MODEF,DMIN,DMAX,DMEAN)
        NPTS = NXF * NYF
C
C       read header, section for map to be rotated/translated
C
        CALL  IRDHDR(NREL,NXYZL,MXYZL,MODEL,DMIN,DMAX,DMEAN)
        NPTS = NXL * NYL
C
C       write header for output map
C
        CALL  ITRHDR(NOUT,NREF)
CTSH        ENCODE(80,30,TITLE) DAT(5:24)
CTSH++
        WRITE(TMPTITLE,30) DAT(5:24)
CTSH--
   30   FORMAT(' IMROT V1.0 rotation/translation averaging program ',
     1         A20)
        DMIN = 0.
        DMAX = 0.
        DMEAN = 0.
        CALL  IWRHDR(NOUT,TITLE,1,DMIN,DMAX,DMEAN)
C
        XCREF = FLOAT(NXREF) * 0.5
        YCREF = FLOAT(NYREF) * 0.5
        XCREL = FLOAT(NXREL) * 0.5
        YCREL = FLOAT(NYREL) * 0.5
C
C        read in part sections ready for rotation / translation
C
        NX1 = INT(XCREF - FLOAT(NXREF) * 0.5)
        NX2 = NX1 + NXREF - 1
        NY1 = INT(YCREF - FLOAT(NYREF) * 0.5)
        NY2 = NY1 + NYREF - 1
C
        CALL  IMPOSN(NREF,0,0)
        CALL  IRDPAS(NREF,REFMAP,NXREF,NYREF,NX1,NX2,NY1,NY2)
C
        NX1 = INT(XCREL - FLOAT(NXREL) * 0.5)
        NX2 = NX1 + NXREL - 1
        NY1 = INT(YCREL - FLOAT(NYREL) * 0.5)
        NY2 = NY1 + NYREL - 1
C
        CALL  IMPOSN(NREL,0,0)
        CALL  IRDPAS(NREL,RELMAP,NXREL,NYREL,NX1,NX2,NY1,NY2)
C
C       type in information about rotations / translations
C
        WRITE(6,*) ' Type in start, finish and
     *  step size for angles in degrees'
        READ(5,*) ANG1,ANG2,ANGSTEP
C
        WRITE(6,*) ' Type in start,finish and step size for',
     1   ' translations in -ontal direction'
        READ(5,*) TRANX1, TRANX2, TRANXSTEP
C
        WRITE(6,*) ' Type in start,finish and step size for',
     1   ' translations in vertical direction'
        READ(5,*) TRANY1, TRANY2, TRANYSTEP
C
        WRITE(6,*)
     *  ' Type in maximum radius for correlation in pixels'
        READ(5,*) RMAX
C
        IF(ANGSTEP.NE.0.) THEN
         NROT = NINT((ANG2 - ANG1) / ANGSTEP) + 1
        ELSE
         NROT = 1
        END IF
C
        IF(TRANXSTEP.NE.0.) THEN
         NTRANX = NINT((TRANX2 - TRANX1) / TRANXSTEP) + 1
        ELSE
         NTRANX = 1
        END IF
C
        IF(TRANYSTEP.NE.0.) THEN
         NTRANY = NINT((TRANY2 - TRANY1) / TRANYSTEP) + 1
        ELSE
         NTRANY = 1
        END IF
C
        NDIM = (NXREL + NTRANX) * (NYREL + NTRANY)
        IF(NDIM.GT.MAP_S) THEN
         WRITE(6,*) ' Map dimension too large for program'
         STOP
        END IF
C
C       commence loop for main part of crunching
C
        TRANY = TRANY1
        CORRMAX = 0.
C
        DO NTY = 1,NTRANY
         TRANX = TRANX1
C
         DO NTX = 1,NTRANX
          ROT = ANG1
C
          DO NR = 1,NROT
           CALL  ROTRAN(ROT,TRANX,TRANY,XCREL,YCREL,XCREF,YCREF,
     1                  NXREL,NYREL,NXREF,NYREF,RELMAP,WORKMAP)
           CALL  CORRELL(REFMAP,WORKMAP,NXREF,NYREF,RMAX,XCREF,YCREF,
     1                   CORRCOEF)
c           WRITE(6,40) ROT,TRANX,TRANY,CORRCOEF
   40      FORMAT(' Rotation = ',F5.1,
     1            ' translations in -ontal,vertical directions =',2F7.1,
     2            ' correlation coef = ',F5.1)
C
           IF(CORRCOEF.GT.CORRMAX) THEN
            CORRMAX = CORRCOEF
            FXTRANS = TRANX
            FYTRANS = TRANY
            FANG = ROT
           END IF
C
           ROT = ROT + ANGSTEP
          END DO
C
          TRANX = TRANX + TRANXSTEP
         END DO
C
         TRANY = TRANY + TRANYSTEP
        END DO
C
C       rotate into final map
C
        CALL  ROTRAN(FANG,FXTRANS,FYTRANS,XCREL,YCREL,XCREF,YCREF,
     1               NXREL,NYREL,NXREF,NYREF,RELMAP,WORKMAP)
C
C       average with reference map
C
        CALL  AVERAGE(NXREF,NYREF,DMIN,DMAX,DMEAN,
     1                WORKMAP,REFMAP,RELMAP)
        CALL  IWRHDR(NOUT,TITLE,-1,DMIN,DMAX,DMEAN)
        CALL  IWRSEC(NOUT,RELMAP)
        WRITE(6,60) CORRMAX,FXTRANS,FYTRANS,FANG
   60   FORMAT(/' Final correlation coefficient =',F10.3/
     2          ' Horizontal translation        =',F10.3/
     1          ' Vertical translation          =',F10.3/
     3          ' Rotation                      =',F10.3)
        CALL  IMCLOSE(NREF)
        CALL  IMCLOSE(NREL)
        CALL  IMCLOSE(NOUT)
        END
C
C***************************************************************************
C
        SUBROUTINE ROTRAN(THETA,TRANX,TRANY,XCEN1,YCEN1,XCEN2,YCEN2,
     1                    NX1,NY1,NX2,NY2,ARRAY1,ARRAY2)
C
C        subroutine to rotate/translate ARRAY1, interpolating the
C        densities & writing them into ARRAY2
C
        DIMENSION ARRAY1(NX1,NY1), ARRAY2(NX2,NY2)
        DATA CNV/ .017453291/
C
C       initialize output array
C
        DO IY=1,NY2
         DO IX=1,NX2
          ARRAY2(IX,IY) = 0.
         END DO
        END DO
C
        TH = THETA * CNV
        SINTH = SIN(TH)
        COSTH = COS(TH)
        XSHIFT = XCEN1 - TRANX + 1.0! add in relative map centre
        YSHIFT = YCEN1 - TRANY + 1.0! and 1.0 to increase origin
C
C       start main loop for rotating/translating
C
        DO IY = 1,NY2
         YDIST = FLOAT(IY-1) - YCEN2
         YSINTH = YDIST * SINTH
         YCOSTH = YDIST * COSTH
C
         DO 100 IX = 1,NX2
          XDIST = FLOAT(IX-1) - XCEN2
          XSINTH = XDIST * SINTH
          XCOSTH = XDIST * COSTH
C
C         calculate X,Y coords in relative map
C
          XCOORD = XCOSTH - YSINTH + XSHIFT
          YCOORD = XSINTH + YCOSTH + YSHIFT
C
C         now interpolate to extract density at exact point
C
          KX = INT(XCOORD)
          IF(KX.LT.1) GO TO 100
          IF(KX.GE.NX1) GO TO 100
C
          KY = INT(YCOORD)
          IF(KY.LT.1) GO TO 100
          IF(KY.GE.NY1) GO TO 100
C
          XBIT = XCOORD - FLOAT(KX)
          YBIT = YCOORD - FLOAT(KY)
          XBAR = 1. - XBIT
          YBAR = 1. - YBIT
C
          DEN1 = ARRAY1(KX,KY)
          DEN2 = ARRAY1(KX+1,KY)
          DEN3 = ARRAY1(KX,KY+1)
          DEN4 = ARRAY1(KX+1,KY+1)
          ARRAY2(IX,IY) = YBAR * (XBAR * DEN1 + XBIT * DEN2)
     1                  + YBIT * (XBAR * DEN3 + XBIT * DEN4)
  100    CONTINUE
        END DO
C
        RETURN
        END
C
C*******************************************************************
C
        SUBROUTINE AVERAGE(NX,NY,DMIN,DMAX,DMEAN,
     1                     ARRAY1,ARRAY2,ARRAY3)
C
C       subroutine to average two arrays the same size, already
C       aligned
C
        DIMENSION ARRAY1(NX,NY), ARRAY2(NX,NY), ARRAY3(NX,NY)
C
        DMIN = 1000000.
        DMAX = -DMIN
        DTOT = 0.
C
        DO IY=1,NY
         DO IX=1,NX
          DEN = (ARRAY1(IX,IY) + ARRAY2(IX,IY)) * 0.5
          DMIN = AMIN1(DMIN,DEN)
          DMAX = AMAX1(DMAX,DEN)
          DTOT = DTOT + DEN
          ARRAY3(IX,IY) = DEN
         END DO
        END DO
        DMEAN = DTOT / FLOAT(NX * NY)
        RETURN
        END
C
C*******************************************************************
C
	SUBROUTINE CORRELL(ARRAY1,ARRAY2,NX,NY,RMAX,
     1	 XCEN,YCEN,CORRCOEF)
C
C       subroutine to calculate correlation coefficient between
C       two map sections
C
	DIMENSION ARRAY1(NX,NY),ARRAY2(NX,NY)
C
C	Initialize accumulators.  P1 refers to the map section, P2 refers to
C	the reference section.
C
	  N = 0
	  SP1P2 = 0.0
	  SP1 = 0.0
	  SP2 = 0.0
	  SP12 = 0.0
	  SP22 = 0.0
C
	  DO IY = 1 , NY
	    RY2 = ( ( IY - 1 ) - YCEN)**2
	    DO IX = 1 , NX
	      R = SQRT( ( ( IX - 1 ) - XCEN )**2 + RY2 )
	      IF ( R .LE. RMAX ) THEN
		N = N + 1
		SP1P2 = SP1P2 + ARRAY1(IX,IY) * ARRAY2(IX,IY)
		SP1 = SP1 + ARRAY2(IX,IY)
		SP2 = SP2 + ARRAY1(IX,IY)
		SP12 = SP12 + ARRAY2(IX,IY)**2
		SP22 = SP22 + ARRAY1(IX,IY)**2
C	      ELSE
C		ARRAY1(IX,IY) = 0.0
	      END IF
	    END DO
	  END DO
C
C	Calculate correlation coefficient
C
	  ANUMERATOR = N * SP1P2 - SP1*SP2
	  DENOMINATOR = SQRT( N * SP12 - SP1**2 ) * SQRT( N * SP22 - SP2**2 )
	  CORRCOEF = ANUMERATOR / DENOMINATOR * 100.0
C
	RETURN
	END

