C***	bandpass - program to bandpass fourier filter stack of images
C***		   in MRC image format.
C***
C*** input :
C***    1.    (i5) iplot - 0  filters images
C***                       1  produces postscript file of filter shape
C***
C***	   1a)   if iplot = 0 : (a) Input file name
C***
C***    2.    (a) Output file name
C***
C***	3.    (*) radmin, radmax - minimum, maximum filter radii in
C***                             range 0.0 - 0.5
C***
C***    4.    (i5) - filter type :
C***                              1 = Gauss
C***                              2 = Cauchy
C***                              3 = Cosine bell
C***                              4 = Sigmoid
C***
C***    5.    (f10.5) - Peak halfwidth in the same units as the filter radii
C***                    (Default value .025)
C***
C***
C***       5a)   if filter type = 2 (Cauchy) : (i5) Shape parameter
C***                          (range 1 - 5) to set the curve shape. In theory,
C***                          1 has the shallower slope, 5 falls faster, but
C***                          both are affected by the peak width, which may
C***                          reverse this. It is best to experiment to find
C***                          the most suitable shape and cutoff.
C***
C***	6.    (i5) norm - 0 do not normalize
C***			  1 normalize by setting mean to 0 and scaling
C***		            data to +/- standard deviation.
C***			
C***
C***	Version 2.0  17.01.2002 jms
C***	Version 2.01 27.03.2002 jms corrected so that low frequency data
C***				    can be left in.
C***	Version 2.02 03.10.2002 jms density scaling between 0-255
C***				    only for byte images with densities > 255
C***				    minimum subtraction for all densities
C***	Version 2.03 26.10.2006 jms corrections to formats for LINUX
C***	Version 2.1  27.09.2007 jms addition of sigmoid function at cutoffs
C***                                and replacement of scale parameter with
C***				    peak halfwidth + addition of normalization
C***				    option
C***
	character filter*8
C***
	integer*4 nxyz(3)
	integer*4 mxyz(3)
C***
	include 'bandpass.inc'
C***
	write(6,'(''Bandpass - Fourier filtering program V2.1 27.09.2007'')')
	write(6,'(''Filter images ........... <cr> ''/
     *            ''Plot filter curves ......  1'')')
	read(5,'(i5)') iplot
	if(iplot .le. 0) then
	 write(6,'(''Input file name ?'')')
	 read(5,'(a)') filin
	 write(6,'(''Output file name ?'')')
	 read(5,'(a)') filout
	else
C*** curvy output file ?
	 write(6,'(''Type curvy output file name ... '')')
	 read(5,'(a)') filout
	 open(unit=idevout,file=filout,status='unknown')
	 nxtrans = 130
	 ny = 128
	end if
	write(6,'(''Enter minimum, maximum filter radii ...'')')
	read(5,*) radmin, radmax
	write(6,'(''Gaussian filter .............. 1'')')
	write(6,'(''Cauchy filter ................ 2'')')
	write(6,'(''Cosine bell filter ........... 3'')')
	write(6,'(''Sigmoid filter ............... 4'')')
	read(5,*) ifilter
C*** Gauss filter
	if(ifilter .eq. 1) then
	 write(6,'(
     *   ''Type in peak halfwidth <cr> default = '',f5.3)')
     *   peakdefault
	 read(5,'(f10.5)') peakwidth
	 if(peakwidth .eq. 0.0) peakwidth = peakdefault
	 if(iplot .eq. 1) then
	  write(title,'(''T=Gaussian, peak halfwidth='',f5.2)') peakwidth
	  write(idevout,'(a)') title
	  call plotgauss(nxtrans,ny,radmin,radmax,peakwidth)
	  stop
	 end if
C*** Cauchy filter
	else if(ifilter .eq. 2) then
	 write(6,'(
     *   ''Type in peak halfwidth <cr> default = '',f5.3)')
     *   peakdefault
	 read(5,'(f10.5)') peakwidth
	 if(peakwidth .eq. 0.0) peakwidth = peakdefault
	 write(6,'(''Type in shape parameter, <cr> default ='',i2)')
     *   nint(shapedefault)
	 read(5,'(i5)') ishape
	 if(ishape .eq. 0) then
	  shape = shapedefault
	 else
	  shape = max(shapemin,min(shapemax,float(ishape)))
	 end if
	 if(iplot .eq. 1) then
	  write(title,'(''T=Cauchy, peak halfwidth ='',f5.2,
     *    '' shape ='',i2)') peakwidth,nint(shape)
	  write(idevout,'(a)') title
	  call plotcauchy(nxtrans,ny,radmin,radmax,shape,peakwidth)
	  stop
	 end if
C*** Cosine bell filter
	else if(ifilter .eq. 3) then
	 write(6,'(
     *   ''Type in peak halfwidth <cr> default = '',f5.3)')
     *   peakdefault
	 read(5,'(f10.5)') peakwidth
	 if(peakwidth .eq. 0.0) peakwidth = peakdefault
	 if(iplot .eq. 1) then
	  write(title,'(''T=Cosine, peak halfwidth='',f5.3)') peakwidth
	  write(idevout,'(a)') title
C*** multiply cosine width to compare with other curve shapes
	  call plotcosine(nxtrans,ny,radmin,radmax,peakwidth)
	  stop
	 end if
C*** Sigmoid filter
	else if(ifilter .eq. 4) then
	 write(6,'(
     *   ''Type in peak halfwidth <cr> default = '',f5.3)')
     *   peakdefault
	 read(5,'(f10.5)') peakwidth
	 if(peakwidth .eq. 0.0) peakwidth = peakdefault
	 if(iplot .eq. 1) then
	  write(title,'(''T=Sigmoid, peak halfwidth='',f5.3)') peakwidth
	  write(idevout,'(a)') title
C*** multiply cosine width to compare with other curve shapes
	  call plotsigmoid(nxtrans,ny,radmin,radmax,peakwidth)
	  stop
	 end if
	end if
C*********************************************************
C*** Normalize ?
C*********************************************************
	write(6,'('' Do not normalize ............... 0''/
     *            '' Do normalize ....................1'')')
	read(5,*) norm
	call imopen(idevin,filin,'ro')
	call irdhdr(idevin,nxyz,mxyz,mode,dmin,dmax,dmean)
	call imopen(idevout,filout,'new')
	call itrhdr(idevout,idevin)
	modereal = 2
	if(norm .eq. 1) call ialmod(idevout,modereal)
	nx = nxyz(1)
	ny = nxyz(2)
	nz = nxyz(3)
	if(nx .ne. ny) then
	 write(6,'(''Error - square boxes only.'')')
	 stop
	else if (nx .gt. maxsize) then
	 write(6,'(''Error - box size too large for program.'')')
	 stop
	end if
	nxtrans = nx + 2
C****************************************************************
C*** loop over sections
C****************************************************************
	amean = 0.
	amin = 1000000.
	amax = -amin
	do iz=1,nz
	 do ixy=1,nxtrans * ny
	  mapbuf(ixy) = 0
	 end do
	 bmin = 1000000.
	 bmax = -amin
C*** read the image
	 do iy=1,ny
	  ixy = (iy - 1) * nxtrans + 1
	  call irdlin(idevin,mapbuf(ixy))
	  den = mapbuf(ixy)
	  bmin = min(bmin,den)
	  bmax = max(bmax,den)
	 end do
C*** section read in, calculate forward fft
	 call todfft(mapbuf,nx,ny,0)
C*** bandpass filter
	 if(ifilter .eq. 1) then
	  call filtgauss(nxtrans,ny,radmin,radmax,peakwidth,mapbuf)
	  filter = 'Gaussian'
	 else if(ifilter .eq. 2) then
	  call filtcauchy(nxtrans,ny,radmin,radmax,shape,
     *    peakwidth,mapbuf)
	  filter = 'Cauchy  '
	 else if(ifilter .eq. 3) then
	  call filtcosine(nxtrans,ny,radmin,radmax,peakwidth,mapbuf)
	  filter = 'Cosine  '
	 else if(ifilter .eq. 4) then
	  call filtsigmoid(nxtrans,ny,radmin,radmax,peakwidth,mapbuf)
	  filter = 'Sigmoid  '
	 end if
C*** back transform
	 call todfft(mapbuf,nx,ny,1)
C*** normalize ?
	 if(norm .eq. 1) then
	  call normalize(nxtrans,ny,mapbuf)
	 else
	  call stats(nxtrans,ny,mapbuf)
	 end if
C*** write the image to the new file
	 do iy=1,ny
	  ixy = nxtrans * (iy - 1) + 1
	  call iwrlin(idevout,mapbuf(ixy))
	 end do
	end do
	write(title,'(
     *  ''Stack bandpass '',a8,'' filtered. Min, max radii : '',2f6.3
     *  )') filter, radmin, radmax
	call iwrhdr(idevout,title,0,dmin,dmax,dmean)
	call imclose(idevin)
	call imclose(idevout)
	close(idevout)
	end
C********************************************************************
C***
        subroutine filtgauss(mx,my,radmin,radmax,peakwidth,mapbuf)
C***
C********************************************************************
C*** subroutine to gaussian bandpass filter density array
C********************************************************************
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        complex mapbuf(*)
C***
	nxhalf = mx / 2
	nyhalf = my / 2
        xnum = float(mx)**2
        ynum = float(my)**2
	distmax = float(nyhalf - 1) / float(my)
	variance = peakwidth * peakwidth
	do iy=1,my
	 ixy = nxhalf * (iy - 1)
	 fy = float(iy - 1)
         if(fy .gt. nyhalf) fy = fy - float(my)
         ysum = fy * fy / ynum
         do ix=1,nxhalf
          nxy = ixy + ix
          fx = float(ix - 1)
          xsum = fx * fx / xnum
          xydist = xsum + ysum
	  if(xydist .gt. 0.0) xydist = sqrt(xydist)
	  if(xydist .lt. radmin) then
	   diff = radmin - xydist
	   mapbuf(nxy) = mapbuf(nxy) *
     *     min(transmax,max(transmin,exp(-diff * diff/ variance)))
C*** set outside densities to transmission frequency
	  else if(xydist .gt. distmax) then
	   mapbuf(nxy) = transmin
C*** set freqeucies beyond radmax
	  else if(xydist .ge. radmax) then
	   diff = xydist - radmax
	   mapbuf(nxy) = mapbuf(nxy) *
     *     min(transmax,max(transmin,exp(-diff * diff/ variance)))
C*** frequencies lying between the filter radii do not need scaling
	  end if
	 end do
        end do
	return
	end
C********************************************************************
C***
        subroutine filtcauchy
     *  (mx,my,radmin,radmax,shape,peakwidth,mapbuf)
C***
C********************************************************************
C*** subroutine to gauss filter the image
C***
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        complex mapbuf(*)
C***
        nxhalf = mx / 2
        nyhalf = my / 2
        xnum = float(mx)**2
        ynum = float(my)**2
	distmax = float(nyhalf - 1) / float(my)
        scfac = (peakwidth / shape) **2
	do iy=1,my
	 ixy = nxhalf * (iy - 1)
	 fy = float(iy - 1)
         if(fy .gt. nyhalf) fy = fy - float(my)
         ysum = fy * fy / ynum
         do ix=1,nxhalf
          nxy = ixy + ix
          fx = float(ix - 1)
          xsum = fx * fx / xnum
          xydist = xsum + ysum
	  if(xydist .gt. 0.0) xydist = sqrt(xydist)
	  if(radmin - xydist .gt. 0.0) then
	   diff = radmin - xydist
	   mapbuf(nxy) = mapbuf(nxy) * min(transmax,max
     *     (transmin,1./(1. + diff * diff / scfac)**shape))
C*** set outside densities to min transmission frequency
	  else if(xydist .gt. distmax) then
	   mapbuf(nxy) = transmin
	  else if(xydist .ge. radmax) then
	   diff = xydist - radmax
	   mapbuf(nxy) = mapbuf(nxy) * min(transmax,max
     *     (transmin,1./(1. + diff * diff / scfac)**shape))
C*** frequencies lying between the filter radii do not need scaling
	  end if
	 end do
        end do
        return
        end
C********************************************************************
C***
        subroutine filtcosine(mx,my,radmin,radmax,peakwidth,mapbuf)
C***
C********************************************************************
C*** subroutine to gauss filter the image
C***
        real*4          pi
        parameter       (pi = 3.14159)
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        complex mapbuf(*)
C***
        nxhalf = mx / 2
        nyhalf = my / 2
        xnum = float(mx)**2
        ynum = float(my)**2
	distmax = float(nyhalf - 1) / float(my)
	rfirst = radmin - peakwidth
	rlast = radmax + peakwidth
	rsc = pi / peakwidth
	do iy=1,my
	 ixy = nxhalf * (iy - 1)
	 fy = float(iy - 1)
         if(fy .gt. nyhalf) fy = fy - float(my)
         ysum = fy * fy / ynum
         do ix=1,nxhalf
          nxy = ixy + ix
          fx = float(ix - 1)
          xsum = fx * fx / xnum
          xydist = xsum + ysum
	  if(xydist .gt. 0.0) xydist = sqrt(xydist)
C*** set outside densities to transmission frequency
	  if(xydist .lt. rfirst .or. xydist .gt. rlast) then
	   mapbuf(nxy) = transmin
	  else if(radmin - xydist .gt. 0.0) then
	   diff = radmin - xydist
	   mapbuf(nxy) = mapbuf(nxy) *
     *     min(transmax,max(transmin,0.5 * (cos(rsc * diff) + 1.0)))
	  else if(xydist .ge. radmax) then
	   diff = xydist - radmax
	   mapbuf(nxy) = mapbuf(nxy) *
     *     min(transmax,max(transmin,0.5 * (cos(rsc * diff) + 1.0)))
C*** frequencies lying between the filter radii do not need scaling
	  end if
	 end do
        end do
        return
        end
C********************************************************************
C***
        subroutine filtsigmoid(mx,my,radmin,radmax,peakwidth,mapbuf)
C***
C********************************************************************
C*** subroutine to sigmoid bandpass filter density array
C********************************************************************
        integer*4       nsig
        parameter       (nsig = 16)
        real*4          sigrange
        parameter       (sigrange = 16.0)
	real*4  halfsig
	parameter (halfsig = 8.)
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        complex mapbuf(*)
C***
	nxhalf = mx / 2
	nyhalf = my / 2
        xnum = float(mx)**2
        ynum = float(my)**2
	distmax = float(nyhalf - 1) / float(my)
	sigscale = sigrange / peakwidth
	offset = radmin - peakwidth
	do iy=1,my
	 ixy = nxhalf * (iy - 1)
	 fy = float(iy - 1)
         if(fy .gt. nyhalf) fy = fy - float(my)
         ysum = fy * fy / ynum
         do ix=1,nxhalf
          nxy = ixy + ix
          fx = float(ix - 1)
          xsum = fx * fx / xnum
          xydist = xsum + ysum
	  if(xydist .gt. 0.0) xydist = sqrt(xydist)
	  if(xydist .le. radmin) then
	   fx = sigscale * (xydist - offset) - halfsig
	   mapbuf(nxy) = mapbuf(nxy) *
     *     min(transmax,max(transmin,1./(1. + exp(-fx))))
C*** set outside densities to transmission frequency
	  else if(xydist .gt. distmax) then
	   mapbuf(nxy) = transmin
C*** set freqeucies beyond radmax
	  else if(xydist .ge. radmax) then
	   diff = xydist - radmax
           fx = sigscale * (radmax - xydist) + halfsig
	   mapbuf(nxy) = mapbuf(nxy) *
     *     min(transmax,max(transmin,1./(1. + exp(-fx))))
C*** frequencies lying between the filter radii do not need scaling
	  end if
	 end do
        end do
	return
	end
C********************************************************************
C***
        subroutine plotgauss(mx,my,radmin,radmax,peakwidth)
C***
C********************************************************************
C*** subroutine to gaussian bandpass filter density array
C********************************************************************
C***
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        include 'bandpass.inc'
C***
	variance = peakwidth * peakwidth
	finc = drange / mx
	do i=1,mx
         xydist = float(i) * finc
	 if(xydist .lt. radmin) then
	  diff = radmin - xydist
	  denscale =
     *    min(transmax,max(transmin,exp(-diff * diff/ variance)))
C*** set outside densities to transmission frequency
	 else if(xydist .gt. drange) then
	  denscale = transmin
	 else if(xydist .ge. radmax) then
	  diff = xydist - radmax
	  denscale =
     *    min(transmax,max(transmin,exp(-diff * diff/ variance)))
C*** set frequencies lying between filter radii to 1
	 else
	  denscale = transmax
	 end if
	 write(idevout,'(2f10.3)') xydist,denscale
        end do
	return
	end
C********************************************************************
C***
        subroutine plotcauchy(mx,my,radmin,radmax,shape,peakwidth)
C***
C********************************************************************
C*** subroutine to gauss filter the image
C***
C***
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        include 'bandpass.inc'
C***
	scale = (peakwidth / shape) **2
	finc = drange / mx
	do i=1,mx
         xydist = float(i) * finc
	 if(xydist .lt. radmin) then
	  diff = radmin - xydist
	  denscale = min(transmax,max
     *    (transmin,1./(1. + diff * diff / scale)**shape))
C*** set outside densities to transmission frequency
	 else if(xydist .gt. drange) then
	  denscale = transmin
	 else if(xydist .ge. radmax) then
	  diff = xydist - radmax
	  denscale = min(transmax,max
     *    (transmin,1./(1. + diff * diff / scale)**shape))
C*** set frequencies lying between filter radii to 1
	 else
	  denscale = transmax
	 end if
	 write(idevout,'(2f10.5)') xydist,denscale
        end do
        return
        end
C********************************************************************
C***
        subroutine plotcosine(mx,my,radmin,radmax,peakwidth)
C***
C********************************************************************
C*** subroutine to cosine bell filter the image
C***
C***
        real*4          pi
        parameter       (pi = 3.14159)
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        include 'bandpass.inc'
C***
	rfirst = radmin - peakwidth
	rlast = radmax + peakwidth
	rsc = pi / peakwidth
	finc = drange / mx
	do i=1,mx
	 xydist = float(i) * finc
C*** set outside densities to transmission frequency
	 if(xydist .lt. rfirst .or. xydist .gt. rlast) then
	  denscale = transmin
	 else if(xydist .le. radmin) then
	  diff = radmin - xydist
	  denscale =
     *    min(transmax,max(transmin,0.5 * (cos(rsc * diff) + 1.0)))
	 else if(xydist .ge. radmax) then
	  diff = xydist - radmax
	  denscale =
     *    min(transmax,max(transmin,0.5 * (cos(rsc * diff) + 1.0)))
C*** set frequencies lying between filter radii to 1
	 else
	  denscale = transmax
	 end if
	 write(idevout,'(2f10.3)') xydist,denscale
        end do
        return
        end
C********************************************************************
C***
        subroutine plotsigmoid(mx,my,radmin,radmax,peakwidth)
C***
C********************************************************************
C*** subroutine to gaussian bandpass filter density array
C********************************************************************
C***
        real*4          halfsig
        parameter       (halfsig = 8.)
	integer*4 nsig
	parameter (nsig = 16)
	real*4  sigrange
	parameter (sigrange = 16.0)
        real*4          transmax
        parameter       (transmax = 1.)
        real*4          transmin
        parameter       (transmin = 0.0005)
        include 'bandpass.inc'
C***
	finc = drange / mx
	sigscale = sigrange / peakwidth
	offset = radmin - peakwidth
	do i=1,mx
         xydist = float(i) * finc
	 if(xydist .le. radmin) then
	  fx = sigscale * (xydist - offset) - halfsig
	  denscale =
     *    min(transmax,max(transmin,1./(1. + exp(-fx))))
	 else if(xydist .ge. radmax) then
	  fx = sigscale * (radmax - xydist) + halfsig
	  denscale =
     *    min(transmax,max(transmin,1./(1. + exp(-fx))))
C*** set frequencies lying between filter radii to 1
	 else
	  denscale = transmax
	 end if
	 write(idevout,'(2f10.3)') xydist,denscale
        end do
	return
	end
C*******************************************************************
C***
        subroutine normalize(mx,my,mapbuf)
C***
C*******************************************************************
C*** subroutine to normalize images by subtracting mean and dividing
C*** by standard deviation.
        include 'bandpass.inc'
C*** calculate min & max
	fmxy = float(mx * my)
	dsum = 0.
	sumsq = 0.
        dmin = bignum
        dmax = -bignum
        do ixy=1,mx *my
         den = mapbuf(ixy)
	 dsum = dsum + den
	 sumsq = sumsq + den * den
         dmax = max(dmax,den)
         dmin = min(dmin,den)
        end do
	dmean = dsum / fmxy
	sdev = sqrt(sumsq / fmxy - dmean * dmean)
        dmin = bignum
        dmax = -bignum
	do ixy=1,mx * my
	 mapbuf(ixy) = (mapbuf(ixy) - dmean) / sdev
	 den = mapbuf(ixy)
         dmax = max(dmax,den)
         dmin = min(dmin,den)
	end do
	dmean = 0.0
	return
	end
C*******************************************************************
C***
        subroutine stats(mx,my,mapbuf)
C***
C*******************************************************************
C*** subroutine to calculate min, max, mean,sdev from an array
        include 'bandpass.inc'
C*** calculate min & max
        dmin = bignum
        dmax = -bignum
        do iy=1,my
         nxy = mx * (iy - 1)
         do ix=1,my
          den = mapbuf(nxy + ix)
          dmax = max(dmax,den)
          dmin = min(dmin,den)
         end do
        end do
C*** calculate scale factor if mode 0 (byte map) and dmax > 255
        if(mode .eq. 0 .and. dmax .gt. denmax) then
         scale = denmax / (dmax - dmin)
        else
         scale = 1.0
        end if
C*** subtract dmin to make all densities >= 0
        sumden = 0.
        dmax = -bignum
        do iy=1,my
         nxy = mx * (iy - 1)
         do ix=1,my
          den = scale * (mapbuf(nxy + ix) - dmin)
          mapbuf(nxy + ix) = den
          sumden = sumden + den
          dmax = max(dmax,den)
         end do
        end do
        dmin = 0.
        fmxysq = float(my * my)
        dmean = sumden / fmxysq
        return
        end

