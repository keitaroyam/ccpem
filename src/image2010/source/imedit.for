C
C*****************************************************************
C     IMEDIT- V6.3	30-Jul-2014
C             Program to edit an image file header
C
C             Last mod: changes to IALSAM in IMSUBS needed
C                       1 line change (lines to bytes) in
C                       option to alter symmetry data 11.10.85 JMS
C                       option to modify min, max, mean
C			option to turn map through 90 degrees 05.07.94 jms
C			v slight mods to mean calculation 15.01.96
C			27.02.96 :
C			maxrows,maxcols modified to suit indigo which
C			coredumps at 14000, 14000 and many other numbers
C			25.04.96 :
C			large array section put into common to get round
C			the problem of using stack so it now uses data
C			(system binary limit gives stacksize and datasize)
C			06.06.96 :
C			installed option to invert densities mode 0 or 1
C			10.02.97
C			Bug fix in map rotation and mod in main menu write
C			to make it run on SGI
C                       09.02.98
C                       Added option to modify fast,med,slow axes
C                       20.05.98
C                       New option to pad/float an image
C			17.05.99 extended density inversion to real*4
C			7.07.99 modifiable scale factor for print densities
C			29.03.00 zorigin added to ialorg,irtorg for imsubs2000
C			29.11.00 major rewrite for image2000 compatibility
C			22.01.02 removed symmetry operator mod as whole
C				map must be rewritten afterwards. This should
C				be part of a major rewrite which copies only
C				the header, and rewrites the map each time
C			31.03.03 added rescaling option
C			26.02.04 included symmetry modification - writes to
C				 a temp file then copies back again
C			09.03.06 added option for section order reversal
C			17.08.06 rotation in Z added, all map inversions corrected
C			23.06.11 mod for GFORTRAN - finish with a 0
C			30.07.14 RMS deviation calc installed
C
C     UNIT numbers
C                 1 map input/output
C                 2 output file for change symmetry operators
C                 3 input file for symmetry data
C                 4 o/p printer file
C                 7 command file for printer o/p
C*****************************************************************
C
	integer*4  idevmap
	parameter  (idevmap = 1)
	integer*4  idevtmp
	parameter  (idevtmp = 2)
	integer*4  maxrows
	parameter  (maxrows=8192)
	integer*4  maxcols
	parameter  (maxcols=8192)
C***
	character*256  filin
	character*256  filout
	character*80  labels(20)
	character*80   string
	character*1   option
	character*80  symop
	character*1  yesno
C***
	integer*4  icol(25)
	integer*4          iuvw(3)
	integer*4  jline(20)
	integer*4  mxyz(3)
	integer*4  nxyz(3)
	integer*4  nxyzst(3)
C***
	logical   there
	logical   first
	logical   iprint
C***
	real*4   array(16384)
	real*4   cell(6)
	real*4   section(maxcols,maxrows)
	real*4   slice(maxcols*maxrows)
	real*4   temp
C***
	real*8    dtot
	real*8    dnum
C***
	common//nx,ny,nz
	common/big/section
C***
	equivalence (nx,nxyz)
	equivalence (section(1,1),slice(1))
	data ntflag/-1/
C***
	write(6,'(/
     *  '' **********************************************''/
     *  ''  IMEDIT v6.3 image editing program 30.07.2014''/
     *  '' **********************************************''/)')
C***
C*** open input file
	write(6,'('' Type input file name ...'')')
	first = .true.
	read(5,'(a)') filin
C*** open output file, transfer header and write new header
  100   write(6,'('' Type output file name ...'')')
	read(5,'(a)') filout
	there = .false.
	inquire(file=filout,exist=there)
	if(there) then
	 write(6,'(''File already exists, overwrite it ? (y/n)'')')
	 read(5,'(a)') yesno
	 if(yesno .ne. 'y' .and. yesno .ne. 'Y') go to 100
	end if
C*** copy directly to output file and work from this
	ncfilin = numchars(filin)
	call system('cp '//filin(1:ncfilin)//' '//
     *  filout(1:numchars(filout)))
	call imopen(idevmap,filout,'old')
	call irdhdr(idevmap,nxyz,mxyz,mode,dmin,dmax,dmean)
C********************************************************
C*** Main menu
C********************************************************
        do i=1,3
         nxyzst(i) = 0
	end do
 1000 	option = ' '
	write(6,'(/'' Type option for:'')')
	write(6,'('' change NXYZ size parameters......... 1'')')
	write(6,'('' change MXYZ sampling parameters..... 2'')')
	write(6,'('' change cell parameters.............. 3'')')
	write(6,'('' change origin on X,Y,Z.............. 4'')')
	write(6,'('' change fast, med, slow axes ........ 5'')')
	write(6,'('' change title........................ 6'')')
	write(6,'('' modify symmetry data ............... 7'')')
	write(6,'('' recalculate min, max, mean.......... 8'')')
	write(6,'('' Calculate RMS deviation ...........  9'')')
	write(6,'('' print selected area of file.........10'')')
	write(6,'('' turn map........................... 11'')')
	write(6,'('' invert densities .................. 12'')')
	write(6,'('' pad/float image ................... 13'')')
	write(6,'('' rescale image ..................... 14'')')
	write(6,'('' finish ............................ 0''/)')
	read(5,'(i5)') ichoose
C***
        go to (1100,1200,1300,1400,1500,1600,1700,1800,1900,
     *  2000,2100,2200,2300,2400),ichoose
	go to 5000
C********************************************************
C*** change NXYZ size parameters
C********************************************************
 1100 	write(6,'('' Current NX, NY, NZ ='',3i5)') nxyz
	write(6,'('' Type in new NXYZ'')')
	read(5,*) nxyz
	write(6,'(
     *  '' Type in new start points for cols,rows,sections'')')
	read(5,*) nxyzst
	call ialsiz(idevmap,nxyz,nxyzst)
	write(6,*) nxyz,nxyzst
	go to 1000
C********************************************************
C***  change MXYZ entire unit cell parameters
C********************************************************
 1200 	WRITE(6,'('' Current MX, MY, MZ ='',3i5)') mxyz
	write(6,'('' Type in new MXYZ'')')
	read(5,*) mxyz
	call ialsam(idevmap,mxyz)
	go to 1000
C********************************************************
C*** change cell parameters
C********************************************************
 1300 	call irtcel(idevmap,cell)
	write(6,'('' Current CELL parameters ='',6f10.2)') cell
	write(6,'('' Type in new cell parameters'')')
	read(5,*) cell
	call ialcel(idevmap,cell)
	go to 1000
C********************************************************
C*** change origin
C********************************************************
 1400 	call irtorg(idevmap,xorigin,yorigin,zorigin)
	write(6,'('' Current origin values ='',3f10.2)')
     *  xorigin, yorigin, zorigin
	write(6,'('' Type in new X,Y,Z origin'')')
	read(5,*) xorigin,yorigin,zorigin
	call ialorg(idevmap,xorigin,yorigin,zorigin)
	go to 1000
C********************************************************
C*** change fast, med slow axes
C********************************************************
 1500 	write(6,'('' type xyz order e.g. 3 1 2 for Z,X,Y'')')
	read(5,*) iuvw
      	call ialuvw(idevmap,iuvw)
	go to 1000
C********************************************************
C*** change title
C********************************************************
 1600 	if(first) call irtlab(idevmap,labels,nl)
      	first = .false.
 1650   write(6,'(/15x,
     *  ''******'',i3,'' current titles in header: ******''//)') nl
      	do i=1,nl
         write(6,'(1x,a)') labels(i)(1:80)
      	end do
	write(6,'(//''Add a title .......... <cr>'')')
	write(6,'(''Remove a title ....... <1>'')')
	write(6,'(''Return main menu ..... <2>'')')
      	read(5,'(i5)') iopt
C******************
C*** add a title
C******************
	if(iopt .eq. 0) then
         write(6,'('' Type in new title'')')
         read(5,'(a)') string
         write(6,'('' Type in required position e.g. 1,2,3 etc.'')')
         read(5,*) ipos
C*** move labels up
         if(ipos .le. nl) then
          num = min(nl,9)
          do i=num,ipos,-1
           labels(i+1)(1:80) = labels(i)(1:80)
          end do
         end if
C*** insert new title
         labels(ipos)(1:80) = string(1:80)
         nl = nl + 1
C******************
C*** delete a title
C******************
        else if(iopt .eq. 1) then
         write(6,'(
     *   '' Type position of label to be deleted e.g. 1,2,3 etc.'')')
         read(5,*) ipos
         nl = nl - 1
         do i=ipos,nl
          labels(i)(1:80) = labels(i+1)(1:80)
         end do
	else
         go to 1000
        end if
C**************************
C*** compile all the titles
C**************************
        do i=1,nl
         write(6,'(1x,a)') labels(i)(1:80)
        end do
        call iallab(idevmap,labels,nl)
	go to 1650
C*******************************************************************************
C*** change symmetry operators
C*******************************************************************************
 1700   call irtsym(idevmap,ispg,nbs)
        write(6,'(10x,
     *  ''****** Current space group ='',i3,'','',i3,'' bytes'')')
     *  ispg, nbs
C*** add symmetry data
	if(nbs .eq. 0) then
	 write(6,'(17x,''Modifying symmetry data to space group 1'')')
	 ispg = 1
	 nbs = 80
C*** do not write symmetry data
	else
	 write(6,'(17x,''Modifying symmetry data to space group 0'')')
	 ispg = 0
	 nbs = 0
	end if
C*** open temporary workspace map
	call system('/bin/rm imedit.tmp')
	call imopen(idevtmp,'imedit.tmp','new')
	call itrhdr(idevtmp,idevmap)
C*** change symmetry operator parameters in header
        call ialsym(idevtmp,ispg,nbs)
C*** read and write entire map from input to temporary file
	call imposn(idevmap,0,0)
	call imposn(idevtmp,0,0)
	dmin = 1000000000.
	dmax = -dmin
	dmean = 0.
        do iz = 1,nz
	 do iy = 1,ny
	  call irdlin(idevmap,array)
	  do ix = 1, nx
	   density = array(ix)
	   dmin = min(dmin,density)
	   dmax = max(dmax,density)
	   dmean = dmean + density
	  end do
	  call iwrlin(idevtmp,array)
	 end do
        end do
	dmean = dmean / float(nx * ny * nz)
	write(string,'(
     *  ''Symmetry data modified in imedit to space group'',i3)') ispg
	ntflag = 0
	write(6,'(''Symmetry data modified to space group'',i3)') ispg
	call iwrhdr(idevtmp,string,ntflag,dmin,dmax,dmean)
C*** close both map file and temporary file and overwrite map file
        call imclose(idevmap)
        call imclose(idevtmp)
	string = 'mv imedit.tmp '//filout(1:numchars(filout))
	call system(string)
	stop
C********************************************************
C*** recalculate min,max,mean
C********************************************************
 1800	dmin = 100000000.
	dmax = -dmin
	dtot = 0.
	do iz =1,nz
	 do iy=1,ny
	  call irdlin(idevmap,array)
	  do ix=1,nx
	   density = array(ix)
	   dmin = min(dmin,density)
	   dmax = max(dmax,density)
	   dtot = dtot + density
	  end do
	 end do
	end do
	dmean = dtot / float(nx * ny * nz)
	go to 1000
C********************************************************
C*** recalculate RMS deviation
C********************************************************
 1900	dtot = 0.
	do iz =1,nz
	 do iy=1,ny
	  call irdlin(idevmap,array)
	  do ix=1,nx
	   density = array(ix)
	   dmin = min(dmin,density)
	   dmax = max(dmax,density)
	   dtot = dtot + density * density
	  end do
	 end do
	end do
	rms = sqrt(dtot / float(nx * ny * nz))
	call ialrms(idevmap,rms)
	go to 1000
C********************************************************
C*** Print part of data file
C********************************************************
 2000   write(6,'('' Type P for printer, <cr> for Online'')')
        read(5,'(a)') option
        if(option .eq. 'p' .or. option .eq. 'P') then
         iprint = .true.
         idevwrt = 4
         open(unit=idevwrt,file='imedit.lis',status='unknown')
        else
         idevwrt = 6
         iprint = .false.
        end if
C*** rac improvement 1
C*** Use first label as output header
        if(first) call irtlab(idevmap,labels,nl)
        first = .false.
        write(idevwrt,'((1x,a))') (labels(i)(1:80),i=1,nl)
C*** set density scale factor
        sca = 1.
 2020   if(abs(dmax * sca) .ge. 1000.
     *    .or. abs(dmin * sca) .gt. 1000.) then
         sca = sca * 0.1
         go to 2020
        end if
        write(idevwrt,'(/'' Scale factor applied to densities'',
     *                g12.4)') sca
        write(6,'(''Type new scale factor or <cr> to accept'')')
        read(5,'(g12.4)') scanew
        if(scanew .ne. 0.0) then
         sca = scanew
         write(idevwrt,'(/'' Scale factor reset to'',g12.4)') sca
        end if
C*** end of rac improvement 1
 2040   ncol1 = 0
	ncol2 = 0
	nrow1 = 0
	nrow2 = 0
	nsec1 = 0
	nsec2 = 0
	write(6,'(/
     *  '' Type col,row,section ranges(NB numbers start at 0), '',
     *  ''<cr> to quit'')')
        read(5,'(6i5)') ncol1,ncol2,nrow1,nrow2,nsec1,nsec2
	if(ncol2 .eq. 0 .and. nrow2 .eq. 0) go to 1000
        ncol = ncol2 - ncol1 + 1
C*** rac improvement 2 - commented out lines
C***      if(mode.le.2) then
        if(ncol.gt.26) then
         write(6,'('' Column truncated to 26'')')
         ncol2 = ncol1 + 25
         ncol = 26
        end if
        write(idevwrt,'(//'' Area of map printed '',6i6)')
     *  ncol1, ncol2, nrow1, nrow2, nsec1, nsec2
C*** end of rac improvement 2
        nrow = nrow2 - nrow1 + 1
        nsec = nsec2 - nsec1 + 1
        call imposn(idevmap,nsec1,nrow1)
        do i=1,nsec
         write(idevwrt,'(//'' *** section'',i10/)') i
         do k=1,nrow
          call irdpal(idevmap,array,ncol1,ncol2,*2050)
C*** rac improvement 3
          k1 = k + nrow1 - 1
          write(idevwrt,'(/i5,25f5.0)') k1,(array(j)*sca,j=1,ncol)
         end do
         do nc = 1,ncol
	  icol(nc) = nc + ncol1 - 1
         end do
         write(idevwrt,'(/4x,25i5)') (icol(nc),nc=1,ncol)
C*** end of rac improvement 3
        end do
        if(iprint) then
         close(idevwrt)
         call  system('lprint imedit.lis')
         call  system('\rm -f imedit.lis')
        end if
        go to 2040
 2050   write(6,'('' Error in reading file'')')
        stop
C********************************************************
C*** turn map
C********************************************************
 2100   if(nx .gt. maxcols .or. ny .gt. maxrows) then
	  write(6,'(
     *    ''Map too large for program dimensions, nx, ny ='',2i10)')
     *    nx, ny
	  go to 1000
        end if
 2120   write(6,'('' 90 degrees (clockwise) ......... <cr>''/
     *            '' Mirrored about x axis .............1''/
     *            '' Mirrored about y axis .............2''/
     *            '' Mirrored about both axes ..........3''/
     *            '' 270 degrees .......................4'')')
        if(nz .gt. 1) then
         write(6,'('' Rotation abut Y ...................5''/
     *             '' Re-order sections front to back ...6''/)')
        end if
        read(5,'(i5)',err=2120) iturn
        write(6,'('' Wait for map rotation ...'')')
C*** copy input file to temporary file
        call system('/bin/rm imedit.tmp')
        call imopen(idevtmp,'imedit.tmp','new')
        call itrhdr(idevtmp,idevmap)
	dmin = 100000000.
	dmax = -dmin
	dtot = 0.
C*** 90 degrees clockwise
        if(iturn .eq. 0) then
	 string =
     *   'Map rotated 90 degrees in imedit from : '//filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy = 1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
	   do ix = 1, nx
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	    section(ny - iy + 1,ix) = density
	   end do
	  end do
C*** rewrite map
          do iy = 1, ny
           do ix = 1,nx
            array(ix) = section(ix,iy)
           end do
           call iwrlin(idevtmp,array)
          end do
         end do
	 go to 2130
C*** 180 degrees, mirrored about horizontal axis
        else if(iturn .eq. 1) then
	 string =
     *   'Map mirrored about x axis in imedit from : '//filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy = 1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
	   do ix = 1, nx
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	    section(ix,ny-iy+1) = density
	   end do
	  end do
C*** rewrite map
          do iy = 1, ny
           do ix = 1,nx
            array(ix) = section(ix,iy)
           end do
           call iwrlin(idevtmp,array)
          end do
         end do
C*** 180 degrees, mirrorred about vertical axis
        else if(iturn .eq. 2) then
	 string =
     *   'Map mirrored about y axis in imedit from : '
     *   //filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy = 1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
	   do ix = 1, nx
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	    section(nx-ix+1,iy) = density
	   end do
	  end do
C*** rewrite map
          do iy = 1, ny
           do ix = 1,nx
            array(ix) = section(ix,iy)
           end do
           call iwrlin(idevtmp,array)
          end do
         end do
C*** Mirror about both axes
        else if(iturn .eq. 3) then
	 string =
     *   'Map mirrored about xand y axes in imedit from : '
     *   //filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy = 1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
	   do ix = 1, nx
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	    section(nx-ix+1,ny-iy+1) = density
	   end do
	  end do
C*** rewrite map
          do iy = 1, ny
           do ix = 1,nx
            array(ix) = section(ix,iy)
           end do
           call iwrlin(idevtmp,array)
          end do
         end do
C*** 270 degrees clockwise
        else if(iturn .eq. 4) then
	 string =
     *   'Map rotated 270 degrees in imedit from : '//filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy = 1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
	   do ix = 1, nx
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	    section(iy,nx-ix+1) = density
	   end do
	  end do
C*** rewrite map
          do iy = 1, ny
           do ix = 1,nx
            array(ix) = section(ix,iy)
           end do
           call iwrlin(idevtmp,array)
          end do
         end do
	 go to 2130
C*** 180 in z
        else if(iturn .eq. 5) then
	 string =
     *   'Map sections reversed in imedit from : '//filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy=1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
C*** reverse order of array
	   do ix=1,nx
	    density = array(nx-ix+1)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	    slice(ix) = density
	   end do
	   call imposn(idevtmp,nz-iz,iy-1)
	   call iwrlin(idevtmp,slice)
	  end do
         end do
C*** re-order sections
        else if(iturn .eq. 6) then
	 string =
     *   'Map sections reversed in imedit from : '//filin(1:ncfilin)
C*** read map
         do iz = 1,nz
	  do iy=1,ny
	   call imposn(idevmap,iz-1,iy-1)
	   call irdlin(idevmap,array)
	   call imposn(idevtmp,nz-iz,iy-1)
	   call iwrlin(idevtmp,array)
	   do ix=1,nx
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
	   end do
	  end do
         end do
        else
         go to 2120
        end if
	go to 2150
C*** rewrite header information, switching nx,ny, alter size params
 2130   itemp = nxyz(1)
        nxyz(1) = nxyz(2)
        nxyz(2) = itemp
        itemp = nxyzst(1)
        nxyzst(1) = nxyzst(2)
        nxyzst(2) = itemp
        call ialsiz(idevmap,nxyz,nxyzst)
C*** alter grid sampling
        itemp = mxyz(1)
        mxyz(1) = mxyz(2)
        mxyz(2) = itemp
        call ialsam(idevmap,mxyz)
C*** alter cell parameters
        call irtcel(idevmap,cell)
        temp = cell(1)
        cell(1) = cell(2)
        cell(2) = temp
        temp = cell(4)
        cell(4) = cell(5)
        cell(5) = temp
        call ialcel(idevmap,cell)
C*** read header from input map
 2150   ntflag = 0
	dmean = dtot / float(nx * ny * nz)
	call iwrhdr(idevtmp,string,ntflag,dmin,dmax,dmean)
C*** close both map file and temporary file and overwrite map file
        call imclose(idevmap)
        call imclose(idevtmp)
	string = 'mv imedit.tmp '//filout(1:numchars(filout))
	call system(string)
	stop
C********************************************************
C*** invert densities
C********************************************************
 2200   if(mode .gt. 2) then
	 write(6,'(''This option currently unavailable for transforms'')')
	 go to 1000
        end if
        if(mode .eq. 0) then
         amax = 255.
        else if(mode .eq. 1) then
         amax = 65535.
        else
	 amax = dmax
	 amin = dmin
	 amean = amin + (amax - amin) * 0.5
        end if
        dmin = 100000000.
        dmax = - dmin
        dtot = 0.
        if(mode .le. 1) then
         do iz = 1, nz
          do iy = 1, ny
           call imposn(idevmap,iz-1,iy-1)
   	   call irdlin(idevmap,array)
           do ix = 1, nx
	    array(ix) = amax - array(ix)
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
           end do
           call imposn(idevmap,iz-1,iy-1)
	   call iwrlin(idevmap,array)
          end do
         end do
        else
C*** real*4
         do iz = 1, nz
          do iy = 1, ny
           call imposn(idevmap,iz-1,iy-1)
  	   call irdlin(idevmap,array)
           do ix = 1, nx
	    diff = amean - array(ix)
	    array(ix) = amean + sign(1.,diff) * abs(diff)
	    density = array(ix)
	    dmin = min(dmin,density)
	    dmax = max(dmax,density)
	    dtot = dtot + density
           end do
           call imposn(idevmap,iz-1,iy-1)
	   call iwrlin(idevmap,array)
          end do
         end do
        end if
        dmean = dtot / float(nx * ny * nz)
	string =
     *  'Map densities inverted in imedit from : '//filin(1:ncfilin)
	ntflag = 0
	call iwrhdr(idevmap,string,ntflag,dmin,dmax,dmean)
        call irdhdr(idevmap,nxyz,mxyz,mode,dmin,dmax,dmean)
        call imclose(idevmap)
	stop
C********************************************************
C*** pad/float image
C********************************************************
 2300   if(nxyz(3) .ne. 1) then
         write(6,'('' Operation invalid for multi-section maps.'')')
         go to 1000
        else if(nx .gt. maxcols .or. ny . gt. maxrows) then
         write(6,'('' Map too large for program.'')')
         go to 1000
        end if
        write(6,'(''Current dimensions are'',2i10)') (nxyz(i),i=1,2)
        write(6,'(''Type new dimensions ....'')')
        read(5,*) kx, ky
        nx = nxyz(1)
        ny = nxyz(2)
        ifloat = 0
        write(6,'(''Set padded area to 0 ............   0''/
     *            ''Set padded area to perimeter ...... 1''/)')
        read(5,'(i5)') ifloat
        if(mode .eq. 0 .and. ifloat .eq. 1)
     *   write(6,'(
     *   ''Warning - map will be rewritten as 2-byte integers.'')')
        mode = 1
        backgd = 0.
C*** start reading the map to calculate average perimeter
        if(ifloat .eq. 1) then
         call imposn(idevmap,0,0)
         do iy = 1,ny
          call irdlin(idevmap,array)
          if(iy .eq. 1 .or. iy .eq. ny) then
           do ix = 1,nx
            backgd = backgd + array(ix)
           end do
          else
           backgd = backgd + array(1) + array(ny)
          end if
         end do
         backgd = backgd / (2.0 * float(nx + ny - 2))
        end if
C*** initialize the output array
        do iy = 1,ky
         do ix = 1,kx
          section(ix,iy) = backgd
         end do
        end do
C*** rewrite the map
        call imposn(idevmap,0,0)
        do iy = 1,ny
         call irdlin(idevmap,array)
         do ix = 1,nx
          section(ix,iy) = array(ix)
         end do
        end do
        nx = kx
        ny = ky
        cell(1) = float(nx)
        cell(2) = float(ny)
        cell(3) = 1.0
        do i=4,6
         cell(i) = 90.
        end do
        do i=1,3
         mxyz(i) = nxyz(i)
        end do
        call ialsiz(idevmap,nxyz,nxyzst)
        call ialsam(idevmap,mxyz)
        call ialmod(idevmap,mode)
        call ialcel(idevmap,cell)
        call iwrhdr(idevmap,label,ntflag,dmin,dmax,dmean)
        call imposn(idevmap,0,0)
	dmin = 100000000.
	dmax = -dmin
	dtot = 0.
        do iy = 1,ny
         do ix = 1,nx
          array(ix) = section(ix,iy)
	  density = array(ix)
	  dmin = min(dmin,density)
	  dmax = max(dmax,density)
	  dtot = dtot + density
         end do
         call iwrlin(idevmap,array)
        end do
	dmean = dtot / float(nx * ny * nz)
	string =
     *  'Map padded/floated in imedit from : '//filin(1:ncfilin)
	ntflag = 0
	call iwrhdr(idevmap,string,ntflag,dmin,dmax,dmean)
        call irdhdr(idevmap,nxyz,mxyz,mode,dmin,dmax,dmean)
        call imclose(idevmap)
	stop
C********************************************************
C*** rescale
C********************************************************
 2400   write(6,'(''Type new minimum, maximum ...'')')
	read(5,*) anewmin, anewmax
	scale = (anewmax - anewmin) / (dmax - dmin)
	anewmean = 0.
	do iz = 1,nz
	 do iy = 1,ny
	  call imposn(idevmap,iz-1,iy-1)
	  call irdlin(idevmap,array)
	  do ix=1, nx
	   array(ix) = (array(ix) - dmin) * scale + anewmin
	   anewmean = anewmean + array(ix)
	  end do
C*** reposition to overwrite line
	  call imposn(idevmap,iz-1,iy-1)
	  call iwrlin(idevmap,array)
	 end do
	end do
	string =
     *  'Map rescaled in imedit from : '//filin(1:ncfilin)
	ntflag = 0
	anewmean = anewmean / float(nx * ny * nz)
	write(6,'(''Map rescaled between'',f10.2,'' and'',f10.2,
     *  '' recalculated mean ='',f10.2)') anewmin, anewmax, anewmean
	call iwrhdr(idevmap,string,ntflag,anewmin,anewmax,anewmean)
        call irdhdr(idevmap,nxyz,mxyz,mode,dmin,dmax,dmean)
        call imclose(idevmap)
	stop
C********************************************************
C*** rewrite header, close file
C********************************************************
 5000   call iwrhdr(idevmap,label,ntflag,dmin,dmax,dmean)
        call irdhdr(idevmap,nxyz,mxyz,mode,dmin,dmax,dmean)
        call imclose(idevmap)
        stop
        end
