#!/bin/tcsh
#
#image.cshrc
#
set MRC_IMAGE_COM=/home/tom/Code/ccpem_svn/ccpem/ccpem/lib/MRCImageLibraries/image2010/com
if ($?MRC_IMAGE_COM == 0) then
  echo "\nPath not set in image.login"
  echo "\nPlease edit ../ccpem/lib/MRCImageLibraries/image2010/test/image.login\n"
  # Uncomment and edit first line above to include full path to this directory:
  # e.g.:
  # set MRC_IMAGE_COM=home/ccpem/lib/MRCImageLibraries/image2010/test/
else
  set LASER_PUBLIC=$MRC_IMAGE_COM/../../../../../build/bin/
  setenv LASER_PUBLIC
  alias lasertext '$LASER_PUBLIC/mrc_lasertext'
  alias lasertone '$LASER_PUBLIC/mrc_lasertone'
endif
