


lasertext(lmb)		       lmb users guide		       lasertext(lmb)



NAME

lasertext. Enable printing of raw ASCII text on postscript laserprinter.


SYNOPSIS


  lasertext [-opt[=param] -opt...] [sourcefile]

  sourcefile is the name of the input text file. If this is omitted, input is
  read from stdin.



OPTIONS

     options can be in mixed case and can be unambiguously abbreviated


     -a3
	  Select A3 paper.
	  Paper-selection is printer-specific and requires that a printer be
	  specified with the -queue= option. If you dont want to specify a
	  queue, dont use -a3; instead direct stdout to a file and print the
	  file using 'laser1 -a3' or whatever. (See lmbhelp laserprinters).
	  default: A4.


     -bold
	  Forces bold printing. For those of us long in the tooth (or short
	  in the sight) normal-weight characters may be difficult to read.
	  default: not bold.


     -bottommargin=f
	  Sets the bottom margin (inches).
	  default: 0.7in


     -charsperinch=f
	  Sets the chars per inch. No underline option with this (yet).
	  default: computed from the pointsize.


     -colour
	  Selects the colour version of the letterhead.


     -duplex[=flip]
	  Specifies duplex printing (i.e. both sides of the paper). Duplex
	  with no parameter specifies book-style (head-to-head); duplex=flip
	  specifies flip-chart style (head-to-tail). At present only laser1
	  supports duplex printing. It will be ignored on other printers.
	  Duplex-control is printer-specific and requires that a printer be
	  specified with the -queue= option. If you dont want to specify a
	  queue, dont use -duplex; instead direct stdout to a file and print
	  the file using dup_laser1 or whatever. (See lmbhelp laserprinters).
	  default: no duplex


     -font=n
	  Specifies the font number. 'lasertext -sample | laser1' for a demo.
	  default: font=3 (Courier)


     -header[=string]
	  If -header is specified without a parameter, the input filename,
	  date and time is printed at the top of each page. If a parameter is
	  specified, that is printed instead. If the parameter contains
	  spaces or other characters to which the shell is sensitive, the
	  parameter should be enclosed in quotes (").
	  default: no header.


     -landscape
	  Prints in landscape orientation on the paper.
	  default: portrait.


     -letter=name
	  Causes a letter-heading to be printed at the top of the page;
	  letter=lmb or letter=coru so far. Changes the default left margin
	  to 0.75 in.
	  default:no letter-heading


     -leftmargin=f
	  Sets the left margin in inches.
	  default: 0.5 inches (except see -letter).


     -linesperinch=f
	  Sets the lines per inch.
	  default: computed from the pointsize. 6 for the default pointsize.


     -number[=n]
	  Causes the pages to be numbered in the top r.h. corner. Page
	  numbering starts at the given parameter, 1 if no parameter.
	  default: no page numbering.


     -outputfile=file
	  Specifies the name of the output file to contain the postscript. If
	  no output file is given, output will be directed to stdout, which
	  means it can be re-directed to a file using the > operator, or
	  piped to another program or print queue using the | operator.
	  default: stdout.


     -pagelines=n
	  Sets the number of lines per page.pdefault: computed from the
	  pointsize. 66 for the default pointsize.


     -pointsize=f
	  Specifies the character height, in points (1/72 inch) of the main
	  text.	 Sub/superscripts are printed at pointsize=6.
	  default: 11 point.


     -queue=queuename
	  Specifies which output print queue to use. Available queues are
	  laser1, laser2, colour1, clc (Canon colour copier in photographic),
	  lw305 (Apple laserwriter in room 305) and lw6006 (ditto in rm
	  6006). If this option is requested, the postscript output is
	  directed to a temporary file and automatically submitted for
	  printing. This allows aliases to be set for lasertext commands,
	  which then no longer need to pipe their output to a printer.
	  default: not queued


     -rightmargin=f
	  Specifies the right margin in inches.
	  default: 0.25 inches


     -sample
	  Produces a sample of the character sets, how to achieve them from a
	  dumb terminal, and the available fonts.
	  default: no sample.


     -topmargin=f
	  Specifies the top margin in inches.
	  default: 0.7 inches.


     Examples:


     lasertext myinput > out.ps
       reads input from myinput and generates postscript file out.ps


     cat myletter | lasertext -let=lmb -dup | laser1
       cat's 'myletter', pipes it to lasertext requesting lmb letterhead
       and duplex printing, and pipes the output to the laser1 queue.


     lasertext -font=1 -point=60 -land < poster | laser1
       reads 'poster' on stdin, selects font 1, pointsize 60, landscape
       and pipes the output to laser1.


     lmbhelp lasertext | lasertext -dup | laser1
       will print this file, both sides, on laser1























