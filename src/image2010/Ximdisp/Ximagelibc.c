/**************************************************************************/
/*	Ximagelibc.c	v14.1				20.12.12	  */
/*	Library of c routines called by the fortran library Ximagelibf.f  */
/*	to display a byte map, with colour tables, draw lines, circles,   */
/*	vectors. Mouse buttons operate as :				  */
/*	lh      : read cursor position					  */
/*	middle  : down starts to pan the image				  */
/*		  up concludes the pan operation			  */
/*	rh	: zooms an area about the cursor position		  */
/*	ctrl/m  : re-displays a hidden menu				  */
/*	ctrl/l  : re-displays a hidden label				  */
/*	ctrl/s  : re-displays a hidden slider				  */
/*	ctrl/btn1down : starts rubberbanding line			  */
/*	ctrl/btn1up : pauses rubberbanding line and adds it to vector     */
/*      list								  */
/*      btn1 double click finishes rubberbanding			  */
/*									  */
/*	Control Widgets are managed as follows :			  */
/*	label or iobox top left corner 					  */
/*	slider bar for adjusting contrast of image below label or iobox	  */
/*	menu beneath label/iobox and slider				  */
/*									  */
/*      Multiple pixmaps have the ability to be scrolled fast but 	  */
/*	pan/zoom disabled						  */
/*									  */
/*	if fonts are unavailable, use xlsfonts to get a listing		  */
/*									  */
/* Note :								  */
/*	Three areas concerned with map display ,			  */
/*	1) image_area - window filled with the map - variable size to	  */
/*			   fit the input map				  */
/*	2) display window - device dependant screen size or smaller	  */
/*	3) screen pixmap - variable size, but never less than screen size */
/*			   has map drawn into it			  */
/*	map size parameters are as follows :				  */
/*	window_width, window_height are the actual window size		  */
/*	max_window_width,max_window_height     "       "    "		  */
/*	map_width,map_height requested map size				  */
/*	image_width,image_height set to maximum of window size or 	  */
/*	requested map size. 						  */
/*									  */
/*	V11.0 runs on 8-bit, 6-bit pseudocolor or 24-bit directcolor      */
/*		minor bug fix to ring cursor				  */
/*		minor bug fix to cursors not appearing in sliders	  */
/*		cursors all msb work, lsb cursors not used 13.12.05	  */
/*	V11.1 errors reported for overlarge maps. Number of scrolling maps*/
/*		increased to 256					  */
/*	V11.5 Keypress installed for menu item,pointer position,zoom,pan  */
/*      V12.0 operations enabled in overlay window                        */
/*	V12.01 Bug fix to box dragging 11.01.08                           */
/*	V12.02 Addition of include files stdlib.h string.h 29.06.09       */
/*      V12.03 Addition of routine to return iobox width getioboxwidth    */
/**************************************************************************/
/*      V13.00 Conversion to 24-bit true colour visual. Many mods         */
/*	V13.01 Error trap for X bug - 16384 limit on screen pixmap(x or y)*/
/*	V13.02 29.11.23011 : drawpixmap vector redraw now functions for   */
/*             store pixmap and screen pixmap (only store before)         */ 
/*	V13.03 22.02.12 : store pixmap has colour representation applied  */
/*	V13.04 23.05.12 : zoom display - removed +1 from vectors          */
/*	V13.10 12.06.12 : bug fix in drawing lines - seg fault due to     */
/*                        nlines -ve					  */
/*	V14.0  05.07.12 : rewrite to handle overlay plane vectors 	  */
/*			separately so vectors from the main wo=indow don't*/
/*			get into the overlay plane event handler value    */
/*			gets set to 0					  */
/*	V14.05 11.07.12 : Bug fix to giant xhair after overlayinit        */
/*	V14.06 06.12.12 : Bug fix for scrolling sections in drawpixmap    */
/*			  increased max_maps				  */
/*	V14.1  20.12.12 : Further bug fixes to section scroll/colour	  */
/**************************************************************************/
//scanf("%ijunk",&junk);
/*
 *  So that we can use fprintf:
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/* 
 * Standard Toolkit include files:
 */
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Shell.h>
#include <X11/Xproto.h>
#include <X11/cursorfont.h>

/*
 * Public include files for widgets :
 */
#include <X11/Xaw/Box.h>
#include <X11/Xaw/Form.h>
#include <X11/Xaw/Label.h>
#include <X11/Xaw/List.h>
#include <X11/Xaw/Scrollbar.h>
#include <X11/Xaw/Simple.h>
#include <X11/Xaw/Text.h>

#include <X11/Xaw/AsciiText.h>
#include <X11/Xaw/AsciiSrc.h>
#include <X11/Xaw/AsciiSink.h>
#include <X11/Xaw/Text.h>
#include <X11/Xaw/TextSrc.h>
#include <X11/Xaw/TextSink.h>


/******************************************************************/
/*
 * count declarations 
 */
int		i;
int		j;
int		k;
int		n;

int		imin_ (int i1,int i2);
int		imax_ (int i1,int i2);
/*
 * Window declarations :
 */

XtAppContext 	app_context;
int		zero=0;
Display		*display_id;
Visual		*visual_id;
Window		topLevel_window_id, 
		colour_bar1_window_id,
		colour_bar2_window_id,
		ioBox_window_id,
		label_window_id,
		sliderBox_window_id,
		image_area_window_id,
		menuBox_window_id,
		overlay_area_window_id,
		zoom_area_window_id,
		root_id;
int        	event_true;
int             expose_flag;
int		image_refresh;
int		zoom_refresh;
int 		overlay_refresh;
Dimension	border_size = 50;
Dimension	iobox_height;
Dimension	iobox_width = 800;
Dimension	label_width = 790;

/* window sizes */
unsigned int    window_width;
unsigned int    window_height;
int 		root_x;
int 		root_y;


/*
 * Widget declarations
 */

Widget	topLevel, 
        mainForm, 
        image_area, 
	colour_bar1,
	colour_bar2,
        menuBox,
	label,
        ioBox,
	ioBox_label,
	ioBox_text,
	overlay_area,
	slider1,
	slider2,
	sliderBox,
	slider_label,
	zoom_area;

/*
 * Action declarations :
 */

static char Trans[] = "Ctrl<Btn1Up>: action_rubber_pause()\n\
                       Ctrl<Btn1Down>: action_rubber_read()\n\
                       <Btn1Down>(2+): action_rubber_finish()\n\
                       <Btn1Down>: action_read_pointer()\n\
		       <Btn2Down>: action_pan_start()\n\
		       <Btn2Up>: action_pan_finish()\n\
		       <Btn3Down>: action_zoom_init()\n\
		       Ctrl<Key>m: action_menudisplay_()\n\
                       Ctrl<Key>l: action_label_display_()\n\
                       Ctrl<Key>s: action_slider_display_()\n\
		       <Key>1: action_menu_1()\n\
		       <Key>2: action_menu_2()\n\
		       <Key>3: action_menu_3()\n\
		       <Key>4: action_menu_4()\n\
		       <Key>5: action_menu_5()\n\
		       <Key>6: action_menu_6()\n\
		       <Key>7: action_menu_7()\n\
		       <Key>8: action_menu_8()\n\
		       <Key>9: action_menu_9()\n\
		       <Key>a: action_menu_a()\n\
		       <Key>A: action_menu_a()\n\
		       <Key>b: action_menu_b()\n\
		       <Key>B: action_menu_b()\n\
		       <Key>c: action_menu_c()\n\
		       <Key>C: action_menu_c()\n\
		       <Key>d: action_menu_d()\n\
		       <Key>D: action_menu_d()\n\
		       <Key>e: action_menu_e()\n\
		       <Key>E: action_menu_e()\n\
		       <Key>f: action_menu_f()\n\
		       <Key>F: action_menu_f()\n\
		       <Key>g: action_menu_g()\n\
		       <Key>G: action_menu_g()\n\
		       <Key>h: action_menu_h()\n\
		       <Key>H: action_menu_h()\n\
		       <Key>i: action_menu_i()\n\
		       <Key>I: action_menu_i()\n\
		       <Key>j: action_menu_j()\n\
		       <Key>J: action_menu_j()\n\
		       <Key>k: action_menu_k()\n\
		       <Key>K: action_menu_k()\n\
		       <Key>x: action_pan_start()\n\
		       <Key>X: action_pan_start()\n\
		       <KeyUp>x: action_pan_finish()\n\
		       <KeyUp>X: action_pan_finish()\n\
		       <Key>z: action_zoom_init()\n\
		       <Key>Z: action_zoom_init()\n\
		       <Key>space: action_continue()";

XtTranslations trans_table;

static char Io_trans[] = "<Key>Return: action_iobox()";
XtTranslations iobox_trans_table;

/*
 * callback declarations :
   icallback_value = -1 no event
                      0 read pointer
                      1 - 99 menu item
                     100 iobox
                     101 slider1
		     102 slider2
 		     103 zoom
                     104 rubber on
                     105 rubber off
		     106 return
 */
int		icallback_value;

/*
 * colour table declarations
 */
Colormap		colourmap;
XColor			colours[256];
int			colourmap_size;
unsigned long		colour_white;
unsigned long		colour_red;
unsigned long		colour_black;
unsigned long		colour_green;
unsigned long		colour_blue;
unsigned long		white;
unsigned long		red;
unsigned long		black;
unsigned long		green;
unsigned long		blue;
int			ncolours;
int 			colour_start;
int 			colour_entries;
XSetWindowAttributes	attributes;
unsigned long		valuemask;
// Tim's next 3 lines
char 			red_value;
char 			blue_value;
char 			green_value;


/*
 * Graphics context :
 */ 

GC		gc_draw_vector[2];
GC		gc_remove_vector;
GC		gc_fill_rectangle;
GC		gc_red_rectangle;
int		scr_num;
int		max_boxes = 16384;
int		max_circles = 16384;
int		max_points = 16384;
int		max_lines = 32768;
int		max_text = 16384;
int 		remove_vectors;
unsigned int	line_width = 0;
int		cap_style = CapButt;
int		join_style = JoinMiter;
int		dash_style;
int		style;

/*
 * Cursor :
 */

Cursor          cursor;
Pixmap		cursor_shape_pixmap;
Pixmap		cursor_mask_pixmap;
static char	cursor_shape[30];
static char	cursor_mask[30];
unsigned int	cursor_size = 15;
unsigned int	cursor_depth = 1;
XColor		cursor_background;
XColor		cursor_foreground;
unsigned int	cursor_x;
unsigned int	cursor_y;
unsigned int    ring_diam;
float		ring_rad;
int		ring_line;
int		arrow = XC_top_left_arrow;
int		cross = XC_crosshair;
int		circle = XC_circle;
int		hand = XC_hand2;
int		bird = XC_gobbler;
int		skull = XC_pirate;
int		centre_x;
int		centre_y;
int		crosshair_x;
int		crosshair_y;


/*
 * Font :
 */


XFontStruct	*font_default;
XFontStruct	*font_text;
Font		font_id;
char		fontname1[] = "*helvetica-bold-r-normal-*-17-120*100*";
char		fontname2[] = "*fixed-bold-r-normal-*-13-120-*-75-c-70*";
XCharStruct	min_bounds;
XCharStruct	max_bounds;
short int	ascent;
short int	descent;
short int	font_height;
/* extra font declarations for font choice */
char		*fonts[100];
char		**courier_fonts;
char		**fixed_fonts;
char		**helvetica_fonts;
char		**times_fonts;
char		courier_font[] = "*courier-bold-*";
char		fixed_font[] = "*fixed-bold-*";
char		helvetica_font[] = "*helvetica-bold-*";
char		times_font[] = "*times-bold-*";
int		font_number;
int		font_length = 64;
int		font_spacer = 4;
int		max_fonts = 20;
int		maxfontsperstyle = 20;
int		no_fonts;
int		total_fonts;
/* Tim hack next 2 lines */
int     	text_font_width;
int     	text_font_height;


/* default arrow cursor */

static char cursor_shape0[] =
	{
   0x00, 0x00, 0x10, 0x00, 0x30, 0x00, 0x70, 0x00, 
   0xf0, 0x00, 0xf0, 0x01, 0xf0, 0x03, 0xf0, 0x07, 
   0xf0, 0x0f, 0xf0, 0x1f, 0xf0, 0x03, 0xb0, 0x03,
   0x10, 0x03, 0x00, 0x06, 0x00, 0x06
	};

static char cursor_mask0[] =
	{
   0x00, 0x00, 0x10, 0x00, 0x30, 0x00, 0x70, 0x00, 
   0xf0, 0x00, 0xf0, 0x01, 0xf0, 0x03, 0xf0, 0x07, 
   0xf0, 0x0f, 0xf0, 0x1f, 0xf0, 0x03, 0xb0, 0x03,
   0x10, 0x03, 0x00, 0x06, 0x00, 0x06
	};

/* LSB server -  default arrow cursor */

static char cursor_shape0_lsb[] =
        {
   0xff, 0xff, 0xef, 0xff, 0xcf, 0xff, 0x8f, 0xff,
   0x0f, 0xff, 0x0f, 0xfe, 0x0f, 0xfc, 0x0f, 0xf8, 
   0x0f, 0xf0, 0x0f, 0xe0, 0x0f, 0xfc, 0x4f, 0xfc, 
   0xef, 0xfc, 0xff, 0xf9, 0xff, 0xf9
        };
        
static char cursor_mask0_lsb[] =
        {
   0xff, 0xff, 0xef, 0xff, 0xcf, 0xff, 0x8f, 0xff,
   0x0f, 0xff, 0x0f, 0xfe, 0x0f, 0xfc, 0x0f, 0xf8, 
   0x0f, 0xf0, 0x0f, 0xe0, 0x0f, 0xfc, 0x4f, 0xfc, 
   0xef, 0xfc, 0xff, 0xf9, 0xff, 0xf9
        };

/* fine crosshair cursor */


static char cursor_shape1[] =
	{
   0x01, 0x40, 0x02, 0x20, 0x04, 0x10, 0x08, 0x08, 
   0x10, 0x04, 0x20, 0x02, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x20, 0x02, 0x10, 0x04, 0x08, 0x08,
   0x04, 0x10, 0x02, 0x20, 0x01, 0x40
	};

static char cursor_mask1[] = 
	{
   0x01, 0x40, 0x02, 0x20, 0x04, 0x10, 0x08, 0x08, 
   0x10, 0x04, 0x20, 0x02, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x20, 0x02, 0x10, 0x04, 0x08, 0x08,
   0x04, 0x10, 0x02, 0x20, 0x01, 0x40
	};

/* LSB server -  fine crosshair cursor */

static char cursor_shape1_lsb[] =
        {
   0xfe, 0xbf, 0xfd, 0xdf, 0xfb, 0xef, 0xf7, 0xf7,
   0xef, 0xfb, 0xdf, 0xfd, 0xff, 0xff, 0xff, 0xff, 
   0xff, 0xff, 0xdf, 0xfd, 0xef, 0xfb, 0xf7, 0xf7, 
   0xfb, 0xef, 0xfd, 0xdf, 0xfe, 0xbf
        };
        
static char cursor_mask1_lsb[] =
        {
   0xfe, 0xbf, 0xfd, 0xdf, 0xfb, 0xef, 0xf7, 0xf7,
   0xef, 0xfb, 0xdf, 0xfd, 0xff, 0xff, 0xff, 0xff, 
   0xff, 0xff, 0xdf, 0xfd, 0xef, 0xfb, 0xf7, 0xf7,
   0xfb, 0xef, 0xfd, 0xdf, 0xfe, 0xbf
        };

/* box crosshair */

static char cursor_shape2[] =
	{
   0xc0, 0x81, 0xc0, 0x81, 0xc0, 0x81, 0xc0, 0x81, 
   0xf0, 0x87, 0x10, 0x84, 0x1f, 0xfc, 0x1f, 0xfc, 
   0x1f, 0xfc, 0x10, 0x84, 0xf0, 0x87, 0xc0, 0x81,
   0xc0, 0x81, 0xc0, 0x81, 0xc0, 0x81
	};

static char cursor_mask2[] = 
	{
   0xc0, 0x81, 0xc0, 0x81, 0xc0, 0x81, 0xc0, 0x81, 
   0xf0, 0x87, 0x10, 0x84, 0x1f, 0xfc, 0x1f, 0xfc, 
   0x1f, 0xfc, 0x10, 0x84, 0xf0, 0x87, 0xc0, 0x81,
   0xc0, 0x81, 0xc0, 0x81, 0xc0, 0x81
	};

/* LSB server -  box crosshair */

static char cursor_shape2_lsb[] =
        {
   0x3f, 0x7e, 0x3f, 0x7e, 0x3f, 0x7e, 0x3f, 0x7e,
   0x0f, 0x78, 0xef, 0x7b, 0xe0, 0x03, 0xe0, 0x03, 
   0xe0, 0x03, 0xef, 0x7b, 0x0f, 0x78, 0x3f, 0x7e,
   0x3f, 0x7e, 0x3f, 0x7e, 0x3f, 0x7e
        };

static char cursor_mask2_lsb[] =
        {
   0x1f, 0x7c, 0x1f, 0x7c, 0x1f, 0x7c, 0x1f, 0x7c,
   0x0f, 0x78, 0xe0, 0x03, 0xe0, 0x03, 0xe0, 0x03, 
   0xe0, 0x03, 0xe0, 0x03, 0x0f, 0x78, 0x1f, 0x7c,
   0x1f, 0x7c, 0x1f, 0x7c, 0x1f, 0x7c
        };

/* centre giant crosshair cursor */

static char cursor_shape3_lsb[] =
	{
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};

static char cursor_mask3_lsb[] =
	{
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
   0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
	};

/* centre giant crosshair cursor */

static char cursor_shape3[] =
	{
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};

static char cursor_mask3[] = 
	{
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
	};

/*
 * Box arrays
*/

int		box_x[16384];
int		box_y[16384];
unsigned int	box_width[16384];
unsigned int	box_height[16384];
int		box_style[16384];
int		nboxes = 0;

int		box_x_overlay[16384];
int		box_y_overlay[16384];
unsigned int	box_width_overlay[16384];
unsigned int	box_height_overlay[16384];
int		nboxes_overlay = 0;

/*
 * Circle arrays
 */

int		irad;
int		circle_x[16384];
int		circle_y[16384];
int		circle_diam[16384];
int		circle_style[16384];
int		circle_angle = 360 * 64;
int		ncircles = 0;

int		circle_x_overlay[16384];
int		circle_y_overlay[16384];
int		circle_diam_overlay[16384];
int		ncircles_overlay = 0;

/*
 * Line arrays
 */

int		line_x1[32768];
int		line_y1[32768];
int		line_x2[32768];
int		line_y2[32768];
int		line_style[32768];
XSegment	segments[32768];
int		mlines;
int		nlines = 0;

int		line_x1_overlay[32768];
int		line_y1_overlay[32768];
int		line_x2_overlay[32768];
int		line_y2_overlay[32768];
XSegment	segments_overlay[32768];
int		nlines_overlay = 0;

/*
 * Text arrays
 */

int		text_x[16384];
int		text_y[16384];
char		text_string[16384][80];
int		text_lengths[16384];
Font		text_font[16384];
int		ntext = 0;
int		string_length = 80;

int		text_x_overlay[16384];
int		text_y_overlay[16384];
char		text_string_overlay[16384][80];
int		text_lengths_overlay[16384];
int		ntext_overlay = 0;

/*
 * Point arrays
 */

int		point_x[16384];
int		point_y[16384];
int		npoints = 0;

int		point_x_overlay[16384];
int		point_y_overlay[16384];
int		npoints_overlay = 0;

/*
 * overlay area declarations, note array *16 to allow for 24-bit colour
 */
int		max_overlay_size = 1034 * 1034;
static char	overlay_pointer[4*1034*1034];
int		overlay_flag;
XImage		*overlay_map;
Pixmap		overlay_pixmap;
Pixmap		overlay_store_pixmap;
int		overlay_height;
int		overlay_width;
int		overlay_coord_x;
int		overlay_coord_y;
int		overlay_pointer_x;
int		overlay_pointer_y;
int		overlay_window_x;
int		overlay_window_y;


/*
 * zoom area declarations, note array *4 to allow for 
24-bit colour,
   use overlay window.
 */
int		zoom_height;
int		zoom_width;
XImage		*zoom_map;
int		zoom_factor_x;
int		zoom_factor_y;
int		zoom_image_pointer_x;
int		zoom_image_pointer_y;
int		zoom_pointer_x;
int		zoom_pointer_y;
Pixmap		zoom_pixmap;
Pixmap		zoom_store_pixmap;
int		zoom_window_x1;
int		zoom_window_y1;
int		zoom_window_x2;
int		zoom_window_y2;
int		zoom_flag;
int		zoom_x1;
int		zoom_y1;
int		zoom_x2;
int		zoom_y2;
int		zoom_x3;
int		zoom_y3;
int		zoom_x4;
int		zoom_y4;
int		zoom_centre_x;
int		zoom_centre_y;
unsigned int	zoom_diam_x;
unsigned int	zoom_diam_y;

/*
 * Event declarations 
 */
XEvent		event_return;
int		event_flag = 0;
int		event_proc = 0;

/*
 * external fortran declarations :
*/
extern  int ximagecallback_();
extern  int ximagedrawlines_();
extern  int ximagemenudisplay_();
extern  int ximagemenuhide_();

/*
 * iobox declarations
 */
char			store_string[80] = {" "};
int			expose_iobox_flag;
int			iobox_flag;
Dimension               iobox_gap = 20;


/*
 * label declarations
 */
int			expose_label_flag;
int			label_flag;

/*
 * menu declarations
 */
static	char		*menu_pointer[100];
int			expose_menu_flag;
int			menu_flag;

/*
 * map declarations
 */
//static char	image_pointer[4*8192*8192];
unsigned char	image_pointer[4*8192*8192];
unsigned int	image_depth = 24;
unsigned int	image_height;
unsigned int	image_width;
unsigned int	map_height;
unsigned int	map_width;
int		max_map_size = 8192 * 8192;
int		map_size;
int		image_format;
int		image_offset = 0;
int		image_bitmap_pad = 8;
int		image_bytes_per_line = 0;
int 		image_source_x = 0;
int 		image_source_y = 0;
int 		image_dest_x;
int 		image_dest_y;
XImage		*image;
Pixmap		image_pixmap;
int     	image_pointer_x;
int	        image_pointer_y;
int		image_flag;
XImage		*readimage;
static char	*dataline;

/* colour bar declarations */
XImage		*colour_bar_image;
int		colour_bar_flag;
static char	colour_bar_pointer[4*25*128];
char		colour_bar_map[25][128];

/* pan pixmap declarations */
Pixmap		screen_pixmap[256];
Pixmap		store_pixmap[256];
int        	no_maps;
int             map_no;
int		max_maps = 512;
int		pan_pointer_x1;
int		pan_pointer_y1;
int		pan_pointer_x2;
int		pan_pointer_y2;
int		pan_x;
int		pan_y;
int		pan_min = 10;
int		pan_flag;

/* pointer tracking declarations */
int		ichange_origin;
int		pointer_flag;
int		pointer_text_length;
int		pointer_x;
int		pointer_y;
int		pointer_shift_max = 10;
int		pointer_shift_x;
int		pointer_shift_y;
int		pointer_save_x;
int		pointer_save_y;
int		pointer_factor;
char		pointer_text[20];

/* rubber banding declarations */
float		rubber_angle;
float		rubber_radius;
float		rubber_halfwidth;
float		pio2 = 1.57;
int		rubber_circle_x;
int		rubber_circle_y;
unsigned int	rubber_circle_diam;
int		rubber_sin;
int		rubber_cos;
int		rubberline_x1[5];
int		rubberline_y1[5];
int		rubberline_x2[5];
int		rubberline_y2[5];
int		rubber_flag;
int		rubber_min_x;
int		rubber_min_y;
int		rubber_max_x;
int		rubber_max_y;
int		rubber_pointer_x;
int		rubber_pointer_y;
int		rubber_size_x;
int		rubber_size_y;
int		rubber_start_x;
int		rubber_start_y;
int		rubber_finish_x;
int		rubber_finish_y;
int		rubber_type;
int		rubber_x1;
int		rubber_y1;
int		rubber_x2;
int		rubber_y2;

/* slider bar declarations */
float   	slider_position;
int		expose_slider_flag;
int		ksliders;
int		slider_flag;
float		slider1_start_position;
float		slider2_start_position;
float		slider_size = 0.05;
Dimension	slider_height = 25;
Dimension	slider_width = 128;

/******************************************************************/
/* list of functions */
/******************************************************************/
void	convert_to_image (int x_zoom, int y_zoom,
			  int *x_image, int *y_image);
void	convert_to_zoom (int x_image,int y_image,
			 int *x_zoom,int *y_zoom);
void    expose_iobox ();
void    expose_label ();
void    expose_menu ();
void    expose_slider ();
void	delay_ (int *idelay);
void	giant_cursor_draw ();
void	giant_cursor_remove ();
void    ring_cursor_draw ();
void    ring_cursor_remove ();
void	pointer_track ();
void	pointertrackon_ (int *point_shift_x, int *point_shift_y, 
		 int *iturn_origin, int *ifactor);
void	pointertrackoff_ ();
void    redraw_image (int *image_refresh);
void 	redraw_overlay (int *overlay_refresh);
void	redrawvectors_ ();
void 	redraw_zoom (int *zoom_refresh);
void 	readpointer_ (int *image_pointer_x, int *image_pointer_y);
void 	readoverlaypointer_ (int *overlay_coord_x, int *overlay_coord_y);
void 	rubberenable_ (int *rflag,int *iboxwidth);
void 	rubberdragbox ();
void 	rubberdrawbox ();
void 	rubberdrawcircle ();
void 	rubberdrawline ();
void 	rubberread_ (int *ix1, int *iy1, int *ix2, int *iy2);
void 	rubberremoveline_ (int *ix1, int *iy1, int *ix2, int *iy2);
void 	readslider_ (float *slider_percent);
void	action_iobox ();
void	action_label_display_ ();
void	action_menudisplay_ ();
void 	action_read_pointer ();
void 	action_pan_start ();
void 	action_pan_finish ();
void	action_menu_1 ();
void	action_menu_2 ();
void	action_menu_3 ();
void	action_menu_4 ();
void	action_menu_5 ();
void	action_menu_6 ();
void	action_menu_7 ();
void	action_menu_8 ();
void	action_menu_9 ();
void	action_menu_a ();
void	action_menu_b ();
void	action_menu_c ();
void	action_menu_d ();
void	action_menu_e ();
void	action_menu_f ();
void	action_menu_g ();
void	action_menu_h ();
void	action_menu_i ();
void	action_menu_j ();
void	action_menu_k ();
void	action_continue ();
void 	action_rubber_read ();
void 	action_rubber_pause ();
void 	action_rubber_finish ();
void	action_slider_display_ ();
void	action_slider_callback (Widget scrollbar, XtPointer client_data,
			  XtPointer percent);
void 	action_zoom_init ();
void    changecursor_ (int *icurs, unsigned int *iring_rad);
void	changefont_ (int *fontnumber);
void	changezoom_ (int *zoom_fx, int *zoom_fy,
		 unsigned int *zoom_w, unsigned int *zoom_h);
void	checkimage_ (int *nx, int *ny, int *nmaps);
void	clearimage_ ();
void	colourbarinit();
void	colourbarhide();
void	delay_ (int *idelay);
void	drawbox_ (int *ix1, int *iy1, int *ix2, int *iy2);
void 	drawcircle_ (int *ix, int *iy, float *rad);
void    drawlines_  (int *ix1, int *iy1, int *ix2, int *iy2, int *ilines);
void	drawimage_  (int *nx, int *ny, int *isec, unsigned char *map, int *cflag);
void 	drawpixmap_ (int *isec, int *image_refresh);
void	drawoverlay_ (int *overlay_refresh);
void	drawpoint_   (int *ix, int *iy);
void 	drawtext_   (int *ix, int *iy, 
		char *text_pointer, int *length);
int	error_trap(Display *error_id, XErrorEvent *p_error);
void 	event_get_next_ (int *event_true);
void 	eventdispatch_ ();
void	getfontsize_ (int *fontsize);
void	getioboxwidth_ (int *ioboxwidth);
void	init_ (int *max_window_width,  int *max_window_height,
	       int *max_zoom_size, char *fontlist, int *nfonts);
void	ioboxdisplay_ (char *out_string, char *edit_string, int *istrings);
void	ioboxreadstring_ (char *in_string);
void	ioboxhide_ ();
void	labeldisplay_ (char *label_string);
void	labelhide_ ();
void	menu_callback(Widget w, XtPointer client_data,
			XawListReturnStruct *call_data);
void	menuevent_();
void 	menuhide_ ();
void	menuinit_ (int *nitems, int *max_length, char *menulist);
void	overlayhide_ ();
void	overlayinit_ (int *ix, int *iy, int *overlay_w, 
			int *overlay_h, unsigned char *omap);
void	readimage_  (int *icol, int *irow, int *ipixmap, unsigned char *pixels);
void	setcolourtable_ 
	(int *index_start, int *nentries, 
	int *red_pointer, int *green_pointer, int *blue_pointer);
void	quit_ ();
void	removebox_ (int *ix1, int *iy1, int *ix2, int *iy2);
void 	removecircle_ 
	(int *ix, int *iy,float *rad);
void 	removelines_ 
	(int *ix1, int *iy1, int *ix2, int *iy2, int *ilines);
void	removepoint_ (int *ix, int *iy);
void	removetext_ (int *ix, int *iy, char *text_pointer, int *length);
void 	removevectors_ ();
void	setdash_ ();
void	sliderhide_ ();
void	sliderinit_ (int *nsliders, float *slider1_start, 
                                     float *slider2_start);
void	setsolid_ ();
void	zoominit_ ();
void 	zoom_display ();
void 	zoomhide_ ();
/*********************************************************************/
/*	routines to be called before action routines */
/*********************************************************************/
/*********************************************************************/
/* change font type */
/*********************************************************************/
void	changefont_ (int *fontnumber)
{
	if((font_text = 
		XLoadQueryFont(display_id, fonts[*fontnumber])) == NULL)
		{
		fprintf(stderr,"Cannot load font number %d\n",*fontnumber);
		*fontnumber = -*fontnumber;
		return;
		}
	font_id = font_text->fid;
	XSetFont(display_id, gc_draw_vector[0], font_id);
	XSetFont(display_id, gc_draw_vector[1], font_id);

/* Tim hack next 2 lines */
        text_font_width = font_text->max_bounds.rbearing - font_text->min_bounds.lbearing;
        text_font_height = font_text->max_bounds.ascent + font_text->max_bounds.descent;

}
/*********************************************************************/
/* change zoom parameters */
/*********************************************************************/
void	changezoom_ (int *zoom_fx, int *zoom_fy, 
		unsigned int *zoom_w, unsigned int *zoom_h)
{

/* hide and destroy the zoom window if already present */
	if(zoom_flag != 0) 
		zoomhide_();

/* hide overlay window if present */
	if(overlay_flag != 0) overlayhide_ ();

/* initialize */
	zoom_width = *zoom_w;
	zoom_height = *zoom_h;
	zoom_factor_x = *zoom_fx;
	zoom_factor_y = *zoom_fy;

/* image too large for array size ? */
	if(zoom_width * zoom_height > max_overlay_size)
	  {
		printf("error - zoom area too large \n");
		icallback_value = 999;
		ximagecallback_ (&icallback_value);
	  }

/* force even zoom factors, integral odd number in zoom area
	width and height */
	if(zoom_factor_x != 1)
	{
		zoom_factor_x = (zoom_factor_x / 2) * 2;
		zoom_width = ((((zoom_width / zoom_factor_x) / 2) * 2) + 1) 
			* zoom_factor_x;
	}

	if(zoom_factor_y != 1)
	{
		zoom_factor_y = (zoom_factor_y / 2) * 2;
		zoom_height = ((((zoom_height / zoom_factor_y) / 2) * 2) + 1) 
			* zoom_factor_y;
	}

/* create image */
	zoom_map = XCreateImage(display_id,visual_id,
		     image_depth, image_format, image_offset,
		     &overlay_pointer[0], 
		     (unsigned int)zoom_width, (unsigned int)zoom_height,
		     image_bitmap_pad, image_bytes_per_line);

/* create pixmaps */
	zoom_pixmap = XCreatePixmap(display_id,zoom_area_window_id,
		(unsigned int)zoom_width, (unsigned int)zoom_height, 
		image_depth);

	zoom_store_pixmap = XCreatePixmap(display_id,zoom_area_window_id,
		(unsigned int)zoom_width, (unsigned int)zoom_height, 
		image_depth);

/* test resources are sufficient by synchronizing calls to server
	and calling up error trap if create pixmap fails */
	XSync(display_id, False);
	if (icallback_value == 999) return;

/* copy pixmap into image */
	XPutImage(display_id, zoom_pixmap,
		  gc_fill_rectangle,zoom_map,image_source_x, image_source_y,
		  image_source_x, image_source_y, 1,1);

	zoom_flag = 1;
	zoomhide_();
}
/*********************************************************************/
/*	Hide zoom area */
/*********************************************************************/
void	zoomhide_ ()
{
/* Hide the zoom window if present */
	if(zoom_flag == 1)
	{
		zoom_map->data = 0;
		XDestroyImage(zoom_map);	
		XFreePixmap(display_id, zoom_pixmap);
//		XFreePixmap(display_id, zoom_store_pixmap);
		XtUnmanageChild(zoom_area);
		XSync(display_id, False);
	}
	zoom_flag = 0;
}
/*********************************************************************/
/* Convert zoom coords to image */
/*********************************************************************/
void convert_to_image (int x_zoom, int y_zoom, 
		       int *x_image, int *y_image)
/* to zoom an area, a pixel position is chosen to become the 'centre'.
   It lies one pixel to the right and down from the true box centre
   which must be between pixels as only even zoom factors are allowed. */
{
	int	shift;

/* calculate shift according to whether coord lies in right or 
   left side of box. Note pixel coords start at 0,0 */

	if(x_zoom > zoom_width / 2) 
		shift = zoom_factor_x / 2;
	else
		shift = -zoom_factor_x / 2 + 1;

	*x_image = zoom_pointer_x
		- (zoom_width/2 - x_zoom - shift) / zoom_factor_x;

/* calculate shift according to whether coord lies in top or 
	bottom part of box */

	if(y_zoom > zoom_height / 2) 
		shift = zoom_factor_y / 2;
	else
		shift = -zoom_factor_y / 2 + 1;

	*y_image = zoom_pointer_y
		- (zoom_height/2 - y_zoom - shift) / zoom_factor_y;
}
/*********************************************************************/
/* Convert image coords to zoom */
/*********************************************************************/
void convert_to_zoom (int x_image, int y_image,
		      int *x_zoom, int *y_zoom) 
{
	*x_zoom = zoom_width / 2 
		+ zoom_factor_x * (x_image - zoom_pointer_x + pan_x);

	*y_zoom = zoom_height / 2 
		+ zoom_factor_y * (y_image - zoom_pointer_y + pan_y);
}
/*********************************************************************/
/* Handle iobox exposure */
/*********************************************************************/
void 	expose_iobox ()
{
	expose_iobox_flag = 1;
}
/*********************************************************************/
/* Handle label exposure */
/*********************************************************************/
void 	expose_label ()
{
	expose_label_flag = 1;
}
/*********************************************************************/
/* Handle menu exposure */
/*********************************************************************/
void 	expose_menu ()
{
	expose_menu_flag = 1;
}
/*********************************************************************/
/* Handle slider exposure */
/*********************************************************************/
void 	expose_slider ()
{
	expose_slider_flag = 1;
}
/*********************************************************************/
/* Draw giant crosshair cursor */
/*********************************************************************/
void	giant_cursor_draw ()
{
/* redraw image each time to clear old cursor if pointer tracking is off */
	if(pointer_flag == 2)
	        redraw_image (&image_refresh);

	crosshair_x = image_pointer_x + pan_x;
	crosshair_y = image_pointer_y + pan_y;

	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 0, crosshair_y, crosshair_x-5, crosshair_y);
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 crosshair_x+5, crosshair_y, window_width, crosshair_y);

	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	crosshair_x, 0, crosshair_x, crosshair_y-5);
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	crosshair_x, crosshair_y+5, crosshair_x, window_height);


/* draw new crosshair in zoom window */
	if(zoom_flag == 1)
	{
		redraw_zoom (&zoom_refresh);

		convert_to_zoom
		(image_pointer_x, image_pointer_y, &zoom_x1, &zoom_y1);

		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 0, zoom_y1, zoom_x1-5, zoom_y1);
		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 zoom_x1+5, zoom_y1, zoom_width, zoom_y1);

		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 zoom_x1 , 0, zoom_x1, zoom_y1-5);
		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 zoom_x1, zoom_y1+5, zoom_x1, zoom_height);
			 
	}
}
/*********************************************************************/
/* Remove giant crosshair cursor */
/*********************************************************************/
void	giant_cursor_remove ()
{
	crosshair_x = crosshair_x + pan_x;
	crosshair_y = crosshair_y + pan_y;

	redraw_image (&image_refresh);		

/* reset crosshair coords without pan for screen pixmap */
	crosshair_x = crosshair_x - pan_x;
	crosshair_y = crosshair_y - pan_y;

/* repeat operation in zoom window */
	if (zoom_flag == 1) redraw_zoom (&zoom_refresh);

/* redraw vector list if crosshair moved by more than 5 pixels */
	if(abs(crosshair_x - pointer_save_x) > pointer_shift_max ||
	   abs(crosshair_y - pointer_save_y) > pointer_shift_max)
		{
		redrawvectors_ ();
		pointer_save_x = crosshair_x;
		pointer_save_y = crosshair_y;
		}
}
/*********************************************************************/
/* Draw ring crosshair cursor */
/*********************************************************************/
void	ring_cursor_draw ()
{
/* redraw image each time to clear old cursor if pointer tracking off */
        if(pointer_flag == 4)
		redraw_image (&image_refresh);

	crosshair_x = image_pointer_x + pan_x;
	crosshair_y = image_pointer_y + pan_y;
	centre_x = crosshair_x + (int)ring_rad;
	centre_y = crosshair_y + (int)ring_rad;

/* draw circle in image area */
	XDrawArc
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 crosshair_x, crosshair_y, ring_diam, ring_diam,
         circle_angle, circle_angle);

/* draw crosshairs in image area */
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 centre_x-ring_line, centre_y, centre_x-5, centre_y);
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 centre_x+5, centre_y, centre_x+ring_line, centre_y);
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 centre_x, centre_y+ring_line, centre_x, centre_y+5);
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	 centre_x, centre_y-5, centre_x, centre_y-ring_line);

/* draw new crosshair in zoom window */
	if(zoom_flag == 1)
	{
		redraw_zoom (&zoom_refresh);

		convert_to_zoom
		(image_pointer_x, image_pointer_y, &zoom_x1, &zoom_y1);

		zoom_diam_x = ring_diam * zoom_factor_x;
		zoom_diam_y = ring_diam * zoom_factor_y;
		centre_x = zoom_x1 + zoom_diam_x / 2;
		centre_y = zoom_y1 + zoom_diam_y / 2;

/* draw circle and crosshairs into zoom area */
        	XDrawArc
	        (display_id, zoom_area_window_id, gc_draw_vector[dash_style],
	        zoom_x1, zoom_y1, zoom_diam_x, zoom_diam_y,
                circle_angle, circle_angle);

		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
	 	centre_x-ring_line, centre_y, centre_x-5, centre_y);
		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
	 	centre_x+5, centre_y, centre_x+ring_line, centre_y);
		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 centre_x, centre_y+ring_line, centre_x, centre_y+5);
		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 centre_x, centre_y-5, centre_x, centre_y-ring_line);

	}
}
/*********************************************************************/
/* Remove ring crosshair cursor */
/*********************************************************************/
void	ring_cursor_remove ()
{
	redraw_image (&image_refresh);		

/* repeat operation in zoom window */
	if (zoom_flag == 1) redraw_zoom (&zoom_refresh); 


/* redraw vector list if crosshair moved by more than 5 pixels */
	if(abs(crosshair_x - pointer_save_x) > pointer_shift_max ||
	   abs(crosshair_y - pointer_save_y) > pointer_shift_max)
		{
		redrawvectors_ ();
		pointer_save_x = crosshair_x;
		pointer_save_y = crosshair_y;
		}
}
/*********************************************************************/
/* Draw pointer position to screen */
/*********************************************************************/
void 	pointer_track ()
{
/* note : pointer_flag = 0 - no pointer tracking
		       = 1 - reflects coords top rh corner
		       = 2 - draws giant crosshair cursor
		       = 3 - reflects coords and draws giant cursor
		       = 4 - draws ring cursor
                       = 5 - reflects coords and draws ring cursor
*/
	int pointer_coord_x;
	int pointer_coord_y;

	if(pointer_flag == 0) return;

/* remove existing coordinates from top rh screen */
	if(pointer_flag == 1 || pointer_flag == 3 || pointer_flag == 5)
		{
// 11.07.2012 replaced this
//			if(overlay_flag == 0)
//			{
//                      redraw_image (&image_refresh);		
//                        redraw_zoom (&zoom_refresh);
//			}
//			else
//			redraw_overlay (&overlay_refresh);		
                        redraw_image (&image_refresh);		
                        redraw_zoom (&zoom_refresh);
			if(overlay_flag != 0)
			redraw_overlay (&overlay_refresh);		
		}
/* read pointer position */
	readpointer_ (&image_pointer_x, &image_pointer_y);

/* apply x shift to reflect actual map coordinates */
	if(pointer_flag == 1 || pointer_flag == 3 || pointer_flag == 5)
	{
		pointer_coord_x = image_pointer_x * pointer_factor
						+ pointer_shift_x;

/* apply y shift and y origin flip if specified */
		if(ichange_origin != 0) 
			{
			pointer_coord_y = (ichange_origin - image_pointer_y)
					* pointer_factor + pointer_shift_y;
			}
		else
			{
			pointer_coord_y = image_pointer_y * pointer_factor 
					+ pointer_shift_y;
			}
/* convert coords to character string */
		sprintf(pointer_text,"%d %d",
				pointer_coord_x,pointer_coord_y);
		pointer_text_length = strlen(pointer_text);

/* draw to screen */
		XDrawString(display_id,image_area_window_id,
			gc_draw_vector[0], pointer_x, pointer_y,
			pointer_text, pointer_text_length);
	}

/* remove old crosshair  and draw new one */
	if(pointer_flag > 1)
		{ 
	        if(pointer_flag < 4)
		       {
		       giant_cursor_draw ();
		       }
	        else
		       {
//			 image_pointer_x = image_pointer_x - (int)ring_rad;
//			 image_pointer_y = image_pointer_y - (int)ring_rad;
			 ring_cursor_draw ();
                       }
		}

}
/*********************************************************************/
/* Enable pointer tracking */
/*********************************************************************/
void	pointertrackon_
		(int *point_shift_x, int *point_shift_y, 
		 int *iturn_origin, int *ifactor)
{
	XtAddEventHandler(image_area, PointerMotionMask, False,
			  (XtEventHandler)pointer_track,0);
	XtAddEventHandler(zoom_area, PointerMotionMask, False,
			  (XtEventHandler)pointer_track,0);
	pointer_flag = pointer_flag + 1;
	ichange_origin = *iturn_origin;
	pointer_shift_x = *point_shift_x;
	pointer_shift_y = *point_shift_y;
	pointer_save_x = *point_shift_x;
	pointer_save_y = *point_shift_y;
	pointer_factor = *ifactor;
}
/*********************************************************************/
/* Disable pointer tracking */
/*********************************************************************/
void 	pointertrackoff_ ()
{
	if(pointer_flag == 1 || pointer_flag == 3 || pointer_flag == 5)
	{
/* remove existing coordinates from screen */
		redraw_image (&image_refresh);
		pointer_flag = pointer_flag - 1;
	}
/* remove event handler */
	if(pointer_flag == 0)
	{
		XtRemoveEventHandler(image_area, PointerMotionMask, 
				False, (XtEventHandler)pointer_track,0);
		XtRemoveEventHandler(zoom_area, PointerMotionMask, 
				False, (XtEventHandler)pointer_track,0);
	}
}
/*********************************************************************/
/* Redraw main image */
/*********************************************************************/
void 	redraw_image (int *refresh_image)
{
	image_refresh = *refresh_image;
        drawpixmap_ (&map_no, &image_refresh);

/* always reset flag to 0 after draw pixmap */
	image_refresh = 0;
}
/*********************************************************************/
/* Redraw overlay area */
/*********************************************************************/
void 	redraw_overlay (int *refresh_overlay)
{
	if(abs(*refresh_overlay) > 1) 
		overlay_refresh = 0;
	else
		overlay_refresh = *refresh_overlay;
	drawoverlay_ (&overlay_refresh);
	overlay_refresh = 0;
}
/*********************************************************************/
/* Redraw vectors */
/*********************************************************************/
void 	redrawvectors_ ()
/*********************************************************************/
{
	int		nvectors;
	int		ix;
        int		iy;
	float		rad;
	int		rline_x1[32768];
	int		rline_y1[32768];
	int		rline_x2[32768];
	int		rline_y2[32768];
	int		ixtemp;
	int		iytemp;
int junk;

	if(overlay_flag == 0)
	{
/* Draw over all boxes in list */
		nvectors = nboxes;
/* set nboxes to 0 as it will be reset in drawbox */
		nboxes = 0;
		for ( n = 0; n < nvectors; n++)
		{
		ix = box_x[n];
		iy = box_y[n];
		ixtemp = ix + abs(box_width[n]);
		iytemp = iy + abs(box_height[n]);
		drawbox_ (&ix, &iy, &ixtemp, &iytemp);
		}

/* Draw over all circles in list */
		nvectors = ncircles;
		ncircles = 0;
		for ( n = 0; n < nvectors; n++)
		{
		rad = (float)circle_diam[n] * 0.5;
		ix = circle_x[n] + (int)rad;
		iy = circle_y[n] + (int)rad;
		drawcircle_(&ix,&iy,&rad);
		}

/* Draw over all lines in list */
		for (n = 0; n < nlines; n++)
		{
		rline_x1[n] = line_x1[n];
		rline_y1[n] = line_y1[n];
		rline_x2[n] = line_x2[n];
		rline_y2[n] = line_y2[n];
		}
		mlines = nlines;
		nlines = 0;
		drawlines_ (rline_x1, rline_y1, rline_x2, rline_y2, &mlines);

/* Draw over all points in list */
		nvectors = npoints;
		npoints = 0;
		for ( n = 0; n < nvectors; n++)
		drawpoint_ (&point_x[n], &point_y[n]);

/* Draw over all text in list */
		nvectors = ntext;
		ntext = 0;
		for ( n = 0; n < nvectors; n++)
		{
		ixtemp = text_x[n];
		iytemp = text_y[n];
		drawtext_ (&ixtemp, &iytemp, 
		      &text_string[n][0], &text_lengths[n]);
		}
	}
/* draw in overlay window */
	else
	{
/* Draw over all boxes in list */
		nvectors = nboxes_overlay;
		nboxes_overlay = 0;
		for ( n = 0; n < nvectors; n++)
		{
		ix = box_x_overlay[n];
		iy = box_y_overlay[n];
		ixtemp = ix + abs(box_width_overlay[n]);
		iytemp = iy + abs(box_height_overlay[n]);
		drawbox_ (&ix, &iy, &ixtemp, &iytemp);
		}

/* Draw over all circles in list */
		nvectors = ncircles_overlay;
		ncircles_overlay = 0;
		for ( n = 0; n < nvectors; n++)
		{
		rad = (float)circle_diam_overlay[n] * 0.5;
		ix = circle_x_overlay[n] + (int)rad;
		iy = circle_y_overlay[n] + (int)rad;
		drawcircle_(&ix,&iy,&rad);
		}

/* Draw over all lines in list */
		for (n = 0; n < nlines_overlay; n++)
		{
		rline_x1[n] = line_x1_overlay[n];
		rline_y1[n] = line_y1_overlay[n];
		rline_x2[n] = line_x2_overlay[n];
		rline_y2[n] = line_y2_overlay[n];
		}
		mlines = nlines_overlay;
		nlines_overlay = 0;
		drawlines_ 
                (&rline_x1[n], &rline_y1[n], &rline_x2[n], &rline_y2[n], &mlines);

/* Draw over all points in list */
		nvectors = npoints_overlay;
		npoints_overlay = 0;
		for ( n = 0; n < nvectors; n++)
		drawpoint_ (&point_x_overlay[n], &point_y_overlay[n]);

/* Draw over all text in list */
		nvectors = ntext_overlay;
		ntext_overlay = 0;
		for ( n = 0; n < nvectors; n++)
		{
		ixtemp = text_x_overlay[n];
		iytemp = text_y_overlay[n];
		drawtext_ (&ixtemp, &iytemp, 
		      &text_string_overlay[n][0], &text_lengths_overlay[n]);
		}
	}
}
/*********************************************************************/
/* Redraw zoom area */
/*********************************************************************/
void 	redraw_zoom (int *zoom_refresh)
{

/* copy the zoom map into the zoom pixmap */

	if(zoom_flag == 1)
	  {

/* copy original stored pixmap to zoom pixmap */
	if(*zoom_refresh == 1)
 		XCopyArea(display_id, zoom_store_pixmap, zoom_pixmap,
 		  gc_fill_rectangle, 0, 0, 
 	          (unsigned int)zoom_width+2, (unsigned int)zoom_height+2, 
                     0, 0);

		XCopyArea(display_id, zoom_pixmap, zoom_area_window_id,
		  gc_fill_rectangle, 0, 0, 
	          (unsigned int)zoom_width+2, (unsigned int)zoom_height+2,
                    0, 0);

		XSync(display_id, False);
		zoom_refresh = 0;
	
	}
}
/*********************************************************************/
/* Read Pointer position */
/*********************************************************************/
void 	readpointer_ (int *image_pointer_x, int *image_pointer_y)
{
	Window		child_id;
	int		window_x;
	int		window_y;
	unsigned int 	keys_buttons;

/* read pointer coords wrt top left corner of window(not image) */
	XQueryPointer
	(display_id,
	 image_area_window_id,
	 &root_id,
	 &child_id,
	 &root_x,
	 &root_y,
	 &window_x,
	 &window_y,
	 &keys_buttons);
/* check to see if coordinates in zoom area, if so translate coords */
	if(zoom_flag == 1 &&
	   window_x >= zoom_window_x1 && window_x <= zoom_window_x2 &&
	   window_y >= zoom_window_y1 && window_y <= zoom_window_y2)

/* convert zoom to image coordinates */
		{
	XQueryPointer
	(display_id,
	 zoom_area_window_id,
	 &root_id,
	 &child_id,
	 &root_x,
	 &root_y,
	 &window_x,
	 &window_y,
	 &keys_buttons);
		convert_to_image(window_x, 
				 window_y,
				 &zoom_x1, &zoom_y1);
	 	*image_pointer_x = zoom_x1 - pan_x;	 
	 	*image_pointer_y = zoom_y1 - pan_y;	 
		}
	else
/* return coordinates origin top left image, not window */
		{
		 *image_pointer_x = window_x - pan_x;	 
		 *image_pointer_y = window_y - pan_y;	 
		}
}
/*********************************************************************/
/* Read Pointer position from overlay window */
/*********************************************************************/
void 	readoverlaypointer_ (int *overlay_coord_x, int *overlay_coord_y)
{
	Window		child_id;
	int		window_x;
	int		window_y;
	unsigned int 	keys_buttons;

/* read pointer coords wrt top left corner of window(not image) */
	XQueryPointer
	(display_id,
	 overlay_area_window_id,
	 &root_id,
	 &child_id,
	 &root_x,
	 &root_y,
	 &window_x,
	 &window_y,
	 &keys_buttons);
/* return coordinates origin top left image, not window */
	*overlay_coord_x = window_x;	 
	*overlay_coord_y = window_y;	 
}
/*********************************************************************/
/* start read rubber band coord */
/*********************************************************************/
void 	rubberenable_ (int *rflag, int *iboxwidth)
{
/* rubber_flag = 1 read 1st pointer position, 2 continuous read, 3 pause */
	rubber_flag = 1;
/* rubber_type = 1 for lines, 2 for box, 3 for circle, 4 for fixed
                   width box dragged at any angle */
	rubber_type = *rflag;
	rubber_halfwidth = *iboxwidth / 2;
}
/*********************************************************************/
/* Read rubber band coord */
/*********************************************************************/
void 	rubberread_ (int *ix1, int *iy1, int *ix2, int *iy2)
{
	*ix1 = rubber_start_x;
	*iy1 = rubber_start_y;
	*ix2 = rubber_finish_x;
	*iy2 = rubber_finish_y;
}
/*********************************************************************/
/* Drag rubber band box of fixed width any angle */
/*********************************************************************/
void 	rubberdragbox ()
{
	mlines = 4;
/* calculate centre points of line with pan */
	rubber_x1 = rubber_start_x;
	rubber_y1 = rubber_start_y;

/* new endpoints */
	rubber_x2 = rubber_pointer_x;
	rubber_y2 = rubber_pointer_y;
	
/* tangent */
	if(rubber_x2 == rubber_x1)
		rubber_angle = pio2;
	else
		rubber_angle 
           = pio2-atan((double)(rubber_y2 - rubber_y1)/(rubber_x2 - rubber_x1));

/* integer conversion */ 
	rubber_cos = rubber_halfwidth * cos(rubber_angle);
	rubber_sin = rubber_halfwidth * sin(rubber_angle);

/* calculate 4 box corners */
	rubberline_x1[0] = rubber_x1 - rubber_cos;
	rubberline_y1[0] = rubber_y1 + rubber_sin;
	rubberline_x2[0] = rubber_x1 + rubber_cos;
	rubberline_y2[0] = rubber_y1 - rubber_sin;

	rubberline_x1[1] = rubberline_x2[0];
	rubberline_y1[1] = rubberline_y2[0];
	rubberline_x2[1] = rubber_x2 + rubber_cos;
	rubberline_y2[1] = rubber_y2 - rubber_sin;

	rubberline_x1[2] = rubberline_x2[1];
	rubberline_y1[2] = rubberline_y2[1];
	rubberline_x2[2] = rubber_x2 - rubber_cos;
	rubberline_y2[2] = rubber_y2 + rubber_sin;

	rubberline_x1[3] = rubberline_x2[2];
	rubberline_y1[3] = rubberline_y2[2];
	rubberline_x2[3] = rubberline_x1[0];
	rubberline_y2[3] = rubberline_y1[0];


        for (i = 0; i < mlines; i++)
                {
                segments[i].x1 = rubberline_x1[i] + pan_x;
                segments[i].y1 = rubberline_y1[i] + pan_y; 
                segments[i].x2 = rubberline_x2[i] + pan_x;
                segments[i].y2 = rubberline_y2[i] + pan_y;
                }


/* draw new box */

        XDrawSegments
        (display_id, image_area_window_id, gc_draw_vector[dash_style],
         segments, mlines);

         XSync(display_id, False);

}
/*********************************************************************/
/* Draw rubber band box */
/*********************************************************************/
void 	rubberdrawbox ()
{
	rubber_min_x = imin_ (rubber_start_x,rubber_pointer_x);
	rubber_min_y = imin_ (rubber_start_y,rubber_pointer_y);
	rubber_max_x = imax_ (rubber_start_x,rubber_pointer_x);
	rubber_max_y = imax_ (rubber_start_y,rubber_pointer_y);
	rubber_size_x = rubber_max_x - rubber_min_x ;
	rubber_size_y = rubber_max_y - rubber_min_y ;

/* draw new box to image area and screen pixmap */
	XDrawRectangle
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	rubber_min_x + pan_x, rubber_min_y + pan_y, 
		rubber_size_x, rubber_size_y);

/* repeat both operations in zoom window */
	if (zoom_flag == 1) 
	{
/* remove old zoom box */
        	redraw_zoom (&zoom_refresh);

		convert_to_zoom
		(rubber_pointer_x,rubber_pointer_y, &zoom_x2, &zoom_y2);
		rubber_min_x = imin_ (zoom_x1, zoom_x2);	
		rubber_min_y = imin_ (zoom_y1, zoom_y2);
		rubber_max_x = imax_ (zoom_x1, zoom_x2);	
		rubber_max_y = imax_ (zoom_y1, zoom_y2);
		rubber_size_x = rubber_max_x - rubber_min_x ;
		rubber_size_y = rubber_max_y - rubber_min_y ;

/* draw new zoom box */
		XDrawRectangle
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		rubber_min_x, rubber_min_y, rubber_size_x, rubber_size_y);

	}
}
/*********************************************************************/
/* Draw rubber band circle *
/*********************************************************************/
void 	rubberdrawcircle ()
{
/* remove old circle from image area and screen pixmap */
/* draw new circle to image area and screen pixmap */
/* repeat both operations in zoom area */

	rubber_size_x = rubber_pointer_x - rubber_start_x;
	rubber_size_y = rubber_pointer_y - rubber_start_y;
	rubber_size_x = rubber_size_x * rubber_size_x;
	rubber_size_y = rubber_size_y * rubber_size_y;
	rubber_circle_diam = 
		(int)(sqrt((double)(rubber_size_x + rubber_size_y)) * 2.0);
	rubber_circle_x = rubber_start_x - (float)rubber_circle_diam * 0.5;
	rubber_circle_y = rubber_start_y - (float)rubber_circle_diam * 0.5;

/* draw circle in image area */
	XDrawArc
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	rubber_circle_x + pan_x, rubber_circle_y + pan_y,
	rubber_circle_diam, rubber_circle_diam,
	circle_angle, circle_angle);

/* remove and draw circle in zoom window if present */
	if(zoom_flag == 1)
	{
/* remove zoomed circle into zoom window */
		redraw_zoom (&zoom_refresh);

		convert_to_zoom	
		(rubber_pointer_x, rubber_pointer_y, &zoom_x2, &zoom_y2);
		rubber_size_x = zoom_x2 - zoom_x1;
		rubber_size_y = zoom_y2 - zoom_y1;
		rubber_size_x = rubber_size_x * rubber_size_x;
		rubber_size_y = rubber_size_y * rubber_size_y;
		rubber_circle_diam = 
		(int)(sqrt((double)(rubber_size_x + rubber_size_y)) * 2.0);
		rubber_circle_x = zoom_x1 - (float)rubber_circle_diam * 0.5;
		rubber_circle_y = zoom_y1 - (float)rubber_circle_diam * 0.5;

/* draw zoomed circle into zoom window */
		XDrawArc
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		rubber_circle_x, rubber_circle_y,
		rubber_circle_diam, rubber_circle_diam,
		circle_angle, circle_angle);
	}
}
/*********************************************************************/
/* Draw rubber band line */
/*********************************************************************/
void 	rubberdrawline ()
{
/* draw new line */
	XDrawLine
	(display_id, image_area_window_id, gc_draw_vector[dash_style],
	rubber_start_x + pan_x, rubber_start_y + pan_y,	
	rubber_pointer_x + pan_x, rubber_pointer_y + pan_y);


/* repeat operation in zoom window */
	if (zoom_flag == 1) 
	{
		convert_to_zoom
		(rubber_start_x,rubber_start_y, &zoom_x1, &zoom_y1);
		convert_to_zoom	
		(rubber_finish_x,rubber_finish_y, &zoom_x2, &zoom_y2);

/* remove old zoom line */
		redraw_zoom (&zoom_refresh);

/* draw new zoom line */
		convert_to_zoom
		(rubber_pointer_x,rubber_pointer_y, &zoom_x2, &zoom_y2);
		XDrawLine
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 zoom_x1, zoom_y1, zoom_x2, zoom_y2);

	}
}
/*********************************************************************/
/* Remove rubber band line */
/*********************************************************************/
void 	rubberremoveline_ (int *ix1, int *iy1, int *ix2, int *iy2)
{
/* remove old line */
//        redraw_image (&image_refresh);

/* repeat operation in zoom window */
	if (zoom_flag == 1) 
	{
		convert_to_zoom
		(rubber_start_x,rubber_start_y, &zoom_x1, &zoom_y1);
		convert_to_zoom
		(rubber_finish_x,rubber_finish_y, &zoom_x2, &zoom_y2);

/* remove old zoom line */
		redraw_zoom (&zoom_refresh);

	}

/* move line out of arrays */
	nlines--;
	rubber_start_x = line_x1[nlines];
	rubber_start_y = line_y1[nlines];
	rubber_finish_x = line_x2[nlines];
	rubber_finish_y = line_y2[nlines];
}
/*********************************************************************/
/* Read slider position */
/*********************************************************************/
void 	readslider_ (float *slider_percent)
{
/* dummy routine to return slider position */
        *slider_percent = slider_position;
}
/*********************************************************************/
/*	Action routines */
/*********************************************************************/

/*********************************************************************/
/* io box callback */
/*********************************************************************/
void	action_iobox ()
{
	icallback_value = 100;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* display label */
/*********************************************************************/
void	action_label_display_ ()
{
/* position at top of screen */
	XtVaSetValues( label,
		XtNfromVert, NULL,
		XtNvertDistance,4,
        	NULL);                

/* if slider present, position wrt slider */
	if(slider_flag >= 1)
		XtVaSetValues(sliderBox,
		XtNfromVert, label,
		XtNvertDistance,1,
		XtNcursor, cursor,
        	NULL);                

/* if menu present, reposition it wrt slider */
	if(slider_flag >= 1 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, sliderBox,
		XtNvertDistance,1,
        	NULL);                

		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}
/* reposition menu wrt label if no slider */
	else if(slider_flag == 0 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, label,
		XtNvertDistance,1,
        	NULL);                

		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}

	XtManageChild(label);
	XRaiseWindow(display_id,label_window_id);
	while (expose_label_flag == 0)
	 eventdispatch_ ();

	XSync(display_id, False);
	expose_label_flag = 0;
	label_flag = 1;
}

/*********************************************************************/
/* display menu */
/*********************************************************************/
void	action_menudisplay_ ()
{
/* if slider present, set menu position with respect to slider */
	if(slider_flag >= 1)
		{
		XtVaSetValues(menuBox,
			XtNfromVert, sliderBox,
			XtNvertDistance,1,
	        	NULL);                   /* argument list*/
		}
/* if label present, set menu position with respect to label */
	else if(label_flag == 1)
		{
		XtVaSetValues(menuBox,
			XtNfromVert, label,
			XtNvertDistance,1,
	        	NULL);                   /* argument list*/
		}
/* if no label or slider, set menu position with respect to top of window */
	else
		XtVaSetValues(menuBox,
			XtNfromVert, NULL,
			XtNvertDistance,4,
        	NULL);  
              
/* manage menu widget */
	XtManageChild(menuBox);

	if(event_proc == 0)
	{
		XtAddEventHandler
		(menuBox,ExposureMask,False,(XtEventHandler)expose_menu,0);
		while (expose_menu_flag == 0)
		 eventdispatch_ ();
	}
/*	else
	{
           event_get_next_(&event_true);
           if(event_true != 0) eventdispatch_ ();
	}*/

	XSync(display_id, False);
	expose_menu_flag = 0;
	menu_flag = 1;
}

/*********************************************************************/
/* Start Pan function callback */
/*********************************************************************/
void 	action_pan_start ()
{
        int 	pan_xy;
/* disable panning for multi-pixmap images */
	if(no_maps > 1) return;
	zoomhide_ ();
	pan_flag = 1;
	readpointer_(&image_pointer_x, &image_pointer_y);

	pan_pointer_x1 = image_pointer_x + pan_x;
	pan_pointer_y1 = image_pointer_y + pan_y;

	while(pan_flag == 1)
	{
/* read pointer coordinates */
		readpointer_(&image_pointer_x, &image_pointer_y);
		pan_pointer_x2 = image_pointer_x + pan_x;
		pan_pointer_y2 = image_pointer_y + pan_y;

/* test to see if pointer has shifted enough to pan */
		pan_xy = abs(pan_pointer_x2 - pan_pointer_x1)
		       + abs(pan_pointer_y2 - pan_pointer_y1);

		if(pan_xy > pan_min)
		{
			pan_x = pan_x + pan_pointer_x2 - pan_pointer_x1;
			pan_y = pan_y + pan_pointer_y2 - pan_pointer_y1;

			redraw_image (&image_refresh);
			pan_pointer_x1 = pan_pointer_x2;
			pan_pointer_y1 = pan_pointer_y2;

		}

	           event_get_next_(&event_true);
	           if(event_true != 0) eventdispatch_ ();
	}
}
/*********************************************************************/
/* Finish Pan function callback */
/*********************************************************************/
void 	action_pan_finish ()
{
/* disable panning for multi-pixmap images */
	if(no_maps > 1) return;
/* read pointer coordinates */
	readpointer_(&image_pointer_x, &image_pointer_y);

	pan_pointer_x2 = image_pointer_x + pan_x;
	pan_pointer_y2 = image_pointer_y + pan_y;

/* reset pan dimensions */
	pan_x = pan_x + pan_pointer_x2 - pan_pointer_x1;
	pan_y = pan_y + pan_pointer_y2 - pan_pointer_y1;

	pan_flag = 0;
	redraw_image (&image_refresh);

}
/*********************************************************************/
/* action menu 1 */
/*********************************************************************/
void	action_menu_1 ()
{
	icallback_value = 1;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 2 */
/*********************************************************************/
void	action_menu_2 ()
{
	icallback_value = 2;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 3 */
/*********************************************************************/
void	action_menu_3 ()
{
	icallback_value = 3;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 4 */
/*********************************************************************/
void	action_menu_4 ()
{
	icallback_value = 4;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 5 */
/*********************************************************************/
void	action_menu_5 ()
{
	icallback_value = 5;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 6 */
/*********************************************************************/
void	action_menu_6 ()
{
	icallback_value = 6;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 7 */
/*********************************************************************/
void	action_menu_7 ()
{
	icallback_value = 7;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 8 */
/*********************************************************************/
void	action_menu_8 ()
{
	icallback_value = 8;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu 9 */
/*********************************************************************/
void	action_menu_9 ()
{
	icallback_value = 9;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu a */
/*********************************************************************/
void	action_menu_a ()
{
	icallback_value = 10;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu b */
/*********************************************************************/
void	action_menu_b ()
{
	icallback_value = 11;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu c */
/*********************************************************************/
void	action_menu_c ()
{
	icallback_value = 12;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu d */
/*********************************************************************/
void	action_menu_d ()
{
	icallback_value = 13;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu e */
/*********************************************************************/
void	action_menu_e ()
{
	icallback_value = 14;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu f */
/*********************************************************************/
void	action_menu_f ()
{
	icallback_value = 15;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu g */
/*********************************************************************/
void	action_menu_g ()
{
	icallback_value = 16;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu h */
/*********************************************************************/
void	action_menu_h ()
{
	icallback_value = 17;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu i */
/*********************************************************************/
void	action_menu_i ()
{
	icallback_value = 18;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu j */
/*********************************************************************/
void	action_menu_j ()
{
	icallback_value = 19;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action menu k */
/*********************************************************************/
void	action_menu_k ()
{
	icallback_value = 20;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* action return */
/*********************************************************************/
void	action_continue ()
{
	icallback_value = 0;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* rubber band start */
/*********************************************************************/
void 	action_rubber_read ()
{
/* if first line, read first pointer position, store it */
	if(rubber_flag == 0)
		return;
/* continuous pointer read */
	else if(rubber_flag <= 2)
	{
		readpointer_(&rubber_pointer_x, &rubber_pointer_y);
		rubber_start_x = rubber_pointer_x;
		rubber_start_y = rubber_pointer_y;
		rubber_finish_x = rubber_pointer_x;
		rubber_finish_y = rubber_pointer_y;
	}
/* pointer pause */
	else if (rubber_flag == 3)
	{
		rubber_start_x = line_x2[nlines-1];
		rubber_start_y = line_y2[nlines-1];
		rubber_finish_x = line_x2[nlines-1];
		rubber_finish_y = line_y2[nlines-1];
	}
	rubber_flag = 2;

/* start loop to continually read pointer and update line */
	while (rubber_flag == 2)
	{
		readpointer_(&rubber_pointer_x, &rubber_pointer_y);
/* move giant crosshair, update tracked coords if flag set */
		if(pointer_flag > 0) 
			pointer_track ();
		else
			{
			redraw_image (&image_refresh);
			redraw_zoom (&zoom_refresh);
			}

/* draw rubber shape */
		if(rubber_type == 1) 
			rubberdrawline ();
		else if(rubber_type == 2) 
			rubberdrawbox ();
		else if(rubber_type == 3) 
			rubberdrawcircle ();
		else if(rubber_type == 4)
			rubberdragbox ();

		XSync(display_id, False);

/* store new endpoint for next time */
		rubber_finish_x = rubber_pointer_x;
		rubber_finish_y = rubber_pointer_y;
		event_get_next_(&event_true);
		if(event_true != 0) eventdispatch_ ();
	}
}
/*********************************************************************/
/* rubber band pause between lines */
/*********************************************************************/
void 	action_rubber_pause ()
{
/* return if flag 0 */
	if(rubber_flag == 0)
		return;

/* finish rubberbanding if box or circle */
	if(rubber_type > 1)
	{

		action_rubber_finish ();
		return;
	}

/* pause between lines for line rubberbanding */
	rubber_flag = 3;

/* add vector to list */
	mlines = 1;
	drawlines_ (&rubber_start_x,&rubber_start_y,
		&rubber_finish_x,&rubber_finish_y,&mlines);

/* callback to fortran */
	icallback_value = 104;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* rubber band end */
/*********************************************************************/
void 	action_rubber_finish ()
{
//int junk;
	if(rubber_flag == 0)
		return;

	readpointer_(&rubber_pointer_x, &rubber_pointer_y);

//scanf("%ijunk",&junk);

/* move giant crosshair + update tracked coords if flag set */
	if(pointer_flag > 0) pointer_track ();

/* test for single point specification */
	if(rubber_flag == 1)
	{
		rubber_start_x = rubber_pointer_x;
		rubber_start_y = rubber_pointer_y;

	}

/* draw final line, box or circle */
	if(rubber_type == 1) 
		rubberdrawline  ();
	else if(rubber_type == 4)
		rubberdragbox ();

	rubber_finish_x = rubber_pointer_x;
	rubber_finish_y = rubber_pointer_y;

/* add final vector(s) to list */
	if(rubber_type == 1)
	{
		if(rubber_flag >= 2)
		{
			line_x1[nlines] = rubber_start_x;
			line_y1[nlines] = rubber_start_y;
			line_x2[nlines] = rubber_finish_x;
			line_y2[nlines] = rubber_finish_y;
			nlines++;
		}
	}
/* box */
	else if(rubber_type == 2)
	{
		drawbox_ (&rubber_start_x, &rubber_start_y, &rubber_finish_x, &rubber_finish_y);
	}
/* circle */
	else if(rubber_type == 3)
	{
		rubber_size_x = rubber_finish_x - rubber_start_x;
		rubber_size_y = rubber_finish_y - rubber_start_y;
		rubber_size_x = rubber_size_x * rubber_size_x;
		rubber_size_y = rubber_size_y * rubber_size_y;
		rubber_radius = sqrt((double)((float)rubber_size_x + (float)rubber_size_y));
		drawcircle_ (&rubber_start_x,&rubber_start_y,&rubber_radius);
	}
/* dragbox */
	else if(rubber_type == 4)
	{
		for (i = 0; i < 4; i++)
		{
		line_x1[nlines] = rubberline_x1[i];
		line_y1[nlines] = rubberline_y1[i];
		line_x2[nlines] = rubberline_x2[i];
		line_y2[nlines] = rubberline_y2[i];
		line_style[nlines] = dash_style;
		segments[i].x1 = rubberline_x1[i] + pan_x;
		segments[i].y1 = rubberline_y1[i] + pan_y;
		segments[i].x2 = rubberline_x2[i] + pan_x;
		segments[i].y2 = rubberline_y2[i] + pan_y;
		nlines++;
		}
/* draw new box */
	drawlines_ (rubberline_x1, rubberline_y1, rubberline_x2, rubberline_y2, &mlines);
	nlines = nlines - 4;

	}

/* callback to fortran */
	icallback_value = 105;
	ximagecallback_ (&icallback_value);
	rubber_flag = 0;
//	ring_rad = 0.;

}
/*********************************************************************/
/* display slider                                                    */
/*********************************************************************/
void	action_slider_display_ ()
/* only used if menu/labels/slider hidden, then redisplayed */
{
	slider_flag = ksliders;
/* position wrt label if present */
	if(label_flag == 1)
		XtVaSetValues(sliderBox,
			XtNfromVert, label,
			XtNvertDistance,1,
			XtNcursor, cursor,
	        	NULL);

/* position wrt iobox if present */
	else if(iobox_flag == 1)
		XtVaSetValues(sliderBox,
			XtNfromVert, ioBox,
			XtNvertDistance,1,
			XtNcursor, cursor,
	        	NULL);

/* if menu present reposition it */
	if(menu_flag == 1)
		{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
			XtNfromVert, sliderBox,
			XtNvertDistance,1,
	        	NULL);                   /* argument list*/
		XtManageChild(menuBox);
		XRaiseWindow(display_id,menuBox_window_id);
		}

/* position slider1 in the centre and add callback */
	XawScrollbarSetThumb(slider1, slider1_start_position, slider_size);
/*	XtAddCallback(slider1, XtNjumpProc, 
		(XtCallbackProc)action_slider_callback, (XtPointer)1);
*/

/* reset geometry for single or double slider */
	if(slider_flag == 1)
	{
		XtUnmanageChild(slider2);
		XtUnmanageChild(colour_bar2);
		XtVaSetValues(slider1,
		XtNfromHoriz, slider_label,
		XtNhorizDistance, 4,
		XtNfromVert, NULL,
		XtNvertDistance, 4,
		XtNscrollDCursor, cursor,
		XtNscrollHCursor, cursor,
		XtNscrollLCursor, cursor,
		XtNscrollRCursor, cursor,
		XtNscrollUCursor, cursor,
		XtNscrollVCursor, cursor,
		XtNshown, slider_size,
		NULL);

		XtVaSetValues(colour_bar1,
		XtNfromHoriz, slider1,
		XtNhorizDistance, 4,
		XtNfromVert, NULL,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		NULL);
	}
/* position slider 2 in the centre and add callback */
	else if(slider_flag == 2)
	{
		XawScrollbarSetThumb(slider2, slider2_start_position, 
                                                        slider_size);
/*		XtAddCallback(slider2, XtNjumpProc, 
			(XtCallbackProc)action_slider_callback, (XtPointer)2);
*/
/* reset slider 1*/
		XtVaSetValues(slider1,
		XtNfromHoriz, NULL,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNscrollDCursor, cursor,
		XtNscrollHCursor, cursor,
		XtNscrollLCursor, cursor,
		XtNscrollRCursor, cursor,
		XtNscrollUCursor, cursor,
		XtNscrollVCursor, cursor,
		XtNshown, slider_size,
		NULL);

/* reset colour bar 1*/
		XtVaSetValues(colour_bar1,
		XtNfromHoriz, slider1,
		XtNhorizDistance, 4,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		NULL);

/* reset slider 2 */
		XtVaSetValues(slider2,
		XtNfromHoriz, colour_bar1,
		XtNhorizDistance,4,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNscrollDCursor, cursor,
		XtNscrollHCursor, cursor,
		XtNscrollLCursor, cursor,
		XtNscrollRCursor, cursor,
		XtNscrollUCursor, cursor,
		XtNscrollVCursor, cursor,
		XtNshown, slider_size,
		NULL);

/* reset colour bar 2 */
		XtVaSetValues(colour_bar2,
		XtNfromHoriz, slider2,
		XtNhorizDistance, 4,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		NULL);
		XtManageChild(slider2);
		XtManageChild(colour_bar2);
	}

/* push slider in front */
	XtManageChild(sliderBox);
	XRaiseWindow(display_id, sliderBox_window_id);

/* synchronise */
	XSync(display_id, False);
	while (expose_slider_flag == 0)
	 eventdispatch_ ();

	XSync(display_id, False);
	expose_slider_flag = 0;
}
/*********************************************************************/
/* slider callback */
/*********************************************************************/
void	action_slider_callback (Widget scrollbar, XtPointer client_data,
			  XtPointer percent)
{
	float	*fptr = (float*)percent;
	slider_position = (*fptr) * 100.0;
	if(client_data == (void*)1)
		icallback_value = 101;
	else if(client_data == (void*)2)
		icallback_value = 102;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* Start Zoom function callback */
/*********************************************************************/
void 	action_zoom_init ()
{
        icallback_value = 103;
/* if scrolling pixmaps, start zoom centred */
	if(no_maps <= 1) 
	{
	readpointer_(&zoom_image_pointer_x, &zoom_image_pointer_y);
	zoom_display ();
	}
}
/*********************************************************************/
/* Read Pointer callback */
/*********************************************************************/
void 	action_read_pointer ()
{
	icallback_value = 0;

/* pass values to ximagecallback */
	ximagecallback_ (&icallback_value);
}
/*****************************************************************/
/*  set up action lists */
/*****************************************************************/
static XtActionsRec actlist[] = {
			{"action_read_pointer",(XtActionProc)
						action_read_pointer},
			{"action_menu_1",(XtActionProc)
						action_menu_1},
			{"action_menu_2",(XtActionProc)
						action_menu_2},
			{"action_menu_3",(XtActionProc)
						action_menu_3},
			{"action_menu_4",(XtActionProc)
						action_menu_4},
			{"action_menu_5",(XtActionProc)
						action_menu_5},
			{"action_menu_6",(XtActionProc)
						action_menu_6},
			{"action_menu_7",(XtActionProc)
						action_menu_7},
			{"action_menu_8",(XtActionProc)
						action_menu_8},
			{"action_menu_9",(XtActionProc)
						action_menu_9},
			{"action_menu_a",(XtActionProc)
						action_menu_a},
			{"action_menu_b",(XtActionProc)
						action_menu_b},
			{"action_menu_c",(XtActionProc)
						action_menu_c},
			{"action_menu_d",(XtActionProc)
						action_menu_d},
			{"action_menu_e",(XtActionProc)
						action_menu_e},
			{"action_menu_f",(XtActionProc)
						action_menu_f},
			{"action_menu_g",(XtActionProc)
						action_menu_g},
			{"action_menu_h",(XtActionProc)
						action_menu_h},
			{"action_menu_i",(XtActionProc)
						action_menu_i},
			{"action_menu_j",(XtActionProc)
						action_menu_j},
			{"action_menu_k",(XtActionProc)
						action_menu_k},
			{"action_continue",(XtActionProc)
						action_continue},
			{"action_label_display_",(XtActionProc)
						action_label_display_},
			{"action_menudisplay_",(XtActionProc)
						action_menudisplay_},
			{"action_slider_display_",(XtActionProc)
						action_slider_display_},
			{"action_pan_start",(XtActionProc)
						action_pan_start},
			{"action_pan_finish",(XtActionProc)
						action_pan_finish},
			{"action_rubber_read",(XtActionProc)
						action_rubber_read},
			{"action_rubber_pause",(XtActionProc)
						action_rubber_pause},
			{"action_rubber_finish",(XtActionProc)
						action_rubber_finish},
			{"action_zoom_init",(XtActionProc)
						action_zoom_init},
			{"action_iobox",(XtActionProc)action_iobox}
};
/************************************************************************/
/* Change cursor type                                                   */
/************************************************************************/
void    changecursor_ (int *icurs,unsigned int *iring_rad)
{
/* 
   icurs 0 default cursor
         1 fine crosshair
         2 box crosshair
         3 huge crosshair
         4 ring crosshair
         5 cross
         6 circle
         7 hand
         8 bird
         9 skull
*/

        XUndefineCursor(display_id,image_area_window_id);

/* reset pointer flag */
	if(pointer_flag > 1)
		{
		      if(pointer_flag < 4)
		      {
			giant_cursor_remove ();
			pointer_flag = pointer_flag - 2;
	      		}
	      		else
			{
		  	ring_cursor_remove ();
		  	pointer_flag = pointer_flag - 4;
                	}
		}
	if(pointer_flag == 0)
	{
		XtRemoveEventHandler(image_area, PointerMotionMask, 
				False, (XtEventHandler)pointer_track,0);
		XtRemoveEventHandler(zoom_area, PointerMotionMask, 
				False, (XtEventHandler)pointer_track,0);
	}

/* initialize ring_rad after (possible) ring cursor removal which needs the
old value but before adding event handlers which need it reset */
	ring_rad = 0.;

/* set cursor hotspot to bitmap centre for all but default arrow */
	cursor_x = 7;
	cursor_y = 7;

        if(*icurs == 0)
/* default arrow cursor */
	{
/* copy default arrow cursor shape and mask */
		 memcpy(cursor_shape,cursor_shape0,30);
		 memcpy(cursor_mask,cursor_mask0,30);
	 	 cursor_x = 4;
		 cursor_y = 1;
	}
/* crosshair cursor built from bit shape and mask */

	else if(*icurs == 1)
/* fine crosshair */
	{
		memcpy(cursor_shape,cursor_shape1,30);
		memcpy(cursor_mask,cursor_mask1,30);
	   }
/* boxed crosshair */
	else if(*icurs == 2)
	   {
		memcpy(cursor_shape,cursor_shape2,30);
		memcpy(cursor_mask,cursor_mask2,30);
	   }

/* giant or ring crosshair */
	else if (*icurs == 3 || *icurs == 4)
	{

/* enable pointer tracking if not already done */
		if(pointer_flag == 0)
		{
			XtAddEventHandler(image_area, PointerMotionMask, 
			False, (XtEventHandler)pointer_track,0);
			XtAddEventHandler(zoom_area, PointerMotionMask, 
			False,(XtEventHandler)pointer_track,0);

		}

			{
			memcpy(cursor_shape,cursor_shape3,30);
			memcpy(cursor_mask,cursor_mask3,30);
			}

		if(*icurs == 3)
			pointer_flag = pointer_flag + 2;
		else 
		{
			pointer_flag = pointer_flag + 4;
			ring_rad = (float)*iring_rad;
		  	ring_diam = *iring_rad * 2;
			ring_line = *iring_rad + 5;
		}
	}
/* cross */
	else if (*icurs == 5)
		cursor = XCreateFontCursor(display_id,cross);
/* circle */
	else if (*icurs == 6)
		cursor = XCreateFontCursor(display_id,circle);
/* hand */
	else if (*icurs == 7)
		cursor = XCreateFontCursor(display_id,hand);
/* bird */
	else if (*icurs == 8)
		cursor = XCreateFontCursor(display_id,bird);
/* skull */
	else if (*icurs == 9)
		cursor = XCreateFontCursor(display_id,skull);

/* set pixmaps for designer cursors */
	if (*icurs < 5)
	{
/* set up cursor colour, depth 1 so use 1-byte value for 8 & 24-bit */
		cursor_foreground.pixel = green;
		cursor_foreground.flags = DoRed | DoGreen | DoBlue;
		cursor_foreground.red = 0;
		cursor_foreground.green = 65535;
		cursor_foreground.blue = 0;

		cursor_background.pixel = black;
		cursor_background.flags = DoRed | DoGreen | DoBlue;
		cursor_background.red = 0;
		cursor_background.green = 0;
		cursor_background.blue = 0;

		cursor_shape_pixmap = 
			XCreatePixmapFromBitmapData
			(display_id,topLevel_window_id,
			cursor_shape, cursor_size, cursor_size, 
			green, black, cursor_depth);

		cursor_mask_pixmap = 
			XCreatePixmapFromBitmapData
			(display_id,topLevel_window_id,
			cursor_mask, cursor_size, cursor_size, 
			green, black, cursor_depth);

		cursor = XCreatePixmapCursor(display_id,
			cursor_shape_pixmap, cursor_mask_pixmap,
			&cursor_foreground, &cursor_background,
			cursor_x, cursor_y);
	}

	XDefineCursor(display_id,image_area_window_id,cursor);
	XDefineCursor(display_id,zoom_area_window_id,cursor);

/* now do pixmaps for label and slider bars */
	cursor_foreground.pixel = black;
	cursor_foreground.flags = DoRed | DoGreen | DoBlue;
	cursor_foreground.red = 0;
	cursor_foreground.green = 0;
	cursor_foreground.blue = 0;

	cursor_background.pixel = white;
	cursor_background.flags = DoRed | DoGreen | DoBlue;
	cursor_background.red = 65535;
	cursor_background.green = 65535;
	cursor_background.blue = 65535;

	cursor_shape_pixmap = 
		XCreatePixmapFromBitmapData
		(display_id,topLevel_window_id,
		cursor_shape, cursor_size, cursor_size, 
		white, black, cursor_depth);

	cursor_mask_pixmap = 
		XCreatePixmapFromBitmapData
		(display_id,topLevel_window_id,
		cursor_mask, cursor_size, cursor_size, 
		white, black, cursor_depth);

	cursor = XCreatePixmapCursor(display_id,
		cursor_shape_pixmap, cursor_mask_pixmap,	
		&cursor_foreground, &cursor_background,
		cursor_x, cursor_y);


	XDefineCursor(display_id,colour_bar1_window_id,cursor);
	XDefineCursor(display_id,colour_bar2_window_id,cursor);
	XDefineCursor(display_id,ioBox_window_id,cursor);
	XDefineCursor(display_id,label_window_id,cursor);
	XDefineCursor(display_id,sliderBox_window_id,cursor);
}
/************************************************************************/
/* Check resources can create a pixmap this size, called from fortran   */
/************************************************************************/
void	checkimage_ (int *nx, int *ny, int *nmaps)
{
/* test for size in horizontal or vertical max - bug in xorg */
	if(*nx > 16384 || *ny > 16384)
	{
		printf
                ("error - map height/width limit is 16384. \n");
		icallback_value = 999;
		ximagecallback_ (&icallback_value);
	}

	no_maps = *nmaps;
/* too many maps ? */
	if( no_maps > max_maps)
	  {
		printf
                ("error - insufficient resources - too many sections. \n");
		icallback_value = 999;
		ximagecallback_ (&icallback_value);
	  }

	image_width = *nx;
	image_height = *ny;

/* image too large for array size ? */
	if(image_width * image_height > max_map_size)
	{
		printf
                ("error - insufficient resources - image too large. \n");
		icallback_value = 999;
		ximagecallback_ (&icallback_value);
	}

/* remove pixmap,image */
	if(image_flag != 0) XFreePixmap(display_id, screen_pixmap[map_no]);

/* Create pixmaps to hold image */
	for (i = 0; i < no_maps; i++)
	  {
	    screen_pixmap[i] = XCreatePixmap(display_id, image_area_window_id,
			image_width, image_height, image_depth);

/* check resources sufficient, else call error trap and return */
	    XSync(display_id, False);
	    if (icallback_value == 999) return;
	  }

/* free pixmaps if successful */
	for (i = 0; i < no_maps; i++)
	  {
	    XFreePixmap(display_id, screen_pixmap[i]);
	  }
	XSync(display_id, False);
}
/*********************************************************************/
/* clear image from the window */
/*********************************************************************/
void	clearimage_ ()
{
	int	i;
	if(image_flag != 0)
	{
         pan_x = 0;
         pan_y = 0;

/* fill screen pixmap with blanks */
		for (i = 0; i < no_maps; i++)
		{
		XFillRectangle(display_id, screen_pixmap[i], gc_fill_rectangle, 
		image_source_x, image_source_y,
		image_width, image_height);

/* copy image to pixmap */
		XCopyArea(display_id, screen_pixmap[i], image_area_window_id,
		  gc_fill_rectangle, image_source_x, image_source_y,
		  image_width, image_height,
	          image_dest_x, image_dest_y);

		XFreePixmap(display_id, screen_pixmap[i]);
		}

/* hide the zoom window */
	zoomhide_ ();
	image_flag = 0;
	}
}
/*********************************************************************/
/* Initialize colour bar */
/*********************************************************************/
void colourbarinit_ ()
{
	if(slider_flag == 0)
		{
		fprintf(stderr,"Slider not initialized");
	        return;
		}

/* create colour bar image */
	colour_bar_image = XCreateImage(display_id,visual_id,
		     image_depth, image_format, image_offset,
		     &colour_bar_pointer[0], slider_width, slider_height, 
		     image_bitmap_pad, image_bytes_per_line);

/* copy colour bar to image 1 ready for display */
	XPutImage(display_id,colour_bar1_window_id,
		  gc_fill_rectangle, colour_bar_image, 
		  image_source_x, image_source_y,
		  image_source_x, image_source_y, slider_width, slider_height);

	colour_bar_flag = 1;
	XtManageChild(colour_bar1);
/* copy colour bar to image 2 ready for display */
	if(slider_flag == 2)
	{
		XPutImage(display_id,colour_bar2_window_id,
		  gc_fill_rectangle, colour_bar_image, 
		  image_source_x, image_source_y,
		  image_source_x, image_source_y, slider_width, slider_height);

		colour_bar_flag = 2;
		XtManageChild(colour_bar2);
	}
	XSync(display_id, False);
}
/*********************************************************************/
/* Hide colour bar */
/*********************************************************************/
void colourbarhide_ ()
{
	if(slider_flag >= 1) sliderhide_ ();
	if(colour_bar_flag >= 1)
	{
		colour_bar_image->data = 0;
		XDestroyImage(colour_bar_image);	
		XSync(display_id, False);
	}
	colour_bar_flag = 0;
}
/*********************************************************************/
/* Set delay in microseconds                                         */
/*********************************************************************/
void	delay_ (int *idelay)
{
	int delay;
	delay = *idelay;
	sleepdelay_ (&delay);
}
/*********************************************************************/
/* Draw a rectangular box, called from the call-back routine */
/*********************************************************************/
void	drawbox_ (int *ix1, int *iy1, int *ix2, int *iy2)
{

/* if overlay flag set draw only in that window */
/* draw box in main window */
	if(overlay_flag == 0)
	{
		if ( nboxes >= max_boxes )
		{
		printf ("Too many boxes, max_boxes too small.\n");
	        nboxes = max_boxes;
		return;
		}
		box_x[nboxes] = imin_ (*ix1, *ix2);
		box_y[nboxes] = imin_ (*iy1, *iy2);
		box_width[nboxes] = abs(*ix2 - *ix1);
		box_height[nboxes] = abs(*iy2 - *iy1);
		box_style[nboxes] = dash_style;

	 	XDrawRectangle
		(display_id, image_area_window_id, gc_draw_vector[dash_style],
		box_x[nboxes] + pan_x, box_y[nboxes] + pan_y, 
		box_width[nboxes], box_height[nboxes]);

/* draw box into pixmap */
		XDrawRectangle
		(display_id, screen_pixmap[map_no], gc_draw_vector[dash_style],
		box_x[nboxes], box_y[nboxes], 
		box_width[nboxes], box_height[nboxes]);

/* Draw box in zoom window if present */
		if(zoom_flag == 1)
		{
		zoom_diam_x = (int)box_width[nboxes] * zoom_factor_x;
		zoom_diam_y = (int)box_height[nboxes] * zoom_factor_y;
		convert_to_zoom(box_x[nboxes], box_y[nboxes],
		        &zoom_centre_x, &zoom_centre_y);
/* draw zoomed box in zoom area */
		XDrawRectangle
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
	 	zoom_centre_x, zoom_centre_y, zoom_diam_x, zoom_diam_y);

/* draw box into zoom pixmap */
		XDrawRectangle
		(display_id, zoom_pixmap, gc_draw_vector[dash_style],
		 zoom_centre_x, zoom_centre_y, zoom_diam_x, zoom_diam_y);
		}
		nboxes++;
	}
/* draw in overlay window */
	else
	{
		if ( nboxes_overlay >= max_boxes )
		{
		printf ("Too many boxes, max_boxes too small.\n");
	        nboxes_overlay = max_boxes;
		return;
		}
		box_x_overlay[nboxes_overlay] = imin_ (*ix1, *ix2);
		box_y_overlay[nboxes_overlay] = imin_ (*iy1, *iy2);
		box_width_overlay[nboxes_overlay] = abs(*ix2 - *ix1);
		box_height_overlay[nboxes_overlay] = abs(*iy2 - *iy1);
		box_style[nboxes_overlay] = dash_style;


/* draw box in overlay area if boxes present */
		if(nboxes_overlay > 0)
		{
		XDrawRectangle
		(display_id, overlay_area_window_id, gc_draw_vector[dash_style],
	 	box_x_overlay[nboxes_overlay], box_y_overlay[nboxes_overlay], 
         	box_width_overlay[nboxes_overlay], box_height_overlay[nboxes_overlay]);

/* draw box into zoom pixmap */
		XDrawRectangle
		(display_id, overlay_pixmap, gc_draw_vector[dash_style],
	 	box_x_overlay[nboxes_overlay], box_y_overlay[nboxes_overlay], 
         	box_width_overlay[nboxes_overlay], box_height_overlay[nboxes_overlay]);
	 	nboxes_overlay++;
		}
	}

	XSync(display_id, False);

}
/*********************************************************************/
/* Draw a circle in the window, called from the call-back routine */
/*********************************************************************/
void drawcircle_ (int *ix, int *iy, float *rad)
{
	if(overlay_flag == 0)
	{
		if ( ncircles >= max_circles )
		{
		printf ("Too many circles, max_circles too small.\n");
	        ncircles = max_circles;
		return;
		}
		irad = (int)*rad;
		circle_x[ncircles] = *ix - irad;
		circle_y[ncircles] = *iy - irad;
		circle_diam[ncircles] = 2.0  * *rad;
		circle_style[ncircles] = dash_style;

		XDrawArc
		(display_id, image_area_window_id, gc_draw_vector[dash_style],
		circle_x[ncircles] + pan_x, circle_y[ncircles] + pan_y,
		circle_diam[ncircles], circle_diam[ncircles],
		circle_angle, circle_angle);

/* draw circle into pixmap */
		XDrawArc
		(display_id, screen_pixmap[map_no], gc_draw_vector[dash_style],
		circle_x[ncircles], circle_y[ncircles],
		circle_diam[ncircles], circle_diam[ncircles],
		circle_angle, circle_angle);

/* Draw circle in zoom window if present */
	 	if(zoom_flag == 1)
		{
		zoom_diam_x = circle_diam[ncircles] * zoom_factor_x;
		zoom_diam_y = circle_diam[ncircles] * zoom_factor_y;

/* convert coordinates to lie in zoom area */
		convert_to_zoom(circle_x[ncircles]+circle_diam[ncircles]/2,
				circle_y[ncircles]+circle_diam[ncircles]/2,
				&zoom_centre_x, &zoom_centre_y);
		zoom_centre_x = zoom_centre_x - zoom_diam_x / 2;
		zoom_centre_y = zoom_centre_y - zoom_diam_y / 2;

/* draw zoomed circle into zoom window */
		XDrawArc
		(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		zoom_centre_x, zoom_centre_y,
		zoom_diam_x, zoom_diam_y,
		circle_angle, circle_angle);

/* draw zoomed circle into zoom pixmap */
		XDrawArc
		(display_id, zoom_pixmap, gc_draw_vector[dash_style],
		zoom_centre_x, zoom_centre_y,
		zoom_diam_x, zoom_diam_y,
		circle_angle, circle_angle);
		}
		ncircles++;
	}

/* draw circle in overlay area */
	else
	{
		if ( ncircles_overlay >= max_circles )
		{
		printf ("Too many circles, max_circles too small.\n");
	        ncircles_overlay = max_circles;
		return;
		}

		irad = (int)*rad;
		circle_x_overlay[ncircles_overlay] = *ix - irad;
		circle_y_overlay[ncircles_overlay] = *iy - irad;
		circle_diam_overlay[ncircles_overlay] = 2.0  * *rad;
		circle_style[ncircles_overlay] = dash_style;

		XDrawArc
		(display_id, overlay_area_window_id, gc_draw_vector[dash_style],
		circle_x_overlay[ncircles_overlay], circle_y_overlay[ncircles_overlay],
		circle_diam_overlay[ncircles_overlay], circle_diam_overlay[ncircles_overlay],
		circle_angle, circle_angle);

/* draw circle into overlay pixmap */
		XDrawArc
		(display_id, overlay_pixmap, gc_draw_vector[dash_style],
		circle_x_overlay[ncircles_overlay], circle_y_overlay[ncircles_overlay],
		circle_diam_overlay[ncircles_overlay], circle_diam_overlay[ncircles_overlay],
		circle_angle, circle_angle);

		ncircles_overlay++;
	}

	XSync(display_id, False);
}
/*********************************************************************/
/* Draw lines in the window */
/*********************************************************************/
void drawlines_ 
	(int *ix1, int *iy1, int *ix2, int *iy2, int *ilines)
{
	if(overlay_flag == 0)
	{
		for (i = 0; i < *ilines; i++)
		{
		line_x1[nlines] = ix1[i];
		line_y1[nlines] = iy1[i];
		line_x2[nlines] = ix2[i];
		line_y2[nlines] = iy2[i];
		line_style[nlines] = dash_style;
		segments[i].x1 = ix1[i] + pan_x;
		segments[i].y1 = iy1[i] + pan_y;
		segments[i].x2 = ix2[i] + pan_x;
		segments[i].y2 = iy2[i] + pan_y;
		nlines++;
		}

		if ( nlines >= max_lines )
		{
			printf ("Too many lines : %d max_lines too small.\n",nlines);
			nlines = max_lines;
			return;
		}

		XDrawSegments
		(display_id, image_area_window_id, gc_draw_vector[dash_style],
		 segments, *ilines);
		XSync(display_id, False);
	
		for (i = 0; i < *ilines; i++)
		{
		segments[i].x1 = ix1[i];
		segments[i].y1 = iy1[i];
		segments[i].x2 = ix2[i];
		segments[i].y2 = iy2[i];

		}

/* draw line in pixmap */
		XDrawSegments
		(display_id, screen_pixmap[map_no], gc_draw_vector[dash_style],
		 segments, *ilines);
		XSync(display_id, False);

		if (zoom_flag == 1) 
		{
		 	for (i = 0; i < *ilines; i++)
			{
			convert_to_zoom
			(ix1[i], iy1[i], &zoom_x1, &zoom_y1);
			segments[i].x1 = zoom_x1;
			segments[i].y1 = zoom_y1;

			convert_to_zoom
			(ix2[i], iy2[i], &zoom_x2, &zoom_y2);
			segments[i].x2 = zoom_x2;
			segments[i].y2 = zoom_y2;
			}

/* draw line in zoomed area window */
			XDrawSegments
			(display_id, zoom_area_window_id, gc_draw_vector[dash_style],
		 	segments, *ilines);

/* draw line in zoom pixmap */
			XDrawSegments
			(display_id, zoom_pixmap, gc_draw_vector[dash_style],
		 	segments, *ilines);
		}
	}
/* draw into overlay window */
	else
	{
		for (i = 0; i < *ilines; i++)
		{
		line_x1_overlay[nlines] = ix1[i];
		line_y1_overlay[nlines] = iy1[i];
		line_x2_overlay[nlines] = ix2[i];
		line_y2_overlay[nlines] = iy2[i];
		segments_overlay[i].x1 = ix1[i];
		segments_overlay[i].y1 = iy1[i];
		segments_overlay[i].x2 = ix2[i];
		segments_overlay[i].y2 = iy2[i];
		nlines_overlay++;
		}

		if ( nlines_overlay >= max_lines )
		{			
			printf ("Too many lines : %d max_lines too small.\n",nlines);
			nlines_overlay = max_lines;
			return;
		}

/* draw line in overlay pixmap */
		XDrawSegments
		(display_id, overlay_area_window_id, gc_draw_vector[dash_style],
		 segments_overlay, *ilines);

 		XDrawSegments
		(display_id, overlay_pixmap, gc_draw_vector[dash_style],
		 segments_overlay, *ilines);
	}

	XSync(display_id, False);

}
/*********************************************************************/
/* Draw a map in the window, called from the fortran                 */
/*********************************************************************/
void	drawimage_ (int *nx, int *ny, int *isec, unsigned char *map, int *cflag)
{
	unsigned int	ncheck;
	int		row;
	int		col;
	static char	*pixel;

/* initialize */
	pan_flag = 0;
	pan_x = 0;
	pan_y = 0;
	image_dest_x = 0;
	image_dest_y = 0;
	map_width = *nx;
	map_height = *ny;
	map_no = *isec;
	map_size = map_width * map_height;

/* set number of maps to 1 if not set */
	if(no_maps == 0) no_maps = 1;

/* set image height and width */
	image_width = map_width;
	image_height = map_height;

/* calculate map size */
	if(no_maps == 1)
	{
	if(image_width < window_width)
		image_width = window_width;

	if(image_height < window_height)
		image_height = window_height;
	}
	else
	  {
	   image_dest_x = (window_width - map_width) / 2;
	   image_dest_y = (window_height - map_height) / 2;
	 }

/* if not first image, clear window */
	if(image_flag == 1) 
	{
		image->data = 0;
		XDestroyImage(image);
		XFreePixmap(display_id, screen_pixmap[map_no]);
		XSync(display_id, False);
		if (icallback_value == 999) return;
	}

/* Create pixmap to hold image */
	screen_pixmap[map_no] = XCreatePixmap(display_id, image_area_window_id,
			image_width, image_height, image_depth);

/* Create pixmap to store image for vector refresh*/
	store_pixmap[map_no] = XCreatePixmap(display_id, image_area_window_id,
			image_width, image_height, image_depth);

/* check resources sufficient, else trap and return */
	XSync(display_id, False);
	if (icallback_value == 999) return;

/* fill pixmaps with blanks */
	XFillRectangle(display_id, screen_pixmap[map_no], gc_fill_rectangle, 
			image_source_x, image_source_y,
			image_width, image_height);

	XFillRectangle(display_id, store_pixmap[map_no], gc_fill_rectangle, 
			image_source_x, image_source_y,
			image_width, image_height);

/* load colours to image pointer array */
        for (i = 0; i < map_size; i++)
        {
		image_pointer[i*4] = (char) (colours[map[i]].blue / 256);
                image_pointer[i*4 + 1] = (char) (colours[map[i]].green / 256);
                image_pointer[i*4 + 2] = (char) (colours[map[i]].red / 256);
                image_pointer[i*4 + 3] = map[i];
        }

/* create image */
	image = XCreateImage(display_id,visual_id,
		     image_depth, image_format, image_offset,
		     &image_pointer[0], map_width, map_height, 
		     image_bitmap_pad, image_bytes_per_line);

/* copy map into pixmap ready for display */
	XPutImage(display_id,screen_pixmap[map_no],
		  gc_fill_rectangle, image, image_source_x, image_source_y,
		  image_source_x, image_source_y, map_width, map_height);

/* copy map into pixmap for store */
	XPutImage(display_id,store_pixmap[map_no],
		  gc_fill_rectangle, image, image_source_x, image_source_y,
		  image_source_x, image_source_y, map_width, map_height);

	XSync(display_id, False);
	if (icallback_value == 999) return;

/* start consistency check to see if pixmap can be read back correctly */
	j = 0;
	i = 0;
	*cflag = 0;

/* find first nonzero entry */
	if(map[0] == 0)
	{
		while (j == 0)
		{
			if(map[i] == 0)
			{
				i++;
				if(i > map_size)
				{
					fprintf(stderr,"Map all zeroes.");
					*cflag = 2;
					goto allzero;
				}
			}
			else
			        j = i;
		}
	}


	row = j / map_width;
	col = j - row * map_width;

/* read back first 50 non-zero pixels */
	if(map_width >= 50)
		ncheck = 50;
	else
		ncheck = map_width;

	if(map_width - col < 50)
		ncheck = map_width - col;

	readimage = XGetImage(display_id,screen_pixmap[map_no],
			col, row, ncheck, 1, -1, image_format);

	if(readimage == NULL)
		{
		printf("XGetImage in screen read back failed\n");
		exit(1);
		}

	pixel = readimage->data;

/* compare newly read pixmap entries with those written - increment by 4 as 4 bytes loaded*/
	for (i = j; i < j + ncheck; i++)
		     pixel = pixel + 4;

	XDestroyImage(readimage);
	XSync(display_id, False);
	if (icallback_value == 999) 
	{
		fprintf(stderr,"Failed consistency check.");
		return;
	}

/* display image in window */
allzero:if(map_no == no_maps - 1)
	{
	image_flag = 1;
	image_refresh = 1;
        drawpixmap_ (&map_no, &image_refresh);
	}
}
/*********************************************************************/
/* draw pixmap in window */
/*********************************************************************/
void 	drawpixmap_ (int *isec, int *refresh)
{
	map_no = *isec;
/* Draw pixmap */
	if (image_flag != 0)
	{
/* copy store_pixmap to screen_pixmap */
		if(*refresh == 1)
		{
/* fill pixmaps with blanks */
		XFillRectangle(display_id, screen_pixmap[map_no], gc_fill_rectangle, 
			image_source_x, image_source_y,
			image_width, image_height);

		XCopyArea(display_id, store_pixmap[map_no], screen_pixmap[map_no], 
		  gc_fill_rectangle, 0, 0, 
		  image_width, image_height,
	          image_dest_x, image_dest_y);
		}

/* copy screen_pixmap to image area */
		XCopyArea(display_id, screen_pixmap[map_no], image_area_window_id,
		  gc_fill_rectangle, -pan_x, -pan_y, 
		  image_width, image_height,
	          image_dest_x, image_dest_y);

/* redraw vector list */
		if(nboxes > 0 || ncircles > 0 || nlines > 0 || npoints > 0 || ntext > 0) 
			{
			redrawvectors_ ();
			}
	}

 	image_refresh = 0;
/* process waiting events and flush the remainder */ 
	XFlush(display_id);
	event_get_next_(&event_true);
	if(event_true != 0) eventdispatch_ ();
	expose_flag = 1;
	refresh = 0;

}
/*********************************************************************/
/* draw overlay map in window */
/*********************************************************************/
void 	drawoverlay_ (int *overlay_refresh) 
/* copies map into the panned position moves overlay window with main window */
{

	if(overlay_flag != 0)
	  {
/* move overlay widget to correct place on window and set colours */
		XtVaSetValues( overlay_area,
			XtNfromHoriz, NULL,
			XtNfromVert, NULL,
			XtNwidth, (Dimension)overlay_width+2,
			XtNheight, (Dimension)overlay_height+2,
			XtNhorizDistance, overlay_window_x,
			XtNvertDistance, overlay_window_y,
			XtNborderWidth, (Dimension) 0,
			XtNborderColor, (Pixel)colour_red,
			XtNforeground, (Pixel)colour_black,
			XtNbackground, (Pixel)colour_white,
			NULL);

		XtManageChild(overlay_area);

/* put overlay window in front of image area */
		XRaiseWindow(display_id,overlay_area_window_id);

/* copy image to overlay store pixmap */
	if(*overlay_refresh == 1)
		{
		XCopyArea(display_id, overlay_store_pixmap, overlay_pixmap,
		  gc_fill_rectangle, 0, 0, 
	          (unsigned int)overlay_width+2, 
		  (unsigned int)overlay_height+2,
	          0, 0);

		XCopyArea(display_id, overlay_pixmap, overlay_area_window_id,
		  gc_fill_rectangle, 0, 0, 
	          (unsigned int)overlay_width+2, 
		  (unsigned int)overlay_height+2,
	          0, 0);

/* copy vectors into overlay */
		redrawvectors_ ();	
		}
/* copy overlay pixmap into image area */
		else
		{

		XCopyArea(display_id, overlay_pixmap, overlay_area_window_id,
		  gc_fill_rectangle, 0, 0, 
	          (unsigned int)overlay_width+2, 
		  (unsigned int)overlay_height+2,
	          0, 0);
		}

	}

/* process waiting events and flush the remainder */ 
	XFlush(display_id);
	event_get_next_(&event_true);
	if(event_true != 0) eventdispatch_ ();
	expose_flag = 1;
//	*overlay_refresh = 0;
	overlay_refresh = 0;
}
/*********************************************************************/
/* Draw a point in the window, called from the call-back routine */
/*********************************************************************/
void drawpoint_ (int *ix, int *iy)
{
/* draw in main window */
	if(overlay_flag == 0)
	{
		if ( npoints >= max_points )
	 	{
		printf ("Too many points, max_points too small.\n");
		npoints = max_points;
		return;
	 	}
		point_x[npoints] = *ix;
		point_y[npoints] = *iy;

/* draw point in image area */
		XDrawPoint
		(display_id, image_area_window_id, gc_draw_vector[0],
		 point_x[npoints] + pan_x, point_y[npoints] + pan_y);

/* draw point in pixmap */
		XDrawPoint
		(display_id, screen_pixmap[map_no], gc_draw_vector[0],
		 point_x[npoints], point_y[npoints]);

/* draw point in zoomed area */
		if (zoom_flag == 1) 
		{
		convert_to_zoom(point_x[npoints],point_y[npoints],&zoom_x1, &zoom_y1);

/* draw into zoom window */
		XDrawPoint(display_id, zoom_area_window_id, gc_draw_vector[0], 
		zoom_x1, zoom_y1);

/* draw into zoom pixmap */
		XDrawPoint(display_id, zoom_pixmap, gc_draw_vector[0], 
		zoom_x1, zoom_y1);
		}
		npoints++;
	}
/* draw in overlay window */
	else
	{
		if ( npoints_overlay >= max_points )
	 	{
		printf ("Too many points, max_points too small.\n");
		npoints_overlay = max_points;
		return;
	 	}
		point_x_overlay[npoints_overlay] = *ix;
		point_y_overlay[npoints_overlay] = *iy;

/* draw point in overlay area */
		XDrawPoint
		(display_id, overlay_area_window_id, gc_draw_vector[0],
		 point_x_overlay[npoints_overlay], point_y_overlay[npoints_overlay]);

/* draw point in overlay pixmap */
		XDrawPoint
		(display_id, overlay_pixmap, gc_draw_vector[0],
		 point_x_overlay[npoints_overlay], point_y_overlay[npoints_overlay]);
		npoints_overlay++;
	}

	XSync(display_id, False);

}
/*********************************************************************/
/* Draw a text string in the window, called from the call-back routine */
/*********************************************************************/
void drawtext_ (int *ix, int *iy, char *text_pointer, int *length)
{
	if(overlay_flag == 0)
	{	
		if ( ntext >= max_text )
		{
		printf ("Too many text strings, max_text too small.\n");
	        ntext = max_text;
		return;
		}

/* draw in main window */
		text_x[ntext] = *ix;
		text_y[ntext] = *iy;
		strcpy(&text_string[ntext][0],text_pointer);
		text_lengths[ntext] = *length;
		text_font[ntext] = font_id;

/* draw string in image area */
		XDrawString
		(display_id, image_area_window_id, gc_draw_vector[0], 
		text_x[ntext] + pan_x, text_y[ntext] + pan_y,
		&text_string[ntext][0], text_lengths[ntext]);

/* draw string in pixmap */
		XDrawString
		(display_id, screen_pixmap[map_no], gc_draw_vector[0], 
		text_x[ntext], text_y[ntext],
		&text_string[ntext][0], text_lengths[ntext]);

/* draw in zoom window if present */
		if (zoom_flag == 1)
	
		{
		convert_to_zoom(text_x[ntext], text_y[ntext], 
					&zoom_x1, &zoom_y1);

		XDrawString(display_id, zoom_area_window_id, gc_draw_vector[0], 
		zoom_x1, zoom_y1,
		&text_string[ntext][0], text_lengths[ntext]);

/* draw into zoom pixmap */
		XDrawString(display_id, zoom_pixmap, gc_draw_vector[0], 
		zoom_x1, zoom_y1,
		&text_string[ntext][0], text_lengths[ntext]);
		}
		ntext++;
	}
/* draw in overlay window */
	else
	{
		if ( ntext_overlay >= max_text )
		{
		printf ("Too many text strings, max_text too small.\n");
	        ntext_overlay = max_text;
		return;
		}

		text_x_overlay[ntext_overlay] = *ix;
		text_y_overlay[ntext_overlay] = *iy;
		strcpy(&text_string_overlay[ntext_overlay][0],text_pointer);
		text_lengths_overlay[ntext_overlay] = *length;
		text_font[ntext_overlay] = font_id;

/* draw string in overlay area */
		XDrawString
		(display_id, overlay_area_window_id, gc_draw_vector[0], 
		text_x_overlay[ntext_overlay], text_y_overlay[ntext_overlay],
		&text_string_overlay[ntext_overlay][0], text_lengths_overlay[ntext_overlay]);

/* draw string in overlay pixmap */
		XDrawString
		(display_id, overlay_pixmap, gc_draw_vector[0], 
		text_x_overlay[ntext_overlay], text_y_overlay[ntext_overlay],
		&text_string_overlay[ntext_overlay][0], text_lengths_overlay[ntext_overlay]);
		ntext_overlay++;
	}

	XSync(display_id, False);
}
/*****************************************************************/
/* error handler */
/*********************************************************************/
int	error_trap(Display *error_id, XErrorEvent *p_error)
{
	char	error_message[80];
	icallback_value = 0;
	if(p_error->error_code == BadAlloc &&
		p_error->request_code == X_CreatePixmap)
	{
		printf("error trapped \n");
		icallback_value = 999;
		ximagecallback_ (&icallback_value);
	}
	else
	{
		XGetErrorText(error_id,p_error->error_code,
			error_message, 80);
		fprintf(stderr," Error detected:\n %s\n", error_message);
		fprintf(stderr," Error code: %d\n", p_error->request_code);
		exit(1);
	}
	return((int) 0);
}
/*****************************************************************/
/* get next event */
/*********************************************************************/
void 	event_get_next_ (int *event_true)
{
	XtInputMask	event_mask;

	event_mask = XtAppPending(app_context);
	if (event_mask == 0)
	{
		*event_true = 0;
		return;
	}
	*event_true = 1;
}
/*********************************************************************/
/* get the event and dispatch it */
/*********************************************************************/
void 	eventdispatch_ ()
{
	XtAppNextEvent(app_context, &event_return);
	XtDispatchEvent(&event_return);
}
/*********************************************************************/
/* return font height */
/*********************************************************************/
void	getfontsize_ (int *fontsize)
{
	*fontsize = font_height;
}
/*********************************************************************/
/* return font height */
/*********************************************************************/
void	getioboxwidth_ (int *ioboxwidth)
{
	*ioboxwidth = (int)iobox_width;
}
/*********************************************************************/
/* Initialize and display a window and basic menu on the screen */
/*********************************************************************/

void	init_ (int *max_window_width, int *max_window_height,
	       int *max_zoom_size, char *fontlist, int *nfonts)
{
        XVisualInfo 	vTemplate;
	int 		nvisuals;
	int		width;
	int		height;

/* set error handler */

	XSetErrorHandler(error_trap);

/* initialize variables*/
	map_no = 0;
	no_maps = 0;
	colour_bar_flag = 0;
	expose_flag = 0;
	expose_iobox_flag = 0;
	expose_label_flag = 0;
	expose_menu_flag = 0;
	expose_slider_flag = 0;
	image_flag = 0;
	label_flag = 0;
	iobox_flag = 0;
	menu_flag = 0;
	pointer_flag = 0;
	overlay_flag = 0;
	rubber_flag = 0;
	rubber_type = 0;
	slider_flag = 0;
	overlay_width = 128;
	overlay_height = 128;
	pan_x = 0;
	pan_y = 0;
	ring_rad = 0.;
	zoom_factor_x = 8;
	zoom_factor_y = 8;
	zoom_width = 128;
	zoom_height = 128;
	zoom_flag = 0;
	zoom_window_x1 = 0;
	zoom_window_y1 = 0;
	zoom_window_x2 = 0;
	zoom_window_y2 = 0;
	image_refresh = 0;
	zoom_refresh = 0;
	overlay_refresh = 0;
	remove_vectors = 0;

/* force integral,odd number in zoom area width, height */
	zoom_width = ((((zoom_width / zoom_factor_x) / 2) * 2) + 1) 
			* zoom_factor_x;
	zoom_height = ((((zoom_height / zoom_factor_y) / 2) * 2) + 1) 
			* zoom_factor_y;

/* Initialize top level widget */
	topLevel = XtVaAppInitialize(
        &app_context,       /* Application context */
        "Xdraw",            /* application class name */
        NULL, 0,            /* command line option list */
        &zero,NULL,         /* command line args */
        NULL,               /* for missing app-defaults file */
        NULL);              /* terminate varargs list */


	display_id = XtDisplay(topLevel);
	scr_num = DefaultScreen(display_id);

/* set up visual 24 bit truecolour */
  	nvisuals = 0;
  	vTemplate.screen = DefaultScreen(display_id);

	if (!XMatchVisualInfo(display_id, XDefaultScreen(display_id), image_depth, TrueColor, &vTemplate))
    	{
      		fprintf(stderr, "No matching visual available\n");
      		return;
    	}


	visual_id = vTemplate.visual;

/* create colour map for root window */
	colourmap = DefaultColormap(display_id, scr_num);

/* 24-bit truecolor */
        colour_black = BlackPixel(display_id, scr_num);
        colour_blue = (unsigned long) (255 + (0 <<8) + (0 << 16));
	colour_red = (unsigned long) (0 + (0 <<8) + (255 << 16));
	colour_green = (unsigned long) (0 + (255 <<8) + (0 << 16));
        colour_white = WhitePixel(display_id, scr_num);

/* set variables dependent on toplevel */
	colourmap_size = DisplayCells(display_id,scr_num);

/* destroy toplevel and recreate with corrected visual- this needs to be done
 for servers where the default visual does not match the requirement */

	XtDestroyWidget(topLevel);

	topLevel = XtVaAppCreateShell(
	(char *) NULL,
	"Xdraw",
	applicationShellWidgetClass,
	display_id,
	XtNvisual, visual_id,
	XtNcolormap, colourmap,
	XtNdepth, image_depth,
	NULL);


/* set font */
	if((font_default = XLoadQueryFont(display_id, fontname1)) == NULL)
		{
		fprintf(stderr,"Cannot load font: %s\n",fontname1);
		if((font_default = XLoadQueryFont(display_id, fontname2))
								  == NULL)
		{
			fprintf(stderr,"Cannot load font: %s\n",fontname2);
			exit(1);
		}
		}
/* Tim's hack next 2 lines */
        text_font_width = font_default->max_bounds.rbearing - font_default->min_bounds.lbearing;
        text_font_height = font_default->max_bounds.ascent + font_default->max_bounds.descent;

	font_id = font_default->fid;
	XSetFont(display_id,DefaultGC(display_id,scr_num),font_id);
	text_font[0] = font_id;
	font_height = (short int)font_default->max_bounds.ascent +
			(short int)font_default->max_bounds.descent;
	iobox_height = font_height + 1;

/* get font list for courier */
	courier_fonts = XListFonts(display_id, courier_font,
				maxfontsperstyle, &no_fonts);
	for (i = 0; i < no_fonts && i < max_fonts; i++)
	{
		fonts[i] = courier_fonts[i];
		strcpy(fontlist,courier_fonts[i]);
		fontlist = fontlist + font_length;
	}
	total_fonts = no_fonts;

/* get font list for fixed - under test */
	fixed_fonts = XListFonts(display_id, fixed_font,
				maxfontsperstyle, &no_fonts);
	for (i = 0; i < no_fonts && i+total_fonts < max_fonts; i++)
	{
		fonts[i+total_fonts] = fixed_fonts[i];
		strcpy(fontlist,fixed_fonts[i]);
		fontlist = fontlist + font_length;
	}
	total_fonts = total_fonts + no_fonts;

/* get font list for helvetica - under test */
	helvetica_fonts = XListFonts(display_id, helvetica_font,
				maxfontsperstyle, &no_fonts);
	for (i = 0; i < no_fonts && i+total_fonts < max_fonts; i++)
	{
		fonts[i+total_fonts] = helvetica_fonts[i];
		strcpy(fontlist,helvetica_fonts[i]);
		fontlist = fontlist + font_length;
	}
	total_fonts = total_fonts + no_fonts;

/* get font list for times - under test */
	times_fonts = XListFonts(display_id, times_font,
				maxfontsperstyle, &no_fonts);
	for (i = 0; i < no_fonts && i+total_fonts < max_fonts; i++)
	{ 
		fonts[i+total_fonts] = times_fonts[i];
		strcpy(fontlist,times_fonts[i]);
		fontlist = fontlist + font_length;
	}
	total_fonts = total_fonts + no_fonts;
	if(total_fonts > max_fonts) total_fonts = max_fonts;
	*nfonts = total_fonts;

/* Create form widget to manage all the others */
	mainForm = XtVaCreateManagedWidget(
        "mainForm",              /* widget name */
        formWidgetClass,         /* widget class */
        topLevel,               /* parent widget*/
        NULL);                   /* argument list*/

/* get screen size */
	window_width = DisplayWidth(display_id, scr_num) - border_size;
	window_height = DisplayHeight(display_id, scr_num) - border_size;
	width = *max_window_width;
	height = *max_window_height;
	if (width < 0 || height < 0)
		{
		if (abs(width)  <= window_width &&
		    abs(height) <= window_height)
			{
			window_width  = abs(width);
			window_height = abs(height);
			}
		else
			{
			printf("Requested window size too large\n");
			return;
			}
		}
	*max_window_height = window_height;
	*max_window_width = window_width;
	*max_zoom_size = window_width * window_height;

/* set pointer tracking coordinate positions */
	pointer_x = window_width - border_size * 2;
	pointer_y = border_size;

/* add image area widget */
       image_area = XtVaCreateManagedWidget(
        "image_area",	
        simpleWidgetClass,
        mainForm,	
	XtNwidth, (Dimension)window_width,
	XtNheight, (Dimension)window_height,
	XtNbackground, None,
	XtNborderColor, (Pixel)colour_red,
	XtNborderWidth, (Dimension) 1,
        NULL);

/* add zoom area widget */
    zoom_area = XtVaCreateWidget(
        "zoom_area",		/* widget name */
        simpleWidgetClass,		/* widget class */
        mainForm,		/* parent widget*/
	XtNwidth, (Dimension)zoom_width,
	XtNheight, (Dimension)zoom_height,
	XtNborderWidth, (Dimension) 1,
	XtNborderColor, (Pixel)colour_red,
	XtNborderWidth, (Dimension) 0,
        NULL			/* argument list*/
        );

/* add overlay area widget */
    overlay_area = XtVaCreateWidget(
        "overlay_area",		/* widget name */
        simpleWidgetClass,		/* widget class */
        mainForm,		/* parent widget*/
	XtNwidth, (Dimension)overlay_width,
	XtNheight, (Dimension)overlay_height,
	XtNborderColor, (Pixel)colour_red,
	XtNborderWidth, (Dimension) 0,
        NULL			/* argument list*/
        );

/* add label widget */
    label = XtVaCreateWidget(
	"label",              
	labelWidgetClass,     
	mainForm,
	XtNfont, font_default,
	XtNborderColor, (Pixel)colour_red,
	XtNforeground, (Pixel)colour_black,
	XtNbackground, (Pixel)colour_white,
        NULL			/* argument list*/
        );

/* Create box widget to manage ioBox */
    ioBox = XtVaCreateWidget(
	"ioBox",              /* widget name */
	boxWidgetClass,         
	mainForm,               /* parent widget*/
	XtNheight, iobox_height,
	XtNwidth, iobox_width,
	XtNborderColor, (Pixel)colour_red,
	XtNbackground, (Pixel)colour_white,
	XtNforeground, (Pixel)colour_black,
	NULL);      

/* create label part of ioBox widget */
    ioBox_label = XtVaCreateWidget(
	"ioBox_label",              /* widget name */
	labelWidgetClass,         /* widget class */
	ioBox,               /* parent widget*/
	XtNfont, font_default,
	XtNjustify, XtJustifyLeft,
	XtNwidth, label_width,
	XtNborderColor, (Pixel)colour_red,
	XtNforeground, (Pixel)colour_black,
	XtNbackground, (Pixel)colour_white,
	NULL);      

/* create text part of ioBox widget */
	strcpy(store_string," ");
    ioBox_text = XtVaCreateWidget(
	"ioBox_text",              /* widget name */
	asciiTextWidgetClass,         /* widget class */
	ioBox,               		/* parent widget*/
	XtNuseStringInPlace, (Boolean)True,
	XtNstring, store_string,
	XtNeditType, XawtextEdit,
	XtNfont, font_default,
	XtNwidth, label_width,
	XtNlength, string_length,
	XtNborderColor, (Pixel)colour_red,
	XtNbackground, (Pixel)colour_white,
	XtNforeground, (Pixel)colour_black,
	NULL);      

/* Create box widget to manage slider */
    sliderBox = XtVaCreateWidget(
	"sliderBox",              /* widget name */
	formWidgetClass,         /* widget class */
	mainForm,               /* parent widget*/
	XtNborderColor, (Pixel)colour_red,
	XtNbackground, (Pixel)colour_white,
	XtNforeground, (Pixel)colour_black,
	NULL); 
     
/* create slider bar label */
     slider_label = XtVaCreateManagedWidget(
        "slider_label",
	labelWidgetClass,         /* widget class */
        sliderBox,
	XtNfont, font_default,
	XtNfromHoriz, NULL,
	XtNlabel, 
	"Centre mouse button moves slider,Click left button in map to quit",
	XtNborderColor, (Pixel)colour_red,
	XtNforeground, (Pixel)colour_black,
	XtNbackground, (Pixel)colour_white,
	NULL);

/* create slider bar 1 widget */
     slider1 = XtVaCreateManagedWidget(
        "slider1",
        scrollbarWidgetClass,
        sliderBox,
        XtNwidth, slider_width,
        XtNheight, slider_height,
        XtNorientation, XtorientHorizontal,
	XtNborderColor, (Pixel)colour_red,
	XtNforeground, (Pixel)colour_black,
	XtNbackground, (Pixel)colour_white,
	XtNfromVert, slider_label,
	XtNvertDistance, 5,
	XtNscrollDCursor, cursor,
	XtNscrollHCursor, cursor,
	XtNscrollLCursor, cursor,
	XtNscrollRCursor, cursor,
	XtNscrollUCursor, cursor,
	XtNscrollVCursor, cursor,
	NULL);

	XtAddCallback(slider1, XtNjumpProc, 
		(XtCallbackProc)action_slider_callback, (XtPointer)1);

/* add colour bar 1 widget */
       colour_bar1 = XtVaCreateManagedWidget(
        "colour_bar1",	
        simpleWidgetClass,
        sliderBox,
	XtNwidth, slider_width,
	XtNheight, slider_height,
	XtNbackground, (Pixel)colour_black,
	XtNborderColor, (Pixel)colour_red,
        NULL);

/* create slider bar 2 widget */
     slider2 = XtVaCreateManagedWidget(
        "slider2",
        scrollbarWidgetClass,
        sliderBox,
        XtNwidth, slider_width,
        XtNheight, slider_height,
        XtNorientation, XtorientHorizontal,
	XtNborderColor, (Pixel)colour_red,
	XtNforeground, (Pixel)colour_black,
	XtNbackground, (Pixel)colour_white,
	XtNfromVert, slider_label,
	XtNvertDistance, 5,
	XtNscrollDCursor, cursor,
	XtNscrollHCursor, cursor,
	XtNscrollLCursor, cursor,
	XtNscrollRCursor, cursor,
	XtNscrollUCursor, cursor,
	XtNscrollVCursor, cursor,
	NULL);

	XtAddCallback(slider2, XtNjumpProc, 
		(XtCallbackProc)action_slider_callback, (XtPointer)2);

/* add colour bar 2 widget */
       colour_bar2 = XtVaCreateManagedWidget(
        "colour_bar2",	
        simpleWidgetClass,
        sliderBox,
	XtNwidth, slider_width,
	XtNheight, slider_height,
	XtNbackground, (Pixel)colour_black,
	XtNborderColor, (Pixel)colour_red,
        NULL);

	XtRealizeWidget(topLevel);

/* Add actions to read pointer position and re-display menu */
	XtAppAddActions(app_context, actlist, XtNumber(actlist));
	trans_table = XtParseTranslationTable(Trans);
	XtAugmentTranslations(mainForm,trans_table);

/* Add actions to return string after <cr> in ioBox */
	iobox_trans_table = XtParseTranslationTable(Io_trans);
	XtOverrideTranslations(ioBox_text,iobox_trans_table);


/* Add event handlers for expose events - added (XtPointer) 13.06.2012 */
	XtAddEventHandler
	(image_area,ExposureMask,False,(XtEventHandler)redraw_image,(XtPointer)0);
	XtAddEventHandler
	(overlay_area,ExposureMask,False,(XtEventHandler)redraw_overlay,(XtPointer)0);
	XtAddEventHandler
	(zoom_area,ExposureMask,False,(XtEventHandler)redraw_zoom,(XtPointer)0);
	XtAddEventHandler
	(ioBox,ExposureMask,False,(XtEventHandler)expose_iobox,(XtPointer)0);
	XtAddEventHandler
	(label,ExposureMask,False,(XtEventHandler)expose_label,(XtPointer)0);
	XtAddEventHandler
	(sliderBox,ExposureMask,False,(XtEventHandler)expose_slider,(XtPointer)0);

/* initialize window ids etc. note : topLevel_window_id can only be
        set after realizing the widget */

	topLevel_window_id = XtWindow(topLevel);
	image_area_window_id = XtWindow(image_area);
	zoom_area_window_id = XtWindow(zoom_area);
	colour_bar1_window_id = XtWindow(colour_bar1);
	colour_bar2_window_id = XtWindow(colour_bar2);
	label_window_id = XtWindow(label);
	ioBox_window_id = XtWindow(ioBox);
	overlay_area_window_id = XtWindow(overlay_area);
	sliderBox_window_id = XtWindow(sliderBox);
	image_format = ZPixmap;

/* set array subscripts for special bits Tim does not have these 5 lines*/
	white = colourmap_size - 1;
	black = colourmap_size - 2;
	red = colourmap_size - 3;
	blue = colourmap_size - 4;
	green = colourmap_size - 5;


/* specify colourmap attribute as not necessarily default visual */
	attributes.colormap = colourmap;
	valuemask |=CWColormap;
	XChangeWindowAttributes(display_id,XtWindow(mainForm),valuemask,
				&attributes);
	XChangeWindowAttributes(display_id,image_area_window_id,valuemask,
				&attributes);
	XChangeWindowAttributes(display_id,zoom_area_window_id,valuemask,
				&attributes);

/* create graphics contexts */
	gc_draw_vector[0] = XCreateGC
		(display_id,image_area_window_id,0,NULL);
	gc_draw_vector[1] = XCreateGC
		(display_id,image_area_window_id,0,NULL);
	gc_fill_rectangle = XCreateGC
		(display_id,image_area_window_id,0,NULL);
	gc_red_rectangle = XCreateGC
		(display_id,image_area_window_id,0,NULL);
	dash_style = 0;

/* set default font */
	XSetFont(display_id, gc_draw_vector[0], font_id);
	XSetFont(display_id, gc_draw_vector[1], font_id);

/* load colour bar map with densities */
	for (n = 0; n < slider_width; n++)
		{
		for (i = 0; i < slider_height; i++)
			colour_bar_map[i][n] = n;
		}

/* copy colour bar map to pointer array */
//	if(image_depth == 8)
//		memcpy(&colour_bar_pointer[0],colour_bar_map,
//		(unsigned int)slider_width * slider_height);
//	else
/* 24-bit colour, load map into 4 bytes per entry */
//	{
		for (n = 0; n < slider_width; n++)
		{
			for (i = 0; i < slider_height; i++)
				for (j = 0; j < 4; j++)
				colour_bar_pointer[(i*slider_width+n)*4+j] = 
					colour_bar_map[i][n];
		}

//	}

}
/*********************************************************************/
/* display io box */
/*********************************************************************/
void	ioboxdisplay_ (char *out_string, char *edit_string, int *istrings)
{
        int     	nstrings;

/* unmanage iobox if present */
	if (iobox_flag == 1)
		ioboxhide_ ();

/* unmanage label if present */
	if(label_flag == 1)
		labelhide_ ();
	label_flag = 0;

/* reset ioBox values to control label and text widgets*/
	nstrings = *istrings;

/* position iobox wrt top of window */
	XtVaSetValues(ioBox,
			XtNheight,
			(Dimension)(iobox_height*(nstrings+1)
						+ iobox_gap + font_spacer),
			XtNfromVert, NULL,
			XtNvertDistance, font_spacer,
	        	NULL);      
	XtManageChild(ioBox);

/* reset values for label part of iobox */
	XtVaSetValues(ioBox_label,
			XtNheight,
			(Dimension)(iobox_height * nstrings + font_spacer),
			XtNlabel,out_string,
	        	NULL);      
	XtManageChild(ioBox_label);

/* reset values for text part of iobox, extra height needed or text will
		not display */

	strcpy(store_string,edit_string);
	XtVaSetValues(ioBox_text,
		        XtNheight, iobox_height + font_spacer,
			XtNstring, store_string,
	        	NULL);      

/* focus keyboard so that the pointer can be anywhere */
	XtSetKeyboardFocus(mainForm,ioBox_text);

	XtManageChild(ioBox_text);
	XRaiseWindow(display_id,ioBox_window_id);

/* next display slider if present */
	if(slider_flag >= 1)
	{
		XtUnmanageChild(sliderBox);
		XtVaSetValues(sliderBox,
		XtNfromVert, ioBox,
		XtNvertDistance,1,
		XtNcursor, cursor,
        	NULL);                

		XRaiseWindow(display_id,sliderBox_window_id);
		XtManageChild(sliderBox);
	}

/* if menu present, reposition it wrt slider */
	if(slider_flag >= 1 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, sliderBox,
		XtNvertDistance,1,
        	NULL);                

		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}
/* if menu but no slider, reposition wrt iobox */
	else if(slider_flag == 0 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, ioBox,
		XtNvertDistance,1,
        	NULL);                

		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}

	while (expose_iobox_flag == 0)
	 eventdispatch_ ();

	XSync(display_id, False);
	expose_iobox_flag = 0;
	iobox_flag = 1;
}
/*********************************************************************/
/* hide iobox */
/*********************************************************************/
void	ioboxhide_ ()
{
	if(iobox_flag == 1)
	{
		XtUnmanageChild(ioBox);
		iobox_flag = 0;
		XtSetKeyboardFocus(mainForm,mainForm);

/* if slider and menu reposition menu wrt slider */
		if(slider_flag >= 1 && menu_flag == 1)
		{
			XtUnmanageChild(sliderBox);
			XtVaSetValues(sliderBox,
			XtNfromVert, NULL,
			XtNvertDistance,1,
			XtNcursor, cursor,
	        	NULL);                

			XRaiseWindow(display_id,sliderBox_window_id);
			XtManageChild(sliderBox);
/* reposition menu */
			XtUnmanageChild(menuBox);
			XtVaSetValues(menuBox,
			XtNfromVert, sliderBox,
			XtNvertDistance,1,
	        	NULL);                
			XRaiseWindow(display_id,menuBox_window_id);
			XtManageChild(menuBox);
		}
/* reposition menu to top of window */
		else if(slider_flag == 0 && menu_flag == 1)
		{
			XtUnmanageChild(menuBox);
			XtVaSetValues(menuBox,
			XtNfromVert, NULL,
			XtNvertDistance,4,
	        	NULL);  
			XRaiseWindow(display_id,menuBox_window_id);
			XtManageChild(menuBox);
		}
	XSync(display_id, False);
	}
}
/*********************************************************************/
/* return io box */
/*********************************************************************/
void	ioboxreadstring_ (char *in_string)
{
	strcpy(in_string,store_string);
}
/*********************************************************************/
/* display label */
/*********************************************************************/
void	labeldisplay_ (char *label_string)
{

/* unmanage label if present */
	if(label_flag == 1)
		labelhide_ ();

/* unmanage iobox if present */
	if (iobox_flag == 1)
		ioboxhide_ ();
	iobox_flag = 0;

/* position label at top of screen */
	XtVaSetValues(label,
	XtNlabel, label_string,
	XtNfromVert, NULL,
	XtNvertDistance,4,
       	NULL);                

/* position slider below it if present */
	if(slider_flag >= 1)
	{
		XtUnmanageChild(sliderBox);
		XtVaSetValues(sliderBox,
		XtNfromVert, label,
		XtNvertDistance,1,
		XtNcursor, cursor,
        	NULL);                

		XRaiseWindow(display_id,sliderBox_window_id);
		XtManageChild(sliderBox);
	}

/* if menu present, reposition it wrt slider */
	if(slider_flag >= 1 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, sliderBox,
		XtNvertDistance,1,
        	NULL);                

		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}
/* if menu but no slider, reposition wrt label */
	else if(slider_flag == 0 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, label,
		XtNvertDistance,1,
        	NULL);                

		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}

/* manage label widget */
	XtManageChild(label);
	XRaiseWindow(display_id,label_window_id);

	while (expose_label_flag == 0)
	 eventdispatch_ ();

	XSync(display_id, False);
	expose_label_flag = 0;
	label_flag = 1;
}
/*********************************************************************/
/* hide label */
/*********************************************************************/
void	labelhide_ ()
{
/* if slider and menu reposition slider to top & menu wrt slider */
	if(slider_flag >= 1 && menu_flag == 1)
	{
		XtUnmanageChild(sliderBox);
		XtVaSetValues(sliderBox,
		XtNfromVert, NULL,
		XtNvertDistance,1,
		XtNcursor, cursor,
        	NULL);                

		XRaiseWindow(display_id,sliderBox_window_id);
		XtManageChild(sliderBox);

/* reposition menu wrt slider */
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, sliderBox,
		XtNvertDistance,1,
        	NULL);                
		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}
/* reposition menu to top of window */
	else if(slider_flag == 0 && menu_flag == 1)
	{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
		XtNfromVert, NULL,
		XtNvertDistance,4,
        	NULL);  
		XRaiseWindow(display_id,menuBox_window_id);
		XtManageChild(menuBox);
	}

/* hide label */
	if(label_flag == 1)
	{
	XtUnmanageChild(label);
	XSync(display_id, False);
	label_flag = 0;
	}
}
/*********************************************************************/
/* Initialize menu */
/*********************************************************************/
void	menu_callback(Widget w, XtPointer client_data,
			XawListReturnStruct *call_data)
{
/* unhighlight item */
	XawListUnhighlight(w);

/* set dummy arguments */
	icallback_value = call_data->list_index + 1;
	ximagecallback_ (&icallback_value);
}
/*********************************************************************/
/* set flag for continuous processing 				     */
/*********************************************************************/
void	menuevent_ ()
{
	event_proc = 1;
}
/*********************************************************************/
/* hide the menu */
/*********************************************************************/
void 	menuhide_ ()
{
	if(menu_flag == 1)
	{
	XtUnmanageChild(menuBox);

/* remove event handler if waiting for events */
	if(event_proc == 0)
		XtRemoveEventHandler
		(menuBox,ExposureMask,False,(XtEventHandler)expose_menu,0);

	event_proc = 0;

	redraw_image (&image_refresh);
	XSync(display_id, False);
	menu_flag = 0;
	}
}
/**************************************************************************/
/* initialize and display menu                                            */
/**************************************************************************/
void	menuinit_ (int *nitems, int *max_length, char *menulist)
{
	int 		nlist;
	int 		length;

	nlist = *nitems;
	length = *max_length;

/* set up cursor colour, depth 1 so use 1-byte value for 8 & 24-bit */
	cursor_foreground.pixel = black;
	cursor_foreground.flags = DoRed | DoGreen | DoBlue;
	cursor_foreground.red = 0;
	cursor_foreground.green = 0;
	cursor_foreground.blue = 0;

	cursor_background.pixel = white;
	cursor_background.flags = DoRed | DoGreen | DoBlue;
	cursor_background.red = 65535;
	cursor_background.green = 65535;
	cursor_background.blue = 65535;

/* copy default arrow cursor shape and mask */
	memcpy(cursor_shape,cursor_shape0,30);
	memcpy(cursor_mask,cursor_mask0,30);
	cursor_x = 4;
	cursor_y = 1;

/* If menu already present, destroy old widget and remove event handler */
	if(menu_flag == 1) 
		{
		XtDestroyWidget(menuBox);
		menu_flag = 0;
		}

	for (i = 0; i < nlist; i++)
	{
		menu_pointer[i] = menulist;
		menulist = menulist + length;
	}

/* slider bar present, position beneath it */
	if(slider_flag >= 1)
	        menuBox = XtVaCreateManagedWidget(
        		"menuBox",              /* widget name */
        		listWidgetClass,         /* widget class */
	        	mainForm,               /* parent widget*/
			XtNfont, font_default,
			XtNlist, menu_pointer,
			XtNnumberStrings, nlist,
			XtNborderColor, (Pixel)colour_red,
			XtNforeground, (Pixel)colour_black,
			XtNbackground, (Pixel)colour_white,
			XtNfromVert, sliderBox,
			XtNvertDistance,1,
			XtNforceColumns,1,
			XtNdefaultColumns,1,
			XtNcursor, cursor,
	        	NULL);                   

/* if label present, create menu below it */
	else if(label_flag == 1)
	        menuBox = XtVaCreateManagedWidget(
        		"menuBox",              /* widget name */
        		listWidgetClass,         /* widget class */
	        	mainForm,               /* parent widget*/
			XtNfont, font_default,
			XtNlist, menu_pointer,
			XtNnumberStrings, nlist,
			XtNborderColor, (Pixel)colour_red,
			XtNforeground, (Pixel)colour_black,
			XtNbackground, (Pixel)colour_white,
			XtNfromVert, label,
			XtNvertDistance,1,
			XtNforceColumns,1,
			XtNdefaultColumns,1,
			XtNcursor, cursor,
	        	NULL);                   
/* no slider or label, position directly under top of window */
	else
	        menuBox = XtVaCreateManagedWidget(
         		"menuBox",              /* widget name */
        		listWidgetClass,         /* widget class */
	        	mainForm,               /* parent widget*/
			XtNfont, font_default,
			XtNlist, menu_pointer,
			XtNnumberStrings, nlist,
			XtNborderColor, (Pixel)colour_red,
			XtNforeground, (Pixel)colour_black,
			XtNbackground, (Pixel)colour_white,
			XtNfromVert, NULL,
			XtNvertDistance,4,
			XtNforceColumns,1,
			XtNdefaultColumns,1,
			XtNcursor, cursor,
        		NULL);                   /* argument list*/

/* Add callback for menu item */
		XtAddCallback(menuBox, XtNcallback, 
		(XtCallbackProc)menu_callback, (XtPointer)0);

		menuBox_window_id = XtWindow(menuBox);
	        XSetWindowColormap(display_id,menuBox_window_id,colourmap);

		cursor_shape_pixmap = 
			XCreatePixmapFromBitmapData
			(display_id,topLevel_window_id,
			cursor_shape, cursor_size, cursor_size, 
			 white, black, cursor_depth);

		cursor_mask_pixmap = 
			XCreatePixmapFromBitmapData
			(display_id,topLevel_window_id,
			cursor_mask, cursor_size, cursor_size, 
		 	white, black, cursor_depth);

		cursor = XCreatePixmapCursor(display_id,
			cursor_shape_pixmap, cursor_mask_pixmap,
			&cursor_foreground, &cursor_background,
			cursor_x, cursor_y);

	XDefineCursor(display_id,menuBox_window_id,cursor);

/* add event handler except where continuous event processing is done */
	if(event_proc == 0)
	{
		XtAddEventHandler
		(menuBox,ExposureMask,False,(XtEventHandler)expose_menu,0);

		while (expose_menu_flag == 0)
		 eventdispatch_ ();
	}
/*	else
	{
		event_get_next_(&event_true);
	        if(event_true != 0) eventdispatch_ ();
	}*/

	XSync(display_id, False);
	expose_menu_flag = 0;
	menu_flag = 1;

}
/*********************************************************************/
/* hide overlay window */
/*********************************************************************/
void	overlayhide_ ()
{
/* Hide the overlay window if present */
	if(overlay_flag != 0)
	{
		removevectors_();
		overlay_map->data = 0;
		XDestroyImage(overlay_map);	
		XFreePixmap(display_id, overlay_pixmap);
		XtUnmanageChild(overlay_area);
		XSync(display_id, False);
/* enable pointer tracking */
//		if(pointer_flag =! 0) pointertrackon_ 
//                (&pointer_shift_x,&pointer_shift_y,&ichange_origin,&pointer_factor);
	}
	overlay_flag = 0;
}
/*********************************************************************/
/* initialize overlay window */
/*********************************************************************/
void	overlayinit_ (int *ix, int *iy, int *overlay_w, 
				int *overlay_h, unsigned char *omap)
{
/* disable pointer tracking */
//	   pointertrackoff_ ();

/* hide overlay window if present */
	if(overlay_flag != 0) 
	{
		overlayhide_ ();
		ncircles_overlay = 0;
		nboxes_overlay = 0;
		nlines_overlay = 0;
		npoints_overlay = 0;
		ntext_overlay = 0;
	}

/* hide and destroy the zoom window if already present */
	if(zoom_flag != 0) 
		zoomhide_();

/* if overlay flag = 1, overlay cannot move, = 2 moves with pan - not implemented yet */ 
	overlay_flag = 1;
	overlay_pointer_x = *ix;
	overlay_pointer_y = *iy;
	overlay_width = *overlay_w;
	overlay_height = *overlay_h;

/* calculate position for overlay window */

/* add pan shifts if window is not to move with the screen */
	overlay_window_x = overlay_pointer_x - overlay_width / 2;
	if(overlay_window_x + overlay_width > window_width) 
         	overlay_window_x = (window_width - overlay_width) / 2;
	overlay_window_y = overlay_pointer_y - overlay_height / 2;
	if(overlay_window_y + overlay_height > window_height)
                overlay_window_y = (window_height - overlay_height) / 2;

/* image too large for array size ? */
	if(overlay_width * overlay_height > max_overlay_size)
	  {
		printf
                ("error - insufficient resources - overlay map too large. \n");
		icallback_value = 999;
		ximagecallback_ (&icallback_value);
	  }

/* load colours to image pointer array */
	for(i = 0; i < overlay_width * overlay_height; i++)
        {
                overlay_pointer[i*4] = (char) (colours[omap[i]].blue / 256);
                overlay_pointer[i*4 + 1] = (char) (colours[omap[i]].green / 256);
                overlay_pointer[i*4 + 2] = (char) (colours[omap[i]].red / 256);
                overlay_pointer[i*4 + 3] = omap[i];
        }

/* create overlay map */
	overlay_map = XCreateImage(display_id,visual_id,
		     image_depth, image_format, image_offset,
		     &overlay_pointer[0], 
		     (unsigned int)overlay_width, (unsigned int)overlay_height,	
 		     image_bitmap_pad, image_bytes_per_line);

/* create overlay pixmap */
	overlay_pixmap = XCreatePixmap(display_id,overlay_area_window_id,
		(unsigned int)overlay_width+2, (unsigned int)overlay_height+2, 
		image_depth);

/* create overlay store pixmap */
	overlay_store_pixmap = XCreatePixmap(display_id,overlay_area_window_id,
		(unsigned int)overlay_width+2, (unsigned int)overlay_height+2, 
		image_depth);

	XSync(display_id, False);
	if (icallback_value == 999) return;

/* fill overlay pixmap with red pixels */
	XFillRectangle(display_id, overlay_pixmap, gc_red_rectangle, 
		image_source_x, image_source_y,
		(unsigned int)overlay_width+2, (unsigned int)overlay_height+2);

/* write overlay map into overlay pixmap leaving single red pixel border */
	XPutImage(display_id, overlay_pixmap,
		  gc_fill_rectangle,overlay_map,image_source_x, image_source_y,
		  image_source_x+1, image_source_y+1, 
		  (unsigned int)overlay_width, (unsigned int)overlay_height);

/* copy overlay pixmap into overlay_store without vectors */
		XCopyArea(display_id, overlay_pixmap, overlay_store_pixmap,
		  gc_fill_rectangle, 0, 0, 
	          (unsigned int)overlay_width+2, 
		  (unsigned int)overlay_height+2,
	          0, 0);

	XSync(display_id, False);
	if (icallback_value == 999) return;

/* set flag to refresh from main pixmap on normal events */
	overlay_refresh = 0;
	drawoverlay_ (&overlay_refresh);
}
/*********************************************************************/
/* read line of image window */
/*********************************************************************/
void	readimage_  (int *icol, int *irow, int *ipixmap, unsigned char *pixels)
{
/* ipixmap 0 read from main screen pixmap
   ipixmap 1 read from overlay pixmap */

	if(*ipixmap == 0)
		readimage = XGetImage(display_id,screen_pixmap[map_no],
			(unsigned int)*icol, (unsigned int)*irow,
			image_width,1,
			-1,image_format);
	else if(*ipixmap == 1)
		readimage = XGetImage(display_id,overlay_pixmap,
			(unsigned int)*icol, (unsigned int)*irow,
			overlay_width,1,
			-1,image_format);

	XSync(display_id, False);
	if (icallback_value == 999) return;

	if(readimage == NULL)
		{
		printf("XGetImage failed\n");
		exit(1);
		}

/* transfer data to *pixels */
	dataline = readimage->data;

	if(*ipixmap == 0)
		{
/* transfer 8-bit colour */
		if(image_depth == 8)
			for (i = 0; i < image_width; i++)
			*pixels++ = *dataline++;

/* transfer 24-bit colour */
		else
			for (i = 0; i < image_width; i++)
			{
			*pixels++ = *dataline++;
			dataline = dataline + 3;
			}
		}
	else if(*ipixmap == 1)
		{
/* transfer 8-bit colour */
		if(image_depth == 8)
			for (i = 0; i < overlay_width; i++)
			*pixels++ = *dataline++;

/* transfer 24-bit colour */
		else
			for (i = 0; i < overlay_width; i++)
			{
			*pixels++ = *dataline++;
			dataline = dataline + 3;
			}
		}

	XDestroyImage(readimage);
}
/*********************************************************************/
/* set up colour table */
/*********************************************************************/
void	setcolourtable_ 
	(int *index_start, int *nentries, 
	 int *red_pointer, int *green_pointer, int *blue_pointer)
{
//int junk;
	colour_entries = *nentries;
	colour_start = *index_start;

/* set users colour table */
	ncolours = colour_start + colour_entries - 1;

	for( i=colour_start; i < ncolours + 1; i++)
	{
                red_value = (char) (unsigned short) *red_pointer / 256;
                blue_value = (char) (unsigned short) *blue_pointer / 256;
                green_value = (char) (unsigned short) *green_pointer / 256;

                colours[i].pixel = (Pixel)(blue_value + (green_value << 8) + (red_value << 16));
                colours[i].flags = DoRed | DoGreen | DoBlue;
                colours[i].red = (unsigned short) *red_pointer;
                colours[i].green = (unsigned short) *green_pointer;
                colours[i].blue = (unsigned short) *blue_pointer;
                *red_pointer++;
                *green_pointer++;
                *blue_pointer++;

        }

	for( i = ncolours + 1; i < colourmap_size - 5; i++)
	{
                colours[i].pixel = (Pixel) colour_green;
		colours[i].flags = DoRed | DoGreen | DoBlue;
		colours[i].red = 0;
		colours[i].green = 65535;
		colours[i].blue = 0;
	}


/* pause for workstations to catch up */

        while (expose_flag == 0)
	    {
	           event_get_next_(&event_true);
	           if(event_true != 0) eventdispatch_ ();
	      }

/* set foreground colours for graphics contexts */
	XSetForeground
        (display_id, gc_fill_rectangle, (Pixel) colour_black);
	XSetForeground
        (display_id, gc_red_rectangle, (Pixel) colour_red);
        XSetForeground
        (display_id, gc_draw_vector[0], (Pixel) colour_green);
        XSetForeground
        (display_id, gc_draw_vector[1], (Pixel) colour_green);

/* set function for drawing and removing vectors */
	XSetFunction(display_id, gc_fill_rectangle, (int)GXcopy);
	XSetFunction(display_id, gc_red_rectangle, (int)GXcopy);
        XSetFunction(display_id, gc_draw_vector[0], (int)GXcopy);
        XSetFunction(display_id, gc_draw_vector[1], (int)GXcopy);

/* set dash and solid attributes*/
	XSetLineAttributes(display_id,gc_draw_vector[0],
	line_width, LineSolid, cap_style, join_style);
	XSetLineAttributes(display_id,gc_draw_vector[1],
	line_width, LineOnOffDash, cap_style, join_style);

	if (no_maps < 1) return;

	for (i = 0; i < map_size; i++)
	{
		image_pointer[i*4] = (char) (colours[image_pointer[i*4 + 3]].blue / 256);
		image_pointer[i*4 + 1] = (char) (colours[image_pointer[i*4 + 3]].green / 256);
		image_pointer[i*4 + 2] = (char) (colours[image_pointer[i*4 + 3]].red / 256);
	}

	image->data = 0;
	XDestroyImage(image);
	XSync(display_id, False);

	for (i = 0; i < no_maps; i++)
	{
	image = XCreateImage(display_id,visual_id,
		     image_depth, image_format, image_offset,
		     &image_pointer[i], map_width, map_height,
		     image_bitmap_pad, image_bytes_per_line);

/* copy map into pixmap ready for display */
	XPutImage(display_id,screen_pixmap[i],
			  gc_fill_rectangle, image, image_source_x, image_source_y,
			  image_source_x, image_source_y, map_width, map_height);

/* copy map into store pixmap  */
	XPutImage(display_id,store_pixmap[i],
			  gc_fill_rectangle, image, image_source_x, image_source_y,
			  image_source_x, image_source_y, map_width, map_height);

	redraw_image (&image_refresh);
	}

/* clear zoom window */
  	if(zoom_flag == 1)
	{
		zoom_display ();
	  	redraw_zoom (&image_refresh);		
	}

	if (colour_bar_flag > 0)
	{

		colour_bar_image->data = 0;
		XDestroyImage(colour_bar_image);
		XSync(display_id, False);

		for (n = 0; n < slider_width; n++)
			{
				for (i = 0; i < slider_height; i++)
				{
					colour_bar_pointer[(i*slider_width+n)*4] = (char) (colours[colour_bar_map[i][n]].blue / 256);
					colour_bar_pointer[(i*slider_width+n)*4 + 1] = (char) (colours[colour_bar_map[i][n]].green / 256);
					colour_bar_pointer[(i*slider_width+n)*4 + 2] = (char) (colours[colour_bar_map[i][n]].red / 256);
					colour_bar_pointer[(i*slider_width+n)*4 + 3] = colour_bar_map[i][n];

				}

			}

		colour_bar_image = XCreateImage(display_id,visual_id,
			     image_depth, image_format, image_offset,
			     &colour_bar_pointer[0], slider_width, slider_height,
			     image_bitmap_pad, image_bytes_per_line);

		/* copy colour bar to image 1 ready for display */
		XPutImage(display_id,colour_bar1_window_id,
			  gc_fill_rectangle, colour_bar_image,
			  image_source_x, image_source_y,
			  image_source_x, image_source_y, slider_width, slider_height);


		XtManageChild(colour_bar1);
	/* copy colour bar to image 2 ready for display */
		if(slider_flag == 2)
		{
			XPutImage(display_id,colour_bar2_window_id,
			  gc_fill_rectangle, colour_bar_image,
			  image_source_x, image_source_y,
			  image_source_x, image_source_y, slider_width, slider_height);

			colour_bar_flag = 2;
			XtManageChild(colour_bar2);
		}
		XSync(display_id, False);
	}

	XSync(display_id, False);
//scanf("%ijunk",&junk);
}
/*********************************************************************/
/* quit the program */
/*********************************************************************/
void	quit_ ()
{
	exit(0);
}
/*********************************************************************/
/* remove box */
/*********************************************************************/
void 	removebox_ (int *ix1, int *iy1, int *ix2, int *iy2)
{
/* main window */
	if(overlay_flag == 0)
	{
/* start loop to look for box */
		n = 0;
		nl1:
		if(box_x[n] == *ix1
		&& box_y[n] == *iy1
		&& box_width[n] == (*ix2 - *ix1)
		&& box_height[n] == (*iy2 - *iy1))
/* shift boxes up in list */
		for(i = n; i < nboxes; i++)
		{
			box_x[i] = box_x[i+1];
			box_y[i] = box_y[i+1];
			box_width[i] = box_width[i+1];
			box_height[i] = box_height[i+1];
		}
		else
		{
			n++;
			goto nl1;
		}

		nboxes = nboxes - 1;
/* check never < 0 */
		if(nboxes < 0) nboxes = 0;

/* copy pixmap to image area */
		if(remove_vectors == 0)
		{
	  	image_refresh = 1;
	  	redraw_image (&image_refresh);		

/* clear zoom window */
	  	if(zoom_flag == 1)
	  	redraw_zoom (&image_refresh);		
		}
	}
/* remove from overlay window */
	else
	{
/* start loop to look for box */
		n = 0;
		nl2:
		if(box_x_overlay[n] == *ix1
		&& box_y_overlay[n] == *iy1
		&& box_width_overlay[n] == (*ix2 - *ix1)
		&& box_height_overlay[n] == (*iy2 - *iy1))
/* shift boxes up in list */
		for(i = n; i < nboxes_overlay; i++)
		{
			box_x_overlay[i] = box_x_overlay[i+1];
			box_y_overlay[i] = box_y_overlay[i+1];
			box_width_overlay[i] = box_width_overlay[i+1];
			box_height_overlay[i] = box_height_overlay[i+1];
		}
		else
		{
			n++;
			goto nl2;
		}

		nboxes_overlay = nboxes_overlay - 1;
/* check never < 0 */
		if(nboxes_overlay < 0) nboxes_overlay = 0;

/* set overlay flag to copy from store pixmap then redraw any vectors */
		overlay_refresh = 1;
		redraw_overlay (&overlay_refresh);
	}
}
/*********************************************************************/
/* remove circle */
/*********************************************************************/
void 	removecircle_ 
	(int *ix, int *iy, float *rad)
{
	irad = (int)(*rad + 0.5);

/* remove circle from main window */
	if(overlay_flag == 0)
	{
		n = 0;
		nl1:
/* start loop to look for circle */
		if(circle_x[n] == *ix - irad
		&& circle_y[n] == *iy - irad
		&& circle_diam[n] == (int)(2.0 * *rad))
	
/* circle matches, shift circles in list */
		for(i = n; i < ncircles; i++)
		{
			circle_x[i] = circle_x[i+1];
			circle_y[i] = circle_y[i+1];
			circle_diam[i] = circle_diam[i+1];
		}

/* no match, move to next circle in list */
		else
		{
			n++;
			goto nl1;
		}

		ncircles = ncircles - 1;

/* check never < 0 */
		if(ncircles < 0) ncircles = 0;

/* copy pixmap to image area */
		if(remove_vectors == 0)
		{
	  	image_refresh = 1;
	  	redraw_image (&image_refresh);		

/* clear zoom window */
	  	if(zoom_flag == 1)
	  	redraw_zoom (&image_refresh);		
		}
	}
/* remove from overlay window */
	else
	{
		n = 0;
		nl2:
/* start loop to look for circle */
		if(circle_x_overlay[n] == *ix - irad
		&& circle_y_overlay[n] == *iy - irad
		&& circle_diam_overlay[n] == (int)(2.0 * *rad))
	
/* circle matches, shift circles in list */
		for(i = n; i < ncircles_overlay; i++)
		{
			circle_x_overlay[i] = circle_x_overlay[i+1];
			circle_y_overlay[i] = circle_y_overlay[i+1];
			circle_diam_overlay[i] = circle_diam_overlay[i+1];
		}
/* no match, move to next circle in list */
		else
		{
			n++;
			goto nl2;
		}

		ncircles_overlay--;

/* check never < 0 */
		if(ncircles_overlay < 0) ncircles_overlay = 0;

/* set overlay flag to copy from store pixmap then redraw any vectors */
		overlay_refresh = 1;
/* refresh overlay window */
		redraw_overlay (&overlay_refresh);
	}
}
/*********************************************************************/
/* remove lines */
/*********************************************************************/
void 	removelines_ 
	(int *ix1, int *iy1, int *ix2, int *iy2, int *ilines)
{
/* remove from main window */
	if(overlay_flag == 0)
	{
/* if ilines = 0, just remove most recent line from list, check >=0 */
		if(*ilines == 0)
		{
			nlines--;
			if(nlines < 0) nlines = 0;
		}
		else
		{
/* remove line(s) from list */
			n = 0;
			i = 0;
			nl1:
/* check for line match */
			if(line_x1[n] == ix1[i] 
			&& line_y1[n] == iy1[i]
			&& line_x2[n] == ix2[i]
			&& line_y2[n] == iy2[i])
			{
			for(j = n;j < nlines; j++)
/* move the existing lines up to fill the space */
			{
				line_x1[j] = line_x1[j+1];
				line_y1[j] = line_y1[j+1];
				line_x2[j] = line_x2[j+1];
				line_y2[j] = line_y2[j+1];
			}
			nlines--;
/* check never < 0 */
			if(nlines < 0) 
			nlines = 0;	
			else
			i++;

			if(i > *ilines) goto nl3;
			}
/* no line match, move to next line */
			else
			{
			n++;
			if(n > nlines) goto nl3;
			}
			goto nl1;
		}

/* refresh image area */
		if(remove_vectors == 0)
		{
	  	image_refresh = 1;
	  	redraw_image (&image_refresh);		

/* clear zoom window */
		if(zoom_flag == 1)
	  	redraw_zoom (&image_refresh);		
	  	image_refresh = 0;
		}
	}
/* remove line from overlay window */
	else
	{
/* if ilines = 0, just remove most recent line from list, check >=0 */
		if(*ilines == 0)
		{
			nlines_overlay--;
			if(nlines_overlay < 0) nlines_overlay = 0;
		}
		else
		{
/* remove line(s) from list */
			n = 0;
			i = 0;
			nl2:
/* check for line match */
			if(line_x1_overlay[n] == ix1[i] 
			&& line_y1_overlay[n] == iy1[i]
			&& line_x2_overlay[n] == ix2[i]
			&& line_y2_overlay[n] == iy2[i])
			{
			for(j = n;j < nlines_overlay; j++)
/* move the existing lines up to fill the space */
			{
				line_x1_overlay[j] = line_x1_overlay[j+1];
				line_y1_overlay[j] = line_y1_overlay[j+1];
				line_x2_overlay[j] = line_x2_overlay[j+1];
				line_y2_overlay[j] = line_y2_overlay[j+1];
			}
			nlines_overlay--;
/* check never < 0 */
			if(nlines_overlay < 0) 
			nlines_overlay = 0;	
			else
			i++;

			if(i > *ilines) goto nl3;
			}
/* no line match, move to next line */
			else
			{
			n++;
			if(n > nlines_overlay) goto nl3;
			}
			goto nl2;
/* set overlay flag to copy from store pixmap then redraw any vectors */
nl3:			overlay_refresh = 1;
		  	redraw_overlay (&overlay_refresh);
		}
	}

	return;
}
/*********************************************************************/
/* remove point */
/*********************************************************************/
void	removepoint_ (int *ix, int *iy)
{

/* start loop to look for point */
	n = 0;
	if(overlay_flag == 0)
	{
		nl1:
		if(point_x[n] == *ix && point_y[n] == *iy)
/* point coords match, remove from list, shuffle array */
		for(i = n;i < npoints; i++) 
		{
			point_x[i] = point_x[i+1];
			point_y[i] = point_y[i+1];
		}
		else
		{
		n++;
		goto nl1;
		}

		npoints = npoints - 1;

/* check never < 0 */
		if(npoints < 0) npoints = 0;

/* copy pixmap to image area */
		if(remove_vectors == 0)
		{
	  	image_refresh = 1;
	  	redraw_image (&image_refresh);		
	
/* clear zoom window */
	 	if(zoom_flag == 1)
	  	redraw_zoom (&image_refresh);		
		}
	}
/* remove point from overlay window */
	else
	{
		nl2:
		if(point_x_overlay[n] == *ix && point_y_overlay[n] == *iy)

/* point coords match, remove from list, shuffle array */
		for(i = n;i < npoints_overlay; i++) 
		{
			point_x_overlay[i] = point_x_overlay[i+1];
			point_y_overlay[i] = point_y_overlay[i+1];
		}
		else
		{
		n++;
		goto nl2;
		}

		npoints_overlay--;

/* check never < 0 */
		if(npoints_overlay < 0) npoints_overlay = 0;
		overlay_refresh = 1;
		redraw_overlay (&overlay_refresh);
	}
}
/*********************************************************************/
/* remove text */
/*********************************************************************/
void removetext_ (int *ix, int *iy, char *text_pointer, int *length)
{
	n = 0;
/* start loop to look for text in main window */
	if(overlay_flag == 0)
	{
		nl1:
		if(text_x[n] == *ix && text_y[n] == *iy)
/* point coords match, remove from list, shuffle rest */
		for (i = n; i < ntext; i++)
		{
			text_x[i] = text_x[i+1];
			text_y[i] = text_y[i+1];
			strncpy(&text_string[i][0],&text_string[i+1][0],
				string_length);
			text_lengths[i] = text_lengths[i+1];
			text_font[i] = text_font[i+1];
		}
		else
		{
		n++;
		goto nl1;
		}
		ntext--;

/* check never < 0 */
		if(ntext < 0) ntext = 0;

/* copy pixmap to image area */
		if(remove_vectors == 0)
		{
	  	image_refresh = 1;
	  	redraw_image (&image_refresh);		

/* clear zoom window */
	 	if(zoom_flag == 1)
	  	redraw_zoom (&image_refresh);		
		}
	}
/* remove text from overlay window */
	else
	{
		nl2:
		if(text_x_overlay[n] == *ix && text_y_overlay[n] == *iy)

/* point coords match, remove from list, shuffle rest */
		for (i = n; i < ntext_overlay; i++)
		{
			text_x_overlay[i] = text_x_overlay[i+1];
			text_y_overlay[i] = text_y_overlay[i+1];
			strncpy(&text_string_overlay[i][0],
                                &text_string_overlay[i+1][0],
				string_length);
			text_lengths_overlay[i] = text_lengths_overlay[i+1];
			text_font[i] = text_font[i+1];
		}
		else
		{
		n++;
		goto nl2;
		}
		ntext_overlay--;

/* check never < 0 */
		if(ntext_overlay < 0) ntext_overlay = 0;

	  	overlay_refresh = 1;
	  	redraw_overlay (&overlay_refresh);
	}
}
/*********************************************************************/
/* Clear image and zoom areas of text, vectors etc. */
/*********************************************************************/
void 	removevectors_ ()
{
/* set flag for clearing all vectors */
	remove_vectors = 1;

/* refresh main window */
	if(overlay_flag == 0)
/* copy pixmap to image area */
	{
		nboxes = 0;
		ncircles = 0;
		nlines = 0;
		npoints = 0;
		ntext = 0;
	 	if(image_flag == 1)
	 	{
	  	image_refresh = 1;
	  	redraw_image (&image_refresh);		

/* clear zoom window */
	  	if(zoom_flag == 1)
	  	redraw_zoom (&image_refresh);
		}		
	}
/* overlay window */
	else
	{
		nboxes_overlay = 0;
		ncircles_overlay = 0;
		nlines_overlay = 0;
		npoints_overlay = 0;
		ntext_overlay = 0;

	  	overlay_refresh = 1;
	  	redraw_overlay (&overlay_refresh);
	}


/* reset vector clearing flag */
	remove_vectors = 0;
}
/*********************************************************************/
/* set line style dashed */
/*********************************************************************/
void 	setdash_ ()
{
	dash_style = 1;
}
/*********************************************************************/
/* set line style solid */
/*********************************************************************/
void 	setsolid_ ()
{
	dash_style = 0;
}
/*********************************************************************/
/* hide slider bar */
/*********************************************************************/
void	sliderhide_ ()
{
	if(slider_flag >= 1)
	{
	XtUnmanageChild(sliderBox);
	slider_flag = 0;

/* both menu and label present, reposition menu wrt label */
	if(label_flag == 1 && menu_flag == 1)
		{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
			XtNfromVert, label,
			XtNvertDistance,1,
	        	NULL);                   /* argument list*/
		XtManageChild(menuBox);
		XRaiseWindow(display_id,menuBox_window_id);
		}
/* menu only, reposition menu wrt top of window */
	else if(label_flag == 0 && menu_flag ==1)
		{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
			XtNfromVert, NULL,
			XtNvertDistance,4,
	        	NULL);
		XtManageChild(menuBox);
		XRaiseWindow(display_id,menuBox_window_id);
		}
	XSync(display_id, False);
	}
}
/*********************************************************************/
/* slider initialize and display */
/*********************************************************************/
void	sliderinit_ (int *nsliders, float *slider1_start, 
                                     float *slider2_start)
{
	if(slider_flag >= 1) XtUnmanageChild(sliderBox);

	slider_flag = *nsliders;
	ksliders = *nsliders;
	slider1_start_position = *slider1_start;
	slider2_start_position = *slider2_start;

/* position wrt label if present */
	if(label_flag == 1)
		XtVaSetValues(sliderBox,
			XtNfromVert, label,
			XtNvertDistance,1,
			XtNcursor, cursor,
	        	NULL);

/* position wrt iobox if present */
	else if(iobox_flag == 1)
		XtVaSetValues(sliderBox,
			XtNfromVert, ioBox,
			XtNvertDistance,1,
			XtNcursor, cursor,
	        	NULL);

/* if menu present reposition it */
	if(menu_flag == 1)
		{
		XtUnmanageChild(menuBox);
		XtVaSetValues(menuBox,
			XtNfromVert, sliderBox,
			XtNvertDistance,1,
	        	NULL);                   /* argument list*/
		XtManageChild(menuBox);
		XRaiseWindow(display_id,menuBox_window_id);
		}

/* position slider1 in the centre and add callback */
	XawScrollbarSetThumb(slider1, slider1_start_position, slider_size);
/*	XtAddCallback(slider1, XtNjumpProc, 
		(XtCallbackProc)action_slider_callback, (XtPointer)1);
*/

		XtUnmanageChild(slider1);
		XtUnmanageChild(colour_bar1);
		XtUnmanageChild(slider2);
		XtUnmanageChild(colour_bar2);

/* reset geometry for single or double slider */
	if(slider_flag == 1)
	{
		XtVaSetValues(slider1,
		XtNfromHoriz, slider_label,
		XtNhorizDistance, 4,
		XtNfromVert, NULL,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		XtNscrollDCursor, cursor,
		XtNscrollHCursor, cursor,
		XtNscrollLCursor, cursor,
		XtNscrollRCursor, cursor,
		XtNscrollUCursor, cursor,
		XtNscrollVCursor, cursor,
		NULL);

		XtVaSetValues(colour_bar1,
		XtNfromHoriz, slider1,
		XtNhorizDistance, 4,
		XtNfromVert, NULL,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		NULL);

		XtManageChild(colour_bar1);
		XtManageChild(slider1);
	}
/* position slider 2 in the centre and add callback */
	else if(slider_flag == 2)
	{
		XawScrollbarSetThumb(slider2, slider2_start_position, 
                                                        slider_size);
/*		XtAddCallback(slider2, XtNjumpProc, 
			(XtCallbackProc)action_slider_callback, (XtPointer)2);
*/
/* reset slider 1*/
		XtVaSetValues(slider1,
		XtNfromHoriz, NULL,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		XtNscrollDCursor, cursor,
		XtNscrollHCursor, cursor,
		XtNscrollLCursor, cursor,
		XtNscrollRCursor, cursor,
		XtNscrollUCursor, cursor,
		XtNscrollVCursor, cursor,
		NULL);

/* reset colour bar 1*/
		XtVaSetValues(colour_bar1,
		XtNfromHoriz, slider1,
		XtNhorizDistance, 4,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		NULL);

/* reset slider 2 */
		XtVaSetValues(slider2,
		XtNfromHoriz, colour_bar1,
		XtNhorizDistance,4,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		XtNscrollDCursor, cursor,
		XtNscrollHCursor, cursor,
		XtNscrollLCursor, cursor,
		XtNscrollRCursor, cursor,
		XtNscrollUCursor, cursor,
		XtNscrollVCursor, cursor,
		NULL);

/* reset colour bar 2 */
		XtVaSetValues(colour_bar2,
		XtNfromHoriz, slider2,
		XtNhorizDistance, 4,
		XtNfromVert, slider_label,
		XtNvertDistance, 4,
		XtNcursor, cursor,
		NULL);

		XtManageChild(slider1);
		XtManageChild(colour_bar1);
		XtManageChild(slider2);
		XtManageChild(colour_bar2);
	}

/* push slider in front */
	XtManageChild(sliderBox);
	XRaiseWindow(display_id, sliderBox_window_id);

/* synchronise */
	XSync(display_id, False);
	while (expose_slider_flag == 0)
	 eventdispatch_ ();

	XSync(display_id, False);
	expose_slider_flag = 0;
}

/*********************************************************************/
/* zoom init - called from fortran */
/*********************************************************************/
void	zoominit_ ()
{
	   image_pointer_x = window_width / 2;
	   image_pointer_y = window_height / 2;
	   zoom_display ();
}
/*********************************************************************/
/* zoom display */
/*********************************************************************/
void	zoom_display ()
{
	int		imap;
	int 		imapx;
	int 		imapy;
	int 		zoom_start_x;
	int 		zoom_start_y;
	int		ix;
	int		iy;
	int		izoom;

/* hide overlay window if present */
	if(overlay_flag != 0) overlayhide_ ();

	zoom_pointer_x = zoom_image_pointer_x + pan_x;
	zoom_pointer_y = zoom_image_pointer_y + pan_y;

/* create zoom area. zoom_start_x, zoom_start_y are map starting points .
   Note that zoom window is forced odd to centre the middle pixel as
   well as possible, that the zoom factor is forced even. The displayed 
   zoom window will be even, but the number of zoomed pixels is always odd */

	zoom_start_x = zoom_image_pointer_x - image_dest_x 
	                    - zoom_width / (2 * zoom_factor_x);
	zoom_start_y = zoom_image_pointer_y - image_dest_y 
	                    - zoom_height / (2 * zoom_factor_y);
/* 24-bit colour zoom map load */
	for (iy = 0; iy < zoom_height; iy++)
		{
		imapy = zoom_start_y + iy / zoom_factor_y;
		for (ix = 0; ix < zoom_width; ix++)
			{
			imapx = zoom_start_x + ix / zoom_factor_x;
			imap = map_width * imapy + imapx;
			izoom = iy * zoom_width + ix;

/* load zoom map if within limits */
			if(imapx >= 0 && imapx < map_width * 4)
				for(i = 0; i < 4; i++)
				overlay_pointer[4 * izoom + i] = 
					image_pointer[4 * imap + i];
			else
				for(i = 0; i < 4; i++)
				overlay_pointer[4 * izoom + i] = 0;
			}
		}
/* hide zoom window */
	if(zoom_flag == 1) zoomhide_ ();

/* create zoom map */
	zoom_map = XCreateImage(display_id,visual_id,
		     image_depth, image_format, image_offset,
		     &overlay_pointer[0], 
		     (unsigned int)zoom_width, (unsigned int)zoom_height,
		     image_bitmap_pad, image_bytes_per_line);

/* create zoom pixmaps 2 pix larger in each direction 
                                  to allow red pixel border */
	zoom_pixmap = XCreatePixmap(display_id,zoom_area_window_id,
		(unsigned int)zoom_width+2, (unsigned int)zoom_height+2, 
		image_depth);

	zoom_store_pixmap = XCreatePixmap(display_id,zoom_area_window_id,
		(unsigned int)zoom_width+2, (unsigned int)zoom_height+2, 
		image_depth);

	XSync(display_id, False);
	if (icallback_value == 999) return;

/* move zoom widget to correct place on window and set colours */
	zoom_window_x1 = zoom_pointer_x - zoom_width / 2 ;
	zoom_window_y1 = zoom_pointer_y - zoom_height / 2 ;
	zoom_window_x2 = zoom_pointer_x + zoom_width / 2 ;
	zoom_window_y2 = zoom_pointer_y + zoom_height / 2 ;

	XtVaSetValues( zoom_area,
			XtNfromHoriz, NULL,
			XtNfromVert, NULL,
			XtNwidth, (Dimension)zoom_width+2,
			XtNheight, (Dimension)zoom_height+2,
			XtNhorizDistance, zoom_window_x1,
			XtNvertDistance, zoom_window_y1,
			XtNborderWidth, (Dimension) 0,
			XtNborderColor, (Pixel)colour_red,
			XtNforeground, (Pixel)colour_black,
			XtNbackground, (Pixel)colour_white,
			NULL);

	XtManageChild(zoom_area);

/* put zoom window in front of image area */
	XRaiseWindow(display_id,zoom_area_window_id);

/* fill zoom pixmaps with red pixels */
	XFillRectangle(display_id, zoom_pixmap, gc_red_rectangle, 
		image_source_x, image_source_y,
		(unsigned int)zoom_width+2, (unsigned int)zoom_height+2);

	XFillRectangle(display_id, zoom_store_pixmap, gc_red_rectangle, 
		image_source_x, image_source_y,
		(unsigned int)zoom_width+2, (unsigned int)zoom_height+2);

/* write zoom map into zoom pixmap */
	XPutImage(display_id, zoom_pixmap,
		  gc_fill_rectangle,zoom_map,image_source_x, image_source_y,
		  image_source_x+1, image_source_y+1, 
		  (unsigned int)zoom_width, (unsigned int)zoom_height);

	XCopyArea(display_id, zoom_pixmap, zoom_store_pixmap,
		  gc_fill_rectangle, 0, 0, 
	          (unsigned int)zoom_width+2, (unsigned int)zoom_height+2, 
                   0, 0);
/* add 1 to all pixels to allow for border */
/* Removed this as it causes all sorts of probs 23.05.2012 */

/* draw all boxes in list */
	for ( n = 0; n < nboxes; n++)
	{
		style = box_style[i];
		zoom_diam_x = (int)box_width[n] * zoom_factor_x;
		zoom_diam_y = (int)box_height[n] * zoom_factor_y;
                convert_to_zoom(box_x[n], box_y[n],
                                &zoom_centre_x, &zoom_centre_y);
/* draw box into zoom pixmap */
                XDrawRectangle
                (display_id, zoom_pixmap, gc_draw_vector[style],
                 zoom_centre_x, zoom_centre_y, zoom_diam_x, zoom_diam_y);

	}
/* Draw all circles in list */
	for ( n = 0; n < ncircles; n++)
	{
		style = circle_style[n];
		zoom_diam_x = circle_diam[n] * zoom_factor_x;
		zoom_diam_y = circle_diam[n] * zoom_factor_y;

/* convert coordinates to lie in zoom area */
		convert_to_zoom(circle_x[n]+circle_diam[n]/2,
			circle_y[n]+circle_diam[n]/2,
			&zoom_centre_x, &zoom_centre_y);
		zoom_centre_x = zoom_centre_x - zoom_diam_x / 2;
		zoom_centre_y = zoom_centre_y - zoom_diam_y / 2;

/* draw zoomed circle */
		XDrawArc
		(display_id, zoom_pixmap, gc_draw_vector[style],
		zoom_centre_x, zoom_centre_y, 
		zoom_diam_x, zoom_diam_y,
		circle_angle, circle_angle);
	}

/* Draw all vectors in list */
	n = 0;
	for ( i = 0; i < nlines; i++)
		{
		if( i > 0 ) 
/* if line style changes, write previous segment */
			{
			if(line_style[i] != line_style[i-1])
				{
/* draw line in zoom pixmap */
				XDrawSegments
				(display_id, zoom_pixmap, 
				gc_draw_vector[style],
				segments, n);
				n = 0;
				}
			}
/* convert coordinates for zoom window */
		style = line_style[i];
		convert_to_zoom
		(line_x1[i], line_y1[i], &zoom_x1, &zoom_y1);
		segments[n].x1 = zoom_x1;
		segments[n].y1 = zoom_y1;

		convert_to_zoom
		(line_x2[i], line_y2[i], &zoom_x2, &zoom_y2);
		segments[n].x2 = zoom_x2;
		segments[n].y2 = zoom_y2;
		n++;
	}
/* draw final segment in zoom pixmap */
	if(n > 0)
		{
		XDrawSegments
		(display_id, zoom_pixmap, gc_draw_vector[style],
		segments, n);
		}

/* Draw all points in list */
	for ( n = 0; n < npoints; n++)
	{
		convert_to_zoom(point_x[n], point_y[n], &zoom_x1, &zoom_y1);

		XDrawPoint(display_id, zoom_pixmap, gc_draw_vector[0], 
		zoom_x1, zoom_y1);
	}

/* Draw all text in list */
	for ( n = 0; n < ntext; n++)
	{
		convert_to_zoom(text_x[n], text_y[n], &zoom_x1, &zoom_y1);

		XDrawString(display_id, zoom_pixmap, gc_draw_vector[0], 
		zoom_x1, zoom_y1, &text_string[n][0], text_lengths[n]);
	}

	zoom_flag = 1;
}
