#!/usr/bin/env python

#2 ways to achieve MRC conversion to GIF - this is overkill!

import sys, os

eman_method = False

try:
    from EMAN2 import *
    eman_method = True
except ImportError:
    print "Can't find an EMAN2 installation; trying plan b"

try:
    import Image
except ImportError:
    print "Can't create GIFs; install the Python Image Library (PIL)"
    exit()

def tif2gif(inp):
    try:
        img = Image.open(inp[1])
        img.save(inp[1] + ".gif")
        os.remove(inp[1]) #clean up tiff file - note it doesn't have a file extension
    except IOError:
        print "Something went wrong with converting to gif"
        exit()

def readargs():
    
    a = sys.argv[1]
    b = sys.argv[2]

    if(a[len(a)-4:] != '.mrc'):
        a = a + '.mrc'

    if(b[len(b)-4:] == '.gif'):
        b = b[:len(b)-4]

    return a, b

def eman_method(inp):
    print "create EM object of " + inp[0] + " from " + inp[1]
    e = EMData()
    try:
        e.read_image(inp[0])
    except:
        print "Error reading image"

    try:
        img_type = IMAGE_TIFF
        print img_type
        e.write_image(inp[1],0,img_type,False,None,EM_SHORT) 
    except:
        print "Error writing image"

def fortran_method(inp):

    arg_string = inp[0] + "\n" + inp[1] + "\n"    

    print arg_string

    import subprocess
    from string import Template

    try:
        if(sys.platform == 'win32'):
            cmd = 'mrc2tif.exe'
        else:
            cmd = './mrc2tif'

        p = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE)

        stdout_data, stderr_data = p.communicate(arg_string)
        print stdout_data
        print stderr_data

        if (p.returncode != 0):
            print "It appears we couldn't run the mrc2tif fortran program"

    except:
        print "Something went wrong"

class main():

    def __init__(self):   
        inp = readargs()

        print inp
        if(eman_method):
            print "Found an EMAN2 installation"
            eman_method(inp)
        else:
            try:
                fortran_method(inp)
            except:
                print "Unable to convert your image to gif; terminating program"    
                exit()

        tif2gif(inp)

if __name__ == '__main__':
    main()
    print "File successfully converted"
