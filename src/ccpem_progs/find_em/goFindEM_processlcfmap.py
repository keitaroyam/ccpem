#!/usr/bin/env python

###
#  Python version of bash script of same name
#    (will update when function has proper docs)
###

import os, sys, subprocess, time, operator

def make_args():

  arg_string = """{0}\n\n{1}\n""".format(sys.argv[1], sys.argv[2])
  return arg_string

class main():

  def __init__(self, errorsdict, numerrors):

    #check valid number of parameters
    if(len(sys.argv) != 3):
      print "You have not submitted the correct number of arguments"
      sys.exit()

    self.errors =  errorsdict
    self.err_count = numerrors
    arg_string = make_args()
    
    self.runSubscript('FindEM_processlcfmapI', arg_string)
    code = (sys.argv[1]).zfill(3)
 
    #sort contents - how does python know to convert the stream from the file into a data object from which the sort should only be done on the 1st column?!
    rawfile = open('rawcoords'+code+'.xyz')
    rawsorted = sorted(rawfile, reverse=True)

    sortedfile = open('sortedrawcoords'+code+'.xyz', 'w')
    sortedfile.writelines(rawsorted)

    rawfile.close()
    sortedfile.close()

    self.runSubscript('FindEM_processlcfmapII', arg_string)

    print "EM map succesfully processed"

    if(len(self.errors)>0):
      print "however, there are issues in " + str(len(self.errors)) + " of the programs that you may want to look at"

      #sort the errors dictionary
      self.errors = sorted(self.errors.iteritems(), key=operator.itemgetter(1))
      for i in self.errors:
        print "Program " + i[0] + ":"
        for j in i[1][1:len(i[1])]:
          print "   " + str(j)
        print "\n"     

  def runSubscript(self, script, arg_string):
  
     if(sys.platform == 'win32'):
       cmd = os.path.join(os.environ['CCPEMBIN'],script + '.exe')
     else:
       cmd = os.path.join(os.environ['CCPEMBIN'],script)
  
     p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
  
     stdout_data, stderr_data = p.communicate(arg_string)
     print stdout_data
   
     info_msgs = []
     
     if stderr_data is not None and stderr_data is not '':
      info_msgs.append(stderr_data)
      
     if (p.returncode != 0):
       info_msgs.append("It appears there may have been an error has been detected in " + script + " - check your input")
      
     if(len(info_msgs) > 0):
        info_msgs.insert(0, self.err_count)
        self.err_count+=1
     
     errors[script] = info_msgs

if __name__ == "__main__":
  errors = {}
  errors_count = 0
  main(errors, errors_count)
