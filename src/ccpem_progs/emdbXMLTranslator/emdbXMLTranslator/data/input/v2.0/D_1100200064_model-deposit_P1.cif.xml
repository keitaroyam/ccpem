<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-10101" version="2.0">
    <admin>
        <current_status>
            <code>HOLD</code>
        </current_status>
        <sites>
            <deposition>RCSB</deposition>
            <last_processing>RCSB</last_processing>
        </sites>
        <key_dates>
            <deposition>2015-10-16</deposition>
            <update>2015-10-16</update>
        </key_dates>
        <title>3D reconstruction of TMV at 4.5A from film micrographs using Frealix</title>
        <authors_list>
            <author>Rohou A</author>
            <author>Grigorieff N</author>
        </authors_list>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="false">
                    <author order="2">Rohou A</author>
                    <author order="4">Grigorieff N</author>
                    <title>Frealix: model-based refinement of helical filament structures from electron micrographs.</title>
                    <journal_abbreviation>J.STRUCT.BIOL.</journal_abbreviation>
                    <volume>186</volume>
                    <first_page>234</first_page>
                    <last_page>244</last_page>
                    <year>2014</year>
                    <external_references type="pubmed">24657230</external_references>
                    <external_references type="doi">10.1016/j.jsb.2014.03.012</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
    </crossreferences>
    <sample>
        <supramolecule_list>
            <supramolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="virus">
                <macromolecule_list>
                    <macromolecule>
                        <id>1</id>
                    </macromolecule>
                </macromolecule_list>
                <details>purified from infected tobacco leaves</details>
                <natural_host database="ncbi">
                    <organism ncbi="4094">Nicotiana sp.</organism>
                </natural_host>
                <molecular_weight>
                    <theoretical units="kDa/nm">0.7</theoretical>
                </molecular_weight>
                <virus_shell id="1">
                    <name>capsid</name>
                    <diameter>300.0</diameter>
                </virus_shell>
                <virus_type>VIRION</virus_type>
                <virus_isolate>OTHER</virus_isolate>
                <virus_enveloped>false</virus_enveloped>
                <virus_empty>false</virus_empty>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list>
            <macromolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="protein_or_peptide">
                <name>tmv capsid</name>
                <enantiomer>levo</enantiomer>
                <sequence>
                    <string>MSYSITTPSQFVFLSSAWADPIELINLCTNALGNQFQTQQARTVVQRQFSEVWKPSPQVT
VRFPDSDFKVYRYNAVLDPLVTALLGAFDTRNRIIEVENQANPTTAETLDATRRVDDATV
AIRSAINNLIVELIRGTGSYNRSSFESSSGLVWTSGPAT</string>
                    <external_references type="UNITPROTKB">P69687</external_references>
                </sequence>
            </macromolecule>
        </macromolecule_list>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>helical</method>
            <aggregation_state>FILAMENT</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_preparation_type">
                    <concentration units="mg/mL">2.5</concentration>
                    <buffer>
                        <ph>7.</ph>
                        <details>Phosphate, 5 mM EDTA</details>
                    </buffer>
                    <grid>
                        <model>Quantifoil</model>
                    </grid>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Blot for 2 seconds before plunging</details>
                    </vitrification>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="base_microscopy_type">
                    <microscope>FEI TECNAI F30</microscope>
                    <illumination_mode>FLOOD BEAM</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>FIELD EMISSION GUN</electron_source>
                    <acceleration_voltage>200</acceleration_voltage>
                    <nominal_cs>2.0</nominal_cs>
                    <nominal_defocus_min>1688.0</nominal_defocus_min>
                    <nominal_defocus_max>3860.0</nominal_defocus_max>
                    <nominal_magnification>59000.</nominal_magnification>
                    <specimen_holder_model>GATAN LIQUID NITROGEN</specimen_holder_model>
                    <cooling_holder_cryogen>NITROGEN</cooling_holder_cryogen>
                    <image_recording_list>
                        <image_recording id="1">
                            <film_or_detector_model>OTHER</film_or_detector_model>
                            <digitization_details>
                                <sampling_interval>7.0</sampling_interval>
                            </digitization_details>
                            <average_electron_dose_per_image>20.0</average_electron_dose_per_image>
                            <details>EMD-6325 reported images scanned using "ZEISS SCAI", but this does not seem to be on the pulldown list in the new system. </details>
                        </image_recording>
                    </image_recording_list>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="helical_processing_type">
                <image_recording_id>1</image_recording_id>
                <final_reconstruction>
                    <number_images_used>10</number_images_used>
                    <applied_symmetry>
                        <helical_parameters>
                            <delta_z>1.39259</delta_z>
                            <delta_phi>22.0318</delta_phi>
                            <axial_symmetry>C1</axial_symmetry>
                        </helical_parameters>
                    </applied_symmetry>
                    <resolution>4.5</resolution>
                    <resolution_method>FSC 0.143 CUT-OFF</resolution_method>
                    <software_list>
                        <software>
                            <name>Frealix</name>
                        </software>
                    </software_list>
                    <details>Only data up to 5.5 Angstrom were used during refinement in the final rounds. In earlier rounds, this threshold was set to a lower resolution. Filaments were processed using Frealix assuming constant helical parameters and without continuity restraints on the helical parameters. </details>
                </final_reconstruction>
                <ctf_correction>
                    <correction_operation>PHASE FLIPPING AND AMPLITUDE CORRECTION</correction_operation>
                </ctf_correction>
                <segment_selection>
                    <software_list>
                        <software>
                            <name>eman</name>
                        </software>
                    </software_list>
                </segment_selection>
                <startup_model>
                    <other>OTHER</other>
                </startup_model>
                <final_angle_assignment>
                    <type>NOT APPLICABLE</type>
                    <software_list>
                        <software>
                            <name>Frealix</name>
                        </software>
                    </software_list>
                </final_angle_assignment>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="32001024" format="CCP4">
        <file>EMD_10101.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as floating point number (4 bytes)</data_type>
        <dimensions>
            <col>200</col>
            <row>200</row>
            <sec>200</sec>
        </dimensions>
        <origin>
            <col>0</col>
            <row>0</row>
            <sec>0</sec>
        </origin>
        <spacing>
            <x>200</x>
            <y>200</y>
            <z>200</z>
        </spacing>
        <cell>
            <a>232.0</a>
            <b>232.0</b>
            <c>232.0</c>
            <alpha>90.0</alpha>
            <beta>90.0</beta>
            <gamma>90.0</gamma>
        </cell>
        <axis_order>
            <fast>x</fast>
            <medium>y</medium>
            <slow>z</slow>
        </axis_order>
        <statistics>
            <minimum>-2.5420177</minimum>
            <maximum>3.5639179</maximum>
            <average>-0.0005712905</average>
            <std>0.6760621</std>
        </statistics>
        <pixel_spacing>
            <x units="Å">1.16</x>
            <y units="Å">1.16</y>
            <z units="Å">1.16</z>
        </pixel_spacing>
        <contour_list>
            <contour primary="true">
                <level>1.3</level>
                <source>author</source>
            </contour>
        </contour_list>
        <label>::::EMDATABANK.org::::D_1100200064::::                                          </label>
        <annotation_details>TMV map filtered using a negative B-factor, with amplitude matching against density derived from fitted atomic coordinates, a figure-of-merit filter, and subsequent helical symmetrization, as detailed</annotation_details>
    </map>
    <interpretation>
        <addiional_map_list>
            <additional_map size_kbytes="582127328" format="CCP4">
                <file>EMD_10101_additional_1.map.gz</file>
                <symmetry>
                    <space_group>1</space_group>
                </symmetry>
                <data_type>Image stored as floating point number (4 bytes)</data_type>
                <dimensions>
                    <col>526</col>
                    <row>526</row>
                    <sec>526</sec>
                </dimensions>
                <origin>
                    <col>0</col>
                    <row>0</row>
                    <sec>0</sec>
                </origin>
                <spacing>
                    <x>526</x>
                    <y>526</y>
                    <z>526</z>
                </spacing>
                <cell>
                    <a>610.16</a>
                    <b>610.16</b>
                    <c>610.16</c>
                    <alpha>90.0</alpha>
                    <beta>90.0</beta>
                    <gamma>90.0</gamma>
                </cell>
                <axis_order>
                    <fast>x</fast>
                    <medium>y</medium>
                    <slow>z</slow>
                </axis_order>
                <statistics>
                    <minimum>-443.303530000000023</minimum>
                    <maximum>1154.176500000000033</maximum>
                    <average>-6.4902105</average>
                    <std>115.753783999999996</std>
                </statistics>
                <pixel_spacing>
                    <x units="Å">1.16</x>
                    <y units="Å">1.16</y>
                    <z units="Å">1.16</z>
                </pixel_spacing>
                <contour_list>
                    <contour primary="true">
                        <level>1.3</level>
                        <source>author</source>
                    </contour>
                </contour_list>
                <label>::::EMDATABANK.org::::D_1100200064::::                                          </label>
                <annotation_details>original reconstruction (?)</annotation_details>
            </additional_map>
        </addiional_map_list>
    </interpretation>
</emd>
