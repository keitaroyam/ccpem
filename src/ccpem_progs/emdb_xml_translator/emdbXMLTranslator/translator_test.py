#!/usr/bin/env python
"""
unittest.py

Test that the translator is working

TODO:
1) Should discover files in data/cif


Version history:
0.1, 2015-11-11, Ardan Patwardhan: Define tests for 1.9 > 2.0 and 2.0 > 1.9 
0.2, 2016-01-06, Ardan Patwardhan: Added tests for emdb_[19|20]_to_json.py
                

Copyright [2015-2016] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""


__author__ = 'Ardan Patwardhan'
__email__ = 'ardan@ebi.ac.uk'
__date__ = '2015-11-11'

import os.path
import logging
import unittest
from glob import glob
from os import path
from emdb_xml_translate import EMDBXMLTranslator
from emdb_19_to_json import EMDBXML19JSONTranslator
from emdb_20_to_json import EMDBXML20JSONTranslator
logging.basicConfig(level=logging.INFO)


class TestTranslator(unittest.TestCase):
    """
    Run the translator on a bunch of example files
    """

    inputXML19Dir = 'data/input/v1.9'
    inputXML20Dir = 'data/input/v2.0'
    outputXML19Dir = 'data/output/v1.9'
    outputXML20Dir = 'data/output/v2.0'
    outputJSON19Dir = 'data/output/json/v1.9'
    outputJSON20Dir = 'data/output/json/v2.0'
    roundtrip19Dir = 'data/output/roundtrip_v1.9'
    
    def setUp(self):
        # Read files from input directories and make file lists
        self.in19SearchPath = path.join(self.inputXML19Dir, '*.xml')
        self.inFullPath19List = glob(self.in19SearchPath)
        self.inFile19List = [path.splitext(path.basename(f)) for f in self.inFullPath19List]
        self.in20SearchPath = path.join(self.inputXML20Dir, '*.xml')
        self.inFullPath20List = glob(self.in20SearchPath)
        self.inFile20List = [path.splitext(path.basename(f)) for f in self.inFullPath20List]
        
    def test_19_to_20(self):
        """
        Test conversion of files in the data/input/v1.9 directory from v1.9 to v2.0, and then back to v1.9
        """
              
        outFileNameTemplate = '%s-v20%s'
        logging.info('*** Testing conversion of v1.9 (%s) files to v2.0 (%s) ***' % (self.inputXML19Dir, self.outputXML20Dir))
        translator = EMDBXMLTranslator()
        i = 0
        outFileList = []
        for inf in self.inFullPath19List:
            outf = path.join(self.outputXML20Dir, outFileNameTemplate % (self.inFile19List[i][0], self.inFile19List[i][1]))
            outFileList.append(outf)
            logging.info('Translating %s to %s' % (inf, outf))
            translator.translate_1_9_to_2_0(inf, outf)
            i += 1
            
        rtFileNameTemplate = '%s-rt%s'
        logging.info('*** Testing roundtrip conversion back to v1.9, from dir %s to dir %s ***' % (self.inputXML20Dir, self.outputXML19Dir))
        i = 0       
        print
        print
        print 'outfilelist=', outFileList
        for outf in outFileList:
            rtripf = path.join(self.roundtrip19Dir, rtFileNameTemplate % (self.inFile19List[i][0], self.inFile19List[i][1]))   
            logging.info('Translating %s to %s' % (outf, rtripf))
            translator.translate_2_0_to_1_9(outf, rtripf)
            i += 1
                 
    def test_20_to_19(self):
        """
        Test conversion of files in the data/input/v2.0 directory from v2.0 to v1.9
        """
                
        outFileNameTemplate = '%s-v19%s'
        logging.info('*** Testing conversion of v2.0 (%s) files to v1.9 (%s) ***' % (self.inputXML20Dir, self.outputXML19Dir))
        translator = EMDBXMLTranslator()
        i = 0        
        for inf in self.inFullPath20List:
            outf = path.join(self.outputXML19Dir, outFileNameTemplate % (self.inFile20List[i][0], self.inFile20List[i][1]))            
            logging.info('Translating %s to %s' % (inf, outf))
            translator.translate_2_0_to_1_9(inf, outf)
            i += 1
      
    def test_19_to_json(self):
        """
        Test conversion of files in data/input/v1.9 directory to jsons in data/output/v2.0
        """
        outFileNameTemplate = '%s-v20.json'
        logging.info('*** Testing conversion of v1.9 (%s) files to summary JSONs (%s) ***' % (self.inputXML19Dir, self.outputJSON19Dir))
        translator = EMDBXML19JSONTranslator()
        i = 0
        outFileList = []
        for inf in self.inFullPath19List:
            outf = path.join(self.outputJSON19Dir, outFileNameTemplate % (self.inFile19List[i][0]))
            outFileList.append(outf)
            logging.info('Translating %s to %s' % (inf, outf))
            translator.translate(inf, outf)
            i += 1

    def test_20_to_json(self):
        """
        Test conversion of files in data/input/v2.0 directory to jsons in data/output/v1.9
        """
        outFileNameTemplate = '%s-v19.json'
        logging.info('*** Testing conversion of v20 (%s) files to summary JSONs (%s) ***' % (self.inputXML20Dir, self.outputJSON20Dir))
        translator = EMDBXML20JSONTranslator()
        i = 0
        outFileList = []
        for inf in self.inFullPath20List:
            outf = path.join(self.outputJSON20Dir, outFileNameTemplate % (self.inFile20List[i][0]))
            outFileList.append(outf)
            logging.info('Translating %s to %s' % (inf, outf))
            translator.translate(inf, outf)
            i += 1
            
if __name__ == '__main__':
    unittest.main()
