#!/usr/bin/env python
"""
emdb_19_to_json

Reads in a EMDB header file following 1.9 schema and outputs summary information as a JSON.
This is an stub example to show how to use emdb_19.py to read the header file.

TODO:

Version history:


Copyright [2014-2015] EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the
"License"); you may not use this file except in
compliance with the License. You may obtain a copy of
the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on
an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied. See the License for the
specific language governing permissions and limitations
under the License.
"""
import sys
import logging
import traceback
import json
from optparse import OptionParser
import emdb_19
from emdb_settings import EMDBSettings

__author__ = 'Ardan Patwardhan, Sanja Abbott'
__email__ = 'ardan@ebi.ac.uk, sanja@ebi.ac.uk'
__date__ = '2017-06-14'


class EMDBXML19JSONTranslator(object):
    """
    Class for translating summary info from EMDB 1.9 to a JSON file
    """

    def __init__(self):
        """
        TO DO
        """
        # 0 = min, 3 = max
        self.warning_level = 1
        logging.basicConfig(level=EMDBSettings.log_level, format=EMDBSettings.log_format)

    def set_warning_level(self, level):
        """
        Set the level of logging warnings. 0 = no warnings, 3 = max warnings, 1 = default

        Parameters
        @param level: warning level 0 -> 3
        """
        if level <= 0:
            self.warning_level = 0
        elif level >= 3:
            self.warning_level = 3
        else:
            self.warning_level = level

    def warn(self, level, msg):
        """
        Log a warning message but take into account the warning_level

        Parameters:
        @param level: only messages with level >= warning_level are printed
        @param msg: warning message
        """
        if level <= self.warning_level:
            logging.warning(msg)

    def translate(self, input_file, output_file):
        """
        Translate EMDB 1.9 to a JSON. Summary info only

        Parameters
        @param input_file: input file in EMDB 1.9 XML
        @param output_file: output JSON file
        """

        def check_set(get_value, key, json_dict, transform=None):
            """
            Call setVar only if get_value does not return None

            Parameters:
            @param get_value: getter function that must return value
            @param key: key in dictionary to be set json_dict[key]
            @param json_dict: JSON dictionary whose key will be set, json_dict[key]
            @param transform: Apply transform(x) before calling setter function
            """

            x_value = get_value()
            if x_value is not None:
                if transform is not None:
                    try:
                        z_value = x_value
                        x_value_trans = transform(z_value)
                    except Exception:
                        self.warn(3, "function check_set: Transform function did not work: %s(%s)" % (transform, z_value))
                        self.warn(3, traceback.format_exc())
                        return

                json_dict[key] = x_value_trans

        def copy_citation(cit_in):
            """
            Return JSON object containing citation - more complex example...

            Parameters:
            @param cite_in: Input citation in 1.9 schema
            @return: python dictionary with reference info
            """
            jrnl_in = cit_in.get_journalArticle()
            cit_out = {}
            if jrnl_in:
                cit_out['citationType'] = 'Journal'
                check_set(jrnl_in.get_authors, 'authors', cit_out)
                check_set(jrnl_in.get_articleTitle, 'title', cit_out)
                check_set(jrnl_in.get_journal, 'journal', cit_out)
                check_set(cit_in.get_published, 'published', cit_out)
                # This is a fix because of bad data - emd-1648.xml has an empty volume tag!
                vol = jrnl_in.get_volume()
                if vol is not None and len(vol) > 0:
                    cit_out['volume'] = vol
                check_set(jrnl_in.get_firstPage, 'firstPage', cit_out)
                check_set(jrnl_in.get_lastPage, 'lastPage', cit_out)
                check_set(jrnl_in.get_year, 'year', cit_out)
            else:
                cit_out['citationType'] = 'Non-journal'
                non_jrnl_in = cit_in.get_nonJournalArticle()
                check_set(non_jrnl_in.get_authors, 'authors', cit_out)
                check_set(non_jrnl_in.get_editor, 'editor', cit_out)
                check_set(non_jrnl_in.get_chapterTitle, 'chapterTitle', cit_out)
                check_set(non_jrnl_in.get_book, 'title', cit_out)
                check_set(non_jrnl_in.get_thesisTitle, 'thesisTitle', cit_out)
                check_set(cit_in.get_published, 'published', cit_out)
                check_set(non_jrnl_in.get_publisher, 'publisher', cit_out)
                check_set(non_jrnl_in.get_publisherLocation, 'location', cit_out)
                check_set(non_jrnl_in.get_volume, 'volume', cit_out)
                check_set(non_jrnl_in.get_firstPage, 'firstPage', cit_out)
                check_set(non_jrnl_in.get_lastPage, 'lastPage', cit_out)
                check_set(non_jrnl_in.get_year, 'year', cit_out)
            return cit_out

        xml_in = emdb_19.parse(input_file, silence=True)
        json_out = {}
        dep_in = xml_in.get_deposition()

        check_set(xml_in.get_accessCode, 'emdbId', json_out)
        check_set(dep_in.get_title, 'title', json_out)
        check_set(dep_in.get_status().get_valueOf_, 'status', json_out)
        check_set(dep_in.get_authors, 'authors', json_out)

        # dates
        check_set(dep_in.get_depositionDate, 'depositionDate', json_out, str)
        check_set(dep_in.get_headerReleaseDate, 'headerReleaseDate', json_out, str)
        check_set(dep_in.get_mapReleaseDate, 'mapReleaseDate', json_out, str)

        # primary citation
        json_out['citation'] = copy_citation(dep_in.get_primaryReference())

        out_file = open(output_file, 'w') if output_file else sys.stdout
        json.dump(json_out, out_file)
        if out_file is not sys.stdout:
            out_file.close()


def main():
    """
    Extract summary info from EMDB XML 1.9 file to JSON

    """
    # Handle command line options
    usage = """
            emdb_19_to_json.py [options] input_file
            Convert EMDB XML

            Examples:
            python emdb_19_to_json.py input_file

            Typical run:
            python emdb_19_to_json.py -f out.json in.xml
            in.xml is assumed to be a EMDB 1.9 XML file
            out.json is a JSON file created with the summary information
            """
    version = "0.1"
    parser = OptionParser(usage=usage, version=version)
    parser.add_option("-f", "--out-file", action="store", type="string", metavar="FILE", dest="outputFile", help="Write output to FILE")
    parser.add_option("-w", "--warning-level", action="store", type="int", dest="warning_level", default=1, help="Level of warning output. 0 is none, 3 is max, default = 1")
    (options, args) = parser.parse_args()

    # Check for sensible/supported options
    if len(args) < 1:
        sys.exit("No input file specified!")
    else:
        input_file = args[0]

    # Call appropriate conversion routine
    translator = EMDBXML19JSONTranslator()
    translator.set_warning_level(options.warning_level)
    translator.translate(input_file, options.outputFile)


if __name__ == "__main__":
    main()
