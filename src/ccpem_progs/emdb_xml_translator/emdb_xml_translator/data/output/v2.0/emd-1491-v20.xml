<?xml version="1.0" encoding="UTF-8"?>
<emd emdb_id="EMD-1491" version="2.0">
    <admin>
        <current_status>
            <code>REL</code>
            <processing_site>PDBe</processing_site>
        </current_status>
        <sites>
            <deposition>PDBe</deposition>
            <last_processing>PDBe</last_processing>
        </sites>
        <key_dates>
            <deposition>2008-03-03</deposition>
            <header_release>2008-03-05</header_release>
            <map_release>2009-05-29</map_release>
            <update>2013-03-27</update>
        </key_dates>
        <title>2D Arrays of F-actin Cross-linked by Villin</title>
        <authors_list>
            <author>Hampton CM</author>
            <author>Liu J</author>
            <author>Taylor DW</author>
            <author>DeRosier DJ</author>
            <author>Taylor KA</author>
        </authors_list>
        <keywords>cytoskeleton, actin, electron tomography, microvilli, image processing, lipid monolayer,</keywords>
    </admin>
    <crossreferences>
        <citation_list>
            <primary_citation>
                <journal_citation published="true">
                    <author order="1">Hampton CM</author>
                    <author order="2">Liu J</author>
                    <author order="3">Taylor DW</author>
                    <author order="4">DeRosier DJ</author>
                    <author order="5">Taylor KA</author>
                    <title>The 3D structure of villin as an unusual F-Actin crosslinker.</title>
                    <journal>STRUCTURE</journal>
                    <volume>16</volume>
                    <first_page>1882</first_page>
                    <last_page>1891</last_page>
                    <year>2008</year>
                    <external_references type="pubmed">19081064</external_references>
                    <external_references type="doi">doi:10.1016/j.str.2008.09.015</external_references>
                </journal_citation>
            </primary_citation>
        </citation_list>
    </crossreferences>
    <sample>
        <name>rabbit F-actin cross-linked with chicken villin</name>
        <supramolecule_list>
            <supramolecule id="1000" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="sample">
                <details>2D rafts are formed on a lipid monolayer</details>
                <number_unique_components>2</number_unique_components>
            </supramolecule>
            <supramolecule id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="organelle_or_cellular_component">
                <name>F-actin</name>
                <oligomeric_state>monomer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <natural_source>
                    <organism ncbi="9986">Oryctolagus cuniculus</organism>
                    <synonym_organism>Rabbit</synonym_organism>
                </natural_source>
                <recombinant_expression/>
            </supramolecule>
            <supramolecule id="2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="organelle_or_cellular_component">
                <name>Villin</name>
                <oligomeric_state>monomer</oligomeric_state>
                <recombinant_exp_flag>false</recombinant_exp_flag>
                <natural_source>
                    <organism ncbi="9031">Gallus gallus</organism>
                    <synonym_organism>chicken</synonym_organism>
                    <tissue>intestinal epithelium</tissue>
                    <cellular_location>microvillus</cellular_location>
                </natural_source>
                <recombinant_expression/>
            </supramolecule>
        </supramolecule_list>
        <macromolecule_list/>
    </sample>
    <structure_determination_list>
        <structure_determination>
            <method>tomography</method>
            <aggregation_state>twoDArray</aggregation_state>
            <specimen_preparation_list>
                <specimen_preparation id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="tomography_preparation_type">
                    <buffer>
                        <ph>8.</ph>
                        <details>Tris-Cl, 0.2 mM Na2ATP, 0.02% beta-mercaptoethanol, 0.2 mM CaCl2, 0.01% NaN3</details>
                    </buffer>
                    <staining>
                        <type>negative</type>
                        <details>2% uranyl acetate</details>
                    </staining>
                    <grid>
                        <details>200-300 mesh copper grids with a reticulated carbon support film</details>
                    </grid>
                    <vitrification>
                        <cryogen_name>ETHANE</cryogen_name>
                        <instrument>HOMEMADE PLUNGER</instrument>
                        <details>Vitrification instrument: plunge freeze</details>
                    </vitrification>
                </specimen_preparation>
                <specimen_preparation id="2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="crystallography_preparation_type">
                    <crystal_formation>
                        <details>Arrays were grown on positively charged lipid monolayers</details>
                    </crystal_formation>
                </specimen_preparation>
            </specimen_preparation_list>
            <microscopy_list>
                <microscopy id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="tomography_microscopy_type">
                    <microscope>FEI/PHILIPS CM300FEG/T</microscope>
                    <illumination_mode>SPOT SCAN</illumination_mode>
                    <imaging_mode>BRIGHT FIELD</imaging_mode>
                    <electron_source>LAB6</electron_source>
                    <acceleration_voltage>300</acceleration_voltage>
                    <nominal_cs>2.0</nominal_cs>
                    <specimen_holder_model>OTHER</specimen_holder_model>
                    <date>2005-05-19</date>
                    <image_recording_list>
                        <image_recording>
                            <film_or_detector_model category="CCD">GENERIC CCD (2k x 2k)</film_or_detector_model>
                            <digitization_details/>
                        </image_recording>
                    </image_recording_list>
                    <specimen_holder>high tilt</specimen_holder>
                </microscopy>
            </microscopy_list>
            <image_processing id="1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="tomography_processing_type">
                <final_reconstruction>
                    <number_images_used>60</number_images_used>
                    <algorithm>weighted back projection</algorithm>
                    <resolution>30</resolution>
                    <resolution_method>first node in CTF</resolution_method>
                    <software_list>
                        <software>
                            <name>protomo</name>
                        </software>
                    </software_list>
                    <details>tomogram id from a single tilt series</details>
                </final_reconstruction>
                <crystal_parameters>
                    <unit_cell/>
                    <plane_group>P 2</plane_group>
                </crystal_parameters>
            </image_processing>
        </structure_determination>
    </structure_determination_list>
    <map size_kbytes="420993" format="CCP4">
        <file>emd_1491.map.gz</file>
        <symmetry>
            <space_group>1</space_group>
        </symmetry>
        <data_type>Image stored as Reals</data_type>
        <dimensions>
            <col>2024</col>
            <row>1024</row>
            <sec>52</sec>
        </dimensions>
        <origin>
            <col>-512</col>
            <row>-1012</row>
            <sec>-26</sec>
        </origin>
        <spacing>
            <x>1024</x>
            <y>2024</y>
            <z>52</z>
        </spacing>
        <cell>
            <a>5898.24</a>
            <b>11658.2</b>
            <c>299.52</c>
            <alpha>90</alpha>
            <beta>90</beta>
            <gamma>90</gamma>
        </cell>
        <axis_order>
            <fast>X</fast>
            <medium>Y</medium>
            <slow>Z</slow>
        </axis_order>
        <statistics>
            <minimum>-8.616390000000001</minimum>
            <maximum>8.42534</maximum>
            <average>0.0000000084551</average>
            <std>1.14466</std>
        </statistics>
        <pixel_spacing>
            <x>5.76</x>
            <y>5.76</y>
            <z>5.76</z>
        </pixel_spacing>
        <annotation_details>A map of F-actin cross-linked by villin</annotation_details>
        <details>::::EMDATABANK.org::::EMD-1491::::</details>
    </map>
    <interpretation>
        <modelling_list>
            <modelling/>
        </modelling_list>
    </interpretation>
</emd>
