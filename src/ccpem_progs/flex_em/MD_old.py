#  ============================================================================
#
#  Simulated annealing molecular dynamics optimization with cross-correlation,
#  non-bonded interaction, and stereochemical restraints.
#
#  =======================  Maya Topf, 4 Dec 2007 =============================
#  =======================  Latest update: 6/8/09 =============================

# To do:
# 1) Tidy metadata output
# 2) Code tidy
# 3) Fix pdb output
#    -> why per iteration?
#    -> Ensemble better?
# 4) Simplify log output
# 5) Why remove *.MRC?
# 6) Why print to stderr?

import json
import sys
from xml.etree import ElementTree
from modeller import *
from modeller.optimizers import conjugate_gradients, molecular_dynamics, actions
from rigid import load_rigid
from ccpem_core.ccpem_utils.file_tools import format_xml


class opt_md:
    def __init__(self, path, code, run_num, rand, em_map_file, input_pdb_file,
                 format, apix, box_size, res, x, y, z, rigid_filename,
                 num_of_iter, quick_test=True, cap_shift=0.39, verbose=False):
        env = environ(rand_seed=-rand)
        env.libs.topology.read(file='$(LIB)/top_heav.lib')
        env.libs.parameters.read(file='$(LIB)/par.lib')
        env.io.hetatm = True
        log.verbose()
        FlexEMResult = ElementTree.Element('FlexEMResult')
        den = density(env, file=em_map_file, #path + '/' + em_map_file,
                      em_density_format=format, em_map_size=box_size,
                      density_type='GAUSS', voxel_size=apix,
                      resolution=res,px=x,py=y,pz=z)
        env.edat.density = den
        env.edat.dynamic_sphere = True

        # Set metadata
        self.metadata = {'em_density': [],
                         'iteration': []}
        self.metadata_filename = 'metadata.json'
        # Test Model
        if quick_test:
            print '-'*80
            print '\n\nTest mode!\n\n'
            print '-'*80
        # read pdb
        aln = alignment(env)
        print >> sys.stderr, input_pdb_file
        mdl2 = model(env, file=input_pdb_file)
        aln.append_model(mdl2,
                         align_codes=input_pdb_file,
                         atom_files=input_pdb_file)
        mdl = model(env, file=input_pdb_file)
        aln.append_model(mdl, align_codes=code, atom_files=code)
        aln.align(gap_penalties_1d=(-600, -400))
        mdl.clear_topology()
        mdl.generate_topology(aln[input_pdb_file])
        mdl.transfer_xyz(aln)
        mdl.build(initialize_xyz=False, build_method='INTERNAL_COORDINATES')
        mdl.res_num_from(mdl2, aln)

        # Renumber chains
        # for c in mdl.chains:
        #     c.name = ' '
        mdl.write(file=code+'_ini.pdb')

        # Build restraints
        sel_all = selection(mdl)
        mdl.restraints.make(sel_all, 
                            restraint_type='stereo',
                            spline_on_site=False)
        mdl.restraints.make(sel_all,
                            aln=aln,
                            restraint_type='phi-psi_binormal',
                            spline_on_site=True,
                            residue_span_range=(0, 99999))

        # Define rigid bodies
        sel_rigid = []
        rand_rigid = []

        # XXX why go to stderr??
        print >> sys.stderr, rigid_filename
        load_rigid(path, mdl, sel_rigid, rand_rigid, rigid_filename)
        for n in sel_rigid:
            mdl.restraints.rigid_bodies.append(rigid_body(n))
        mdl.restraints.write(file=code+'.rsr')

        # Make selections
        s_list = []
        for n in sel_all:
            for m in sel_rigid:
                if n in m:
                    a = 1
                    break
                else:
                    a = 0
            if a == 0:
                s_list.append(n)
        sel_flex = selection(s_list)

        # Print selections
        if verbose:
            print sel_all
            print sel_rigid
            print sel_flex

        # start simulated annealing molecular dynamics
        print 'MD annealing'
        scal = physical.values(default=1.0, em_density=10000)
        timestep = 5.0
        icount = 0
        MD = molecular_dynamics(cap_atom_shift=cap_shift, md_time_step=timestep,
                                md_return='FINAL', output='REPORT',
                                schedule_scale=scal)
        trc_file = open('MD'+run_num+'.trc', "a")

        a = 1
        b = num_of_iter + 1

        while a < b:
            currentIteration = ElementTree.SubElement(FlexEMResult,
                                                      'iteration',
                                                      id=str(a))
            if quick_test:
                equil_its = 1
                equil_equil = 1
                equil_temps = [150.0]
                trc_step = 1
            else:
                equil_its = 100
                equil_equil = 20
                equil_temps = (150.0, 250.0, 500.0, 1000.0)
                trc_step = 5
            init_vel = True
            scals = (
                  physical.values(default=0.0, em_density=1.0),
                  physical.values(default=0.0, em_density=0.0, phi_psi_dihedral=1.0),
                  physical.values(default=0.0, em_density=0.0, bond=1.0),
                  physical.values(default=0.0, em_density=0.0, angle=1.0),
                  physical.values(default=0.0, em_density=0.0, dihedral=1.0),
                  physical.values(default=0.0, em_density=0.0, improper=1.0),
                  physical.values(default=0.0, em_density=0.0, soft_sphere=1.0) #,
                  # physical.values(em_density=10000, default=1.0)
                  )

            # Get initial energies
            if a == 1:
                for scal in scals:
                    currentscal = [currentscal for currentscal in enumerate(scal._dict.iteritems()) if int(currentscal[1][1]) == 1]
                    csind = currentscal[0][0]-1
                    (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                    scal_name = scal.keys()[currentscal[0][0]]._shortname
                    if scal_name == 'em_density':
                        self.metadata['em_density'].append(-molpdf)
                        self.metadata['iteration'].append(0)
                        self.metadata_output()

            for temp in equil_temps:
                icount += equil_its
                (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                print "molpdf total energy =", molpdf
                MD.optimize(sel_all,
                            max_iterations=equil_its,
                            temperature=temp,
                            init_velocities=init_vel,
                            equilibrate=equil_equil,
                            actions=[actions.trace(trc_step, trc_file)])
                object_indexes = (37, 25, 1, 2, 3, 4, 5)
                # scal1 = physical.values(default=0.0, em_density=1.0)
                # scal2 = physical.values(default=0.0, em_density=0.0, phi_psi_dihedral=1.0)
                # scal3 = physical.values(default=0.0, em_density=0.0, bond=1.0)
                # scal4 = physical.values(default=0.0, em_density=0.0, angle=1.0)
                # scal5 = physical.values(default=0.0, em_density=0.0, dihedral=1.0)
                # scal6 = physical.values(default=0.0, em_density=0.0, improper=1.0)
                # scal7 = physical.values(default=0.0, em_density=0.0, soft_sphere=1.0)
                print "iteration number= %s, step= %d temp= %d " %(a,icount,int(temp))
                print "-cross-correlation\tphi_psi_dihedral\tbond\tangle\tdihedral\timproper\tsoft_sphere" #\ttotal" 
                currentStep = ElementTree.SubElement(currentIteration, 'temp', temp=str(temp))

                for scal in scals:
                    currentscal = [currentscal for currentscal in enumerate(scal._dict.iteritems()) if int(currentscal[1][1]) == 1]
                    csind = currentscal[0][0]-1
                    (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                    currentEnergy = ElementTree.SubElement(currentStep, scal.keys()[currentscal[0][0]]._shortname)
                    currentEnergy.text = str(molpdf)
                    if verbose:
                        print "i =",scal
                        print >> sys.stderr,currentscal
                        print "c =",currentscal
                        print scal.keys()
                        print "csind =",csind
                        print "shortname =",scal.keys()[csind]
                        print "molpdf =",molpdf,"\t",
                        print currentStep
                        print scal.keys()
                        print scal.keys()[currentscal[0][0]]._shortname

                (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                print "iteration number= %s  step= %d  temp= %d  EM score= %.3f" %(a,icount,int(temp),-molpdf)
                scal = physical.values(default=1.0, em_density=10000)

            init_vel = False

            if quick_test:
                equil_its = 1
                equil_temps = [50.0]
            else:
                equil_its = 200
                equil_temps = (800.0, 500.0, 250.0, 150.0, 50.0, 0.0)

            MD = molecular_dynamics(cap_atom_shift=cap_shift,
                                    md_time_step=timestep,
                                    md_return='FINAL',
                                    output='REPORT',
                                    schedule_scale=scal)
            for temp in equil_temps:
                icount += equil_its
                (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                print "molpdf total energy =",molpdf
                MD.optimize(sel_all,
                            max_iterations=equil_its,
                            temperature=temp,
                            init_velocities=init_vel,
                            equilibrate=equil_equil,
                            actions=[actions.trace(trc_step,trc_file)])
                # scal1 = physical.values(default=0.0, em_density=1.0)
                # scal2 = physical.values(default=0.0, em_density=0.0, phi_psi_dihedral=1.0)
                # scal3 = physical.values(default=0.0, em_density=0.0, bond=1.0)
                # scal4 = physical.values(default=0.0, em_density=0.0, angle=1.0)
                # scal5 = physical.values(default=0.0, em_density=0.0, dihedral=1.0)
                # scal6 = physical.values(default=0.0, em_density=0.0, improper=1.0)
                # scal7 = physical.values(default=0.0, em_density=0.0, soft_sphere=1.0)
                print "iteration number= %s, step= %d temp= %d " %(a,
                                                                   icount,
                                                                   int(temp))
                print "-cross-correlation\tphi_psi_dihedral\tbond\tangle\tdihedral\timproper\tsoft_sphere" 
                currentStep = ElementTree.SubElement(currentIteration,
                                                     'step',
                                                     step=str(icount),
                                                     temp=str(temp))
                for scal in scals:
                    currentscal = [currentscal for currentscal in enumerate(scal._dict.iteritems()) if int(currentscal[1][1]) == 1]
                    csind = currentscal[0][0]-1
                    (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                    scal_name = scal.keys()[currentscal[0][0]]._shortname
                    currentEnergy = ElementTree.SubElement(
                           currentStep,
                           scal_name)
                    currentEnergy.text = str(molpdf)
                    if verbose:
                        print "c =",currentscal
                        print scal.keys()
                        print "csind =",csind
                        print "shortname =",scal.keys()[csind]
                scal = physical.values(default=1.0, em_density=10000)
            
            # End iteration
            # Get em density and other scores.
            # XXX This code must be tidied!!! XXX
            for scal in scals:
                currentscal = [currentscal for currentscal in enumerate(scal._dict.iteritems()) if int(currentscal[1][1]) == 1]
                csind = currentscal[0][0]-1
                (molpdf, terms) = sel_all.energy(schedule_scale=scal)
                scal_name = scal.keys()[currentscal[0][0]]._shortname
                if scal_name == 'em_density':
                    self.metadata['em_density'].append(-molpdf)
                    self.metadata['iteration'].append(a)
                    self.metadata_output()
            
            # E.g. to get em density alone
#             scal = scals[0]
#             (molpdf, terms) = sel_all.energy(schedule_scale=scal)
#             scal_name = scal.keys()[[(0, (37, 1.0))][0][0]]._shortname

            # XXX why write out so many pdb files!?
            # Output pdb at iteration
            filename = 'md'+run_num+'_'+str(a)+'.pdb'
            sel_all.write(file=filename)
            a += 1

        trc_file.close()
        print "MD step %d: energy all (scaling 1:10000)" % icount
        eval = sel_all.energy(schedule_scale=scal)

        # final minimization with and without CC restraints
        CG = conjugate_gradients()
        print " final conjugate_gradients"

        # Test mode
        if quick_test:
            max_it = 5
        else:
            max_it = 200
        CG.optimize(sel_all, output='REPORT', max_iterations=max_it,
                    schedule_scale=scal)
        eval = sel_all.energy(schedule_scale=scal)
        scal = physical.values(default=1.0, em_density=0.0)
        CG.optimize(sel_all, output='REPORT', max_iterations=max_it,
                    schedule_scale=scal)
        eval = sel_all.energy(schedule_scale=scal)
        sel_all.write(file='final'+run_num+'_mdcg.pdb')

        # Write relevant info to XML file
        with open("FlexEM_MD_trajectory.xml", 'w') as f:
            f.write(format_xml(FlexEMResult))

        # XXX Fix me
        # Not a good idea:
        # os.system("rm -f *.MRC")

    def metadata_output(self):
        '''
        Metadata output
        '''
        json.dump(self.metadata,
                  open(self.metadata_filename, 'w'),
                  indent=4,
                  separators=(',', ': '))
