#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import json
import tempfile
try:
    import modeller  # @UnusedImport
    modeller_available = True
    from ccpem_progs.flex_em.flexem import FlexEM
except ImportError:
    modeller_available = False
from ccpem_core import ccpem_utils


class Test(unittest.TestCase):
    '''
    Test flex EM
    '''
    # Set working directory as FlexEM sets temporary working directories
    # causing multiple tests to crash
    excute_path = os.getcwd()

    def setUp(self):
        '''
        Always run at start of test, e.g. for creating directory to store
        temporary test data producing during unit test.
        '''
        os.chdir(self.excute_path)
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        if os.path.exists(self.test_output):
            shutil.rmtree(self.test_output)

    @unittest.skipIf(not modeller_available,
                     'Modeller not found: skipping FlexEM unittest')
    def test_flex_em_md(self):
        '''
        Test Flex-EM MD (only run if modeller is available)
        '''
        ccpem_utils.print_header(message='Unit test - Flex-EM MD')
        args = {
            'optimization': 'MD',
            'input_pdb_file': self.test_data + '/1akeA.pdb',
            'code': '1oel',
            'em_map_file':  self.test_data + '/1akeA_10A.mrc',
            'map_format': 'mrc',
            'apix': 3,
            'box_size': 22,
            'resolution': 10,
            'x': -6.49399995803833,
            'y': 13.381000518798828,
            'z': -5.409999847412109,
            'path': self.test_output,
            'init_dir': 1,
            'num_of_iter': 1,
            'rigid_filename': self.test_data + '/rigid.txt',
            'job_location': self.test_output,
            'phi_psi_restraints': True,
            'em_map': True,
            'max_atom_disp': 0.39,
            'test_mode': True,
            'distance_restraints': False,
            'max_distance' : 5.0,
            'density_weight' : 1.0,
            'keyword_json' : None}
        json.dump(args, open(self.test_output + '/flex_em_params.json', 'w'))
        args = json.load(
            open(self.test_output + '/flex_em_params.json', 'r'))
        FlexEM(args=args)

        # Test output
        metadata_path = os.path.join(self.test_output,
                                     '1_MD/metadata.json')
        assert os.path.exists(metadata_path)
        test_metadata = json.load(open(metadata_path, 'r'))
        assert len(test_metadata['em_density']) == 2
        map_cc_end = test_metadata['em_density'][1]
        self.assertAlmostEqual(map_cc_end, 0.956465, places=2)

#     @unittest.skipIf(not modeller_available,
#                      'Modeller not found: skipping FlexEM unittest')
#     def test_flex_em_cg(self):
#         '''
#         Test Flex-EM CG (only run if modeller is available)
#         '''
#         ccpem_utils.print_header(message='Unit test - Flex-EM CG')
#         params = {
#             'optimization': 'CG',
#             'input_pdb_file': self.test_data + '/1akeA.pdb',
#             'code': '1oel',
#             'em_map_file':  self.test_data + '/1akeA_10A.mrc',
#             'map_format': 'mrc',
#             'apix': 3,
#             'box_size': 22,
#             'resolution': 10,
#             'x': -6.49399995803833,
#             'y': 13.381000518798828,
#             'z': -5.409999847412109,
#             'path': self.test_output,
#             'init_dir': 1,
#             'num_of_iter': 2,
#             'rigid_filename': self.test_data + '/rigid.txt',
#             'job_location': self.test_output,
#             'em_map': True,
#             'phi_psi_restraints': True,
#             'max_atom_disp': 0.39,
#             'test_mode': True}
#         json.dump(params, open(self.test_output + '/flex_em_params.json', 'w'))
#         args = json.load(
#             open(self.test_output + '/flex_em_params.json', 'r'))
#         FlexEM(args=args)
# 
#         # Test output
#         metadata_path = os.path.join(self.test_output,
#                                      '1_CG/metadata.json')
#         assert os.path.exists(metadata_path)
#         test_metadata = json.load(open(metadata_path, 'r'))
#         assert len(test_metadata['em_density']) == 3
#         map_cc_end = test_metadata['em_density'][2]
#         self.assertAlmostEqual(map_cc_end, 0.83799326, places=2)


if __name__ == '__main__':
    unittest.main()
