import os,sys,re
import subprocess
from ccpem_progs import jpred
import glob
import time
import types
import urllib
import tarfile
import json
from ccpem_core.pdb_tools.biopy_tools import *

commands = {
        'jpredapi' : ['perl',os.path.join(
                        os.path.dirname(jpred.__file__),'jpredapi')]}

class JpredSubmit:
    '''
    Class for Jpred job submission using the jpredapi script
    Given a pdbfile, sets sequence files and submits to jpred server
    
    '''
    def __init__(self,pdbfile=None,job_location=None,seqdir=None):
        cwd = os.getcwd()
        if job_location is None:
            self.job_location = os.getcwd()
        elif os.path.isdir(job_location):
            self.job_location = job_location
            os.chdir(job_location)
        self.nseq = 0
        if not pdbfile is None and os.path.isfile(pdbfile):
            #get sequences from the pdb file
            ps = pdb2seq(pdbfile)
            #get seqres based sequence
            dict_seqres = ps.get_pdbseqres()
            
            #get sequence based on atom records
            dict_atomseq, dict_resdet = ps.get_pdbatomseq()
            
            #use atom record based sequence in case of no seqres records
            if len(dict_atomseq) > len(dict_seqres):
                self.dict_pdbseq = dict_atomseq
            else:
                self.dict_pdbseq = dict_seqres
            self.save_residue_details(dict_atomseq, dict_resdet)
        #save sequences as fasta files
        self.save_seqfiles()
        
        for pdbch in self.dict_pdbseq:
            seq = self.dict_pdbseq[pdbch]
            #jpred specific limits
            if len(seq) > 800:
                print 'Sequence too long for Jpred: ', pdbch
                continue
            elif len(seq) < 30: 
                print 'Sequence too short for Jpred: ', pdbch
                continue
                
            seqfile = os.path.join(self.job_location,pdbch+'.fasta')
            if os.path.isfile(seqfile):
                #submit to server
                self.submit_seq(seqfile)
        os.chdir(cwd)
    
    def save_residue_details(self,dict_atomseq,dict_resdet):
        '''
        save residue details for atom records
        '''
        dict_residue_details = {}
        json_filename = 'chain_atomseq.json'
        for pdbch in dict_atomseq:
            #pdbch: 'pdb5ni1_D'
            pdbch_split = pdbch.split('_')
            pdbid = '_'.join(pdbch_split[:-1])
            chain = pdbch_split[-1]
            if not chain in dict_residue_details: dict_residue_details[pdbch] = {}
            dict_residue_details[pdbch]['atomseq'] = dict_atomseq[pdbch] #seq string
            dict_residue_details[pdbch]['residue_id'] = dict_resdet[pdbch] #[[resnum,resname],..]
        jsonfile = os.path.join(self.job_location,json_filename)
        with open(jsonfile,'w') as js:
            json.dump(dict_residue_details,js)
            
    def save_seqfiles(self):
        '''
        Save sequence fasta files based on dict of sequences
        '''
        #pdbch = pdbname+'_'+ch
        for pdbch in self.dict_pdbseq:
            pdbname = '_'.join(pdbch.split('_')[:-1])
            seqfile = os.path.join(self.job_location,pdbch+'.fasta')
            with open(seqfile,'w') as seqf:
                seqf.write(">{}\n".format(pdbch))
                seqf.write("{}\n".format(self.dict_pdbseq[pdbch]))
            
    def submit_seq(self,sequence_file):
        '''
        Submit sequence using jpredapi with a subprocess call
        '''
        assert os.path.isfile(sequence_file)
        args = ['submit','mode=single', 'format=fasta','silent','longtime=on',
                'file={}'.format(sequence_file)]
        log_file = os.path.splitext(sequence_file)[0]+'_log_running_job_id.txt'
        command_args = commands['jpredapi'][:]
        command_args += args
        #print command_args
        with open(log_file,'w') as of:
            subprocess.call(command_args,stdout=of)
            
def main():
    pdbfile = sys.argv[1]
    if len(sys.argv) > 2:
        job_location = sys.argv[2]
    else: job_location = None
    jp = JpredSubmit(pdbfile=pdbfile,job_location=job_location)

if __name__ == '__main__':
    main()