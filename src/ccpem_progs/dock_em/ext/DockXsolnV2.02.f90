
!C*******************************************************************************
!C    Copyright (c) Medical Research Council, Laboratory of Molecular	       *
!C     Biology.    All rights reserved.					       *
!C									       *								
!C     All files within this package, unless otherwise stated, are Copyright    *
!C     (c) Medical Research Council. Redistribution is forbidden.               *
!C     This program was written by Alan Roseman at the MRC Laboratory of        *
!C     Molecular Biology, Hills Road, Cambridge, CB2 2QH, United Kingdom.       *
!C									       *
!C     The package and the source are provided without warranty of any kind,    *
!C     either express or implied, including but not limited to merchantability  *
!C     or fitness for a particular purpose.				       *
!C									       *
!C     The MRC will not be liable in any way for any losses howsoever caused    *
!C     by the use of the package or the source, such losses to include but not  *
!C     be limited to loss of profit, business interruption, loss of business    *
!C     information, or other pecuniary loss, including but not limited to       *
!C     special incidental consequential or other damages.  The user agrees      *
!C     to hold the MRC harmless for any loss, claim, damage, or liability,      *
!C     of whatsoever kind or nature, which may arise from or in connection      *
!C     with the use thereof.  						       *
!C                                                                              *
!C     COPYRIGHT 2001, 2002.				  	     	 	       *
!C									       *
!C*******************************************************************************
!DockXsolnV2.00.f90, AMR Jan 2003.
!C DockXsoln2V1.02.f90
! mod 5/7/01
! mod for dockem2v1, centre for rotn is at centre of map. nov 2002.
! mod for dockem2v2, centre for rotn is at centre of mass of coordinates. dec 2002.

!v1.02 no shift ofset of 1/5 pix required.

!11/5 amr
!C input: 
!C       pdb file file - fit object
!C       run version key.
!C       sampling of the map used for the search
!C	range of solutions to generate


!C output: pdb files corresponding to searchdoc keys.
!C  

!C v4 F90 matmults.   20/6/01
! 4/3 some changes for sgi compilers



 

	program DockXsoln2
	
        Use coordinates
        
        real sampling
        real psi,theta,phi
        character*40 pdbfile,outfile2
	integer a,angnum,bcount,angle
	real j1,j2,j3,cx,cy,cz,x,y,z
	integer ji1,ji2
	real comx,comy,comz
	integer numangles,numatoms,count
      	integer runcode
      	character*3 num
      	real dx,dy,dz,b
!C sampling for position search, in voxels      		
        integer  counter,counter2,zx,zy,zz,r1,r2,N
	integer nx3,ny3,nz3
	
	
	
	

      
      write(6,*) 'DockXsoln2'
      write(6,*) '=============='
      write(6,*) '(for DockEMv2)'
      write(6,*)

!C 1. read coords
      write (6,*) 'Enter the name of the search object pdb file '
      read (5,*)  pdbfile
10    format(A40)
      print*, pdbfile


      write (6,*) 'Enter a run code, for output files.'
      read (5,*) runcode
      print*, runcode
30    format (I3)


      write (6,*) 'Enter the sampling in Angstoms.'
      read*,sampling

      write (6,*) 'Enter the range of solutions to generate.'
      read *, r1,r2
      print *, r1,r2


	print*,'Enter pdbfile to get centre from.'
	read*,pdbfile

!C ***************************************************



	call readpdb(pdbfile,numatoms)
	call com(cx,cy,cz,numatoms)
	cx=sampling*int((cx/sampling)+0.5)
	cy=sampling*int((cy/sampling)+0.5)
	cz=sampling*int((cz/sampling)+0.5)

	!shift=0.5*sampling
         shift=0
	cx=cx-shift
	cy=cy-shift
	cz=cz-shift

!C open docfile

	open(4,file='searchdoc'//num(runcode)//'.dkm',status='old')
					
510		format (1x,I8,3F8.2,I8,F8.3,3F10.2,2x,2G12.6)	



!C write top set of hits here
	do N=1,r2
		
		
		read (4,510) ji1,x,y,z,angnum,b,psi,theta,phi,j2,ccc
		

		
		if (ji1.ge.r1) then			
			dx=x*sampling
			dy=y*sampling
			dz=z*sampling
			
			
			outfile2='tophit'//num(runcode)//'.'//num(ji1)//'.pdb'
		
			call spirot(pdbfile,outfile2,psi,theta,phi,dx,dy,dz,sampling,cx,cy,cz)
		endif



	end do




	close(4)
				

	goto 999




999    continue
       print*,'Program finished O.K.'
       stop

902    stop 'Error in reading the searchdoc file'
       end
!C ************************************************

 
         
         
         
 

