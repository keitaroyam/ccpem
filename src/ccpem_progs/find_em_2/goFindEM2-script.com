#!/bin/csh -x
# script to launch FindEM2 program
# IN: image,template, runcode
FindEM2 << eot
$1          #imagefile
$2          #template
-200.       #min val applied to template
12.5        #sampling in image (Angstroms/per pixel)
360.        #diameter of particle, or circular mask to appply (Angstroms)
$3          #runcode - see old FindEM documentation
0.,30.,4.   #start angle, upper limit of angle search range, step size; all in degrees
blank.mrc   # An assymetric mask, it will be binarised (Values less than 0.5 = 0, else =1).
eot
