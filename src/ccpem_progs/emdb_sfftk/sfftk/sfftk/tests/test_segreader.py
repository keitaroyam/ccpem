#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division
"""
sfftk.tests.test_segreader

Copyright 2017 EMBL - European Bioinformatics Institute
Licensed under the Apache License, Version 2.0 (the "License"); 
you may not use this file except in compliance with the License. 
You may obtain a copy of the License at 

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software 
distributed under the License is distributed on an 
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
either express or implied. 

See the License for the specific language governing permissions 
and limitations under the License.
"""

__author__ = "Paul K. Korir, PhD"
__email__ = "pkorir@ebi.ac.uk, paul.korir@gmail.com"
__date__ = 2017-03-01

import sys, os
import unittest

from ..readers.segreader import SeggerSegmentation


class TestSeggerSegmentation(unittest.TestCase):
    def test_read(self):
        s = SeggerSegmentation('/Users/pkorir/Documents/workspace/bioimaging-scripts/trunk/sff/test_data/emd_2847.seg', 'r') 
        
        self.assertEqual(s.format, 'segger')
        self.assertEqual(s.format_version, 2)


if __name__ == "__main__":
    unittest.main()