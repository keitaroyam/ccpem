# coding: utf-8

import numpy as np
from mrc_format import MRC_Data


class NumpyTom():

    def __init__(self, ifilepath):
        self.tom    = np.load(ifilepath)
        self.width  = self.tom.shape[-1]
        self.height = self.tom.shape[1]
        self.depth  = self.tom.shape[0]

    def __enter__(self):
        return self

    def __exit__(self, type, value, trace):
        pass

    def get_all_tom_density_values(self):
        return self.tom

    def get_centre(self):
        return (self.width/2, self.height/2)
        

def open_tom(ifilepath):
    return NumpyTom(ifilepath)


class MRCfile():

    def __init__(self, ifilepath):
        self.mrc    = MRC_Data(ifilepath, "mrc")
        self.data   = self.mrc.read_matrix((0,)*3, self.mrc.matrix_size, (1,)*3, None)
        self.width  = self.mrc.matrix_size[-1]
        self.height = self.mrc.matrix_size[1]
        self.depth  = self.mrc.matrix_size[0]

    def __enter__(self):
        return self

    def __exit__(self, type, value, trace):
        pass

    def get_all_tom_density_values(self):
        return self.data

    def get_centre(self):
        return (self.width/2, self.height/2)


