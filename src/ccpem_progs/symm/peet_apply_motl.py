#!/usr/bin/python

import os
import sys
import re
import subprocess 
import argparse


class MOTLClass:
    def __init__(self, line):
        field = line.split(",")
        self.CCC = float(field[0])
        self.p_index = float(field[3])
        self.wedge_wt = float(field[4])
        self.x_offset = float(field[10])
        self.y_offset = float(field[11])
        self.z_offset = float(field[12])
        self.z1 = float(field[16])
        self.z2 = float(field[17])
        self.x = float(field[18])


def load_motl_file(motl_file):
    motls = []
    in_file = open(motl_file, 'r')
    lines = in_file.readlines()
    count = 0
    for line in lines:
        if count > 0:
            MOTL = MOTLClass(line)
            motls.append(MOTL)
        count += 1
    return motls


def transform_particle(tomogram, MOTL, output, convention, inverse=False):
    # Note that this procedure does NOT translate, only rotate

    header = HeaderClass()
    header.read_header(tomogram)

    if convention == "ZXZ":  # PEET
        interpolation = "--interp linear"
        if not inverse:
            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i "+tomogram+":mrc --rotate_volume axis "
                      + str(MOTL.z2) + " 0 0 1  -o .temp1.mrc:mrc > /dev/null")
            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i .temp1.mrc:mrc   --rotate_volume axis "
                      + str(MOTL.x) + " 1 0 0  -o .temp2.mrc:mrc > /dev/null")  # X-axis
            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i .temp2.mrc:mrc   --rotate_volume axis "
                      + str(MOTL.z1) + " 0 0 1  -o .temp3.mrc:mrc > /dev/null")
        else:
            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i "+tomogram+":mrc --rotate_volume axis "
                      + str(-MOTL.z1) + " 0 0 1  -o .temp1.mrc:mrc > /dev/null")
            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i .temp1.mrc:mrc   --rotate_volume axis "
                      + str(-MOTL.x) + " 1 0 0  -o .temp2.mrc:mrc > /dev/null")  # X-axis
            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i .temp2.mrc:mrc   --rotate_volume axis "
                      + str(-MOTL.z2) + " 0 0 1  -o .temp3.mrc:mrc > /dev/null")

#    elif convention == "ZYZ": # Xmipp
#        if not inverse:
#            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i "
#                      +tomogram+":mrc --rotate_volume axis "+ str(MOTL.z2)+" 0 0 1  -o .temp1.mrc:mrc > /dev/null")
#            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i .temp1.mrc:mrc  --rotate_volume axis "
#                      + str(MOTL.x )+" 0 1 0  -o .temp2.mrc:mrc > /dev/null")  # Y-axis
#            os.system("xmipp_transform_geometry --dont_wrap "+interpolation+" -i .temp2.mrc:mrc  --rotate_volume axis "
#                      + str(MOTL.z1)+" 0 0 1  -o .temp3.mrc:mrc > /dev/null")
    else:
        sys.stderr.write("ERROR: Euler convention '"+convention+"' not recognized! Aborting.\n")
        sys.exit(1)

    os.system("cp .temp3.mrc "+output)
    os.system("alterheader "+output+" -mmm")
    os.system("rm .temp1.mrc .temp2.mrc .temp3.mrc")
    alter_pixel_size(output, header.pixSize.x)


def alter_pixel_size(in_file, pixel_size):
    process = subprocess.Popen("alterheader", stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.STDOUT)
    error = process.communicate(input=in_file + "\ndel\n" + str(pixel_size) + " " + str(pixel_size) + " "
                                + str(pixel_size) + "\ndone\n")


class PointClass:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z


class HeaderClass:
    def __init__(self):
        self.pixSize = PointClass(0, 0, 0)
        self.dimensions = PointClass(0, 0, 0)

    def read_header(self, filename):
        pix_spacing_re = re.compile("\s+Pixel spacing \(Angstroms\)\.+\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)")
        dimensions_re = re.compile("\s+Number of columns, rows, sections \.\.\.\.\.\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)")

        commands = [os.environ["IMOD_DIR"]+'/bin/header', filename]
        process = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        try:
            process = subprocess.Popen(commands, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        except:
            sys.stderr.write("ERROR: executing 'header': is it installed?\n\n"+" ".join(commands)+"\n\n")
            sys.exit(1)
        stdout, stderr = process.communicate()
        lines = stdout.rsplit("\n")
        for line in lines:
            m = pix_spacing_re.search(line)
            if m:
                self.pixSize = PointClass(float(m.group(1)), float(m.group(2)), float(m.group(3)))
            m = dimensions_re.search(line)
            if m:
                self.dimensions = PointClass(int(m.group(1)), int(m.group(2)), int(m.group(3)))


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("input_mrc")
    parser.add_argument("motl_file")
    parser.add_argument("motl_particle_number", help="counting from 0", type=int)
    parser.add_argument("output_mrc")
    parser.add_argument("-i", "--inverse",
                        action="store_true",
                        help="apply inverse MOTL",
                        default=False)
    parser.add_argument("-e", "--euler",
                        action="store",
                        type=str,
                        dest="euler",
                        help="Euler convention, e.g., ZXZ (as used in PEET)",
                        default="ZXZ")

    args = parser.parse_args()

    tomogram = args.input_mrc
    motl_file = args.motl_file
    particle = int(args.motl_particle_number)
    output = args.output_mrc

    if not os.path.isfile(tomogram):
        sys.stderr.write("ERROR: " + tomogram + " does not exist!\n")
        sys.exit(1)
    if not os.path.isfile(motl_file):
        sys.stderr.write("ERROR: " + motl_file + " does not exist!\n")
        sys.exit(1)

    motls = load_motl_file(motl_file)

    transform_particle(tomogram, motls[particle], output, args.euler, inverse=args.inverse)


if __name__ == "__main__":

    if "IMOD_DIR" not in os.environ:
        sys.stderr.write("ERROR: Need 'IMOD_DIR' defined as an environment variable!\n")
        sys.exit(1)
    main()
