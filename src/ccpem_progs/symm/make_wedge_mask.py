#!/usr/bin/python

import numpy
import math
import argparse
import npy2mrc


def get_angle(x, y):
    if x != 0:
        angle = math.degrees(math.atan(float(y)/float(x)))
    else:
        if y < 0:
            angle = -90
        else:
            angle = 90

    return angle


def dist_3d(x, y, z):
    return math.sqrt(x*x + y*y + z*z)


def get_missing_wedge_volume(min_tilt, max_tilt, box_size):
    volume = numpy.empty((box_size, box_size, box_size))

    start = -int(box_size / 2)
    end = start + box_size

    for z in range(start, end):
        for y in range(start, end):
            for x in range(start, end):
                if dist_3d(x, y, z) > abs(start):
                    volume[z - start][y - start][x - start] = 0
                else:
                    angle = get_angle(x, z)
                    if (angle >= min_tilt) and (angle <= max_tilt):
                        volume[z - start][y - start][x - start] = 1
                    else:
                        volume[z - start][y - start][x - start] = 0

    return volume


def write_mrc(output_filename):
    print "Write " + output_filename


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("min_tilt", type=float, help="lower end of tiltrange")
    parser.add_argument("max_tilt", type=float, help="upper end of tiltrange")
    parser.add_argument("box_size", type=int, help="side length of the box")
    parser.add_argument("output_filename", type=str, help="name of the output mask file")

    args = parser.parse_args()

    min_tilt = float(args.min_tilt)
    max_tilt = float(args.max_tilt)
    box_size = int(args.box_size)
    output_filename = args.output_filename

    volume = get_missing_wedge_volume(min_tilt, max_tilt, box_size)

    npy2mrc.save_npy_as_mrc_file(output_filename, volume)


if __name__ == "__main__":
    main()
