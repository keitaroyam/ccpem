#!/usr/bin/env python
# coding: utf-8

import unittest
import numpy as np
import helper_functions.nptomo


class TestAnalysisMethods(unittest.TestCase):

    def setUp(self):
        self.mrc_string = "unittests/testdata/get_noise_in.mrc"
        self.npy_string = "unittests/testdata/get_noise_in.npy"
        self.expected_values = np.load(self.npy_string)

    def test_MRCfile_attributes(self):

        with helper_functions.nptomo.MRCfile(self.mrc_string) as in_mrc:
            self.assertEqual(in_mrc.width, 40, "Width of read mrc file different from expected")
            self.assertEqual(in_mrc.depth, 40, "Depth of read mrc file different from expected")
            self.assertEqual(in_mrc.height, 40, "Height of read mrc file different from expected")
            self.assertEqual(in_mrc.get_centre(), (20, 20), "Centre of read mrc file different from expected")

    def test_MRCfile_data(self):
        with helper_functions.nptomo.MRCfile(self.mrc_string) as in_mrc:
            np.testing.assert_array_equal(in_mrc.data, self.expected_values, "Data read from mrc file differs from "
                                                                             "expected values.")

    def test_NumpyTom_attributes(self):
        with helper_functions.nptomo.NumpyTom(self.npy_string) as in_npy:
            self.assertEqual(in_npy.width, 40, "Width of read npy file different from expected")
            self.assertEqual(in_npy.depth, 40, "Depth of read npy file different from expected")
            self.assertEqual(in_npy.height, 40, "Height of read npy file different from expected")
            self.assertEqual(in_npy.get_centre(), (20, 20), "Centre of read npy file different from expected")

    def test_NumpyTom_data(self):
        with helper_functions.nptomo.NumpyTom(self.npy_string) as in_npy:
            np.testing.assert_array_equal(in_npy.tom, self.expected_values, "Data read from npy file differs from "
                                                                            "expected values.")


if __name__ == "__main__":
        unittest.main()
