from chimera import runCommand as rc, selection
import chimera
import sys
import os
import numpy as np

#map1
if os.path.isfile(sys.argv[1]): 
    rc("open %s"%(sys.argv[1]))
    path1 = os.path.abspath(sys.argv[1])
    name1 = os.path.splitext(os.path.split(path1)[-1])[0]
    print path1, name1
else: rc("open emdbID:%s"%(sys.argv[1]))
#map2
if os.path.isfile(sys.argv[2]): 
    rc("open %s"%(sys.argv[2]))
    path2 = os.path.abspath(sys.argv[2])
    name2 = os.path.splitext(os.path.split(path2)[-1])[0]
    print path2,name2
else: rc("open emdbID:%s"%(sys.argv[2]))
         
mList = chimera.openModels.list()
m1 = mList[0]
a1 = m1.data.full_matrix()
mean1 = np.mean(a1)
std1 = np.std(a1)
savename = name2+'_'+name1+'.pdb'
rc("volume #0 step 1 level %f"%(mean1+2.0*std1))
rc("fitmap #1 #0 resolution %s metric correlation"%(sys.argv[3]))
rc("write relative 0 #1 %s"%(savename))
rc("stop")