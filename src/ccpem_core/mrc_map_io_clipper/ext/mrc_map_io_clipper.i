// File : map_io_clipper
%module mrc_map_io_clipper

%include "std_vector.i"
%include "std_string.i"

%{
  // the resulting C file should be built as a python extension
  #define SWIG_FILE_WITH_INIT
  //  Includes the header in the wrapper code
  #include <clipper/clipper.h>
  #include <clipper/clipper-ccp4.h>
  #include <clipper/core/clipper_precision.h>
  #include <clipper/core/clipper_types.h>
  #include "mrc_map_io_clipper.h"
  clipper::Vec3<int> GetGrid();
%}

// numpy support
%include "numpy.i"
%init %{
    import_array();
%}
%apply (float* INPLACE_ARRAY2, int DIM1, int DIM2) {(float *invec, int nx, int ny)}

// std::vector support
namespace std {
    %template(DoubleVector) vector<double>;
/*    %template(DoubleDoubleVector) std::vector<std::vector<double> >;*/
/*    %template(DoubleDoubleDoubleVector) std::vector<std::vector<std::vector<double> > >;*/
    //
    %template(FloatVector) vector<float>;
    %template(FloatFloatVector) std::vector<std::vector<float> >;
    %template(FloatFloatFloatVector) std::vector<std::vector<std::vector<float> > >;
    //
    %template(IntVector) vector<int>;
    %template(StringVector) vector<std::string>;
}
// End std::vector support

// clipper::Vec3 support
%typemap(out) clipper::Vec3<int> GetGrid() {
  int size = 3;
  $result = PyTuple_New(size);
  for (int i = 0; i < size; i++) {
    PyObject *o = PyInt_FromLong((int) $1[i]);
    PyTuple_SetItem($result,i,o);
  }
}
// End clipper::Vec3 support

%include mrc_map_io_clipper.h
