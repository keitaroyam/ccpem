#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
# Test map i/o clipper/cmaplib SWIG bindings
import unittest
import os
import shutil
import time
import tempfile
import numpy as np
from PIL import Image, ImageOps
from ccpem_core.mrc_map_io_clipper.ext import mrc_map_io_clipper as mrc_map_io_clipper_ext
from ccpem_core.mrc_map_io_clipper import map_tools
from ccpem_core.mrc_map_io_clipper import dump_mrc_header
from ccpem_core.mrc_map_io_clipper import dump_spider_header
from ccpem_core.mrc_map_io_clipper import read_mrc_header
from ccpem_core import test_data

import ccpem_core.ccpem_utils


def write_map(filename, cell_a, cell_b, cell_c, map_data):
    mrc_map_io_clipper_ext.ExportNXMap(filename,
                                       cell_a,
                                       cell_b,
                                       cell_c,
                                       map_data)


def dummy_map_data():
    # Dummy numpy array
    x1_array = [0.1, 0.3, 0.5, 0.7]
    x2_array = [0.0, 0.2, 0.4, 0.6]
    xy_array = []
    for n in xrange(3):
        if n < 2: 
            x1_new = map(lambda x: x+n, x1_array)
        else:
            x1_new = map(lambda x: x+n, x2_array)
        xy_array.append(x1_new)
    xy_array_2 = []
    for n in xrange(3):
        if n < 2: 
            x1_new = map(lambda x: x+n+10, x1_array)
        else:
            x1_new = map(lambda x: x+n+10, x2_array)
        xy_array_2.append(x1_new)
    map_array = np.array([xy_array, xy_array_2], dtype=float)
    return map_array


class Test(unittest.TestCase):
    '''
    Unit test
    '''
    def setUp(self):
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_basic_mrc_map_io(self):
        ccpem_core.ccpem_utils.print_sub_header('Test mrc map functions')
        # input mtz, convert to and export map
        mtz_in = os.path.join(self.test_data,
                              'mtz/1UBQ.pdb.mtz')
        map_out = os.path.join(self.test_output,
                               '1UBQ.pdb.map')
        mrc_map_io_clipper_ext.MtzToXMap(mtz_in,
                                         map_out)
        # XMap i/o
        map_in = os.path.join(self.test_data,
                              'map/mrc/emd_1494.map')
        map_out = os.path.join(self.test_output,
                               'emd_1494_x1.map')
        clipper_xmap = map_tools.ClipperXMap(filename=map_in)
        clipper_xmap.print_stats()
        clipper_xmap.write_xmap(filename_out=map_out)
        # NXMap i/o
        clipper_nxmap = map_tools.ClipperNXMap(filename=map_in)
        clipper_nxmap.print_stats()
        mapnx_out = os.path.join(self.test_output,
                                'emd_1494_nx1.map')
        clipper_nxmap.write_nxmap(filename_out=mapnx_out)

    def test_mrc_image_stack(self):
        ccpem_core.ccpem_utils.print_sub_header('\nTest mrc image stack functions')
        # Micrograph image stacks
        images_in = os.path.join(
            self.test_data,
            'images/mrc/run1_it018_classes.mrcs')
        clipper_nx_imagestack = map_tools.ClipperNXMap(filename=images_in)
        clipper_nx_imagestack.print_stats()
        # N.B. all functions assume images always stacked in y dim
        num_stack_images = clipper_nx_imagestack.clipper_nxmap.GetGrid()[2]
        print 'Number of stack images : {0:4d}'.format(num_stack_images)
        assert num_stack_images == 50
        for n_image in xrange(num_stack_images):
            image_array = clipper_nx_imagestack.clipper_nxmap.GetMapSector(
                n_image)
            image_array = np.array(image_array)
            # mode = F by default (floating point)
            # convert to mode = 'L' as b/w micrographs and allows extended
            # image format options e.g. png, jpg
            im = Image.fromarray(image_array)
            im = Image.fromarray(image_array).convert('L')
            # e.g. for better viewing
            im_auto_contrast = ImageOps.autocontrast(im)
            if num_stack_images < 10:
                num_prefix = '%01d_' % n_image
            elif num_stack_images < 100:
                num_prefix = '%02d_' % n_image
            elif num_stack_images < 1000:
                num_prefix = '%03d_' % n_image
            elif num_stack_images < 10000:
                num_prefix = '%04d_' % n_image
            im_auto_contrast.save(
                self.test_output + '/' + num_prefix + 'micrograph_ac.png')
 
    def test_import_mrc_as_numpy_array(self):
        ccpem_core.ccpem_utils.print_sub_header('\nTest import mrc as numpy array')
        # Test 3d numpy array from mrc map
        if True:
            start_time = time.clock()
            print '\nTest converting mrc map to numpy arrays - 3d copy'
            # Get 3D array directly
            map_in = os.path.join(self.test_data,
                                  'map/mrc/emd_1494.map')
            clipper_nx_imagestack = \
                map_tools.ClipperNXMap(filename=map_in)
            map_array = np.array(
                clipper_nx_imagestack.clipper_nxmap.GetMapArray())
            assert clipper_nx_imagestack.clipper_nxmap.GetGrid() \
                == map_array.shape
            print 'Time 3d copy (complete map) (secs) : ', \
                time.clock() - start_time
            del map_array
            del clipper_nx_imagestack
        if True:
            start_time = time.clock()
            print '\nTest converting mrc map to numpy arrays - per sector copy'
            clipper_nx_imagestack = \
                map_tools.ClipperNXMap(filename=map_in)
            grid = clipper_nx_imagestack.clipper_nxmap.GetGrid()
            # Get 3D array via map sector
            map_array = np.array(
                clipper_nx_imagestack.clipper_nxmap.GetMapSector(0))
            for n_z in xrange(1, grid[2]):
                map_array = np.dstack((
                    map_array,
                    clipper_nx_imagestack.clipper_nxmap.GetMapSector(n_z)))
            assert clipper_nx_imagestack.clipper_nxmap.GetGrid() \
                == map_array.shape
            print 'Time 3d copy (per sector) (secs) : ', \
                time.clock() - start_time
            del map_array
            del clipper_nx_imagestack
 
    def test_export_numpy_array_as_mrc(self):
        ccpem_core.ccpem_utils.print_sub_header('\nTest export numpy array as mrc')
        # Export map from numpy array
        md = dummy_map_data()
        filename = os.path.join(
            self.test_output,
            'numpy_export.mrc')
        write_map(filename=filename,
                  cell_a=md.shape[2],
                  cell_b=md.shape[1],
                  cell_c=md.shape[0],
                  map_data=md)
        # Import map from output mrc and test values
        nxmap = map_tools.ClipperNXMap(filename=filename)
        nxmap.print_stats()
        # Test map
        read_md = np.array(nxmap.clipper_nxmap.GetMapArray())
        assert read_md.shape == md.shape
        np.testing.assert_approx_equal(md[0][0][0],
                                       read_md[0][0][0],
                                       significant=7)
        np.testing.assert_approx_equal(md[-1][-1][-1],
                                       read_md[-1][-1][-1],
                                       significant=7)
        # Test y slice
        read_sec = np.array(nxmap.clipper_nxmap.GetMapSector(1))
        assert read_sec.shape == md[1].shape
        np.testing.assert_approx_equal(md[1][0][0],
                                       read_sec[0][0],
                                       significant=7)
        np.testing.assert_approx_equal(md[1][-1][-1],
                                       read_sec[-1][-1],
                                       significant=7)
 
    def test_export_image_as_mrc(self):
        ccpem_core.ccpem_utils.print_sub_header('\nTest export image as mrc')
        # Test converting from png
        image_in = os.path.join(
            self.test_data,
            'images/std/lenna.png')
        im = Image.open(image_in)
        im = im.resize((1024, 512))
        im_array = np.asarray(im)
        im = Image.fromarray(im_array).convert('L')
        im.save(self.test_output + '/lenna_out.tif')
        im_array = np.asarray(im)
        # Must be 3d array of dtype float
        im_array = np.array([im_array], dtype=float)
        filename = self.test_output + '/leena_out.mrc'
        write_map(filename=filename,
                  cell_a=im_array.shape[0],
                  cell_b=im_array.shape[1],
                  cell_c=im_array.shape[2],
                  map_data=im_array)
        # Import image from output mrc and test values
        nxmap = map_tools.ClipperNXMap(filename=filename)
        nxmap.print_stats()
        read_image = np.array(nxmap.clipper_nxmap.GetMapArray())
        assert read_image.shape == im_array.shape
        np.testing.assert_approx_equal(read_image[0][0][0],
                                       im_array[0][0][0],
                                       significant=7)
        np.testing.assert_approx_equal(read_image[-1][-1][-1],
                                       im_array[-1][-1][-1],
                                       significant=7)
        im = Image.fromarray(read_image[0]).convert('L')
        im.save(self.test_output + '/lenna_re_in_out.tif')
 
    def test_identify_volume_volume_stack_image_stack(self):
        ccpem_core.ccpem_utils.print_sub_header(
            '\nIdentify mrc type from volume, volume stack or image stack')
        # Single volume map
        map_in = os.path.join(
            self.test_data,
            'map/mrc/emd_1494.map')
        clipper_nxmap = map_tools.ClipperNXMap(
            filename=map_in)
        clipper_nxmap.print_stats()
        assert clipper_nxmap.is_volume_stack is False
        assert clipper_nxmap.is_image_stack is False
        # volume stack
        vol_in = os.path.join(
            self.test_data,
            'map/mrc/vol_stack/emd_5009_ispg401_2stack.map')
        clipper_nxmap = map_tools.ClipperNXMap(filename=vol_in)
        clipper_nxmap.print_stats()
        assert clipper_nxmap.is_volume_stack is True
        assert clipper_nxmap.is_image_stack is False
        # image stack
        images_in = os.path.join(
            self.test_data,
            'images/mrc/run1_it018_classes.mrcs')
        clipper_nxmap = map_tools.ClipperNXMap(images_in)
        clipper_nxmap.print_stats()
        assert clipper_nxmap.is_volume_stack is False
        assert clipper_nxmap.is_image_stack is True
 
    def test_volume_stack(self):
        ccpem_core.ccpem_utils.print_sub_header('\nTest mrc volume stack')
        vol_in = os.path.join(
            self.test_data,
            'map/mrc/vol_stack/emd_5009_ispg401_2stack.map')
        clipper_nxmap = map_tools.ClipperNXMap(
            filename=vol_in)
        assert clipper_nxmap.is_volume_stack is True
        assert clipper_nxmap.is_image_stack is False
        for v in range(1, 2):
            vol = clipper_nxmap.get_numpy_volume_stack(stack_number=v)
            assert vol.shape == (95, 95, 95)
            np.testing.assert_approx_equal(vol[20][20][20],
                                           -0.0551749,
                                           significant=6)
        map_array = clipper_nxmap.get_numpy_map_array()
        np.testing.assert_approx_equal(map_array[20][20][20],
                                       -0.0551749,
                                       significant=6)
        np.testing.assert_approx_equal(map_array[115][20][20],
                                       -0.0551749,
                                       significant=6)
 
    def test_rescale_3d_map(self):
        # Test changing grid dims in map object
        map_in = os.path.join(
            self.test_data,
            'map/mrc/emd_1494.map')
        filename_out = os.path.join(
            self.test_output,
            'rescale_nx_1494.map')
        nxmap = \
            map_tools.ClipperNXMap(filename=map_in)
        nxmap.clipper_nxmap.ResampleGrid(62)
        nxmap.clipper_nxmap.WriteMap(filename_out)

# 
    def test_inplace_numpy_map_slice(self):
        ccpem_core.ccpem_utils.print_sub_header('\nTest numpy inplace map slice')
        images_in = os.path.join(
            self.test_data,
            'images/mrc/run1_it018_classes.mrcs')
        clipper_nxmap = map_tools.ClipperNXMap(
            filename=images_in)
        map_slice = clipper_nxmap.get_numpy_map_sector(10)
        assert map_slice.shape == (60, 60)
        np.testing.assert_approx_equal(map_slice[22][33],
                                       3.73712,
                                       significant=5)
 
    def test_mrc_dump_header(self):
        mrc_in = os.path.join(
            self.test_data,
            'map/mrc/emd_1494.map')
        spider_in = os.path.join(
            self.test_data,
            'map/spider/smallVolume.vol')
        dump_mrc_header.main(filename=mrc_in)
        dump_spider_header.main(filename=spider_in)
        mrc_map_header_data = read_mrc_header.MRCMapHeaderData(
            filename=mrc_in,
            verbose=True)
        self.assertTupleEqual(mrc_map_header_data.origin, (0.0, 0.0, 0.0))
        self.assertAlmostEqual(mrc_map_header_data.map_volume,
                               8445792.66694,
                               2)
        self.assertAlmostEqual(mrc_map_header_data.voxx_voxy_voxz[2],
                               1.5909999609,
                               2)

if __name__ == '__main__':
    unittest.main()
