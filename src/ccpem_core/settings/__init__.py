#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import sys
import platform

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.ccpem_utils import check_directory_and_make


def ccpem_settings_parser():
    parser = ccpem_argparser.ccpemArgParser(add_help=False)
    #
    location = parser.add_argument_group()
    location.add_argument(
        '-location',
        '--location',
        help='Location of args json file.',
        type=str,
        default=None)
    #
    style = parser.add_argument_group()
    style.add_argument(
        '-style',
        '--style',
        help='Set widget style',
        choices=['default',
                 'plastique',
                 'cleanlooks',
                 'macintosh',
                 'motif',
                 'gtk',
                 'windows'
                 'windowsxp',
                 'windowsvista'
                 ],
        type=str,
        default='default')
    #
    projects = parser.add_argument_group()
    projects.add_argument(
        '-projects',
        '--projects',
        help='Set project manager location',
        type=str,
        default='')
    #
    alpha = parser.add_argument_group()
    alpha.add_argument(
        '-alpha',
        '--alpha',
        help='Set alpha mode (warning: some features may be unstable)',
        type=bool,
        default=False)
    #
    # Third party program locations
    additonal_program_locations = parser.add_argument_group()
    additonal_program_locations.add_argument(
        '-coot_bin',
        '--coot_bin',
        help='Location of Coot binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-pymol_bin',
        '--pymol_bin',
        help='Location of PyMOL binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-chimera_bin',
        '--chimera_bin',
        help='Location of chimera binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-ChimeraX_bin',
        '--ChimeraX_bin',
        help='Location of ChimeraX binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-ccp4mg_bin',
        '--ccp4mg_bin',
        help='Location of ccp4mg binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-relion_bin',
        '--relion_bin',
        help='Location of relion binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-refmac5_bin',
        '--refmac5_bin',
        help='Location of refmac5 binary',
        type=str,
        default=None)
    additonal_program_locations.add_argument(
        '-proshade_bin',
        '--proshade_bin',
        help='Location of proshade binary for testing',
        type=str,
        default=None)
    #
    additonal_program_locations.add_argument(
        '-haruspex_path',
        '--haruspex_path',
        help='Location of root haruspex library path for testing',
        type=str,
        default=None)
    #
    reset_to_defaults = parser.add_argument_group()
    reset_to_defaults.add_argument(
        '-reset',
        '--reset_to_default',
        help='Reset all args to default values',
        type=bool,
        default=False)
    return parser


def get_ccpem_settings():
    args = ccpem_settings_parser().generate_arguments()
    settings_path = get_settings_path()
    ccpem_settings_json = os.path.join(
        settings_path,
        'ccpem_settings.json')
    if os.path.isfile(ccpem_settings_json):
        args.import_args_from_json(filename=ccpem_settings_json)
    args.location.value = ccpem_settings_json
    if args.reset_to_default.value:
        args = reset_to_default()
        print('CCP-EM Settings')
        print('Reset settings')
        print(args.output_args_as_text())
    args.location.value = ccpem_settings_json
    args.output_args_as_json(args.location.value)
    return args


def get_settings_path():
    path = None
    settings_directory = 'ccpem'
    user_platform = platform.system()
    # Linux
    if user_platform == 'Linux':
        path = os.getenv('XDG_DATA_HOME',
                         os.path.expanduser('~/.config'))
    # Mac
    elif user_platform == 'Darwin':
        path = os.path.expanduser('~/Library/Application Support/')

    # Check write permissions, ask user to set directory if not ok
    if path is not None:
        if os.access(path, os.W_OK):
            path = os.path.join(path, settings_directory)
            if not os.path.exists(path):
                check_directory_and_make(path)
            return path

    # If path if found / doesn't have necessary write permissions throw error
    # and quit
    message = ('CCP-EM needs to create a settings directory and '
               'the default does not have write permissions:\n'
               '\n{0}'.format(path))
    print(message)
    sys.exit()


def reset_to_default():
    return ccpem_settings_parser().generate_arguments()


def which(program, mode=os.F_OK | os.X_OK, path=None):
    '''
    From shutil.which in python 3.3 back ported for local use here in 2.7

    Given a command, mode, and a PATH string, return the path which
    conforms to the given mode on the PATH, or None if there is no such
    file.

    `mode` defaults to os.F_OK | os.X_OK. `path` defaults to the result
    of os.environ.get("PATH"), or can be overridden with a custom search
    path.

    This implementation has been extended to look for CCP-EM versions of
    programs before searching the PATH. The search order is as follows:

    1. The ccpem settings file is checked for an entry named <program>_bin.

    2. $CCPEM/bin is searched for the program name.
    
    3. On OS X, known installation locations are checked for chimera and coot
    
    4. If `program` includes a directory part, it is used directly and the
       search stops, whether the program is found or not.
    
    5. All directories in `path` or $PATH are searched for the program name.
    '''

    # Check that a given file can be accessed with the correct mode.
    # Additionally check that `file` is not a directory, as on Windows
    # directories pass the os.access check.
    def _access_check(fn, mode):
        return (os.path.exists(fn) and os.access(fn, mode) and
                not os.path.isdir(fn))

    # Check ccpem settings for specified bin location.
    # E.g. if bin is set via alias it will not be located via which function.
    settings_args = get_ccpem_settings()
    settings_bin = program + '_bin'
    if hasattr(settings_args, settings_bin):
        bin_path = getattr(settings_args, settings_bin).value
        if bin_path not in [None, 'None', '']:
            return bin_path

    # Check ccpem bin directory for command name
    # This means we try to pick up ccpem versions of programs in preference
    # to other copies elsewhere (e.g. from CCP4)
    try:
        cmd = os.path.join(os.environ['CCPEM'], 'bin', program)
        if _access_check(cmd, mode):
            return cmd
    except KeyError:
        # We would normally expect the CCPEM environment variable to be set,
        # but if it's not, things should still be fine if the required programs
        # can be found on the PATH, so we ignore the error.
        pass

    # Find specific programs on Mac
    if platform.system() == 'Darwin':
        if program == 'chimera':
            cmd = '/Applications/Chimera.app/Contents/MacOS/chimera'
            if _access_check(cmd, mode=mode):
                return cmd
        # ChimeraX has version number in directory name.  Find and return
        # most recent version from sorted list
        if program == 'ChimeraX':
            files = os.listdir('/Applications/')
            files.sort(reverse=True)
            for f in files:
                if 'ChimeraX' in f:
                    name = f
                    cmd = '/Applications/' + name + '/Contents/MacOS/ChimeraX'
                    if _access_check(cmd, mode=mode):
                        return cmd
        if program == 'coot':
            # Look for ccp4 coot (ccp-em coot would have been found and
            # returned already if it was available).
            cmd = '/Applications/ccp4-7.0/coot.app/Contents/MacOS/coot'
            if _access_check(cmd, mode=mode):
                return cmd

    cmd = program

    # If we're given a cmd with a directory part, look it up directly rather
    # than referring to PATH directories. This includes checking relative to
    # the current directory, e.g. ./script
    if os.path.dirname(cmd):
        if _access_check(cmd, mode):
            return cmd
        return None

    if path is None:
        path = os.environ.get("PATH", os.defpath)
    if not path:
        return None
    path = path.split(os.pathsep)

    if sys.platform == "win32":
        # The current directory takes precedence on Windows.
        if os.curdir not in path:
            path.insert(0, os.curdir)

        # PATHEXT is necessary to check on Windows.
        pathext = os.environ.get("PATHEXT", "").split(os.pathsep)
        # See if the given file matches any of the expected path extensions.
        # This will allow us to short circuit when given "python.exe".
        # If it does match, only test that one, otherwise we have to try
        # others.
        if any([cmd.lower().endswith(ext.lower()) for ext in pathext]):
            files = [cmd]
        else:
            files = [cmd + ext for ext in pathext]
    else:
        # On other platforms you don't have things like PATHEXT to tell you
        # what file suffixes are executable, so just pass on cmd as-is.
        files = [cmd]

    seen = set()
    for directory in path:
        normdir = os.path.normcase(directory)
        if not normdir in seen:
            seen.add(normdir)
            for thefile in files:
                name = os.path.join(directory, thefile)
                if _access_check(name, mode):
                    return name
    return None


def main():
    '''
    Will generate 'ccpem_settings.json' settings file if not already present.
    Use reset_to_default option to reset settings.
    '''
    get_settings_path()
    args = get_ccpem_settings()
    print('CCP-EM Settings')
    print('Current settings')
    print(args.output_args_as_text())
    args.output_args_as_json(args.location.value)
    print('\nPlease edit {0} to change settings.'.format(args.location.value))


if __name__ == '__main__':
    main()
