#
#     Copyright (C) 2014 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import ccpem_core.star_io as star_io_mmdb
from ccpem_core.star_io.ext import star_io_mmdb as star_io_mmdb_ext
from types import StringType, IntType, FloatType


class Test(unittest.TestCase):
    '''
    Unit test for star file i/o (uses MMDB via SWIG bindings)
    '''
    def setUp(self):
        self.test_data = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_data')
        self.test_output = os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            'test_output')
        if not os.path.exists(self.test_output):
            os.makedirs(self.test_output)

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_star_io(self):
        print 'Test star file i/o using mmdb library and python bindings\n'
        # Star object read
        star_file = os.path.join(self.test_data,
                                 'rln_postprocess_run1.star')
        assert os.path.exists(star_file)

        star_test_obj = star_io_mmdb_ext.MMDBStarObj(
            self.test_data + '/rln_postprocess_run1.star',
            'r',
            False)
        # Return code !0 value for read error
        assert star_test_obj.rc == 0
        # Get list of block names
        block_name_list = star_test_obj.GetBlockNameList()
        assert block_name_list[0] == 'general'
        # Get list of field names (aka tags)
        name_list = star_test_obj.GetStructFieldNameList('general')
        assert name_list[0] == '_rlnFinalResolution'
        # Get list of field data (aka fields)
        data_list = star_test_obj.GetStructFieldDataList('general')
        assert data_list[0] == '14.160000'
        # Struc data values
        value = star_test_obj.GetRealStructValue('general',
                                                 '_rlnFinalResolution')
        assert type(value) is FloatType
        value = star_test_obj.GetIntegerStructValue('general',
                                                    '_rlnFinalResolution')
        assert type(value) is IntType
        value = star_test_obj.GetStringStructValue('general',
                                                   '_rlnFinalResolution')
        assert type(value) is StringType
        # Loop values
        # N.B. '' for loop tags with blank loop name (e.g. relion)
        # N.B. if data name == 'data_fsc' lookup 'fsc'
        value = star_test_obj.GetLoopRealValue('fsc',
                                               '',
                                               '_rlnResolution',
                                               30)
        assert type(value) is FloatType
        value = star_test_obj.GetLoopIntegerValue('fsc',
                                                  '',
                                                  '_rlnResolution',
                                                  30)
        assert type(value) is IntType
        value = star_test_obj.GetLoopStringValue('fsc',
                                                 '',
                                                 '_rlnResolution',
                                                 30)
        assert type(value) is StringType
        # Loop vectors
        vec = star_test_obj.GetLoopRealVector('fsc', '_rlnSpectralIndex')
        assert type(vec[0]) is FloatType
        vec = star_test_obj.GetLoopIntVector('fsc', '_rlnSpectralIndex')
        assert type(vec[0]) is IntType
        vec = star_test_obj.GetLoopStringVector('fsc', '_rlnSpectralIndex')
        assert type(vec[0]) is StringType

        # Star object write and put functions
        # Create mmcif object
        star_test_obj = star_io_mmdb_ext.MMDBStarObj(
            self.test_output + '/tst_write.star',
            'w',
            False)
        # Create data block
        star_test_obj.AddCIFData('data_block_1')
        # Write data of different types (string, integer, real)
        # Struct
        star_test_obj.PutDataField('block_struct_eg',
                                   '_category',
                                   'real_tag',
                                   5.1)
        star_test_obj.PutDataField('block_struct_eg',
                                   '_category',
                                   'int_tag',
                                   5)
        star_test_obj.PutDataField('block_struct_eg',
                                   '_category',
                                   'str_tag',
                                   'five')
        # Loop value
        real_value = 5.5
        star_test_obj.PutLoopData('block_loop_eg',
                                  'loop_xx',
                                  'loop_cat',
                                  'loop_tag_real',
                                  0,
                                  real_value)
        return_real_value = star_test_obj.GetLoopRealValue('block_loop_eg',
                                                           'loop_cat',
                                                           'loop_tag_real',
                                                           0)
        assert return_real_value == real_value
        int_value = 5
        star_test_obj.PutLoopData('block_loop_eg',
                                  'loop_xx',
                                  'loop_cat',
                                  'loop_tag_int',
                                  0,
                                  int_value)
        return_int_value = star_test_obj.GetLoopIntegerValue('block_loop_eg',
                                                             'loop_cat',
                                                             'loop_tag_int',
                                                             0)
        assert return_int_value == int_value
        str_value = 'five'
        star_test_obj.PutLoopData('block_loop_eg',
                                  'loop_xx',
                                  'loop_cat',
                                  'loop_tag_string',
                                  0,
                                  str_value)
        return_str_value = star_test_obj.GetLoopStringValue('block_loop_eg',
                                                            'loop_cat',
                                                            'loop_tag_string',
                                                            0)
        assert return_str_value == str_value
        # Loop vector
        test_vec = (1.1, 1.2, 1.3, 1.4, 1.5)
        star_test_obj.PutLoopData('block_loop_eg',
                                  'loop',
                                  'loop_vec_eg',
                                  'vec1',
                                  test_vec)
        return_vec = star_test_obj.GetLoopRealVector('block_loop_eg',
                                                     'vec1',
                                                     'loop')
        assert len(return_vec) == len(test_vec)
        # Overwrite option, true
        star_test_obj.PutLoopData('block_loop_eg',
                                  'loop',
                                  'loop_vec_eg',
                                  'vec1',
                                  test_vec,
                                  True)
        return_vec = star_test_obj.GetLoopRealVector('block_loop_eg',
                                                     'vec1',
                                                     'loop')
        assert len(return_vec) == len(test_vec)
        # Overwrite option, false
        star_test_obj.PutLoopData('block_loop_eg',
                                  'loop',
                                  'loop_vec_eg',
                                  'vec1',
                                  test_vec,
                                  False)
        return_vec = star_test_obj.GetLoopRealVector('block_loop_eg',
                                                     'vec1',
                                                     'loop')
        assert len(return_vec) == 2*len(test_vec)
        # Relion example
        star_test_obj.PutDataField('general',
                                   '',
                                   '_rlnRes',
                                   3.5)
        star_test_obj.PutLoopData('fsc',
                                  '',
                                  '',
                                  '_rlnSpectralIndex #1',
                                  0,
                                  0)
        star_test_obj.PutLoopData('fsc',
                                  '',
                                  '',
                                  '_rlnResolution #2',
                                  0,
                                  0.001001)
        # Delete functions
        star_test_obj.PutDataField('delete_block',
                                   '_struct_cat',
                                   '_struct_field_1',
                                   1.1)
        star_test_obj.PutDataField('delete_block',
                                   '_struct_cat',
                                   '_struct_field_2',
                                   2.2)
        star_test_obj.PutLoopData('delete_block',
                                  '',
                                  '_loop_cat',
                                  '_del_loop_tag',
                                  0,
                                  0.001001)
        star_test_obj.PutLoopData('delete_block',
                                  '',
                                  '_loop_cat',
                                  '_del_loop_tag',
                                  1,
                                  0.002002)
        # N.B. delete field removes data value
        star_test_obj.DeleteStructField('delete_block',
                                        '_struct_cat',
                                        '_struct_field_1')
        star_test_obj.DeleteLoopField('delete_block',
                                      '_loop_cat',
                                      '_del_loop_tag',
                                      1)
        star_test_obj.DeleteCategory('delete_block', '_struct_cat')
        star_test_obj.DeleteCategory('delete_block', '_loop_cat')
        star_test_obj.DeleteMMCIFData('delete_block')
        # Write star file
        star_test_obj.WriteMMCIFFile(self.test_output + '/tst_write2.star')
        assert star_test_obj.rc == 0
        del star_test_obj
        # EMX star dictionary
        emx_star_dict = star_io.emx_star_dict()
        data_block_name = emx_star_dict.GetBlockNameList()[0]
        emx_def_list = emx_star_dict.GetCatNameList(data_block_name)
        emx_dict = {}
        # Python dictionary of dictionaries for emx defs parsed from star defs
        # file
        # Dict level 1 key = group category definition e.g. 'Index'
        #    will return value of dictionary of sub category
        # Dict level 2 key = sub category fields e.g. 'data'
        #    will return value of field e.g. '1'
        for category in emx_def_list:
            struc_field_list = emx_star_dict.GetStructFieldNameList(
                data_block_name,
                category)
            struc_data_list = emx_star_dict.GetStructFieldDataList(
                data_block_name,
                category)
            assert len(struc_field_list) == len(struc_data_list)
            cat_dict = {}
            for n, field in enumerate(struc_field_list):
                cat_dict[field] = struc_data_list[n]
            emx_dict[category] = cat_dict
        # Debug
        if False:
            for key in emx_dict.keys():
                print '\n', key
                print emx_dict[key]

if __name__ == '__main__':
    unittest.main()
