#
#     Copyright (C) 2014 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
import shutil
import tempfile
import pandas as pd
from collections import OrderedDict
import random
from ccpem_core.star_io import py_star
from ccpem_core import test_data


class Test(unittest.TestCase):
    '''
    Unit test for star file i/o (uses MMDB via SWIG bindings)
    '''
    def setUp(self):
        self.test_data = os.path.join(
            test_data.get_test_data_path(),
            'star')
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)


    def test_relion_pipeline(self):
        print 'Test Relion Pipeline Star I/O'
        # Read star file
        star_filename = os.path.join(self.test_data, 'default_pipeline_2-1_tutorial.star')
        star_parser = py_star.MetaData(filename=star_filename)

        # Read general
        pipeline_general = star_parser.metadata['pipeline_general']
        assert pipeline_general['rlnPipeLineJobCounter'] == '33'

        # Read processes
        pipeline_processes = star_parser.metadata['pipeline_processes']['loop_1']
        keys = []
        job = []
        for key, value in pipeline_processes.iteritems():
            keys.append(key)
            job.append(value[2])
        assert keys == ['rlnPipeLineProcessName', 'rlnPipeLineProcessAlias', 'rlnPipeLineProcessType', 'rlnPipeLineProcessStatus']
        assert job == ['CtfFind/job003/', 'None', '2', '2']

        # Read pipeline nodes
        pipeline_nodes = star_parser.metadata['pipeline_nodes']['loop_1']
        keys = []
        node = []
        for key, value in pipeline_nodes.iteritems():
            keys.append(key)
            node.append(value[2])
        assert keys == ['rlnPipeLineNodeName', 'rlnPipeLineNodeType']
        assert node == ['MotionCorr/job002/corrected_micrographs.star', '1']

        # Read pipeline_input_edges
        pipeline_input_edges = star_parser.metadata['pipeline_input_edges']['loop_1']
        keys = []
        edge = []
        for key, value in pipeline_input_edges.iteritems():
            keys.append(key)
            edge.append(value[2])
        assert keys == ['rlnPipeLineEdgeFromNode', 'rlnPipeLineEdgeProcess']
        assert edge == ['CtfFind/job003/micrographs_ctf.star', 'ManualPick/job004/']

        # Read pipeline_output_edges
        pipeline_output_edges = star_parser.metadata['pipeline_output_edges']['loop_1']
        keys = []
        edge = []
        for key, value in pipeline_output_edges.iteritems():
            keys.append(key)
            edge.append(value[2])
        assert keys == ['rlnPipeLineEdgeProcess', 'rlnPipeLineEdgeToNode']
        assert edge == ['MotionCorr/job002/', 'MotionCorr/job002/corrected_micrographs.star']

        # Example to convert to Pandas DataFrame
        output_edges_df = pd.DataFrame.from_dict(pipeline_output_edges)
        assert output_edges_df.size == 166
        assert output_edges_df.shape == (83,2)
        # Select first row, all columns
        print output_edges_df.loc[0,:]

    def test_py_star(self):
        # Read star file
        star_filename = os.path.join(self.test_data,
                                     'rln_postprocess_run1.star')
        assert os.path.exists(star_filename)
        star_parser = py_star.MetaData(filename=star_filename)

        # Check all expected data blocks present
        for block in ['general', 'fsc', 'guinier']:
            assert block in star_parser.metadata.keys()

        # Check expected pair value
        res = '14.160000'
        assert star_parser.metadata['general']['rlnFinalResolution'] == res
   
        # Check muliline string ok
        ml = ('    yyyyyyyyyyy\n'
              '  xxxxxxxxx\n'
              'asdasd\n')
        assert star_parser.metadata['guinier']['multiline'] == ml
   
        # Check loop
        val = star_parser.metadata[
            'guinier']['loop_1']['rlnLogAmplitudesWeighted'][9]
        assert val == '-7.275771'
        val = star_parser.metadata[
            'guinier']['loop_1']['rlnLogAmplitudesSharpened'][-1]
        assert val == '-10.239869'
  
        # Save and reload
        star_filename2 = os.path.join(self.test_output, 'test.star')
        star_parser.write_star(filename=star_filename2)
        star_parser2 = py_star.MetaData(filename=star_filename2)
        assert star_parser2.metadata['guinier']['multiline'] == ml
  
        # Speed test
        del star_filename
        del star_filename2
        star_create = os.path.join(self.test_output, 'create.star')
        md = py_star.MetaData()
        block_name = 'test_block'
        md.add_block(block_name)
        loop = md.add_loop(data_block=block_name)
        rows = 100000
        # 3 float x 100K lines = create, write in ~1.0sec, using numpy as txt
        # is .72sec
        print 'Random'
        dict = OrderedDict({
            'col1': list(range(1, rows+1)),
            'col2': [random.uniform(0, 10) for r in xrange(rows)],
            'col3': [random.uniform(0, 10) for r in xrange(rows)]})
        print 'Set'
        md.set_loop(data_block=block_name,
                    data_loop=loop,
                    loop_dict=dict)
        print 'Write'
        md.write_star(filename=star_create)
        print star_create

        # Re-load
        # Read 100K line read = 0.8 sec to read
        del md
        star_parser = py_star.MetaData(filename=star_create)

if __name__ == '__main__':
    unittest.main()
