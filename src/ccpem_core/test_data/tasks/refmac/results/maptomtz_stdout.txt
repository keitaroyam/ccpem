
________________________________________________________________________________

__________________________ CCP-EM Process | Map to MTZ _________________________

__ Command _____________________________________________________________________
/Applications/ccp4-7.0/bin/refmac5 mapin /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc hklout /Users/tom/ccpem_project/Refmac5_171/starting_map.mtz  << eof
mode sfcalc
source EM MB
reso 4.5
end
eof
________________________________________________________________________________
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
<html> <!-- CCP4 HTML LOGFILE -->
<hr>
<!--SUMMARY_END--></FONT></B>
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
<pre>
 
 ###############################################################
 ###############################################################
 ###############################################################
 ### CCP4 7.0.067: Refmac          version 5.8.0238 : 10/15/18##
 ###############################################################
 User: tom  Run date:  4/ 3/2019 Run time: 13:16:57 


 Please reference: Collaborative Computational Project, Number 4. 2011.
 "Overview of the CCP4 suite and current developments". Acta Cryst. D67, 235-242.
 as well as any specific reference in the program write-up.

<!--SUMMARY_END--></FONT></B>
 $TEXT:Reference1: $$ Primary reference $$ 
   "Overview of refinement procedures within REFMAC5: utilizing data from different sources"
   O.Kovalevskiy, R.A.Nicholls, F.Long, G.N.Murshudov,(2018) 
   Acta Crystallogr. D74, 492-505

 $$
 $SUMMARY :Reference1:  $$ Refmac: $$
 :TEXT:Reference1: $$

 $TEXT:Reference1: $$ Primary reference for EM $$ 
   "Current approaches for the fitting and refinement of atomic models into cryo-EM maps using CCP-EM
   R.A. Nicholls, M. Tykac, O.Kovalevskiy, G.N.Murshudov,(2018) 
   Acta Crystallogr. D74, 215-227

 $$
 $SUMMARY :Reference2:  $$ Refmac: $$
 :TEXT:Reference1: $$

 $TEXT:Reference1: $$ Secondary reference $$ 
   "REFMAC5 for the refinement of macromolecular crystal structures:"
   G.N.Murshudov, P.Skubak, A.A.Lebedev, N.S.Pannu, R.A.Steiner, R.A.Nicholls, M.D.Winn, F.Long and A.A.Vagin,(2011) 
   Acta Crystallogr. D67, 355-367

 $$
 $SUMMARY :Reference3:  $$ Refmac: $$
 :TEXT:Reference1: $$

 $TEXT:Reference2: $$ Secondary reference $$ 
   "Refinement of Macromolecular Structures by the  Maximum-Likelihood Method:"
   G.N. Murshudov, A.A.Vagin and E.J.Dodson,(1997)
   Acta Crystallogr. D53, 240-255
   EU  Validation contract: BIO2CT-92-0524

 $$
 $SUMMARY :Reference4:  $$ Refmac: $$
 :TEXT:Reference2: $$

  Data line--- mode sfcalc
  Data line--- source EM MB
  Data line--- reso 4.5
  Data line--- END
===> Warning: Reflections file has not been defined
===> Warning: Switching to the idealisation mode

    ****                     Input and Default parameters#                      ****


Input coordinate file.  Logical name - XYZIN actual file name  - XYZIN
Output coordinate file. Logical name - XYZOUT actual file name - XYZOUT

  Refinement type                        : Idealisation
  Refinement type                        : NONE
  SF calculation from MAP: /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc


    ****                           Makecif parameters                           ****

Dictionary files for restraints : /Applications/ccp4-7.0/lib/data/monomers/mon*cif
Parameters for new entry and VDW: /Applications/ccp4-7.0/lib/data/monomers/ener_lib.cif
    Apart from amino acids and DNA/RNA all monomers will be checked to see if atom names and connectivity is correct
    Hydrogens will be restored in their riding positions
    Links between monomers will be checked. Only those links present in the coordinate file will be used
    Standard sugar links will be analysed and used
    For new ligands "ideal restraint" values will be taken from the energetic libary ener_lib.cif
    Symmetry related links will be analysed and used
    Cis peptides will be found and used automatically




Monitoring style is "MEDIUM". Complete information will be printed out in the
first and last cycle. In all other cycles minimum information will be printed out
Sigma cutoffs for printing out outliers
If deviation of restraint parameter > alpha*sigma then information will be printed out
Distance outliers      10.000
Angle outliers         10.000
Torsion outliers       10.000
Chiral volume outliers 10.000
Plane outliers         10.000
Non-bonding outliers   10.000
---------------------------------------------------------------

 
   Current auto weighting coefficient =    13.74990    
 

 Logical Name: /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc   Filename: /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc 

File name for input map file on unit  10 : /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc
file size 501024 ; logical name /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc



           Number of columns, rows, sections ...............   50   50   50
           Map mode ........................................    2
           Start and stop points on columns, rows, sections    -7   42    5   54   -7   42
           Grid sampling on x, y, z ........................   50   50   50
           Cell dimensions .................................   75.0000    75.0000    75.0000    90.0000    90.0000    90.0000
           Fast, medium, slow axes .........................    X    Y    Z
           Minimum density .................................     0.00000
           Maximum density .................................     1.48683
           Mean density ....................................     0.02584
           Rms deviation from mean density .................     0.12309
           Space-group .....................................    0
           Number of titles ................................    1


     Labels:                            
  Created by TEMpy on: 2015-12-16


 res max nominal    3.000000      res max used    4.499959    
 mode : SFCA
 Map manupilation mode = M
 reference map

 Logical Name: /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc   Filename: /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc 

File name for input map file on unit  10 : /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc
file size 501024 ; logical name /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4-5A.mrc



           Number of columns, rows, sections ...............   50   50   50
           Map mode ........................................    2
           Start and stop points on columns, rows, sections    -7   42    5   54   -7   42
           Grid sampling on x, y, z ........................   50   50   50
           Cell dimensions .................................   75.0000    75.0000    75.0000    90.0000    90.0000    90.0000
           Fast, medium, slow axes .........................    X    Y    Z
           Minimum density .................................     0.00000
           Maximum density .................................     1.48683
           Mean density ....................................     0.02584
           Rms deviation from mean density .................     0.12309
           Space-group .....................................    0
           Number of titles ................................    1


     Labels:                            
  Created by TEMpy on: 2015-12-16


 Starting (nominal) resolution   0.1111121       4.499959    
 Number of all maps for SF calculation :           1
 Map file number 1 
 /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4
 -5A.mrc
 File number            1 
 /Users/tom/code/devtools/checkout/ccpem/src/ccpem_core/test_data/map/mrc/1ake_4
 -5A.mrc
          -7           5          -7          42          54          42
          -7           5          -7          42          54          42
          50          50          50           1           2           3
          50          50          50          -7           5          -7
          42          54          42
 Start reading           -7           5          -7          42          54
          42
           1
 
 Centre of gravity of the map =    17.0909820798943        30.2424981605501     
   16.6547719860745     
 Covariance of the map:
   40.5525719120672       -3.98157591536039        4.49597878823676     
  -3.98157614318245        34.6476830954972       -4.23890251270950     
   4.49597887981946       -4.23890220403956        48.7723872030351     
 Eigenvalues =   32.4022552491045        39.1025708751497     
   52.4678160863454     
 Eigenvectors:
  0.373840310087854       0.917624816718068       0.134937460686600     
 -0.825645023686425       0.262966103675822       0.499158414913942     
  0.422556170722023      -0.298015979539164       0.855939693275205     
 
 logname : MAPIN1 file name: 
 Number of maps            1
 Calculating Fourier coefficients for the map number            1
 After fsc_sigma  F F
 Print stats of the original data to the file: orig_data_rescut.txt
 Number of resolution bins           21

    ****      Mean(|F|) and other statistics: Without accounting for mask       ****

 $TABLE: Mean(|F|) and other statistics
 $GRAPHS: Mean(|F|), Mean(|F|^2), Mean(|F|^4) vs resolutions: 1,6,7,8
 $$
2sin(th)/l 2sin(th)/l dmin dmax NREF Mn(|F|) Mn(|F|^2) Mn(|F|^4) Mn(A) Mn(B) Mn(A^2) Mn(B^2) cor(A,B) $$
 $$
0.000 0.003  75.00  19.94   125  0.40965E+03  0.37473E+06  0.10576E+13 -0.88188E+01  0.55631E+01  0.17494E+06  0.19979E+06 -0.067
0.003 0.005  19.94  14.36   184  0.15658E+03  0.31389E+05  0.21055E+10  0.26432E-01 -0.12313E+01  0.15319E+05  0.16070E+05 -0.049
0.005 0.007  14.36  11.80   213  0.12696E+03  0.21454E+05  0.10253E+10  0.48490E+01  0.49686E+01  0.11742E+05  0.97118E+04 -0.063
0.007 0.010  11.80  10.25   301  0.11020E+03  0.15827E+05  0.53676E+09 -0.46974E+01 -0.26227E+01  0.79850E+04  0.78418E+04  0.051
0.010 0.012  10.25   9.18   327  0.90410E+02  0.10528E+05  0.22663E+09  0.12983E+01  0.78055E+00  0.55467E+04  0.49814E+04 -0.074
0.012 0.014   9.18   8.39   322  0.77052E+02  0.77020E+04  0.12229E+09 -0.20015E+01  0.13762E+01  0.36922E+04  0.40097E+04 -0.104
0.014 0.017   8.39   7.78   417  0.66689E+02  0.56944E+04  0.69709E+08  0.23016E+00  0.35979E-01  0.28611E+04  0.28333E+04  0.028
0.017 0.019   7.78   7.28   423  0.59020E+02  0.44390E+04  0.38850E+08 -0.13706E+00 -0.94422E-01  0.22356E+04  0.22034E+04 -0.055
0.019 0.021   7.28   6.87   412  0.56932E+02  0.41139E+04  0.33121E+08 -0.47695E+00 -0.36048E+01  0.20207E+04  0.20933E+04 -0.051
0.021 0.024   6.87   6.52   477  0.51618E+02  0.34198E+04  0.23954E+08  0.16149E+01  0.29722E+01  0.17234E+04  0.16964E+04  0.040
0.024 0.026   6.52   6.22   423  0.48133E+02  0.29905E+04  0.19237E+08 -0.78931E+00  0.15029E+00  0.16207E+04  0.13698E+04 -0.013
0.026 0.028   6.22   5.95   568  0.45029E+02  0.26000E+04  0.13665E+08 -0.62478E-01 -0.19437E+01  0.13026E+04  0.12974E+04 -0.079
0.028 0.031   5.95   5.72   525  0.46506E+02  0.27470E+04  0.15395E+08 -0.14404E-01  0.17842E+01  0.12944E+04  0.14526E+04  0.000
0.031 0.033   5.72   5.51   576  0.46837E+02  0.28196E+04  0.16456E+08 -0.42060E+00  0.14769E+01  0.13212E+04  0.14984E+04  0.008
0.033 0.035   5.51   5.33   583  0.45898E+02  0.27326E+04  0.15804E+08 -0.16527E+01 -0.26157E+01  0.13412E+04  0.13913E+04 -0.015
0.035 0.038   5.33   5.16   606  0.47534E+02  0.29074E+04  0.17658E+08  0.28086E+01 -0.41883E+00  0.14715E+04  0.14359E+04  0.054
0.038 0.040   5.16   5.01   516  0.47334E+02  0.27998E+04  0.14785E+08 -0.11643E+00  0.14799E+01  0.14169E+04  0.13829E+04  0.051
0.040 0.042   5.01   4.87   711  0.47333E+02  0.28467E+04  0.15928E+08  0.18567E-01  0.25051E+01  0.13762E+04  0.14705E+04 -0.039
0.042 0.045   4.87   4.74   622  0.48189E+02  0.29774E+04  0.18046E+08 -0.30336E+01 -0.39634E+01  0.14559E+04  0.15215E+04  0.051
0.045 0.047   4.74   4.62   591  0.46594E+02  0.27835E+04  0.15589E+08  0.35539E+01  0.17289E+01  0.14831E+04  0.13004E+04  0.073
0.047 0.049   4.62   4.51   768  0.46873E+02  0.27605E+04  0.14569E+08 -0.64451E+00 -0.62732E+00  0.13821E+04  0.13783E+04 -0.001
 $$
 sharpening things:            0           1  0.0000000E+00  0.0000000E+00

 WRITTEN OUTPUT MTZ FILE 
 Logical Name: /Users/tom/ccpem_project/Refmac5_171/starting_map.mtz   Filename: /Users/tom/ccpem_project/Refmac5_171/starting_map.mtz 

  Crd file =   F
  Time in seconds: CPU =         0.04
             Elapsed =           0.00

    ****           Things for loggraph, R factor and others vs cycle            ****


$TABLE: Rfactor analysis, stats vs cycle  :
$GRAPHS:<Rfactor> vs cycle :N:1,2,3:
:FOM vs cycle :N:1,4:
:-LL vs cycle :N:1,5:
:-LLfree vs cycle :N:1,6:
:Geometry vs cycle:N:1,7,8,9,10,11:
$$
    Ncyc    Rfact    Rfree     FOM      -LL     -LLfree  rmsBOND  zBOND rmsANGL  zANGL rmsCHIRAL $$
$$
 $$
 $TEXT:Result: $$ Final results $$
 $$
 Harvest: NO PNAME_KEYWRD given - no deposit file created
<B><FONT COLOR="#FF0000"><!--SUMMARY_BEGIN-->
 Refmac:  End of Refmac_5.8.0238  
Times: User:       0.0s System:    0.0s Elapsed:     0:00  
</pre>
</html>
<!--SUMMARY_END--></FONT></B>

CCP-EM version dev 1.2.0-42-g243bc59 ___________________________________________

CCP-EM process finished Monday, 04. March 2019 01:16pm _________________________
________________________________________________________________________________
