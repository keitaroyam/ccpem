
data_pipeline_general

_rlnPipeLineJobCounter                      30
 

data_pipeline_processes

loop_ 
_rlnPipeLineProcessName #1 
_rlnPipeLineProcessAlias #2 
_rlnPipeLineProcessType #3 
_rlnPipeLineProcessStatus #4 
Import/job001/ Import/movies/            0            2 
MotionCorr/job002/ MotionCorr/own/            1            2 
CtfFind/job003/ CtfFind/gctf/            2            2 
ManualPick/job004/ ManualPick/illustrate_only/            3            2 
Select/job005/ Select/5mics/            7            2 
AutoPick/job006/ AutoPick/LoG_based/            4            2 
Extract/job007/ Extract/LoG_based/            5            2 
Class2D/job008/ Class2D/LoG_based/            8            2 
Select/job009/ Select/templates4autopick/            7            2 
AutoPick/job010/ AutoPick/optimise_params/            4            2 
AutoPick/job011/ AutoPick/template_based/            4            2 
Extract/job012/ Extract/template_based/            5            2 
Sort/job013/ Sort/after_autopick/            6            2 
Select/job014/ Select/after_sort/            7            2 
Class2D/job015/ Class2D/after_sorting/            8            2 
Select/job016/ Select/class2d_aftersort/            7            2 
InitialModel/job017/ InitialModel/symC1/           18            2 
Class3D/job018/ Class3D/first_exhaustive/            9            2 
Select/job019/ Select/class3d_first_exhaustive/            7            2 
Extract/job020/ Extract/best3dclass_bigbox/            5            2 
Refine3D/job021/ Refine3D/first3dref/           10            2 
MaskCreate/job022/ MaskCreate/first3dref/           12            2 
PostProcess/job023/ PostProcess/first3dref/           15            2 
CtfRefine/job024/       None           21            2 
Polish/job025/ Polish/train/           20            2 
Polish/job026/ Polish/polish/           20            2 
Refine3D/job027/ Refine3D/polished/           10            2 
PostProcess/job028/ PostProcess/polished/           15            2 
LocalRes/job029/ LocalRes/polished/           16            2 
 

data_pipeline_nodes

loop_ 
_rlnPipeLineNodeName #1 
_rlnPipeLineNodeType #2 
Import/job001/movies.star            0 
MotionCorr/job002/corrected_micrographs.star            1 
MotionCorr/job002/logfile.pdf           13 
CtfFind/job003/micrographs_ctf.star            1 
CtfFind/job003/logfile.pdf           13 
ManualPick/job004/coords_suffix_manualpick.star            2 
ManualPick/job004/micrographs_selected.star            1 
Select/job005/micrographs_selected.star            1 
AutoPick/job006/coords_suffix_autopick.star            2 
AutoPick/job006/logfile.pdf           13 
Extract/job007/particles.star            3 
Class2D/job008/run_it025_data.star            3 
Class2D/job008/run_it025_model.star            8 
Select/job009/particles.star            3 
Select/job009/class_averages.star            5 
AutoPick/job010/coords_suffix_autopick.star            2 
AutoPick/job010/logfile.pdf           13 
AutoPick/job011/coords_suffix_autopick.star            2 
AutoPick/job011/logfile.pdf           13 
Extract/job012/particles.star            3 
Sort/job013/particles_sort.star            3 
Select/job014/particles.star            3 
Class2D/job015/run_it025_data.star            3 
Class2D/job015/run_it025_model.star            8 
Select/job016/particles.star            3 
Select/job016/class_averages.star            5 
InitialModel/job017/run_it150_data.star            3 
InitialModel/job017/run_it150_model.star            8 
InitialModel/job017/run_it150_class001.mrc            6 
InitialModel/job017/run_it150_class001_symD2.mrc            6 
Class3D/job018/run_it025_data.star            3 
Class3D/job018/run_it025_model.star            8 
Class3D/job018/run_it025_class001.mrc            6 
Class3D/job018/run_it025_class002.mrc            6 
Class3D/job018/run_it025_class003.mrc            6 
Class3D/job018/run_it025_class004.mrc            6 
Select/job019/particles.star            3 
Class3D/job018/run_it025_class001_box256.mrc            6 
Extract/job020/particles.star            3 
Extract/job020/coords_suffix_extract.star            2 
Refine3D/job021/run_data.star            3 
Refine3D/job021/run_half1_class001_unfil.mrc           10 
Refine3D/job021/run_class001.mrc            6 
MaskCreate/job022/mask.mrc            7 
PostProcess/job023/postprocess.mrc           11 
PostProcess/job023/postprocess_masked.mrc           11 
PostProcess/job023/logfile.pdf           13 
PostProcess/job023/postprocess.star           14 
CtfRefine/job024/logfile.pdf           13 
CtfRefine/job024/particles_ctf_refine.star            3 
Polish/job025/opt_params.txt           15 
Polish/job026/logfile.pdf           13 
Polish/job026/shiny.star            3 
Refine3D/job027/run_data.star            3 
Refine3D/job027/run_half1_class001_unfil.mrc           10 
Refine3D/job027/run_class001.mrc            6 
PostProcess/job028/postprocess.mrc           11 
PostProcess/job028/postprocess_masked.mrc           11 
PostProcess/job028/logfile.pdf           13 
PostProcess/job028/postprocess.star           14 
LocalRes/job029/relion_locres_filtered.mrc           11 
LocalRes/job029/relion_locres.mrc           12 
LocalRes/job029/flowchart.pdf           13 
Sort/job013/logfile.pdf           13 
 

data_pipeline_input_edges

loop_ 
_rlnPipeLineEdgeFromNode #1 
_rlnPipeLineEdgeProcess #2 
Import/job001/movies.star MotionCorr/job002/ 
MotionCorr/job002/corrected_micrographs.star CtfFind/job003/ 
CtfFind/job003/micrographs_ctf.star ManualPick/job004/ 
ManualPick/job004/coords_suffix_manualpick.star Select/job005/ 
Select/job005/micrographs_selected.star AutoPick/job006/ 
CtfFind/job003/micrographs_ctf.star Extract/job007/ 
AutoPick/job006/coords_suffix_autopick.star Extract/job007/ 
Extract/job007/particles.star Class2D/job008/ 
Class2D/job008/run_it025_model.star Select/job009/ 
Select/job005/micrographs_selected.star AutoPick/job010/ 
Select/job009/class_averages.star AutoPick/job010/ 
CtfFind/job003/micrographs_ctf.star AutoPick/job011/ 
Select/job009/class_averages.star AutoPick/job011/ 
CtfFind/job003/micrographs_ctf.star Extract/job012/ 
AutoPick/job011/coords_suffix_autopick.star Extract/job012/ 
Extract/job012/particles.star Sort/job013/ 
Select/job009/class_averages.star Sort/job013/ 
Sort/job013/particles_sort.star Select/job014/ 
Select/job014/particles.star Class2D/job015/ 
Class2D/job015/run_it025_model.star Select/job016/ 
Select/job016/particles.star InitialModel/job017/ 
Select/job016/particles.star Class3D/job018/ 
InitialModel/job017/run_it150_class001_symD2.mrc Class3D/job018/ 
Class3D/job018/run_it025_model.star Select/job019/ 
CtfFind/job003/micrographs_ctf.star Extract/job020/ 
Select/job019/particles.star Extract/job020/ 
Extract/job020/particles.star Refine3D/job021/ 
Class3D/job018/run_it025_class001_box256.mrc Refine3D/job021/ 
Refine3D/job021/run_class001.mrc MaskCreate/job022/ 
MaskCreate/job022/mask.mrc PostProcess/job023/ 
Refine3D/job021/run_half1_class001_unfil.mrc PostProcess/job023/ 
Refine3D/job021/run_data.star CtfRefine/job024/ 
CtfRefine/job024/particles_ctf_refine.star Polish/job025/ 
CtfRefine/job024/particles_ctf_refine.star Polish/job026/ 
Polish/job026/shiny.star Refine3D/job027/ 
Refine3D/job021/run_class001.mrc Refine3D/job027/ 
MaskCreate/job022/mask.mrc Refine3D/job027/ 
MaskCreate/job022/mask.mrc PostProcess/job028/ 
Refine3D/job027/run_half1_class001_unfil.mrc PostProcess/job028/ 
Refine3D/job027/run_half1_class001_unfil.mrc LocalRes/job029/ 
 

data_pipeline_output_edges

loop_ 
_rlnPipeLineEdgeProcess #1 
_rlnPipeLineEdgeToNode #2 
Import/job001/ Import/job001/movies.star 
MotionCorr/job002/ MotionCorr/job002/corrected_micrographs.star 
MotionCorr/job002/ MotionCorr/job002/logfile.pdf 
CtfFind/job003/ CtfFind/job003/micrographs_ctf.star 
CtfFind/job003/ CtfFind/job003/logfile.pdf 
ManualPick/job004/ ManualPick/job004/coords_suffix_manualpick.star 
ManualPick/job004/ ManualPick/job004/micrographs_selected.star 
Select/job005/ Select/job005/micrographs_selected.star 
AutoPick/job006/ AutoPick/job006/coords_suffix_autopick.star 
AutoPick/job006/ AutoPick/job006/logfile.pdf 
Extract/job007/ Extract/job007/particles.star 
Class2D/job008/ Class2D/job008/run_it025_data.star 
Class2D/job008/ Class2D/job008/run_it025_model.star 
Select/job009/ Select/job009/particles.star 
Select/job009/ Select/job009/class_averages.star 
AutoPick/job010/ AutoPick/job010/coords_suffix_autopick.star 
AutoPick/job010/ AutoPick/job010/logfile.pdf 
AutoPick/job011/ AutoPick/job011/coords_suffix_autopick.star 
AutoPick/job011/ AutoPick/job011/logfile.pdf 
Extract/job012/ Extract/job012/particles.star 
Sort/job013/ Sort/job013/particles_sort.star 
Sort/job013/ Sort/job013/logfile.pdf 
Select/job014/ Select/job014/particles.star 
Class2D/job015/ Class2D/job015/run_it025_data.star 
Class2D/job015/ Class2D/job015/run_it025_model.star 
Select/job016/ Select/job016/particles.star 
Select/job016/ Select/job016/class_averages.star 
InitialModel/job017/ InitialModel/job017/run_it150_data.star 
InitialModel/job017/ InitialModel/job017/run_it150_model.star 
InitialModel/job017/ InitialModel/job017/run_it150_class001.mrc 
InitialModel/job017/ InitialModel/job017/run_it150_class001_symD2.mrc 
Class3D/job018/ Class3D/job018/run_it025_data.star 
Class3D/job018/ Class3D/job018/run_it025_model.star 
Class3D/job018/ Class3D/job018/run_it025_class001.mrc 
Class3D/job018/ Class3D/job018/run_it025_class002.mrc 
Class3D/job018/ Class3D/job018/run_it025_class003.mrc 
Class3D/job018/ Class3D/job018/run_it025_class004.mrc 
Class3D/job018/ Class3D/job018/run_it025_class001_box256.mrc 
Select/job019/ Select/job019/particles.star 
Extract/job020/ Extract/job020/particles.star 
Extract/job020/ Extract/job020/coords_suffix_extract.star 
Refine3D/job021/ Refine3D/job021/run_data.star 
Refine3D/job021/ Refine3D/job021/run_half1_class001_unfil.mrc 
Refine3D/job021/ Refine3D/job021/run_class001.mrc 
MaskCreate/job022/ MaskCreate/job022/mask.mrc 
PostProcess/job023/ PostProcess/job023/postprocess.mrc 
PostProcess/job023/ PostProcess/job023/postprocess_masked.mrc 
PostProcess/job023/ PostProcess/job023/logfile.pdf 
PostProcess/job023/ PostProcess/job023/postprocess.star 
CtfRefine/job024/ CtfRefine/job024/logfile.pdf 
CtfRefine/job024/ CtfRefine/job024/particles_ctf_refine.star 
Polish/job025/ Polish/job025/opt_params.txt 
Polish/job026/ Polish/job026/logfile.pdf 
Polish/job026/ Polish/job026/shiny.star 
Refine3D/job027/ Refine3D/job027/run_data.star 
Refine3D/job027/ Refine3D/job027/run_half1_class001_unfil.mrc 
Refine3D/job027/ Refine3D/job027/run_class001.mrc 
PostProcess/job028/ PostProcess/job028/postprocess.mrc 
PostProcess/job028/ PostProcess/job028/postprocess_masked.mrc 
PostProcess/job028/ PostProcess/job028/logfile.pdf 
PostProcess/job028/ PostProcess/job028/postprocess.star 
LocalRes/job029/ LocalRes/job029/relion_locres_filtered.mrc 
LocalRes/job029/ LocalRes/job029/relion_locres.mrc 
LocalRes/job029/ LocalRes/job029/flowchart.pdf 
 
