#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import os
from ccpem_core.test_data import get_test_data_path

class TestTestData(unittest.TestCase):
    '''
    Unit test for test data.
    '''

    def test_path(self):
        path = os.path.dirname(__file__)
        assert path == get_test_data_path()

if __name__ == '__main__':
    unittest.main()
