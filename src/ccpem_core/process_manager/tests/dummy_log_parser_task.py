#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from __future__ import print_function

import os
import sys

def main():
    """
    Dummy task which copies the contents of a given file to standard output.
    
    This is intended to test the use of log parsers with the CCPEM process
    manager.
    """
    input_name = os.path.join(os.path.dirname(__file__),
                              'example_log_file.txt')
    if len(sys.argv) > 1:
        input_name = sys.argv[1]
    with open(input_name) as f:
        for line in f:
            print(line, end='') # lines from input file already end with '\n'

if __name__ == '__main__':
    main()
