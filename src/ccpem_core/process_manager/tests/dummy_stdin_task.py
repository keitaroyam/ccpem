#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
from __future__ import print_function

import sys

def main():
    """
    Dummy task which reads all input from stdin and writes it to a log file.
    
    Note this task will hang waiting for input if nothing is sent to stdin.
    """
    log_name = 'stdin_string_test.log'
    if len(sys.argv) > 1:
        log_name = sys.argv[1]
    with open(log_name, 'w') as f:
        stdin = sys.stdin.read()
        f.write(stdin)

if __name__ == '__main__':
    main()