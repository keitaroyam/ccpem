#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Created on 13 Feb 2015

@author: tom
'''
import numpy as np
import types
def main():
    
#     f = types.FloatType
#     print f
#     meta_list = np.array((1.1, ),
#                      dtype=[('values', f)
#                             ])
#     
#     s = types.StringType   
#     print s
#     meta_list = np.array(('1.1asdasdasdasd',),
#                      dtype=[('values', np.dtype('S5'))
#                             ])
#     print meta_list
    
#     studentgrades = np.zeros(2, dtype = [('First Name','a20'),
#                                          ('Last Name', 'a20'),
#                                          ('Score', 'f4')]
#                              )
#     print studentgrades
#     studentgrades[0] = ('Dave', 'Thomas', 89.4)
#     studentgrades[1] = ('Tasha', 'Homer', 76.6)
#     
#     studentgrades_new = np.zeros(3, dtype = [('First Name','a20'),
#                                          ('Last Name', 'a20'),
#                                          ('Score', 'f4')]
#                              )
#     
#     studentgrades_new[0] = ('Cool', 'Python', 100)
#     studentgrades_new[1] = ('Stack', 'Overflow', 95.32)
#     studentgrades_new[2] = ('Py', 'Py', 75)
#     print studentgrades
#     print studentgrades_new
#     studentgrades = np.concatenate((studentgrades, studentgrades_new))
#     print studentgrades
#     print studentgrades.shape
    
    # How to add values to array
    
    ids = (1,2,3,4)
    scores = (0.1, 1.2, 1.4, 4.3)
    
    mtable = np.zeros(len(ids), 
                      dtype = [('id', types.IntType),
                      ('score', types.FloatType)]
                      )
    for n, id in enumerate(ids):
        mtable[n] = (id, scores[n])
    print mtable
    
    i = np.array(ids)
    s = np.array(scores)
    
    i_s = np.column_stack((i, s))
    
    print i_s
#     print values
#     print values.transpose()
    # How to add column
    mtable = np.array((s.transpose()),
                      dtype = [('score', types.FloatType)]
                      )

    # How to access values
    
    
    
    # How to add multiple cols
    mtable = np.array((i_s),
                      dtype = [('id', types.IntType),
                               ('score', types.FloatType),
                               ]
                      )
    
    print mtable
    print mtable['score']
    assert 0

    # 1d array example
    # 1d array should be a list of 
    #        (instance_name, ccpem_label, value)
    # Instance names should be unique
    dt = np.dtype([('str25', np.str_, 25),
                    ('str250', np.str_, 250),
                    ('str', np.string_),
                    ('float', np.float16),
                    ('int', np.int8),
                    ('complex', np.complex64)
                    ])
    
    meta_list = np.array(('entry1', 'ccpemlabelx', 1.1),
                         dtype=[('names', dt['str25']),
                                ('labels', dt['str25']),
                                ('values', dt['float'])
                                ])
    print meta_list
    print meta_list['names']
    print meta_list['labels']
    print meta_list['values']
    meta_list = np.array(meta_list)
    meta_list2 = np.array(('entry2', 'ccpemlabelx', 2.3),
                         dtype=[('names', dt['str25']),
                                ('labels', dt['str25']),
                                ('values', dt['float'])
                                ])
 
    meta_list3 = np.array(('entry3', 'ccpemlabelx', 5.1),
                         dtype=[('names', dt['str25']),
                                ('labels', dt['str25']),
                                ('values', dt['float'])
                                ])
 
    meta_list4 = np.array(('entry4', 'ccpemlabelx', 50.1),
                         dtype=[('names', dt['str25']),
                                ('labels', dt['str25']),
                                ('values', dt['int'])
                                ])
     
     
    for mlist in (meta_list2, meta_list3, meta_list4):
        if mlist['names'] in meta_list['names']:
            i = np.where(meta_list['names']==mlist['names'])
            print '\nDelete'
            print meta_list
            print 'index : ', i
            meta_list = np.array([np.delete(meta_list, i)])
            print meta_list
        meta_list = np.column_stack((meta_list, mlist))

    print '\nFinal'
    print meta_list
    print meta_list.shape
    print meta_list['names']
    print meta_list['labels']
    print meta_list['values']
    
    
if __name__ == '__main__':
    main()