#include <iostream>
#include <vector>
#include <algorithm>
#include <cstdio>
using namespace std;

// Compute factorial of n
int Fact(int n) {
  if (n <= 1) return 1;
  else return n*Fact(n-1);
}

vector<vector<double> > ReturnVector2D() {
  printf("\nReturn 2d std vector double array\n");
  // Important to declare vector with specified size & values else leads to memory errors with SWIG!
  int x = 2;
  int y = 3;
  vector<vector<double> > array2d(x, vector<double>(y,0.0));
  array2d[0][0] = 4.4;
  array2d[1][0] = 2.2;
  return array2d;
}

vector<vector<vector<double> > > ReturnVector3D() {
  printf("\nReturn 3d std vector double array\n");
  // Important to declare vector with specified size & values else leads to memory errors with SWIG!
  int x = 2;
  int y = 3;
  int z = 4;

  vector<vector<vector<double> > > array3d(x, vector<vector<double> >(y, vector<double>(z,0.0) ) );
  array3d[0][0][0] = 1.1;
  array3d[0][1][0] = 2.2;
  array3d[0][2][3] = 3.3;
  return array3d;
}

// Testing vector funcions
class VectorClass {
 public:
  VectorClass(int test_int);
  
  void InitiateVectors() {
    cout << "\nInitiate vectors [c++]" << endl;
    vector<int> test_v(4, 0);
  }
  void PrintFunc() {
    cout << "\nTesting vector class [c++]" << endl;
  }
  void PrintFileName() {
    cout << "  File_name : " << endl;
  }
  void VectorSetup() {
    PrintFunc();
  }
  int test_int;
  int other_int;
};

VectorClass::VectorClass (int test_int_) {
  test_int = test_int_;
  PrintFunc();
  InitiateVectors();
}

vector<int> VectorInitFunc(int n) {
  vector<int> v(n,0);
  cout << "\nInitiate vector of size n" << endl;
  cout << "  Vector size : " << v.size() << endl;
  cout << "  Set each element to n" << endl;
  for(unsigned int i = 0; i < v.size(); i++) {
    v[i] = v[i] = i;
  }
  return v;
}

vector<int> VectorReturn(vector<int> v) {
  cout << "  Vector size : " << v.size() << endl;
  cout << "  Multiple each element by 3" << endl;
  for(unsigned int i = 0; i < v.size(); i++) {
    v[i] = v[i] * 3;
  }
  return v;
}

void VectorPass(std::vector<int> VectorTest) {
  cout << "  Vector size : " << VectorTest.size() << endl;
  for (unsigned int i = 0; i < VectorTest.size(); i++) {
      cout << "    n_vector = " << VectorTest[i] << endl;
  }
}

int VectorTest(int n) {
  vector<int> vectorTest(n,0);
  cout << "  Vector size : " << vectorTest.size() << endl;
  cout << "  Multiple each element by 2" << endl;
  for (unsigned int i = 0; i < vectorTest.size(); i++) {
    vectorTest[i] = i * 2;
    cout << "     n_vector = " << vectorTest[i] << endl;
  }

  cout << "\nPass vector to anther c++ function" << endl;
  VectorPass(vectorTest);

  cout << "\nInitiate new vector function" << endl;
  vector<int> newvec = VectorReturn(vectorTest);

  cout << "\nLoop through new and old vectors" << endl;
  for (unsigned int i = 0; i < newvec.size(); i++) {
    cout << "  n_vector : " << i << endl;
    cout << "    Old = " << vectorTest[i] << endl;
    cout << "    New = " << newvec[i] << endl;
  }
  return 0;
}
// End testing vector funcions

// Array testing functions w/ numpy and swig
void ArrayInPlaceMutliply(double *array_dbl, int n){
  for (int i = 0; i < n; i++) {
    array_dbl[i] = array_dbl[i] * 2.0;
    cout << array_dbl[i] << " TEST "<< endl;
  }
};

class NumpyArrayClass {
 public:
  void PrintTest() {cout << "print" << endl;}
  void ArrayDbl (double *array_dbl, int n) {
    for (int i = 0; i <3; i++) {
    cout << "  tst : " << array_dbl[i] << endl;
    array_dbl[i] = array_dbl[i] + 5.0;
    }
  }
};

class DummyClassConstructor {
  char const* file_name;
  int w;
  int h;
 public:
  DummyClassConstructor (char const* file_name, int w = 1, int h = 5);
  int area () {
    return w * h;
  }
  void PrintTest() {cout << "print from c" << endl;
  }
  void PrintVariables() {
    cout << "print variables from c | w : " << w << endl;
    cout << "print variables from c | h : " << h << endl;
  }
};
// Constructor must be after class
DummyClassConstructor::DummyClassConstructor (char const* file_name_, int w_, int h_) {
  file_name = file_name_;
  w = w_;
  h = h_;
}

class ArrayClassConstructor {
  char const* file_name;
  double* array1_dbl;
  int array1_len;
  double* array2_dbl;
  int array2_len;
 public:
  ArrayClassConstructor (
                         char const* file_name,
                         double* array1_dbl, int array1_len,
                         double* array2_dbl, int array2_len);
  void PrintFileName() {
    cout << "  File_name : " << file_name << endl;
  }
  void PrintArrayInfo(char const* message, double* array, int len) {
    cout << "\n\n  Message : " << message << endl;
    cout << "    Array length : " << len << endl;
    for (int i = 0; i < len; i++) {
      cout << "    Array element | " << i << " : " << array[i] << endl;
    }
  }
  void ArrayTest() {
    cout << " Len array : " << array2_len << endl;
    PrintFileName();
    char const* message = "Array 1";
    PrintArrayInfo(message, array1_dbl, array1_len);
    message = "Array 2";
    PrintArrayInfo(message, array2_dbl, array2_len);
  }
  void MultipyArray1() {
    double n = 1000;
    cout << "\n\nMultiply array 1 by : " << n << endl;
    for (int i = 0; i < array1_len; i++) {
      array1_dbl[i] = array1_dbl[i] * n;
    }
  }
};

// Constructor [required for SWIG] must be after class
ArrayClassConstructor::ArrayClassConstructor (
                                              char const* file_name_,
                                              double* array1_dbl_, int array1_len_,
                                              double* array2_dbl_, int array2_len_){
  file_name = file_name_,
  array1_dbl = array1_dbl_;
  array1_len = array1_len_;
  array2_dbl = array2_dbl_;
  array2_len = array2_len_;
}

void ModArrays(int len1, int len2, double* array1, double* array2){
  cout << "ModArrays" << endl;
  for(int i = 0; i < len1; i++) {
    cout << "\ni = " << i << endl;
    cout << "  Array 1 : " << array1[i] << endl;
    cout << "  Array 2 : " << array2[i] << endl;
    array1[i] = array1[i] + 10.0;
    array2[i] = array2[i] + 100.0;
    cout << "  Array 1 : " << array1[i] << endl;
    cout << "  Array 2 : " << array2[i] << endl;
  }
}

class ArrayClass {
 public:
  ArrayClass() {
    pub = 1;
    cout << "Array class" << endl;
    cout << "int public  : " << pub << endl;
    for (int i = 1; i <= 10; i++) {
      array_return[i] = 5.5;
      cout << i << "  " << array_return[i] << endl;
    }
  }
  double array_return[10];
  int pub;
  void ArrayDouble(double *array_double, int n){};
};

void print_array(double x[10]) {
   int i;
   for (i = 0; i < 10; i++) {
      printf("[%d] = %g\n", i, x[i]);
   }
}

void print_int_array(int x[10]) {
   int i;
   for (i = 0; i < 10; i++) {
     cout << i << "  " << x[i] << endl;
   }
}

// Print message sent from python
void PrintTest(char const* message) {
  cout << "Test print from C++" << endl;
  cout << "  String passed from python : " << message << endl;
}

double MyVariable = 3.0;

int MyMod(int n, int m) {
  return(n % m);
}

float *ReturnArrayFloat3() {
  std::cout << "\nTest return float array" << std::endl;
  size_t s = 3;
  float *ret = new float[s];
  for (size_t n = 0; n < s; n++) {
    ret[n] = n * 2.0;
    std::cout << "\n" << n << " : " << ret[n] << std::endl;
  }
  return ret;
}

int *ReturnArrayInt3() {
  std::cout << "\nTest return int array" << std::endl;
  size_t s = 3;
  int *ret = new int[s];
  for (size_t n = 0; n < s; n++) {
    ret[n] = n * 2;
    std::cout << "\n" << n << " : " << ret[n] << std::endl;
  }
  return ret;
}

int *ReturnArrayInt3Tuple() {
  std::cout << "\nTest return int array" << std::endl;
  size_t s = 3;
  int *ret = new int[s];
  for (size_t n = 0; n < s; n++) {
    ret[n] = n * 2;
    std::cout << "\n" << n << " : " << ret[n] << std::endl;
  }
  return ret;
}

// Input 1D, 2D, 3D array examples
void OneDArrayTest(std::vector<float> data) {
  std::cout << "\n\nC++ 1D array : " << std::endl;
  for (unsigned int n = 0; n < data.size(); n++) {
    std::cout << "\n" << data[n];
  }
}

void TwoDArrayTest(std::vector<std::vector<float> > data) {
  std::cout << "\n\nC++ 2D array : " << std::endl;
  std::vector< std::vector<float> >::const_iterator row;
  std::vector<float>::const_iterator col;
  for (row = data.begin(); row != data.end(); ++row) {
    for (col = row->begin(); col != row->end(); ++col) {
      std::cout << "\n" << *col;
    }
  }
}

void ThreeDArrayTest(std::vector<std::vector<std::vector<float> > > data) {
  std::cout << "\n\nC++ 3D array : " << std::endl;
  std::vector< std::vector<std::vector<float> > >::const_iterator x;
  std::vector< std::vector<float> >::const_iterator y;
  std::vector<float>::const_iterator z;
  for (x = data.begin(); x != data.end(); ++x) {
    for (y = x->begin(); y != x->end(); ++y) {
      for (z = y->begin(); z != y->end(); ++z) {
        std::cout << "\n" << *z;
      }
    }
  }
}
