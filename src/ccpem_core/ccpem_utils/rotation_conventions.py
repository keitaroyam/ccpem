"""
   CCPEM
   Convert between rotation conventions
   including rotation matrix, Euler angles and quaternions

   (C) Martyn Winn, STFC, Sept 2013
"""

from numpy import *

class Rotations:

   def __init__(self):

      euler_conventions = { 1 : 'ZYZ',
                            2 : 'ZXZ'}
      angle_defs = { 1 : 'clockwise',
                    2 : 'anticlockwise'}

   def degToMatrix(self,euler_angles,euler_convention=1,angle_def=1):

      matrix = zeros( (3,3), dtype=float64 )

      if len(euler_angles) != 3:
         print "Ugg!"
         return None

      if angle_def==1:
        alpha = radians(euler_angles[0])
        beta = radians(euler_angles[1])
        gamma = radians(euler_angles[2])
      elif angle_def==2:
        alpha = - radians(euler_angles[0])
        beta = - radians(euler_angles[1])
        gamma = - radians(euler_angles[2])

      ca = cos(alpha)
      sa = sin(alpha)
      cb = cos(beta)
      sb = sin(beta)
      cg = cos(gamma)
      sg = sin(gamma)

      if euler_convention==1:
         matrix[0][0] = ca*cb*cg - sa*sg
         matrix[0][1] = sa*cb*cg + ca*sg
         matrix[0][2] = -sb*cg
         matrix[1][0] = -ca*cb*sg - sa*cg
         matrix[1][1] = -sa*cb*sg + ca*cg
         matrix[1][2] = sb*sg
         matrix[2][0] = ca*sb
         matrix[2][1] = sa*sb
         matrix[2][2] = cb
      elif euler_convention==2:
         matrix[0][0] = -sa*cb*sg + ca*cg
         matrix[0][1] = sa*cb*cg + ca*sg
         matrix[0][2] = sa*sb
         matrix[1][0] = -ca*cb*sg - sa*cg
         matrix[1][1] = ca*cb*cg -sa*sg
         matrix[1][2] = ca*sb
         matrix[2][0] = sb*sg
         matrix[2][1] = -sb*cg
         matrix[2][2] = cb

      return matrix

   def spiderToMatrix(self,phi,theta,psi):

      # Spider Euler angles applied as:
      # CP FROM MRC
      # foo.mrc
      # _1
      # Y/N
      # ROT
      # _1
      # foo_out
      # PHI, THETA, PSI
      # F
      # A

      # order of psi,theta,phi reversed because spider defines
      # angles w.r.t. fixed external axes.
      angles = array([psi,theta,phi])
      return self.degToMatrix(angles,1,2)

   def bsoftToMatrix(self,phi,theta,psi):

      ### these not certain yet!
      angles = array([psi,theta,phi])
      return self.degToMatrix(angles,1,2)

   def matrixToDeg(self,rotation_matrix,euler_convention=1,angle_def=1):

      angles = zeros( (3), dtype=float64 )

      if euler_convention==1:
        # from rotmat.f ANG011

        # get second angle (beta)
        cb = rotation_matrix[2][2]
        if cb < -1.0: cb = -1.0
        if cb >  1.0: cb = 1.0
        angles[1] = degrees(arccos(cb))
        # need to treat beta = 0 or pi as special cases

        # get first angle (alpha)
        t1 = rotation_matrix[2][1]
        t2 = rotation_matrix[2][0]
        # trap for t1 and t2 small
        angles[0] = degrees(arctan2(t1,t2))

        # get third angle (gamma)
        t1 = rotation_matrix[1][2]
        t2 = -rotation_matrix[0][2]
        # trap for t1 and t2 small
        angles[2] = degrees(arctan2(t1,t2))

      elif euler_convention==2:

        # get second angle (beta)
        cb = rotation_matrix[2][2]
        if cb < -1.0: cb = -1.0
        if cb >  1.0: cb = 1.0
        angles[1] = degrees(arccos(cb))
        # need to treat beta = 0 or pi as special cases

        # get first angle (alpha)
        t1 = rotation_matrix[0][2]
        t2 = rotation_matrix[1][2]
        # trap for t1 and t2 small
        angles[0] = degrees(arctan2(t1,t2))

        # get third angle (gamma)
        t1 = rotation_matrix[2][0]
        t2 = -rotation_matrix[2][1]
        # trap for t1 and t2 small
        angles[2] = degrees(arctan2(t1,t2))

      # Here we combine the angle convention with the equivalence
      # (ALPHA,BETA,GAMMA) = (PI+ALPHA,-BETA,PI+GAMMA)
      # to ensure beta is in the range 0,pi
      if angle_def==2:
        angles[0] =  180.0 - angles[0]
        angles[2] =  180.0 - angles[2]

      return angles

   def matrixToSpider(self,rotation_matrix):

      angles = self.matrixToDeg(rotation_matrix,1,2)

      # order of psi,theta,phi reversed because spider defines
      # angles w.r.t. fixed external axes.
      return (angles[2],angles[1],angles[0])

   def matrixToBsoft(self,rotation_matrix):

      ### these not certain yet!
      angles = self.matrixToDeg(rotation_matrix,1,2)
      return (angles[2],angles[1],angles[0])

if __name__ == '__main__':

    set_printoptions(precision=3)
    rot = Rotations()

    print 'Default convention (ZYZ)'
    angles = [23.4,34.5,45.6]
    print angles
    matrix = rot.degToMatrix(angles)
    print matrix
    angles2 = rot.matrixToDeg(matrix)
    print angles2

    print
    print 'ZXZ convention'
    angles = [23.4,34.5,45.6]
    print angles
    matrix = rot.degToMatrix(angles,2)
    print matrix
    angles2 = rot.matrixToDeg(matrix,2)
    print angles2

    print
    print 'Spider convention'
    (phi,theta,psi) = (45.6,34.5,23.4)
    print phi,theta,psi
    matrix = rot.spiderToMatrix(phi,theta,psi)
    print matrix
    angles2 = rot.matrixToSpider(matrix)
    print angles2[0],angles2[1],angles2[2]

