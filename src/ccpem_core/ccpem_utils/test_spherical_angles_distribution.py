#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import unittest
import sys
import os
import shutil
import tempfile
from ccpem_core.ccpem_utils.spherical_angles_distribution import angle_distn


class Test(unittest.TestCase):
    '''
    Test distribution of angles on a sphere (in-house angles generation module
    for dockem and shapem)
    '''
    def setUp(self):
        '''
        Always run at start of test, e.g. for creating directory to store
        temporary test data producing during unit test.
        '''
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)

    def test_distribution_completed(self):
        '''
        Tests if the full range of theta is spanned
        '''
        # Prints test function name
        print '\n', sys._getframe().f_code.co_name
        dict_angles = angle_distn(20, output_path=self.test_output)
        self.assertEqual(len(dict_angles)+1, 93)

    def test_values(self):
        '''
        Tests if the correct values of theta and phi are calculated
        '''
        # Prints test function name
        print '\n', sys._getframe().f_code.co_name
        dict_angles = angle_distn(20, output_path=self.test_output)
        # One of the combination of values of psi, theta and phi calculated
        # for theta step 20
        list_to_check = [0.0, 60.0, 282.7785714285714]
        self.assertIn(list_to_check, dict_angles.values())

if __name__ == "__main__":
    unittest.main()
