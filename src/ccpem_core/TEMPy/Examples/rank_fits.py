from TEMPy.MapParser import MapParser
from TEMPy.ScoringFunctions import ScoringFunctions
from TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
import os,sys
from TEMPy.class_arg import TempyParser
from traceback import print_exc


def map_contour(m,t=1.5):
  mName = os.path.basename(m).split('.')[0]
  print 'reading map'
  emmap=MapParser.readMRC(m)
  c1 = None
  print 'calculating contour'
  zeropeak,ave,sigma1 = emmap._peak_density()
  if not zeropeak is None: c1 = zeropeak+(t*sigma1)
  else:
    c1 = 0.0
  return mName,emmap,c1
def model_contour(p,res,emmap_1,t=-1.):
  pName = os.path.basename(p).split('.')[0]
  print 'reading the model'
  structure_instance=PDBParser.read_PDB_file(pName,p,hetatm=False,water=False)
  print 'filtering the model'
  blurrer = StructureBlurrer()
  if res is None: sys.exit('Map resolution required..')
  #emmap = blurrer.gaussian_blur(structure_instance, res,densMap=emmap_1,normalise=True)
  emmap = blurrer.gaussian_blur_real_space(structure_instance, res,densMap=emmap_1,normalise=True)
  c1 = None
  if t != -1.0:
    print 'calculating contour'
    #zeropeak,ave,sigma1 = emmap._peak_density()
    #if not zeropeak is None: c1 = zeropeak+(t*sigma1)
    #else:
    c1 = t*emmap.std()#0.0
  return pName,emmap,c1

if len(sys.argv) == 1:
  path_out='Test_Files'
  if os.path.exists(path_out)==True:
    print "%s exists" %path_out
  else: sys.exit('No input')
  os.chdir(path_out)
  
  p = '1J6Z.pdb'
  m2Name = os.path.basename(p).split('.')[0]
  pName = os.path.basename(p).split('.')[0]
  m = 'emd_5168_monomer.mrc'
  res = 6.6
  m1Name = os.path.basename(m).split('.')[0]
  emmap_1=MapParser.readMRC(m)
  structure_instance=PDBParser.read_PDB_file(pName,p,hetatm=False,water=False)
  blurrer = StructureBlurrer()
  emmap_2 = blurrer.gaussian_blur(structure_instance, res,densMap=emmap_1)
  c1 = 9.7
  c2 = 1.0
else:
  print 'parsing arguments'
  tp = TempyParser()
  tp.generate_args()
  p = None
  m1Name = m2Name = None


  if not tp.args.inp_map1 is None:
    if not tp.args.thr1 is None: 
      m1Name,emmap_1,c = map_contour(tp.args.inp_map1)
      c1 = tp.args.thr1
    elif not tp.args.thr is None: 
      m1Name,emmap_1,c = map_contour(tp.args.inp_map1)
      c1 = tp.args.thr
    else: m1Name,emmap_1,c1 = map_contour(tp.args.inp_map1,1.5)

    if not tp.args.inp_map2 is None:
      if not tp.args.thr2 is None: 
        m2Name,emmap_2,c = map_contour(tp.args.inp_map2)
        c2 = tp.args.thr2
      else: m2Name,emmap_2,c2 = map_contour(tp.args.inp_map2,1.5)

    elif not tp.args.pdb is None:
      if not tp.args.thr2 is None: 
        m2Name,emmap_2,c = model_contour(tp.args.pdb,tp.args.res,emmap_1)
        c2 = tp.args.thr2
      else: m2Name,emmap_2,c2 = model_contour(tp.args.pdb,tp.args.res,emmap_1,1.0)

    else: sys.exit('Input two maps or a map and model, contours are optional')
  elif not tp.args.inp_map is None:
    if not tp.args.thr is None: 
      m1Name,emmap_1,c = map_contour(tp.args.inp_map)
      c1 = tp.args.thr
    elif not tp.args.thr1 is None: 
      m1Name,emmap_1,c = map_contour(tp.args.inp_map)
      c1 = tp.args.thr1
    else: m1Name,emmap_1,c1 = map_contour(tp.args.inp_map,1.5)

    if not tp.args.inp_map2 is None:
      if not tp.args.thr2 is None:
        m2Name,emmap_2,c = map_contour(tp.args.inp_map2)
        c2 = tp.args.thr2
      else: m2Name,emmap_2,c2 = map_contour(tp.args.inp_map2,1.5)
    elif not tp.args.pdb is None:
      if not tp.args.thr2 is None: 
        m2Name,emmap_2,c = model_contour(tp.args.pdb,tp.args.res,emmap_1)
        c2 = tp.args.thr2
      else: m2Name,emmap_2,c2 = model_contour(tp.args.pdb,tp.args.res,emmap_1,1.0)

    else: sys.exit('Input two maps or a map and model, contours are optional')
  elif not tp.args.inp_map2 is None:
    if not tp.args.thr2 is None:
      m1Name,emmap_1,c = map_contour(tp.args.inp_map2)
      c1 = tp.args.thr2
    else: m1Name,emmap_1,c1 = map_contour(tp.args.inp_map2,1.5)

    if not tp.args.pdb is None:
      if not tp.args.thr1 is None:
        m2Name,emmap_2,c = model_contour(tp.args.pdb,tp.args.res,emmap_1)
        c2 = tp.args.thr1
      else: m2Name,emmap_2,c2 = model_contour(tp.args.pdb,tp.args.res,emmap_1,1.0)

    else: sys.exit('Input two maps or a map and model, contours are optional')

