"""
Module with the `Filter` class.

:class:`Filter`: Object pointing to map data and holds 
                 a set of methods for filtering MRC format maps.
"""


from process import MapEdit
import copy
import os
import numpy as np
import math
from numpy.fft import fftn,fftshift,ifftn,ifftshift
#from scipy.fftpack import fftn, ifftn, fftshift
from scipy.ndimage.interpolation import  shift, affine_transform, zoom,\
                        map_coordinates
from scipy.ndimage import laplace, uniform_filter, generic_filter,\
                        minimum_filter, measurements
from scipy.ndimage.morphology import binary_opening, binary_dilation,\
                        binary_closing
import mrcfile
import array_utils
import warnings
# For CCP-EM mac install.
try:
    from TEMPy.ShowPlot import Plot
except RuntimeError:
    Plot = None
import gc

#FOR TESTS
#from memory_profiler import profile
#from datetime import datetime

try:
    import pyfftw
    pyfftw_flag = True
    #print pyfftw.simd_alignment
except ImportError:
    pyfftw_flag = False
    

class Filter(MapEdit):
    def __init__(self, MapInst,datacopy=True):
        if type(MapInst) is MapEdit or type(MapInst) is Filter:
            if not datacopy: self.__dict__ = MapInst.__dict__.copy()
            else: self.__dict__ = copy.deepcopy(MapInst.__dict__.copy())
        else:
            super(Filter, self).__init__(MapInst)
        
        #CHECK DEPENDENCIES    
        try:
            import pyfftw
            self.pyfftw_flag = True
            #print pyfftw.simd_alignment
        except ImportError:
            self.pyfftw_flag = False    

    def copy(self,deep=True,detach=True):
        '''
        Copy contents to a new object
        '''
        #create MapEdit object
        copymap = Filter(self)
        if detach: copymap.mrc = None
        #copy data and header
        copymap.origin = copy.deepcopy(self.origin)
        if deep: copymap.fullMap = self.fullMap.copy()
        else: copymap.fullMap = self.fullMap
        copymap.apix = copy.deepcopy(self.apix)
        copymap.dim = copy.deepcopy(self.dim)
        return copymap
            
    #DUSTING
    # remove smaller patches that can be found randomly w.r.t sizes from all patches
    def remove_dust_by_size(self,contour,prob=0.1,inplace=False):
        """
        Remove small isolated densities
        Arguments:
            *contour* 
                map threshold
            *prob*
                fraction of a size (+-) among all sizes
        """
        fp = array_utils.grid_footprint()
        binmap = self.fullMap > float(contour)
        label_array, labels = measurements.label(
                                self.fullMap*binmap,structure=fp)
        sizes = measurements.sum(binmap, label_array, range(labels + 1))
        
            
        if labels <= 10:
            m_array = sizes < 0.05*sizes.max()
            ct_remove = np.sum(m_array)
            remove_points = m_array[label_array]
            label_array[remove_points] = 0
            if inplace: self.fullMap[:] = (label_array>0)*(self.fullMap*binmap)
            else: 
                newmap = self.copy()
                newmap.fullMap[:] = (label_array>0)*(self.fullMap*binmap)
                return newmap, labels-ct_remove
            return labels-ct_remove+1

        means = measurements.mean(self.fullMap*binmap,
                                  label_array, range(labels + 1))#range(1,labels + 1))
        freq,bins = np.histogram(sizes[1:],20)
        m_array = np.zeros(len(sizes))
        ct_remove = 0
        for i in range(len(freq)):
            fr = freq[i]
            s2 = bins[i+1]
            s1 = bins[i]
            p_size = float(fr)/float(np.sum(freq))
            if p_size > prob:
                #print (sizes >= s1) & (sizes < s2) & (means < (float(c)+0.5*(self.max()-float(c))))
                m_array = m_array+((sizes >= s1) & (sizes < s2) & 
                                   (
                                    means < (float(contour)+
                                    0.35*(np.amax(self.fullMap)-
                                    float(contour)))
                                    ))
                ct_remove += 1
        m_array = m_array>0
        remove_points = m_array[label_array]

        label_array[remove_points] = 0
        if inplace: self.fullMap[:] = (label_array>0)*(self.fullMap*binmap)
        else: 
            newmap = self.copy()
            newmap.fullMap[:] = (label_array>0)*(self.fullMap*binmap)
            return newmap, labels-ct_remove
        return labels-ct_remove
    
    def fourier_transform(self,keep_shape=False,new_inparray=False):
        """
        pythonic FFTs for maps 
        """
        ftarray = array_utils.calculate_fft(self.fullMap,
                                         keep_shape=keep_shape,
                                         new_inparray=new_inparray)
        ftmap = self.copy(deep=False)
        ftmap.fullMap = ftarray
        #TODO: header not set
        #ftmap.set_dim_apix(ftmap.apix)
        #ftmap.update_header()
        return ftmap
    
    def inv_fourier_transform(self,ftmap,output_dtype=None,
                              output_shape=None):
        """
        Calculate inverse fourier transform
        """
        invftarray = array_utils.calculate_ifft(ftmap.fullMap,
                                                output_shape=output_shape,
                                                inplace=False,
                                                new_inparray=False)
        invftmap = self.copy(deep=False)
        invftmap.fullMap = invftarray
        return invftmap   
            
    def fourier_filter(self,ftfilter,inplace=False,plot=False,
                       plotfile='plot',keep_shape=False):
        """
        Apply lowpass/highpass/bandpass filters in fourier space
        
        ftfilter: filter applied on the fourier grid
        """
            
        ftmap = self.fourier_transform(keep_shape=keep_shape)
        #shift zero frequency to the center
        if not keep_shape:
        #shift zero frequency to the center and apply the filter
            ftmap.fullMap[:] = fftshift(ftmap.fullMap,axes=(0,1))
        else: ftmap.fullMap[:] = fftshift(ftmap.fullMap)
        if plot:
            dict_plot = {}
            lfreq,shell_avg = self.get_raps(ftmap.fullMap)
            dict_plot['map'] = [lfreq[:],shell_avg[:]]
        #apply the filter
        ftmap.fullMap[:] = ftmap.fullMap * ftfilter
        if plot:
            lfreq,shell_avg = self.get_raps(ftmap.fullMap)
            dict_plot['filtered'] = [lfreq,shell_avg]
            self.plot_raps(dict_plot,plotfile=plotfile)
        if not keep_shape:
        #shift zero frequency to the center and apply the filter
            ftmap.fullMap[:] = ifftshift(ftmap.fullMap,axes=(0,1))
        else: ftmap.fullMap[:] = ifftshift(ftmap.fullMap)
        invftmap = self.inv_fourier_transform(ftmap, 
                                        output_shape=self.fullMap.shape)
        if inplace: 
            self.fullMap = invftmap.fullMap
        else:
            newmap = self.copy(deep=False)
            newmap.fullMap = invftmap.fullMap
            return newmap
        
    @staticmethod
    def apply_filter(ftmap,ftfilter,inplace=False):
        #fftshifted ftmap
        if inplace:
            ftmap.fullMap[:] = ftmap.fullMap*ftfilter
            return ftmap
        else:
            filteredmap = ftmap.copy()
            filteredmap.fullMap[:] = ftmap.fullMap*ftfilter
            return filteredmap
    
    def softmask_edges(self,window=3,inplace=False):
        newarray = array_utils.softmask_gaussian(self.fullMap,sigma=1)
        if inplace:
            self.fullMap[:] = newarray
        else:
            newmap = self.copy()
            newmap.fullMap = newarray
            return newmap

    @staticmethod
    def plot_raps(dict_plot,plotfile='plot'):
        if Plot is not None:
            plt = Plot()
            plt.lineplot(dict_plot,plotfile)

    def tanh_lowpass(self,cutoff,fall=0.5):
        '''
        Lowpass filter with a hyperbolic tangent function
        
        cutoff: high frequency cutoff [0:0.5]
        fall: smoothness of falloff [0-> tophat,1.0-> gaussian] 
        Return:
            tanh lowpass filter to apply on fourier map
        '''
        #e.g cutoff = apix/reso is the stop band
        if fall == 0.0: fall = 0.01
        drop = math.pi/(2*float(cutoff)*float(fall))
        cutoff = min(float(cutoff),0.5)
        # fall determines smoothness of falloff, 0-> tophat, 1.0-> gaussian
        # make frequency shells 
        dist = self.make_fourier_shell()
        # filter
        dist1 = dist+cutoff
        dist1[:] = drop*dist1
        dist1[:] = np.tanh(dist1)
        dist[:] = dist-cutoff
        dist[:] = drop*dist
        dist[:] = np.tanh(dist)
        dist[:] = dist1-dist
        dist = 0.5*dist
        del dist1
        return dist
              
    def tanh_bandpass(self,low_cutoff=0.0,high_cutoff=0.5,
                      low_fall=0.1,high_fall=0.1):
        """
        Bandpass filter with a hyperbolic tangent function
        low_cutoff: low frequency cutoff [0:0.5]
        high-cutoff : high frequency cutoff [0:0.5]
        fall: determines smoothness of falloff [0-> tophat,1.0-> gaussian] 
        Return:
            tanh lowpass filter to apply on fourier map
        """
        low_drop = math.pi/(2*float(high_cutoff-low_cutoff)*
                            float(low_fall))
        high_drop = math.pi/(2*float(high_cutoff-low_cutoff)*
                             float(high_fall))

        dist = self.make_fourier_shell()
        return 0.5*(np.tanh(high_drop*(dist+high_cutoff))-
                    np.tanh(high_drop*(dist-high_cutoff))-
                    np.tanh(low_drop*(dist+low_cutoff))+
                    np.tanh(low_drop*(dist-low_cutoff)))

    def butterworth_lowpass(self,pass_freq):
        """
        Lowpass filter with a gaussian function
        pass_freq : low-pass cutoff frequency [0:0.5]
        """
        eps = 0.882
        a = 10.624
        high_cutoff = 0.15*math.log10(1.0/pass_freq) + pass_freq # stop band frequency (used to determine the fall off)
        
        fall = 2.0*(math.log10(eps/math.sqrt(a**2 - 1))/
                    math.log10(pass_freq/float(high_cutoff)))
        
        cutoff_freq = float(pass_freq)/math.pow(eps,2/fall)
        
        dist = self.make_fourier_shell()
        # filter
        dist = dist/cutoff_freq
        return np.sqrt(1.0/(1.0+np.power(dist,fall)))
        
    def gauss_bandpass(self,sigma,center=0.0):
        """
        Bandpass filter with a gaussian function
        sigma : cutoff frequency [0:0.5]
        """
        ##sigma = sigma/1.414
        #get frequency shells
        dist = self.make_fourier_shell()
        # filter
        return np.exp(-((dist-center)**2)/(2*sigma*sigma))

    def gauss_lowpass(self,sigma):
        """
        Bandpass filter with a gaussian function
        sigma : cutoff frequency [0:0.5]
        """
        ##sigma = sigma/1.414
        #get frequency shells
        dist = self.make_fourier_shell()
        # filter
        return np.exp(-(dist**2)/(2*sigma*sigma))
    
    
if __name__ == '__main__':
    mapfile = "emd_3061.map"
    #mrcobj=MapParser.readMRC(mapfile)
    mrcobj=mrcfile.open(mapfile,mode='r+')
    #mrcmap = MapProcess(mrcobj)
    mrcfilt = Filter(mrcobj)
    ftfilter = mrcfilt.tanh_lowpass(0.2,fall=0.5)
    mrcfilt.fourier_filter(ftfilter, plot=True, plotfile='tanh0_2f0_5')
    map_name = os.path.basename(os.path.splitext(mapfile)[0])
    map_dir = os.path.dirname(os.path.abspath(mapfile))
    newmap = mrcfile.new(os.path.join(map_dir,
                                      map_name+'_modified.mrc'),
                                      overwrite=True)
    #update data and header
    mrcfilt.set_newmap_data_header(newmap)
    newmap.close()

            

            
            