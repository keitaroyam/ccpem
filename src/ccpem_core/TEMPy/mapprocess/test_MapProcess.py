import unittest
import os
import shutil
import tempfile
from TEMPy.MapParser import MapParser
import mrcfile
from TEMPy.mapprocess import MapEdit
from ccpem_core import test_data


class Test(unittest.TestCase):
    '''
    Unit test for TEMPy Map Processing.
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.join(os.path.dirname(test_data.__file__),'map','mrc')
        self.test_output = tempfile.mkdtemp()
        
    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)
    
    #https://stackoverflow.com/questions/5595425/what-is-the-best-way-to
    #-compare-floats-for-almost-equality-in-python        
    def __isclose(self,a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
    
    def compare_tuple(self,tuple1,tuple2):
        for val1, val2 in zip(tuple1, tuple2):
            if type(val2) is float:
                self.assertAlmostEquals(val1, val2, 2)
            else:
                self.assertEquals(val1, val2)
            
    def setup_tempy(self,mapfile):
        mrcobj=MapParser.readMRC(mapfile)
        self.mrcmap = MapEdit(mrcobj)
        
    def setup_mrcfile(self,mapfile):
        mrcobj=mrcfile.open(mapfile,mode='r+')
        self.mrcmap = MapEdit(mrcobj)
    
    #TODO: check crop function    
    def test_crop_map(self):
        mapfile = os.path.join(self.test_data,
                               '1ake_molmap45.mrc')
        self.setup_mrcfile(mapfile)
        z,y,x = self.mrcmap.fullMap.shape
        peak,avg,sigma = self.mrcmap.peak_density()
        self.assertAlmostEqual(peak, 0.0, 2)
        cropped_map = self.mrcmap.crop_map(contour=0.2,ext=0,inplace=False)
        self.assertGreaterEqual(cropped_map.fullMap[1].max(), 0.2)
        self.mrcmap.close()
        map_name = os.path.basename(os.path.splitext(mapfile)[0])
        map_dir = os.path.dirname(os.path.abspath(mapfile))
        newmap = mrcfile.new(os.path.join(self.test_output,
                                          map_name+'_cropped.mrc'),
                                          overwrite=True)
        #update data and header
        cropped_map.set_newmap_data_header(newmap)
        newmap.close()
    
    def test_pad_map(self):
        mapfile = os.path.join(self.test_data,
                               '1ake_molmap45.mrc')
        self.setup_mrcfile(mapfile)
        padded_map = self.mrcmap.pad_map(10,10,10,inplace=False)
        self.compare_tuple(padded_map.dim,(87.,84.,88.5))
        self.mrcmap.mrc.close()
        
    def test_map_headers(self):
        mapfile = os.path.join(self.test_data,
                               '1ake_molmap45.mrc')
        self.setup_mrcfile(mapfile)
        #check headers
        self.compare_tuple(self.mrcmap.apix,(1.5,1.5,1.5)) 
        self.compare_tuple(self.mrcmap.origin,(-10.995,11.88,-9.9105))
        self.compare_tuple(self.mrcmap.dim,(72.,69.,73.5))
        
    def test_interpolate_map(self):
        mapfile = os.path.join(self.test_data,
                               '1ake_molmap45.mrc')
        self.setup_mrcfile(mapfile)
        interpolated_map = self.mrcmap.interpolate_to_grid(new_gridshape=(32,31,32),
                                        new_origin=(-27.497,-4.62,-26.407),
                                        new_spacing=(3.333,3.333,3.333),
                                        inplace=False)
        self.compare_tuple(interpolated_map.apix,(3.333,3.333,3.333)) 
        self.compare_tuple(interpolated_map.origin,(-27.497,-4.62,-26.407))
        self.compare_tuple(interpolated_map.fullMap.shape,(32,31,32))
        self.mrcmap.close()
        
    def test_bin_map(self):
        mapfile = os.path.join(self.test_data,
                               '1ake_molmap45.mrc')
        self.setup_mrcfile(mapfile)
        binned_map = self.mrcmap.map_digitize(nbins=10,inplace=False)
        map_name = os.path.basename(os.path.splitext(mapfile)[0])
        map_dir = os.path.dirname(os.path.abspath(mapfile))
        newmap = mrcfile.new(os.path.join(self.test_output,
                                          map_name+'_binned.mrc'),
                                          overwrite=True)
        #update data and header
        binned_map.set_newmap_data_header(newmap)
        newmap.close()
        self.mrcmap.close()        
        
        