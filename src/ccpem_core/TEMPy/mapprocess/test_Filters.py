import unittest
import os
import shutil
import tempfile
import mrcfile
from TEMPy.mapprocess import Filter
import numpy as np
from ccpem_core import test_data
import array_utils


class Test(unittest.TestCase):
    '''
    Unit test for TEMPy MapFilter.
    '''
    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_data = os.path.join(os.path.dirname(test_data.__file__),'map','mrc')
        self.test_output = tempfile.mkdtemp()
        
    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_output):
            shutil.rmtree(self.test_output)
    
    #https://stackoverflow.com/questions/5595425/what-is-the-best-way-to
    #-compare-floats-for-almost-equality-in-python        
    def __isclose(self,a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
    
    def compare_tuple(self,tuple1,tuple2):
        for val1, val2 in zip(tuple1, tuple2):
            if type(val2) is float:
                self.assertAlmostEquals(val1, val2, 2)
            else:
                self.assertEquals(val1, val2)
            
    def setup_tempy(self,mapfile):
        mrcobj=MapParser.readMRC(mapfile)
        self.mrcmap = Filter(mrcobj)
        
    def setup_mrcfile(self,mapfile):
        mrcobj=mrcfile.open(mapfile,mode='r+')
        self.mrcmap = Filter(mrcobj)
        
    def test_fft(self):
        mapfile = os.path.join(self.test_data,\
                               '1ake_molmap45.mrc')
        self.setup_mrcfile(mapfile)
        self.mrcmap.fourier_transform()
        self.mrcmap.close()
      
    def test_lowpassfilters(self):
        emanmapfile = os.path.join(self.test_data,\
                                   '1ake_molmap45_tanhlp_eman2.mrc')
        emanmapobj = mrcfile.open(emanmapfile,mode='r')
        mapfile = os.path.join(self.test_data,\
                               '1ake_molmap45.mrc')
        map_name = os.path.basename(os.path.splitext(mapfile)[0])
        map_dir = os.path.dirname(os.path.abspath(mapfile))
        self.setup_mrcfile(mapfile)
        ftfilter = array_utils.tanh_lowpass(self.mrcmap.shape(),
                                            cutoff=0.2, fall=0.5)
        filtmap = self.mrcmap.fourier_filter(ftfilter=ftfilter,inplace=False)
        self.assertAlmostEqual(np.corrcoef(filtmap.fullMap.ravel(),
                                        emanmapobj.data.ravel())[0][1],
                                           1.0, 2)
        
        
        emanmapfile = os.path.join(self.test_data,
                                   '1ake_molmap45_tanhbp_eman2.mrc')
        emanmapobj = mrcfile.open(emanmapfile,mode='r')
        self.mrcmap.reinitialize_data()
        ftfilter = array_utils.tanh_bandpass(self.mrcmap.shape(),
                                             high_cutoff=0.3,
                                             low_cutoff=0.1,
                                             high_fall=0.3,
                                             low_fall=0.3)
        filtmap = self.mrcmap.fourier_filter(ftfilter=ftfilter,inplace=False)
        self.assertAlmostEqual(np.corrcoef(filtmap.fullMap.ravel(),
                                        emanmapobj.data.ravel())[0][1],
                                           1.0, 2)

        emanmapfile = os.path.join(self.test_data,
                                   '1ake_molmap45_gausslp_eman2.mrc')
        emanmapobj = mrcfile.open(emanmapfile,mode='r')
        self.mrcmap.reinitialize_data()
        ftfilter = array_utils.gauss_lowpass(self.mrcmap.shape(),sigma=0.3)
        filtmap = self.mrcmap.fourier_filter(ftfilter=ftfilter,inplace=False)
        self.assertAlmostEqual(np.corrcoef(filtmap.fullMap.ravel(),
                                        emanmapobj.data.ravel())[0][1],
                                           1.0, 2)
        
        emanmapfile = os.path.join(self.test_data,
                                   '1ake_molmap45_gaussbp_eman2.mrc')
        emanmapobj = mrcfile.open(emanmapfile,mode='r')
        self.mrcmap.reinitialize_data()
        ftfilter = array_utils.gauss_bandpass(self.mrcmap.shape(),sigma=0.2)
        filtmap = self.mrcmap.fourier_filter(ftfilter=ftfilter,inplace=False)
        self.assertAlmostEqual(np.corrcoef(filtmap.fullMap.ravel(),
                                        emanmapobj.data.ravel())[0][1],
                                           1.0, 2)
        
        emanmapfile = os.path.join(self.test_data,
                                   '1ake_molmap45_butterworthlp_eman2.mrc')
        emanmapobj = mrcfile.open(emanmapfile,mode='r')
        self.mrcmap.reinitialize_data()
        ftfilter = array_utils.butterworth_lowpass(self.mrcmap.shape(),pass_freq=0.2)
        filtmap = self.mrcmap.fourier_filter(ftfilter=ftfilter,inplace=False)
        self.assertAlmostEqual(np.corrcoef(filtmap.fullMap.ravel(),
                                           emanmapobj.data.ravel())[0][1],
                                           1.0, 2)
        self.mrcmap.mrc.close()
        
    
    
    
if __name__ == '__main__':
    unittest.main()
        
    
    
    