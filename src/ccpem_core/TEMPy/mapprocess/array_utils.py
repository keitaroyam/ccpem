import numpy as np
import math
from scipy.ndimage import generic_filter,uniform_filter,gaussian_filter

try:
    import pyfftw
    pyfftw_flag = True
    #print pyfftw.simd_alignment
except ImportError:
    pyfftw_flag = False

def find_background_peak(arr, iter=2):
    lbin = np.amin(arr)
    rbin = np.amax(arr)
    ave = np.mean(arr)
    sigma = np.std(arr)
    for it in range(iter):
        if it == 0:
            data = arr
        else:
            data = arr[(arr >= lbin) & (arr <= rbin)]
        
        freq,bins = np.histogram(data,100)
        ind = np.nonzero(freq==np.amax(freq))[0]
        peak = None
        for i in ind:
            val = (bins[i]+bins[i+1])/2.
            if val < float(ave)+float(sigma):
                peak = val
                lbin = bins[i]
                rbin = bins[i+1]
        if peak is None: break
    return peak, ave


def plan_fft(arr,keep_shape=False,new_inparray=False):
    input_dtype = str(arr.dtype)
    if not keep_shape:
        output_dtype = 'complex64'
        if not input_dtype in ['float32','float64','longdouble']: 
            input_dtype = 'float32'
        elif input_dtype == 'float64':
            output_dtype = 'complex128'
        elif input_dtype == 'longdouble':
            output_dtype = 'clongdouble'
        #for r2c transforms:
        output_array_shape = \
                arr.shape[:len(arr.shape)-1]+ \
                                    (arr.shape[-1]//2 + 1,)
    else:
        output_dtype = 'complex64'
        output_array_shape = arr.shape
    
    fftoutput = pyfftw.n_byte_align_empty(output_array_shape, 
                                    n=16, dtype=output_dtype)
    #check if array is byte aligned
    #TODO: can we read the map file as byte aligned? 
    if new_inparray or not pyfftw.is_byte_aligned(arr):
        inputarray = pyfftw.empty_aligned(arr.shape,
                                          n=16,dtype='float32')
        fft = pyfftw.FFTW(inputarray,fftoutput,
                          direction='FFTW_FORWARD',axes=(0,1,2),
                          flags=['FFTW_ESTIMATE'])
    elif pyfftw.is_byte_aligned(arr):
        fft = pyfftw.FFTW(arr,fftoutput,
                          direction='FFTW_FORWARD',axes=(0,1,2),
                          flags=['FFTW_ESTIMATE'])
        inputarray = arr
        
    return fft, fftoutput, inputarray

def calculate_fft(arr,keep_shape=False,new_inparray=False):
    
    if pyfftw_flag:
        fft, fftoutput, inputarray = plan_fft(arr, keep_shape=keep_shape,
                                              new_inparray=new_inparray)
        inputarray[:,:,:] = arr
        fft()
    else:
        #TODO: warning raises error in tasks
        #warnings.warn("PyFFTw not found!, using numpy fft")
        if not keep_shape:
            fftoutput = np.fft.rfftn(arr) 
        else:
            fftoutput = np.fft.fftn(arr)
    return fftoutput

def plan_ifft(arr,output_shape=None,output_array_dtype=None,
              new_inparray=False):
    input_dtype = str(arr.dtype)
#         #for c2r transforms:
#             if output_shape is None: output_shape = \
#                                     arr.shape[:len(arr.shape)-1]+\
#                                     ((arr.shape[-1] - 1)*2,)
    if output_array_dtype is None: output_array_dtype = 'float32'
    if output_shape is None: 
        output_shape = arr.shape[:len(arr.shape)-1]+\
                        ((arr.shape[-1] - 1)*2,)
        if not input_dtype in ['complex64','complex128','clongdouble']: 
            input_dtype = 'complex64'
        elif input_dtype == 'complex128':
            output_array_dtype = 'float64'
        elif input_dtype == 'clongdouble':
            output_array_dtype = 'longdouble'
    elif output_shape[-1]//2 + 1 == arr.shape[-1]:
        if not input_dtype in ['complex64','complex128','clongdouble']: 
            input_dtype = 'complex64'
        elif input_dtype == 'complex128':
            output_array_dtype = 'float64'
        elif input_dtype == 'clongdouble':
            output_array_dtype = 'longdouble'
    else:                           
        output_shape = arr.shape
        output_array_dtype = 'complex64'
        
    output_array = pyfftw.empty_aligned(output_shape, 
                                        n=16, dtype=output_array_dtype)
    #check if array is byte aligned
    if new_inparray or not pyfftw.is_byte_aligned(arr):
        inputarray = pyfftw.n_byte_align_empty(arr.shape,
                                               n=16,
                                               dtype=input_dtype)
        ifft = pyfftw.FFTW(inputarray,output_array,
                           direction='FFTW_BACKWARD',\
                       axes=(0,1,2),flags=['FFTW_ESTIMATE'])#planning_timelimit=0.5)
        inputarray[:,:,:] = arr
    else:
        ifft = pyfftw.FFTW(arr,output_array,
                           direction='FFTW_BACKWARD',\
                           axes=(0,1,2),flags=['FFTW_ESTIMATE'])#planning_timelimit=0.5)
        inputarray = arr

    return ifft, output_array, inputarray

def calculate_ifft(arr,output_shape=None,inplace=False,new_inparray=False):
    """
    Calculate inverse fourier transform
    """
    if pyfftw_flag:
        ifft, output_array, inputarray = plan_ifft(arr,output_shape=output_shape,
                                       new_inparray=new_inparray)
        #r2c fft
        ifft()
    else:
        #TODO: warnings raises error in tasks
        #warnings.warn("PyFFTw not found!, using numpy fft")
        if output_shape is None or output_shape[-1]//2 + 1 == arr.shape[-1]:
            output_array = np.fft.irfftn(arr,s=output_shape)
        else:
            output_array = np.real(np.fft.ifftn(arr))
        #np.fft.fftpack._fft_cache.clear()    
    del arr
    return output_array.real.astype(np.float32,copy=False)

def make_fourier_shell(map_shape,keep_shape=False,normalise=True,
                       fftshift=True):
    """
     For a given grid, make a grid with sampling 
     frequencies in the range (0:0.5)
     Return:
         grid with sampling frequencies  
    """
    z,y,x = map_shape
     # numpy fftfreq : odd-> 0 to (n-1)/2 & -1 to -(n-1)/2 and 
     #even-> 0 to n/2-1 & -1 to -n/2 to check with eman
     # set frequencies in the range -0.5 to 0.5
    if keep_shape:
        rad_z,rad_y,rad_x = np.mgrid[-np.floor(z/2.0):np.ceil(z/2.0),
                                     -np.floor(y/2.0):np.ceil(y/2.0),
                                     -np.floor(x/2.0):np.ceil(x/2.0)]
        if not fftshift: rad_x = np.fft.ifftshift(rad_x)
    #r2c arrays from fftw/numpy rfft
    else:
        rad_z,rad_y,rad_x = np.mgrid[-np.floor(z/2.0):np.ceil(z/2.0),
                                     -np.floor(y/2.0):np.ceil(y/2.0),
                                     0:np.floor(x/2.0)+1]
    if not fftshift:
        rad_z = np.fft.ifftshift(rad_z)
        rad_y = np.fft.ifftshift(rad_y)
    if normalise:
        rad_z = rad_z/float(np.floor(z))
        rad_y = rad_y/float(np.floor(y))
        rad_x = rad_x/float(np.floor(x))
    rad_x = rad_x**2
    rad_y = rad_y**2
    rad_z = rad_z**2
    dist = np.sqrt(rad_z+rad_y+rad_x)
    return dist

def tanh_lowpass(map_shape,cutoff,fall=0.3,keep_shape=False):
    '''
     Lowpass filter with a hyperbolic tangent function
     
     cutoff: high frequency cutoff [0:0.5]
     fall: smoothness of falloff [0-> tophat,1.0-> gaussian] 
     Return:
         tanh lowpass filter to apply on fourier map
    '''
    #e.g cutoff = apix/reso is the stop band
    if fall == 0.0: fall = 0.01
    drop = math.pi/(2*float(cutoff)*float(fall))
    cutoff = min(float(cutoff),0.5)
    # fall determines smoothness of falloff, 0-> tophat, 1.0-> gaussian
    # make frequency shells 
    dist = make_fourier_shell(map_shape,keep_shape=keep_shape,
                              fftshift=True)
    ##dist_ini = dist.copy()
    # filter
    dist1 = dist+cutoff
    dist1[:] = drop*dist1
    dist1[:] = np.tanh(dist1)
    dist[:] = dist-cutoff
    dist[:] = drop*dist
    dist[:] = np.tanh(dist)
    dist[:] = dist1-dist
    dist = 0.5*dist
    del dist1
    
#     list_freq = []
#     list_intensities = []
#     rprev = 0.0
#     for r in np.arange(0.02,0.5,0.02):
#         shell_indices = (dist_ini < r) & (dist_ini <= rprev)
#         avg_shell_intensity = np.mean(dist[shell_indices])
#         list_freq.append(r)
#         list_intensities.append(avg_shell_intensity)
#         rprev = r
#     print list_freq
#     print list_intensities
#     
    return dist

def tanh_bandpass(map_shape,low_cutoff=0.0,high_cutoff=0.5,
                  low_fall=0.1,high_fall=0.1,keep_shape=False):
    """
    Bandpass filter with a hyperbolic tangent function
    low_cutoff: low frequency cutoff [0:0.5]
    high-cutoff : high frequency cutoff [0:0.5]
    fall: determines smoothness of falloff [0-> tophat,1.0-> gaussian] 
    Return:
        tanh lowpass filter to apply on fourier map
    """
    low_drop = math.pi/(2*float(high_cutoff-low_cutoff)*
                        float(low_fall))
    high_drop = math.pi/(2*float(high_cutoff-low_cutoff)*
                         float(high_fall))

    dist = make_fourier_shell(map_shape,keep_shape=keep_shape,
                              fftshift=True)
    return 0.5*(np.tanh(high_drop*(dist+high_cutoff))-
                np.tanh(high_drop*(dist-high_cutoff))-
                np.tanh(low_drop*(dist+low_cutoff))+
                np.tanh(low_drop*(dist-low_cutoff)))

def butterworth_lowpass(map_shape,pass_freq,keep_shape=False):
    """
    Lowpass filter with a gaussian function
    pass_freq : low-pass cutoff frequency [0:0.5]
    """
    eps = 0.882
    a = 10.624
    high_cutoff = 0.15*math.log10(1.0/pass_freq) + pass_freq # stop band frequency (used to determine the fall off)
    
    fall = 2.0*(math.log10(eps/math.sqrt(a**2 - 1))/
                math.log10(pass_freq/float(high_cutoff)))
    
    cutoff_freq = float(pass_freq)/math.pow(eps,2/fall)
    
    dist = make_fourier_shell(map_shape,keep_shape=keep_shape,
                              fftshift=True)
    # filter
    dist = dist/cutoff_freq
    return np.sqrt(1.0/(1.0+np.power(dist,fall)))
    
def gauss_bandpass(map_shape,sigma,center=0.0,keep_shape=False):
    """
    Bandpass filter with a gaussian function
    sigma : cutoff frequency [0:0.5]
    """
    ##sigma = sigma/1.414
    #get frequency shells
    dist = make_fourier_shell(map_shape,keep_shape=keep_shape,
                              fftshift=True)
    # filter
    return np.exp(-((dist-center)**2)/(2*sigma*sigma))

def gauss_lowpass(map_shape,sigma,keep_shape=False):
    """
    Bandpass filter with a gaussian function
    sigma : cutoff frequency [0:0.5]
    """
    ##sigma = sigma/1.414
    #get frequency shells
    dist = make_fourier_shell(map_shape,keep_shape=keep_shape,
                              fftshift=True)
    # filter
    return np.exp(-(dist**2)/(2*sigma*sigma))

def apply_filter(ftmap,ftfilter,inplace=False):
    #fftshifted ftmap
    if inplace:
        ftmap[:] = ftmap*ftfilter
        return ftmap
    else:
        return ftmap*ftfilter

# footprint array corresponding to 6 neighboring faces of a voxel
def grid_footprint():
    """
    Generate a footprint array for local neighborhood (6 faces)
    """
    a = np.zeros((3,3,3))
    a[1,1,1] = 1
    a[0,1,1] = 1
    a[1,0,1] = 1
    a[1,1,0] = 1
    a[2,1,1] = 1
    a[1,2,1] = 1
    a[1,1,2] = 1

    return a

def make_spherical_footprint(radius):
    """
    Get spherical footprint of a given diameter
    """
    rad_z = np.arange(radius*-1, 
                      radius+1)
    rad_y = np.arange(radius*-1, 
                      radius+1)
    rad_x = np.arange(radius*-1, 
                      radius+1)

    rad_x = rad_x**2
    rad_y = rad_y**2
    rad_z = rad_z**2
    dist = np.sqrt(rad_z[:,None,None]+rad_y[:,None] + rad_x)
    #set_printoptions(threshold='nan')
    return (dist<=radius)*1

def softmask_mean(arr,window=3,iter=5):
    footprint_sph = make_spherical_footprint(np.ceil(float(window)/2))
    newarray = np.copy(arr)
    bin_arr = arr > 0.
    added_edge_mask = bin_arr[:]
    bin_newarr = added_edge_mask
    added_edge = arr[bin_arr]
    for i in range(iter):
        newarray[:] = generic_filter(newarray,np.mean,
                                     footprint=footprint_sph,
                                     mode='constant',cval=0.0)
        newarray[added_edge_mask] = added_edge[:]
        del added_edge, added_edge_mask
        added_edge_mask =  np.logical_not(bin_newarr)*(newarray > 0.)
        added_edge = newarray[added_edge_mask]
        del bin_newarr
        bin_newarr = newarray > 0.
#         newarray[:] = uniform_filter(newarray,size=window,
#                                      mode='constant',cval=0.0)
    newarray[bin_arr] = arr[bin_arr]
    return newarray

def softmask_gaussian(arr,sigma=1):
    newarray = np.copy(arr)
    bin_arr = arr > 0.
    newarray[:] = gaussian_filter(newarray,sigma=sigma,mode='constant',cval=0.0)
    newarray[bin_arr] = arr[bin_arr]
    return newarray

def calculate_shell_correlation(shell1,shell2):
    cov_ps1_ps2 = shell1*np.conjugate(shell2)
    sig_ps1 = shell1*np.conjugate(shell1)
    sig_ps2 = shell2*np.conjugate(shell2)
    cov_ps1_ps2 = np.sum(np.real(cov_ps1_ps2))
    var_ps1 = np.sum(np.real(sig_ps1))
    var_ps2 = np.sum(np.real(sig_ps2))
    #skip shells with no variance
    if var_ps1 == 0.0 or var_ps2 == 0.0: 
        fsc = 0.0
    else: fsc = cov_ps1_ps2/(np.sqrt(var_ps1*var_ps2))
    return fsc

def calculate_fsc(ftarr1,ftarr2,dist1,dist2=None,
                     step=None,
                     maxlevel=None,
                     plot=False):
    list_freq = []
    list_fsc = []
    nc = 0
    x = 0.0
    highlevel = x+step
        #for grids of different dimensions
    if dist2 is None: dist2 = dist1
    #Assume step=1 and dist is in range 0-N/2, if step is None
    if step is None: 
        assert compare_tuple(ftarr1.shape, ftarr2.shape)
        step = 1
    #Assume maxlevel is N/2 if maxlevel is None
    if maxlevel is None: 
        assert compare_tuple(ftarr1.shape, ftarr2.shape)
        maxlevel = ftarr1.shape[0]//2

    while (x<maxlevel):
        fshell_indices = (dist1 < min(maxlevel,highlevel)) & (dist1 >= x)
        fsc = calculate_shell_correlation(ftarr1[fshell_indices],ftarr2[fshell_indices])
        print x+step/2., fsc
        list_freq.append(x+step/2.)
        list_fsc.append(fsc)
        x = highlevel
        highlevel = x+step
    
    listfreq, listfsc = zip(*sorted(zip(list_radii, list_fsc)))
    return listfreq, listfsc

def compare_tuple(tuple1,tuple2):
    for val1, val2 in zip(tuple1, tuple2):
        if type(val2) is float:
            if round(val1,2) != round(val2,2):
                return False
        else:
            if val1 != val2:
                return False
    return True