#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import os
import json

import gemmi


def convert_pdb_to_mmcif(pdb_path, mmcif_path=None):
    """Convert coordinate file from PDB format to mmCIF/PDBx format"""
    structure = gemmi.read_structure(pdb_path)
    if mmcif_path is None:
        mmcif_path = os.path.splitext(pdb_path)[0] + '.cif'
    write_structure_as_mmcif(structure, mmcif_path)
    return mmcif_path


def write_structure_as_mmcif(structure, mmcif_name):
    """Write a Gemmi structure out to an mmCIF file."""
    structure.make_mmcif_document().write_file(mmcif_name)


def write_structure_as_pdb(structure, pdb_name):
    """Write a Gemmi structure out to a PDB file."""
    structure.write_pdb(pdb_name)


def shift_coordinates(in_model_path, out_model_path, trans_matrix,
                      remove_charges=False):
    """
    Shift atomic coordinates based on a translation matrix
    """
    structure = gemmi.read_structure(in_model_path)
    for model in structure:
        for chain in model:
            for residue in chain:
                for atom in residue:
                    atom.pos = gemmi.Position(atom.pos.x+trans_matrix[0],
                                              atom.pos.y+trans_matrix[1],
                                              atom.pos.z+trans_matrix[2])
                    if remove_charges: #remove negative charges?
                        if atom.charge != 0:
                            atom.charge = 0
    structure.write_pdb(out_model_path)


def get_bfactors(in_model_path,out_json=None):
    """
    Get B-factors of atoms
    """
    dict_chain = {}
    structure = gemmi.read_structure(in_model_path)
    for model in structure:
        for chain in model:
            polymer = chain.get_polymer()
            #skip non polymers
            if not polymer: continue
            if not chain.name in dict_chain:
                dict_chain[chain.name] = {}
            for residue in chain:
                list_bfact = []
                residue_id = str(residue.seqid.num)+'_'+residue.name
                for atom in residue:
                    list_bfact.append(atom.b_iso)
                avg_bfact = sum(list_bfact)/float(len(list_bfact))
                dict_chain[chain.name][residue_id] = round(avg_bfact,3)
        break # ignore other models
    if out_json is not None:
        with open(out_json,'w') as oj:
            json.dump(dict_chain,oj)
            
    return dict_chain


def calc_bfact_deviation(in_model_path,out_json=None):
    structure = gemmi.read_structure(in_model_path)
    dict_deviation = {}
    for dist in [3.0,5.0,10.0]:
        subcells = gemmi.SubCells(structure[0], structure.cell, dist)
        subcells.populate()
        dict_deviation[dist] = {}
        for model in structure:
            for chain in model:
                polymer = chain.get_polymer()
                #skip non polymers
                if not polymer: continue
                if not chain.name in dict_deviation[dist]:
                    dict_deviation[dist][chain.name] = {}
                for residue in chain.get_polymer():
                    list_bfact = []
                    residue_id = str(residue.seqid.num)+'_'+residue.name
#                     for atom in residue:
#                         list_bfact.append(atom.b_iso)
#                     avg_bfact = sum(list_bfact)/float(len(list_bfact))
                    for atom in residue:
                        if atom.name == 'CA':
                            ca_bfact = atom.b_iso
                            list_neigh_bfact = []
                            marks = subcells.find_neighbors(atom, min_dist=0.1, max_dist=dist)
                            for mark in marks:
                                cra = mark.to_cra(model)
                                neigh_atom = cra.atom
                                if neigh_atom.name == 'CA':
                                    list_neigh_bfact.append(neigh_atom.b_iso)
                            try: avg_neigh = sum(list_neigh_bfact)/len(list_neigh_bfact)
                            except ZeroDivisionError: pass
                            break
                    if len(list_neigh_bfact) > 0:
                        dict_deviation[dist][chain.name][residue_id] = abs(ca_bfact - avg_neigh)
            break # ignore other models
    if out_json is not None:
        with open(out_json,'w') as oj:
            json.dump(dict_deviation,oj)
        
    return dict_deviation


def get_residue_ca_coordinates(in_model_path,out_json=None):
    dict_coord = {}
    structure = gemmi.read_structure(in_model_path)
    for model in structure:
        if not model.name in dict_coord: dict_coord[model.name] = {}
        for chain in model:
            polymer = chain.get_polymer()
            #skip non polymers
            #if not polymer: continue
            if not chain.name in dict_coord[model.name]: 
                dict_coord[model.name][chain.name] = {}
            for residue in chain:
                residue_id = str(residue.seqid.num)+'_'+residue.name
                residue_centre = ()
                if residue.name in ['A','T','C','G','U']:#nuc acid
                    for atom in residue:
                        if atom.name in ["P","C3'","C1'"]:
                            residue_centre = (atom.pos.x,atom.pos.y,atom.pos.z)
                else:
                    for atom in residue:
                        if atom.name == 'CA':#prot
                            residue_centre = (atom.pos.x,atom.pos.y,atom.pos.z)
                if len(residue_centre) == 0:#non nuc acid / prot
                    try: 
                        center_index = len(residue)/2
                        atom = residue[center_index]
                        residue_centre = (atom.pos.x,atom.pos.y,atom.pos.z)
                    except: 
                        for atom in residue:
                            residue_centre = (atom.pos.x,atom.pos.y,atom.pos.z)
                            break #first atom
                if len(residue_centre) > 0:
                    dict_coord[model.name][str(chain.name)][str(residue.seqid.num)] = \
                                            [residue_centre, residue.name]

    if out_json is not None:
        with open(out_json,'w') as j:
            json.dump(dict_coord,j)
    return dict_coord


def get_coordinates(in_model_path):
    list_coord = []
    structure = gemmi.read_structure(in_model_path)
    for model in structure:
        for chain in model:
            polymer = chain.get_polymer()
            #skip non polymers
            if not polymer: continue
            for residue in chain:
                residue_id = str(residue.seqid.num)+'_'+residue.name
                for atom in residue:
                    coord = atom.pos #gemmi Position
                    list_coord.append([coord.x,coord.y,coord.z])
    return list_coord


def remove_atomic_charges(in_model_path,out_model_path):
    structure = gemmi.read_structure(in_model_path)
    for model in structure:
        for chain in model:
            polymer = chain.get_polymer()
            #skip non polymers
            #if not polymer: continue
            for residue in chain:
                residue_id = str(residue.seqid.num)+'_'+residue.name
                for atom in residue:
                    if atom.charge != 0:
                        atom.charge = 0
    structure.write_pdb(out_model_path)
