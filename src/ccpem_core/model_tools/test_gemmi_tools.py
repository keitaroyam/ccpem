#
#     Copyright (C) 2019 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

import unittest
import os
import shutil
import tempfile

import gemmi

from ccpem_core import test_data
import gemmi_utils


class Test(unittest.TestCase):
    '''
    Unit test for gemmi utils
    '''
    def setUp(self):
        self.test_data = test_data.get_test_data_path()
        self.test_output = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.test_output)

    def test_convert_pdb_to_mmcif_no_output_path(self):
        # Copy input PDB to ensure output CIF is written in the temp directory
        orig_pdb_path = os.path.join(self.test_data, 'pdb/5me2.pdb')
        temp_pdb_path = os.path.join(self.test_output, '5me2.pdb')
        shutil.copy(orig_pdb_path, temp_pdb_path)
        out_path = gemmi_utils.convert_pdb_to_mmcif(temp_pdb_path)
        mmcif_path = os.path.join(self.test_output, '5me2.cif')
        assert out_path == mmcif_path
        assert os.path.exists(mmcif_path)
        cif_doc = gemmi.cif.read(mmcif_path)
        assert isinstance(cif_doc, gemmi.cif.Document)

    def test_convert_pdb_to_mmcif_with_output_path(self):
        pdb_path = os.path.join(self.test_data, 'pdb/5me2.pdb')
        mmcif_path = os.path.join(self.test_output, '5me2.mmcif')
        out_path = gemmi_utils.convert_pdb_to_mmcif(pdb_path=pdb_path,
                                                    mmcif_path=mmcif_path)
        assert out_path == mmcif_path
        assert os.path.exists(mmcif_path)
        cif_doc = gemmi.cif.read(mmcif_path)
        assert isinstance(cif_doc, gemmi.cif.Document)


if __name__ == '__main__':
    unittest.main()

