//  include the MMDB coordinate Manager file header
#ifndef  __MMDB_Manager__
#include "mmdb_manager.h"
#endif

class mmdbManager {
 public:
  bool          verbose;
  int           selHnd;
  //
  mmdbManager(mmdb::cpstr filename_in_)
    : filename_in(filename_in_){
    ReadCoordFile();
    }
  mmdbManager() {
    mmdb = mmdb::Manager();
    }
  ~mmdbManager(){}
  //
  void WritePDB(mmdb::cpstr filename_out) {
    mmdb.FinishStructEdit();
    RC = mmdb.WritePDBASCII(filename_out);
    printf ("\n  Write PDB : %s\n", filename_out);
    //  interprete the return code
    if (RC) {
     printf ("\n  Error writing coordinate file : %s", filename_out);
    }
  }

  void PrintEntryId(){
    printf("\nEntry id : %s\n", mmdb.GetEntryID());
  }

  void RenameChain(int selChn, mmdb::pstr chain_name){
    int               ic;
    int               nChains;
    mmdb::PPChain     chain;
    mmdb.GetSelIndex(selChn, chain, nChains);
    for (ic=0; ic<nChains; ic++) {
      if (chain[ic]){
        chain[ic]->SetChainID(chain_name);
      }
    }
    mmdb.FinishStructEdit();
  }

  std::vector<std::vector<float> > GetAtomCoords(int sel){
    int                              ia;
    int                              nAtoms;
    mmdb::PPAtom                     atom;

    // Get selection index
    mmdb.GetSelIndex(sel, atom, nAtoms);
    printf("\nNum atoms : %d", nAtoms);
    // Initiate vector
    std::vector<std::vector<float> > xyz_coords(nAtoms, std::vector<float>(3, 0.0));
    // Get atom coords
    for (ia = 0; ia < nAtoms; ia++) {
      xyz_coords[ia][0] = atom[ia]->x;
      xyz_coords[ia][1] = atom[ia]->y;
      xyz_coords[ia][2] = atom[ia]->z;
    }
    return xyz_coords;
  }

  void SetAtomCoords(std::vector<std::vector<float> > xyz_coords, int sel) {
    int                              ia;
    int                              nAtoms;
    mmdb::PPAtom                     atom;

    // Get selection index
    mmdb.GetSelIndex(sel, atom, nAtoms);
    assert(nAtoms == xyz_coords.size());
    // Set atom coords
    for (ia = 0; ia < nAtoms; ia++) {
      atom[ia]->SetCoordinates(xyz_coords[ia][0],
                               xyz_coords[ia][1],
                               xyz_coords[ia][2],
                               atom[ia]->occupancy,
                               atom[ia]->tempFactor);
    }
    mmdb.FinishStructEdit();
  }

  void ApplyTransform(std::vector<std::vector<float> > rotMat,
                      std::vector<float> tranMat){
    mmdb::mat44       TMatrix;

    // The transformation matrix TMatrix contains rotational part in columns 0,1,2, rows 0,1,2 (stands for x,y,z) 
    // and translational part in column 3, rows 0,1,2
    // c++ therefore row major: TMat[row][col]
    //Set rotation
    TMatrix[0][0] = rotMat[0][0];
    TMatrix[0][1] = rotMat[0][1];
    TMatrix[0][2] = rotMat[0][2];
    TMatrix[1][0] = rotMat[1][0];
    TMatrix[1][1] = rotMat[1][1];
    TMatrix[1][2] = rotMat[1][2];
    TMatrix[2][0] = rotMat[2][0];
    TMatrix[2][1] = rotMat[2][1];
    TMatrix[2][2] = rotMat[2][2];
    //Set translation
    TMatrix[0][3] = tranMat[0];
    TMatrix[1][3] = tranMat[1];
    TMatrix[2][3] = tranMat[2];

    mmdb.ApplyTransform(TMatrix);
    mmdb.FinishStructEdit();
  }

  void DeleteSelectedObjects(int selDel){
    mmdb.DeleteSelObjects(selDel);
    mmdb.FinishStructEdit();
  }

  void DeleteSolvent(){
    mmdb.DeleteSolvent();
    mmdb.FinishStructEdit();
  }

  void DeleteAltLocs(){
    mmdb.DeleteAltLocs();
    mmdb.FinishStructEdit();
  }

  void DeleteResidueBFactorMax(float bmax){
    int               im, ic, ir;
    int               nModels, nChains, nResidues;
    mmdb::PPModel     model;
    mmdb::PPChain     chain;
    mmdb::PPResidue   res;
    mmdb::AtomStat    atomStat;

    //  get table of models
    mmdb.GetModelTable ( model,nModels );

    //  loop over all models
    for (im=0;im<nModels;im++) {
      if (model[im])
        //  get chain table of im-th model
        model[im]->GetChainTable ( chain,nChains );
        //  loop over all chains:
        for (ic=0;ic<nChains;ic++) {
          if (chain[ic])
            // get residue table for current chain:
            chain[ic]->GetResidueTable ( res,nResidues );
            // loop over all residues in current chain:
            for (ir=0;ir<nResidues;ir++){
              res[ir]->GetAtomStatistics(atomStat);
              if (atomStat.tFmax > bmax) {
                chain[ic]->DeleteResidue(ir);
              }
            }
        }
    }
    mmdb.FinishStructEdit();
  }

  void DeleteAtomBFactorMax(float bmax){
    int               im, ic, ir, ia;
    int               nModels, nChains, nResidues, nAtoms;
    mmdb::PPModel     model;
    mmdb::PPChain     chain;
    mmdb::PPResidue   res;
    mmdb::PPAtom      atom;

    //  get table of models
    mmdb.GetModelTable ( model,nModels );

    //  loop over all models
    for (im=0;im<nModels;im++) {
      if (model[im])
        //  get chain table of im-th model
        model[im]->GetChainTable ( chain,nChains );
        //  loop over all chains:
        for (ic=0;ic<nChains;ic++) {
          if (chain[ic])
            // get residue table for current chain:
            chain[ic]->GetResidueTable ( res,nResidues );
            // loop over all residues in current chain:
            for (ir=0;ir<nResidues;ir++){
              res[ir]->GetAtomTable( atom,nAtoms );
              for (ia=0;ia<nAtoms;ia++) {
                if (atom[ia]->tempFactor > bmax) {
                  res[ir]->DeleteAtom(ia);
                }
              }
            }
        }
    }
    mmdb.FinishStructEdit();
  }

  int NewSelection(){
    return mmdb.NewSelection();
  }

  void Select(int   selHnd,         // must be obtained from NewSelection()
              mmdb::SELECTION_TYPE sType,  // selection type STYPE_XXXXX
              int   iModel,         // model number; iModel=0 means
                                    // 'any model'
              mmdb::cpstr Chains,   // may be several chains "A,B,W"; "*"
                                    // means 'any chain' (in selected
                                    // model(s))
              int   ResNo1,         // starting residue sequence number
              mmdb::cpstr Ins1,     // starting residue insertion code; "*"
                                    // means 'any code'
              int   ResNo2,         // ending residue sequence number.
              mmdb::cpstr Ins2,     // ending residue insertion code; "*"
                                    // means 'any code'. Combination of
                                    // ResNo1=ResNo2=ANY_RES and
                                    // Ins1=Ins2="*" means 'any residue
                                    // number' (in selected chain(s))
              mmdb::cpstr RNames,   // may be several residue names
                                    // "ALA,GLU,CIS"; "*" means 'any
                                    // residue name'
              mmdb::cpstr ANames,   // may be several names "CA,CB"; "*"
                                    // means 'any atom' (in selected
                                    // residue(s))
              mmdb::cpstr Elements, // may be several element types
                                    // "H,C,O,CU"; "*" means 'any element'
              mmdb::cpstr altLocs,  // may be several alternative
                                    // locations 'A,B'; "*" means
                                    // 'any alternative location'
              mmdb::SELECTION_KEY selKey// selection key
              ){
    mmdb.Select(selHnd, sType, iModel, Chains, ResNo1, Ins1, ResNo2, Ins2, RNames, ANames, Elements, altLocs, selKey);
    }

  int GenerateSymMates(){
    int RC;
    mmdb::PGenSym genSym = NULL;
    RC = mmdb.GenerateSymMates(genSym);
    return RC;
  }

 friend void AtomCopyPaste(int selHnd, mmdbManager *molCopy, mmdbManager *molPaste);
 private:
  int           RC;
  mmdb::Manager mmdb;
  mmdb::cpstr   filename_in;
  //
  void ReadCoordFile() {
    mmdb = mmdb::Manager();
    RC = mmdb.ReadCoorFile(filename_in);
    printf ("\n  Open coordinate file : %s\n", filename_in);
    //  interprete the return code
    if (RC) {
     printf ("\n  Error writing coordinate file");
    }
  }
};

void AtomCopyPaste (int selHnd, mmdbManager *molCopy, mmdbManager *molPaste){
  int           ia;
  int           nAtoms;
  mmdb::PPAtom  atom;

  //Loop through atoms in copy model, put atoms in paste model
  molCopy->mmdb.GetSelIndex(selHnd, atom, nAtoms);
  for (ia = 0; ia < nAtoms; ia++) {
    molPaste->mmdb.PutAtom(ia+1, atom[ia]);
  }
  molPaste->mmdb.FinishStructEdit();
}

// Expose selection codes:
// Selection key
enum SELECTION_KEY {
  SKEY_NEW  = 0,
  SKEY_OR   = 1,
  SKEY_AND  = 2,
  SKEY_XOR  = 3,
  SKEY_CLR  = 4,
  SKEY_XAND = 100  // used internally
};

//  Selection types
enum SELECTION_TYPE {
  STYPE_INVALID   = -1,
  STYPE_UNDEFINED =  0,
  STYPE_ATOM      =  1,
  STYPE_RESIDUE   =  2,
  STYPE_CHAIN     =  3,
  STYPE_MODEL     =  4
};

//  Residue properties for SelectProperties()
enum SELECTION_PROPERTY {
  SELPROP_Solvent    = 0,
  SELPROP_Aminoacid  = 1,
  SELPROP_Nucleotide = 2,
  SELPROP_Sugar      = 3,
  SELPROP_ModRes     = 4
};
