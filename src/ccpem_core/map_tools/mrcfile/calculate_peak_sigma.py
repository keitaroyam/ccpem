import os, sys
import mrcfile
from ccpem_core.map_tools import array_utils
import argparse
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument('-m', '--map', help='Map file',
                    default=None)
parser.add_argument('-t', '--thr', type=float, help='Contour threshold',
                    default=None)

args = parser.parse_args()
mapfile = args.map
assert os.path.isfile(mapfile)
mrcobj=mrcfile.open(mapfile,mode='r')

from TEMPy.mapprocess import Filter

emmap = Filter(mrcobj)
print 'Peak, average, sigma : ', emmap.peak_density()

mrcobj.close()