

from TEMPy.mapprocess import Filter
from TEMPy.mapprocess import array_utils
import numpy as np
from numpy.fft import fftn,fftshift,ifftn,ifftshift
import gc
from __builtin__ import isinstance
import mrcfile
#from memory_profiler import profile


def compare_tuple(tuple1,tuple2):
    for val1, val2 in zip(tuple1, tuple2):
        if type(val2) is float:
            if round(val1,2) != round(val2,2):
                return False
        else:
            if val1 != val2:
                return False
    return True

#two maps are required for following functions
def compare_grid(map1,map2):
    m1_shape = map1.fullMap.shape
    m2_shape = map2.fullMap.shape
    assert map1.compare_tuple(m1_shape,m2_shape)
    assert map1.compare_tuple(map1.origin,map2.origin)
    if isinstance(map1.apix,tuple):
        if isinstance(map2.apix,tuple):
            assert map1.compare_tuple(map1.apix,map2.apix)
        else:
            assert map1.apix[0] == map2.apix
    elif isinstance(map2.apix,tuple): assert map2.apix[0] == map1.apix
    else: assert map1.apix == map2.apix
        

def alignment_box(map1,map2,s):
    m1_shape = map1.fullMap.shape
    m2_shape = map2.fullMap.shape
    (ox,oy,oz) = (map1.origin[0],map1.origin[1],map1.origin[2])
    (o1x,o1y,o1z) = (map2.origin[0],map2.origin[1],map2.origin[2])
    offset = (o1x-ox,o1y-oy,o1z-oz)
    (m1x,m1y,m1z) = (ox+map1.fullMap.shape[2]*map1.apix[0],
                     oy+map1.fullMap.shape[1]*map1.apix[1],
                     oz+map1.fullMap.shape[0]*map1.apix[2])
    (m2x,m2y,m2z) = (o1x+map2.fullMap.shape[2]*map2.apix[0],
                     o1y+map2.fullMap.shape[1]*map2.apix[1],
                     o1z+map2.fullMap.shape[0]*map2.apix[2])
    (nx,ny,nz) = (o1x, o1y, o1z)
    if offset[0] > 0:
            nx = ox
    if offset[1] > 0:
            ny = oy
    if offset[2] > 0:
            nz = oz

    (lz,ly,lx) = ((m2z-nz)/float(s),(m2y-ny)/float(s),(m2x-nx)/float(s))
    if m2x < m1x: lx = (m1x-nx)/float(s)
    if m2y < m1y: ly = (m1y-ny)/float(s)
    if m2z < m1z: lz = (m1z-nz)/float(s)
    gridshape = (int(lz),int(ly),int(lx))
    new_origin = (nx,ny,nz)
    return gridshape, new_origin

def scale_amplitudes(ftarr1,ftarr2,dist1,dist2=None,
                     maxlevel=None,step=None,ref=False,
                     plot=False):
    '''
    Scale amplitudes in each resolution shell:
        to avg (by default)
        to a reference (ref=True)
    dist1,dist2 : frequency shells (fftshifted) [0-0.5 or 0-N/2 or 
                                                0-1/(nyquist resolution)]
    For grids of same dimensions, dist2 = dist1 (dist2=None)
    step: shell width (values in agreement with dist1,dist2)
            e.g. 1/N if dist in [0-0.5]
                1/(N*apix) if dist in [0-1/nyq.res]
                1 if dist in [0-N/2]
    maxlevel : nyquist (values in agreement with dist1,dist2)
    '''
    #SCALING
    # storing for plots
    ft1_avg = []
    ft2_avg = []
    ft1_avg_new = []
    lfreq = []
    nc = 0
    x = 0.0
    highlevel = x+step
    #for grids of different dimensions
    if dist2 is None: dist2 = dist1
    #Assume step=1 and dist is in range 0-N/2, if step is None
    if step is None: 
        assert array_utils.compare_tuple(ftarr1.shape, ftarr2.shape)
        step = 1
    #Assume maxlevel is N/2 if maxlevel is None
    if maxlevel is None: 
        assert array_utils.compare_tuple(ftarr1.shape, ftarr2.shape)
        maxlevel = ftarr1.shape[0]//2
    while (x<maxlevel):
        # indices between upper and lower shell bound
        fshells1 = ((dist1 < min(maxlevel,highlevel)) & (dist1 >= x))
        # radial average
        shellvec1 = ftarr1[fshells1]
        # indices between upper and lower shell bound
        fshells2 = ((dist2 < min(maxlevel,highlevel)) & (dist2 >= x))
        # radial average
        shellvec2 = ftarr2[fshells2]
        
        abs1 = abs(shellvec1)
        abs2 = abs(shellvec2)
        #check non zero amplitudes in a shell
        ns1 = len(np.nonzero(abs1)[0]) #or count_nonzero
        ns2 = len(np.nonzero(abs2)[0]) #or count_nonzero
        #only few fourier terms in a shell: merge two shells
        if (ns1 < 5 or ns2 < 5) and nc < 3:
            nc += 1
            highlevel = min(maxlevel,x+(nc+1)*step) 
            if highlevel< maxlevel: continue
        else: nc = 0
        #scale intensities
        mft1 = np.mean(np.square(abs1))
        mft2 = np.mean(np.square(abs2))
        if mft1 == 0.0 and mft2 == 0.0:
            x = highlevel
            highlevel = x+step
            if highlevel< maxlevel: continue
        if plot:
            # sq of radial avg amplitude
            ft1_avg.append(np.log10(mft1))
            ft2_avg.append(np.log10(mft2))
        # scale to amplitudes of the ref map
        if ref:
            if mft1 == 0.0: continue
            ftarr1[fshells1] = shellvec1*np.sqrt(mft2/mft1)
        else:
            # replace with avg amplitudes for the two maps
            ftarr1[fshells1] = shellvec1*np.sqrt((mft2+mft1)/(2.*mft1))
            ftarr2[fshells2] = shellvec2*np.sqrt((mft2+mft1)/(2.*mft2))

        if plot: 
            # new radial average (to check)
            mft1 = np.mean(np.square(abs(ftarr1[fshells1])))#numsum(absolute(ft1.fullMap[fshells1]))/len(shellvec1)
            ft1_avg_new.append(np.log10(mft1))
            lfreq.append(x + (highlevel-x)/2.)

        del fshells1,fshells2,shellvec1,shellvec2
        x = highlevel
        highlevel = x+step
        gc.collect()
        
    dict_plot = {}
    if plot:
        dict_plot['map1'] = [lfreq,ft1_avg]
        dict_plot['map2'] = [lfreq,ft2_avg]
        dict_plot['scaled'] = [lfreq,ft1_avg_new]
        return dict_plot


# match power spectra for two maps
def amplitude_match(map_1,map_2,step=0.01,reso=None,lpfiltb=False,
                    lpfilta=False,ref=False,plot=False):
    '''
    Scale amplitudes to the average in each resolutions shell
    
    Arguments:
        *step : shell width (1/A)
    '''
    #fourier transform
    ft1 = map_1.fourier_transform()
    ft2 = map_2.fourier_transform()
    ft1.fullMap[:] = fftshift(ft1.fullMap,axes=(0,1))
    ft2.fullMap[:] = fftshift(ft2.fullMap,axes=(0,1))
    dict_plot = {}
    #low pass filter before scaling?
    if reso != None:
        cutoff1 = map_1.apix[0]/float(reso)
        cutoff2 = map_2.apix[0]/float(reso)
        if lpfiltb and not lpfilta:
            print 'Lowpass filtering'
            ftfilter1 = array_utils.tanh_lowpass(map_1.fullMap.shape,
                                                 cutoff=cutoff1, fall=0.2)
            ftfilter2 = array_utils.tanh_lowpass(map_2.fullMap.shape,
                                                 cutoff=cutoff2, fall=0.2)
            ft1 = map_1.apply_filter(ft1,ftfilter1,inplace=True)
            ft2 = map_2.apply_filter(ft2,ftfilter2,inplace=True)
            del ftfilter1, ftfilter2
    gc.collect()
    # min dimension
    size1 = min(map_1.x_size(),map_1.y_size(),map_1.z_size())
    size2 = min(map_2.x_size(),map_2.y_size(),map_2.z_size())
    #make shells for scaling (1/resolution)
    dist1 = array_utils.make_fourier_shell(map_1.fullMap.shape,
                                           fftshift=True)/map_1.apix[0]
    dist2 = array_utils.make_fourier_shell(map_2.fullMap.shape,
                                           fftshift=True)/map_2.apix[0]
    if step is None: step = 1./min(size1*map_1.apix[0],size2*map_2.apix[0])
    else: step = max(1./min(size1*map_1.apix[0],size2*map_2.apix[0]),step)
    maxlevel = 0.5/min(map_1.apix[0],map_2.apix[0])
    gc.collect()
    #scale amplitudes
    dict_plot = scale_amplitudes(ft1.fullMap,ft2.fullMap,dist1,dist2=dist2,
                     maxlevel=maxlevel,step=step,ref=ref,plot=True)
    # low pass filter after?
    #low pass filter after scaling
    if reso != None:
        if lpfilta and not lpfiltb:
            ftfilter1 = array_utils.tanh_lowpass(cutoff=cutoff1, fall=0.2)
            ftfilter2 = array_utils.tanh_lowpass(cutoff=cutoff2, fall=0.2)
            ft1 = map_1.apply_filter(ft1,ftfilter1,inplace=True)
            ft2 = map_2.apply_filter(ft2,ftfilter2,inplace=True)
            del ftfilter1, ftfilter2
    
    ft1.fullMap[:] = ifftshift(ft1.fullMap,axes=(0,1))
    ft2.fullMap[:] = ifftshift(ft2.fullMap,axes=(0,1))
    map1_scaled = map_1.inv_fourier_transform(ft1, 
                                    output_shape=map_1.fullMap.shape,
                                    output_dtype=np.float32)
    map2_scaled = map_2.inv_fourier_transform(ft2, 
                                    output_shape=map_2.fullMap.shape,
                                    output_dtype=np.float32)
        
    return map1_scaled,map2_scaled,dict_plot 

def calculate_shell_correlation(shell1,shell2):
    cov_ps1_ps2 = shell1*np.conjugate(shell2)
    sig_ps1 = shell1*np.conjugate(shell1)
    sig_ps2 = shell2*np.conjugate(shell2)
    cov_ps1_ps2 = np.sum(np.real(cov_ps1_ps2))
    var_ps1 = np.sum(np.real(sig_ps1))
    var_ps2 = np.sum(np.real(sig_ps2))
    #skip shells with no variance
    if np.round(var_ps1,15) == 0.0 or np.round(var_ps2,15) == 0.0: 
        fsc = 0.0
    else: fsc = cov_ps1_ps2/(np.sqrt(var_ps1*var_ps2))
    return fsc

def calculate_fscavg(listfreq,listfsc,listnsf,
                              map_apix,
                              maxRes=None,minRes=None,
                              list_weights=None):
    if maxRes is None:
        maxfreq_index = len(listfreq)
    else:
        maxfreq = 1./maxRes
        print maxfreq
        maxfreq_index = np.searchsorted(listfreq,maxfreq,side='right')
    if minRes is None:
        minfreq_index = 0
    else:
        minfreq = 1./minRes
        minfreq_index = np.searchsorted(listfreq,minfreq)
        
    maxfreq_index = min(maxfreq_index,len(listfreq))
    weighted_fsc_sum = 0.0
    sum_nsf = 0.
    for ind in xrange(minfreq_index,maxfreq_index):
        print listfreq[ind], listfsc[ind],listnsf[ind]
        weighted_fsc_sum += listfsc[ind]*listnsf[ind]
        sum_nsf += listnsf[ind] 
    FSCavg = weighted_fsc_sum/sum_nsf
    return FSCavg

def calculate_fsc(ftarr1,ftarr2,dist1,dist2=None,
                     step=None,
                     maxlevel=None,
                     map_apix=None,
                     reso=None,
                     plot=False):
    list_freq = []
    list_fsc = []
    list_nsf = []
    nc = 0
    x = 0.0
        #for grids of different dimensions
    if dist2 is None: dist2 = dist1
    #Assume maxlevel is N/2 if maxlevel is None
    if step is None: 
        assert compare_tuple(ftarr1.shape, ftarr2.shape)
        maxlevel = ftarr1.shape[0]//2
    #Assume step=1 and dist is in range 0-N/2, if step is None
    if step is None: 
        assert compare_tuple(ftarr1.shape, ftarr2.shape)
        step = 1.
    highlevel = x+step
    
    while (x<maxlevel):
        fshell_indices = (dist1 < min(maxlevel,highlevel)) & (dist1 >= x)
        fsc = calculate_shell_correlation(ftarr1[fshell_indices],ftarr2[fshell_indices])
        #print highlevel, fsc
        list_freq.append(x)
        list_fsc.append(fsc)
        num_nonzero_avg = \
                min(np.count_nonzero(ftarr1[fshell_indices]), \
                    np.count_nonzero(ftarr2[fshell_indices]))
        list_nsf.append(num_nonzero_avg)
        x = highlevel
        highlevel = x+step
    if step == 1. and not map_apix is None:
        list_freq = np.array(list_freq)/(ftarr1.shape[0]*map_apix)
    
    listfreq, listfsc, listnsf = zip(*sorted(zip(list_freq, list_fsc, list_nsf)))
    return listfreq,listfsc,listnsf

    # FSC for two maps
def fsc(map_1,map_2,reso=None,plot=False,
        lpfiltb=False,lpfilta=False):
    #fourier transform
    ft1 = map_1.fourier_transform()
    ft2 = map_2.fourier_transform()
    ft1.fullMap[:] = fftshift(ft1.fullMap,axes=(0,1))
    ft2.fullMap[:] = fftshift(ft2.fullMap,axes=(0,1))
    gc.collect()
    #low pass filter before scaling?
    if reso != None:
        cutoff1 = map_1.apix[0]/float(reso)
        cutoff2 = map_2.apix[0]/float(reso)
        if lpfiltb and not lpfilta:
            ftfilter1 = array_utils.tanh_lowpass(map_1.fullMap.shape,
                                                 cutoff=cutoff1, fall=0.2)
            ftfilter2 = array_utils.tanh_lowpass(map_2.fullMap.shape,
                                                 cutoff=cutoff2, fall=0.2)
            ft1 = map_1.apply_filter(ft1,ftfilter1,inplace=True)
            ft2 = map_2.apply_filter(ft2,ftfilter2,inplace=True)
            del ftfilter1, ftfilter2
    # min dimension
    size1 = min(map_1.x_size(),map_1.y_size(),map_1.z_size())
    size2 = min(map_2.x_size(),map_2.y_size(),map_2.z_size())
    #make shells for scaling (0 - N/2)
    dist1 = array_utils.make_fourier_shell(map_1.fullMap.shape,
                                           fftshift=True,
                                           normalise=False)
    dist2 = array_utils.make_fourier_shell(map_2.fullMap.shape,
                                           fftshift=True,
                                           normalise=False)
    dist1 = np.round(dist1,0)
    dist2 = np.round(dist2,0)
    #step = 1./min(size1*map_1.apix[0],size2*map_2.apix[0])
    maxlevel = 0.5/max(map_1.apix[0],map_2.apix[0])
    gc.collect()
    #FSC
    listfreq,listfsc, listnsf = calculate_fsc(ft1.fullMap,ft2.fullMap,
                                              dist1,dist2=dist2,step=None,
                                              maxlevel=maxlevel,
                                              map_apix = max(map_1.apix[0],map_2.apix[0]),
                                              reso=reso,
                                              plot=plot)
    if plot:
        dict_plot = {}
        dict_plot['FSC'] = listfsc
        dict_plot['Freq'] = listfreq
        Filter.plot_raps(dict_plot,plotfile="fsc.png")
    #avg_fsc = np.sum(listfsc)/len(listfsc)    
    return listfreq,listfsc, listnsf

