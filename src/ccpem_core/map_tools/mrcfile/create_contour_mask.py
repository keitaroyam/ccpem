import os, sys
import mrcfile
from ccpem_core.map_tools import array_utils
import argparse
import numpy as np


parser = argparse.ArgumentParser()
parser.add_argument('-m', '--map', help='Map file',
                    default=None)
parser.add_argument('-t', '--thr', type=float, help='Contour threshold',
                    default=None)

args = parser.parse_args()
mapfile = args.map
assert os.path.isfile(mapfile)
mrcobj=mrcfile.open(mapfile,mode='r')

contour = args.thr
maskarray = array_utils.create_contour_mask(mrcobj.data,contour)

newmapfile = os.path.splitext(args.map)[0]+'_mask.mrc'
newmap = mrcfile.new(newmapfile,overwrite=True)
newmap.set_data(np.array(maskarray*1,dtype=np.int8))
newmap.header.origin = mrcobj.header.origin
newmap.header.cella = mrcobj.header.cella
newmap.voxel_size = mrcobj.voxel_size
newmap.close()
