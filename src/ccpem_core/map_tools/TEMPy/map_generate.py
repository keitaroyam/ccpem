import os
import mrcfile
from TEMPy.mapprocess import MapEdit, Filter
from TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.class_arg import TempyParser
from ccpem_core import ccpem_utils
import numpy as np
mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False

def blur_model(p,res=4.0,emmap=None):
    print 'Reading the model'
    structure_instance=PDBParser.read_PDB_file('pdbfile',p,hetatm=True,water=False)
    print 'Generating map from the model'
    blurrer = StructureBlurrer()
    if res is None: sys.exit('Map resolution required..')
    modelmap = blurrer.gaussian_blur_real_space(structure_instance, 
                                              res,sigma_coeff=0.225,
                                              densMap=emmap,normalise=True) 
    return modelmap, structure_instance    

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r')
        if np.any(np.isnan(mrcobj.data)):
            sys.exit('Map has NaN values: {}'.format(map_path))
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.set_apix_as_tuple()
        emmap.fix_origin()
    return emmap

def write_mapfile(mapobj,map_path):
    #TEMPy map
    if not mrcfile_import and mapobj.__class__.__name__ == 'Map':
        mapobj.write_to_MRC_file(map_path)
    #mrcfile map
    elif mrcfile_import:
        newmrcobj = mrcfile.new(map_path,overwrite=True)
        mapobj.set_newmap_data_header(newmrcobj)
        newmrcobj.close()
    #tempy mapprocess map
    else:
        newmrcobj = Map(np.zeros(mapobj.fullMap.shape), 
                        list(mapobj.origin), 
                        mapobj.apix, 'mapname')
        mapobj.set_newmap_data_header(newmrcobj)
        newmrcobj.update_header()
        newmrcobj.write_to_MRC_file(map_path)
        
def generate_map(model_path, map_path, resolution):
    emmap = read_mapfile(map_path)
    model_name = os.path.splitext(os.path.basename(model_path))[0]
    modelmap_file = model_name+'_syn.mrc'
    map_name = os.path.splitext(os.path.basename(map_path))[0]
    modelmap, structure_instance = blur_model(model_path,res=resolution,emmap=emmap)
    write_mapfile(Filter(modelmap),modelmap_file)
    
        
def main():
    tp = TempyParser()
    tp.generate_args()

    # Command line option
    m = tp.args.inp_map
    p = tp.args.pdb
    res = tp.args.res
    thr = None
    thr = tp.args.thr
    if res is None:
        res = 10.0

    if m is None:
        ccpem_utils.print_warning('Input map required')
        return
    if not os.path.exists(m):
        ccpem_utils.print_warning('Input map {} not found'.format(m))
        return
    if p is None:
        ccpem_utils.print_warning('Input PDB required')
        return
    if not os.path.exists(p):
        ccpem_utils.print_warning('Input PDB {} not found'.format(p))
        return

    generate_map(model_path=p,
                map_path=m,
                resolution=res)

if __name__ == '__main__':
    main()
