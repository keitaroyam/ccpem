import os
import mrcfile
from TEMPy.mapprocess import MapEdit
from TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
from TEMPy.class_arg import TempyParser
from ccpem_core import ccpem_utils

def blur_model(p, res=10.0, emmap=False):
    pName = os.path.basename(p).split('.')[0]
    structure_instance=PDBParser.read_PDB_file(
        pName,
        p,
        hetatm=True,
        water=False)
    blurrer = StructureBlurrer()
    modelmap = blurrer.gaussian_blur_real_space(
        structure_instance,
        res,
        sigma_coeff=0.187,
        densMap=emmap,
        normalise=True) 
    return pName,modelmap

def create_mask(pdb_path, map_path, resolution=10.0,
                threshold=None):
    # Read map
    mrcobj=mrcfile.open(map_path, mode='r')
    emmap = MapEdit(mrcobj)

    # Map from model
    pName, modelmap = blur_model(pdb_path,
                                 res=resolution,
                                 emmap=emmap)

    # Mask of modelmap
    mask = modelmap.fullMap>0
    if not threshold is None:
        mask = mask*(emmap.fullMap>threshold)

    # Save mask
    maskmap = emmap.copy()
    maskmap.fullMap = (mask*1).astype('int8')
    mName = os.path.basename(map_path).split('.')[0]
    mapdir = os.path.dirname(map_path)
    new_maskmap = mrcfile.new(os.path.join(mapdir,mName+'_mask.mrc'),
                         overwrite=True)
    new_map = mrcfile.new(os.path.join(mapdir,mName+'_masked.mrc'),
                         overwrite=True)
    maskedmap = emmap.copy()
    maskedmap.fullMap = mask*emmap.fullMap

    # Update data and header
    maskmap.set_newmap_data_header(new_maskmap)
    maskedmap.set_newmap_data_header(new_map)
    emmap.mrc.close()
    new_maskmap.close()
    new_map.close()

def main():
    tp = TempyParser()
    tp.generate_args()

    # Command line option
    m = tp.args.inp_map
    p = tp.args.pdb
    res = tp.args.res
    thr = None
    thr = tp.args.thr
    if res is None:
        res = 10.0

    if m is None:
        ccpem_utils.print_warning('Input map required')
        return
    if not os.path.exists(m):
        ccpem_utils.print_warning('Input map {} not found'.format(m))
        return
    if p is None:
        ccpem_utils.print_warning('Input PDB required')
        return
    if not os.path.exists(p):
        ccpem_utils.print_warning('Input PDB {} not found'.format(p))
        return

    create_mask(pdb_path=p,
                map_path=m,
                resolution=res,
                threshold=thr)

if __name__ == '__main__':
    main()
