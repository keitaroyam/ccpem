#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils


class SymExpand(task_utils.CCPEMTask):
    '''
    Local symmetry expansion using Refmac
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='SymExpand',
        author='Brown A, Long F, Nicholls RA, Long F, Toots J, Murshudov G',
        version='5.8.0103',
        description=(
            'Symmetry expansion using REFMAC.  '
            'It copies changes from the first chain (A) into all other chains.  '),
        short_description=(
            'Copy changes from the first chain (A) into all other chains.  '),
        references=None)

    commands = {'refmac': which(program='refmac5')}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        #
        super(SymExpand, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-xyzin',
            '--xyzin',
            help='''Input PDB file''',
            type=str,
            metavar='Input PDB file',
            default=None)
        #
        parser.add_argument(
            '-xyzout',
            '--xyzout',
            help='''Output PDB file''',
            metavar='Output PDB file',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-libin',
            '--libin',
            help='Ligand library (CIF)',
            metavar='Ligand library (CIF)',
            type=str,
            default=None)
        #
        return parser

    def run_pipeline(self, run=True, job_id=None, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        self.log_path = os.path.join(self.job_location,
                                     'stdout.txt')

        # Convert map to mtz and blur/sharpen

        # On finish process data for plot
        xyzout_path = os.path.join(self.job_location,
                                   'symmetrised.pdb')
        if self.args.xyzout.value:
            if len(self.args.xyzout.value) > 1:
                xyzout_path = self.args.xyzout.value

        libin_path = None
        if self.args.libin.value:
            if os.path.exists(self.args.libin.value):
                libin_path = self.args.libin.value

        #
        #
        self.process_symExpand = RefmacSymExpand(
            command=self.commands['refmac'],
            name='Symmetry expansion',
            job_location = self.job_location,
            xyzin = self.args.xyzin.value,
            xyzout = xyzout_path,
            libin = libin_path)

        # Set pipeline
        pl = [[self.process_symExpand.process]]

        if run:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                database_path=self.database_path,
                db_inject=db_inject,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose)
            self.pipeline.start()

    def validate_args(self):
        # Now do at gui level
        return True


class RefmacSymExpand(object):
    '''
    Refmac process to expand symmetry in the PDB file
    '''

    def __init__(self,
                 command,
                 name,
                 job_location,
                 xyzin=None,
                 xyzout=None,
                 libin=None):

      self.command = command
      self.job_location = job_location
      self.name = name
      self.xyzin = xyzin
      self.xyzout = xyzout
      self.libin = libin
      self.args = None
      self.stdin = None

      self.set_args()
      self.set_stdin()
      self.process = process_manager.CCPEMProcess(
          name = self.name,
          command = self.command,
          args = self.args,
          location = self.job_location,
          stdin = self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.xyzin]
        self.args += ['xyzout', self.xyzout]
        if self.libin is not None:
            self.args += ['libin', self.libin]


    def set_stdin(self):
        self.stdin = 'make hydr no\n'
        self.stdin += 'ncsr expand\n'
        self.stdin += 'ncsr global\n'
        self.stdin += 'end\n\n'
