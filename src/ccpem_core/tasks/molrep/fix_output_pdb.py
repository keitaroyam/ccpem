#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os.path

def main():
    '''
    Strip problematic Molrep PDB separators starting with '#'

    This is done in Python (rather than a command line tool such as sed) to
    make it work more easily on all platforms.
    '''
    output_pdb_name = 'molrep.pdb'
    backup_pdb_name = 'molrep.pdb.orig'
    print "Backing up original output file {} to {}".format(output_pdb_name, backup_pdb_name)
    os.rename(output_pdb_name, backup_pdb_name)
    print "Stripping bad separators from {}...".format(output_pdb_name)
    line_count = 0
    bad_line_count = 0
    with open(backup_pdb_name) as orig, open(output_pdb_name, 'w') as output:
        for line in orig:
            line_count += 1
            if not line.startswith('#'):
                output.write(line)
            else:
                bad_line_count += 1
    print "Processed {} lines, removed {} bad separators".format(line_count, bad_line_count)

if __name__ == "__main__":
    main()
