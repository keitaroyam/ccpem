#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
import tempfile
import unittest

from . import fix_output_pdb

molrep_pdb = 'molrep.pdb'
backup_pdb = 'molrep.pdb.orig'

contents = """Fake MOLREP output file
Line 2
# Bad separator to be removed
Line 3
"""

class Test(unittest.TestCase):
    '''
    Unit test for Molrep output PDB fixing script
    '''

    def setUp(self):
        '''
        Setup test data and output directories.
        '''
        self.test_dir = tempfile.mkdtemp()
        os.chdir(self.test_dir)
        with open(molrep_pdb, 'w') as test_file:
            test_file.write(contents)

    def tearDown(self):
        '''
        Remove temporary data.
        '''
        if os.path.exists(path=self.test_dir):
            shutil.rmtree(self.test_dir)

    def test_molrep_output_pdb_fix(self):
        fix_output_pdb.main()
        assert os.path.isfile(molrep_pdb)
        assert os.path.isfile(backup_pdb)
        with open(molrep_pdb) as fixed:
            fixed_contents = fixed.read()
            assert '#' not in fixed_contents
            orig_lines = contents.split('\n')
            fixed_lines = fixed_contents.split('\n')
            assert orig_lines[0] == fixed_lines[0]
            assert orig_lines[1] == fixed_lines[1]
            assert orig_lines[3] == fixed_lines[2]
        with open(backup_pdb) as original:
            lines = original.read()
            assert '#' in lines
            assert lines == contents


if __name__ == '__main__':
    unittest.main()
