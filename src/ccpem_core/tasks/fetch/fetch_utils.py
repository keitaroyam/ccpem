#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import sys
import os
import json
from urllib import urlopen
from urllib import urlretrieve
import utils
import urllib2

def read_emdb_JSON_eid(eid):
    json_eid = 'http://www.ebi.ac.uk/pdbe/emdb/search/?q=emd-'+eid+'&rows=-1'
    return json.loads(urlopen(json_eid).read())

def get_pdb_from_pdb(pdb_id, job_location=None):
    pdb_id = pdb_id.lower()
    url = 'http://www.rcsb.org/pdb/files/%s.pdb' % pdb_id
    print url
    print get_file_from_url(url=url,
                            job_location=job_location,
                            filename='%s.pdb' % pdb_id)

def get_bundle_from_emdb(emdb_id, get_pdb=True, job_location=None):
    get_bundle = False
    if get_bundle:
        url = 'http://www.ebi.ac.uk/pdbe/entry/download/EMD-%s/bundlezip' % emdb_id
        print url
        print get_file_from_url(url=url,
                                job_location=job_location,
                                filename='%s_bundle.zip' % emdb_id)
    if get_pdb:
        print 'TEST'
        json_url = 'http://www.ebi.ac.uk/pdbe/emdb/search/?q=emd-'+emdb_id+'&rows=-1'
        json_emdb = json.loads(urlopen(json_url).read())
        print json_emdb['ResultSet']['Result'][0].keys()
        for pdb_id in json_emdb['ResultSet']['Result'][0]['FittedPDBs']:
            print pdb_id
            get_pdb_from_pdb(pdb_id=pdb_id, job_location=job_location)


def get_file_from_url(url, job_location, filename):
    try:
        urllib2.urlopen(url)
        url_exists = True
    except urllib2.HTTPError, e:
        print(e.code)
        url_exists = False
    except urllib2.URLError, e:
        print(e.args)
        url_exists = False
    if url_exists:
        if job_location is None:
            job_location = os.getcwd()
        filename = os.path.join(job_location,
                                filename)
        urlretrieve(url, filename)
        return filename
    else:
        return None


def main():
    utils.print_header(message='EMDB JSON meta data parser')
#     eid = '2638'
#     JSON_eid = read_emdb_JSON_eid(eid=eid)
#     print JSON_eid['ResultSet']['Result'][0].keys()
#     resolution = JSON_eid['ResultSet']['Result'][0]['Resolution']
#     print 'Resolution : ', resolution
#     utils.print_footer()

#     utils.print_header(message='Fetch PDB')
#     get_pdb_from_pdb(pdb_id='1zen')
    
    utils.print_header(message='Fetch EMDB')
    get_bundle_from_emdb(emdb_id='8192')

if (__name__ == '__main__'):
    main()