import sys
import re
import os
import time
import pyrvapi
import fileinput
import collections
import shutil
import numpy as np
import math
from lxml import etree
from ccpem_core import process_manager
from ccpem_core import ccpem_utils
from ccpem_core.tasks.refmac import refmac_results
from ccpem_core.ccpem_utils.ccp4_log_parser import smartie
from ccpem_core.data_model import metadata_utils
import pyrvapi_ext as API

# Resolution label (equivalent to <4SSQ/LL>
ang_min_one = (ur'Resolution (\u00c5-\u00B9)').encode('utf-8')
angstrom_label = (ur'Resolution (\u00c5)').encode('utf-8')


class PipelineResultsViewer(object):
    # class ResultViewer(object):
    '''
    Get Nautilus results from program.xml
    '''

    def __init__(self,
                 pipeline=None,
                 pipeline_path=None,
                 xmlfilename=None):
        if pipeline_path is not None:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=None,
                import_json=pipeline_path)
        else:
            self.pipeline = pipeline
        self.refine_process = []
        self.nautilus_process = []
        self.buccaneer_process = []
        self.mrc2mtz_process = None
        self.xmlfilename = []
        # setup doc
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        self.directory = os.path.join(self.pipeline.location, 'report')
        self.index = os.path.join(self.directory, 'index.html')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        xml_relpath = os.path.join(self.directory, 'output.xml')
        # setup pages
        pyrvapi.rvapi_init_document(self.pipeline.location, self.directory, self.pipeline.location,
                                    1, pyrvapi.RVAPI_LAYOUT_Tabs,
                                    share_jsrview, None, 'index.html', None, xml_relpath)
        pyrvapi.rvapi_add_header('Parsed output')
        pyrvapi.rvapi_flush()

        # get the process that ran
        for jobs in self.pipeline.pipeline:
            for job in jobs:
                if job.name == 'Map to MTZ':
                    self.mrc2mtz_process = job
                if 'Refmac refine (global)' in job.name:
                    self.refine_process.append(job)
                if 'Refmac refine (local)' in job.name:
                    self.refine_process.append(job)
                if 'Nautilus build' in job.name:
                    self.nautilus_process.append(job)
                if 'Buccaneer build' in job.name:
                    self.buccaneer_process.append(job)

        # get xml file from Buccaneer/Nautilus
        if xmlfilename is not None:
            if isinstance(xmlfilename, str):
                self.xmlfilename.append(os.path.join(
                    self.pipeline.location, xmlfilename))
            else:
                self.xmlfilename = list.copy(xmlfilename)
        else:
            # print "XML filename not specified. Trying to use default filename
            # 'program.xml'"
            if len(self.nautilus_process) != 0:
                for i in range(1, len(self.nautilus_process) + 1):
                    infile = 'program' + str(i) + '.xml'
                    self.xmlfilename.append(os.path.join(
                        self.pipeline.location, infile))
            if len(self.buccaneer_process) != 0:
                for i in range(1, len(self.buccaneer_process) + 1):
                    infile = 'program' + str(i) + '.xml'
                    self.xmlfilename.append(os.path.join(
                        self.pipeline.location, infile))

        # set results table and graphs
        pipeline_tab = 'pipeline_tab'
        if len(self.nautilus_process) != 0:
            build_data = self.GetXML2Table('Nautilus', pipeline_tab)
            refine_data = self.set_refine_results()
            self.buildref_summary_graph(
                'Nautilus', build_data, refine_data, pipeline_tab)
        if len(self.buccaneer_process) != 0:
            build_data = self.GetXML2Table('Buccaneer', pipeline_tab)
            refine_data = self.set_refine_results()
            self.buildref_summary_graph(
                'Buccaneer', build_data, refine_data, pipeline_tab)
        if self.mrc2mtz_process is not None:
            self.set_mrctomtz_results()

    def buildref_summary_graph(self,
                               process=None,
                               builddata=None,
                               refinedata=None,
                               pipeline_tab=None):
        '''
        Make build completeness & avg FSC graph for Buccaneer (in the same plot)
        Make build amount & avg FSC graph for Nautilus (FSC in separate plot)
        '''
        # make graph widget
        graphWid1 = API.loggraph(pipeline_tab)
        brdata = API.graph_data(graphWid1, 'Build & Refinement Summary')
        dx = API.graph_dataset(brdata, 'Cycle', 'Cycle')
        dy3 = API.graph_dataset(brdata, 'Average FSC', 'y3', isint=False)
        # set different labels for Nautilus/Buccaneer
        if process == 'Nautilus':
            dy1 = API.graph_dataset(brdata, 'Residues build', 'y2')
            dy2 = API.graph_dataset(brdata, 'Residues sequenced', 'y2')
            y1col = 'ResBuilt'
            y2col = 'ResSeq'
            yaxlabel = 'Amount built'
        if process == 'Buccaneer':
            dy1 = API.graph_dataset(
                brdata, 'Completeness by residues', 'y1', isint=False)
            dy2 = API.graph_dataset(
                brdata, 'Completeness by chains', 'y2', isint=False)
            y1col = 'CompByRes'
            y2col = 'CompByChain'
            yaxlabel = 'Percentage (%)'
        xmax = len(builddata)
        # add data
        for i in range(0, xmax):
            dx.add_datum(i + 1)
            if process == 'Buccaneer':
                dy1.add_datum(float(builddata[i][y1col].text) * 100)
                dy2.add_datum(float(builddata[i][y2col].text) * 100)
            else:
                dy1.add_datum(int(builddata[i][y1col].text))
                dy2.add_datum(int(builddata[i][y2col].text))
            dy3.add_datum(
                float(refinedata[i].results_summary['FSC average'][1]) * 100)
        # make plot, same plot for Buccaneer(all %), separate for Nautilus
        plot1 = API.graph_plot(
            graphWid1, 'Completeness after each cycle', 'Cycle', yaxlabel)
        plot1.reset_xticks()
        for i in range(0, xmax, 1):
            plot1.add_xtick(i + 1, '%d' % (i + 1))
        dx_y1 = API.plot_line(plot1, brdata, dx, dy1)
        dx_y2 = API.plot_line(plot1, brdata, dx, dy2)
        if process == 'Buccaneer':
            dx_y3 = API.plot_line(plot1, brdata, dx, dy3)
        if process == 'Nautilus':
            plot2 = API.graph_plot(
                graphWid1, 'Average FSC after each cycle', 'Cycle', 'Percentage (%)')
            plot2.reset_xticks()
            for i in range(0, xmax, 1):
                plot2.add_xtick(i + 1, '%d' % (i + 1))
            dx_dy3 = API.plot_line(plot2, brdata, dx, dy3)
        API.flush()

    def get_reference(self, process):
        '''
        Get references to put in a section in results tab
        '''
        if process == 'Nautilus':
            file_input = self.nautilus_process[0].stdout
        if process == 'Buccaneer':
            file_input = self.buccaneer_process[0].stdout
        if process == 'Refinement':
            file_input = self.refine_process[0].stdout

        found_reference = False
        reference_list = []
        with open(file_input) as f:
            for line in f:
                if "$TEXT:Reference" in line:
                    reference = []
                    found_reference = True
                    continue
                if found_reference:
                    line = line.strip('\n')
                    if line != '':
                        if '$$' not in line:
                            if 'SUMMARY_END' in line:  # for nautilus/buccaneer
                                break
                            reference.append(line)
                        else:
                            if len(reference) != 0:
                                reference_list.append(reference)
                            reference = []

                if 'Data line' in line:  # for refmac
                    break
        f.close()
        return reference_list

    def GetXML2Table(self, process, pipeline_tab):
        '''
        Get data from XML output and fill table
        '''
        data = []
        tab_name = ''
        len(self.xmlfilename)
        for infile in self.xmlfilename:
            # print infile
            tree = etree.parse(infile)  # self.xmlfilename)
            i_entries = collections.OrderedDict()
            if process == 'Nautilus':
                tab_name = 'Nautilus Build Results'
                for child in tree.findall('Final'):
                    i_entries['FragBuilt'] = child.find('FragmentsBuilt')
                    i_entries['ResBuilt'] = child.find('ResiduesBuilt')
                    i_entries['ResSeq'] = child.find('ResiduesSequenced')
                    i_entries['ResLongFrag'] = child.find(
                        'ResiduesLongestFragment')
                data.append(i_entries)
            if process == 'Buccaneer':
                tab_name = 'Buccaneer Build Results'
                for child in tree.findall('Final'):
                    i_entries['CompByRes'] = child.find(
                        'CompletenessByResiduesBuilt')
                    i_entries['CompByChain'] = child.find(
                        'CompletenessByChainsBuilt')
                    i_entries['ChainsBuilt'] = child.find('ChainsBuilt')
                    i_entries['FragBuilt'] = child.find('FragmentsBuilt')
                    i_entries['ResBuilt'] = child.find('ResiduesBuilt')
                    i_entries['ResSeq'] = child.find('ResiduesSequenced')
                    i_entries['UniqRes'] = child.find('ResiduesUnique')
                    i_entries['ResLongFrag'] = child.find(
                        'ResiduesLongestFragment')
                data.append(i_entries)
        build_sec = 'built_sec'
        build_table = 'built_table'
        # set tabs and sections
        pyrvapi.rvapi_add_tab(pipeline_tab, tab_name, True)
        pyrvapi.rvapi_add_section(
            build_sec, 'Please reference', pipeline_tab, 0, 0, 1, 1, False)
        reference_text = self.get_reference(process)
        # add reference to cite found from logfile
        r = 0
        for i in range(0, len(reference_text)):
            pyrvapi.rvapi_add_text(str(i + 1) + ') ', build_sec, r, 0, 1, 1)
            for j in range(0, len(reference_text[i])):
                pyrvapi.rvapi_add_text(
                    reference_text[i][j], build_sec, r, 1, 1, 1)
                r += 1
        pyrvapi.rvapi_flush()
        # set table headers
        if process == 'Nautilus':
            pyrvapi.rvapi_add_table(
                build_table, 'Nautilus Build Summary', pipeline_tab, 1, 0, 1, 1, False)
            pyrvapi.rvapi_put_horz_theader(
                build_table, 'Run #', 'nth run of cnautilus', 0)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Fragments<br>built',
                                           'total number of fragments built', 1)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Residues<br>built',
                                           'total number of residues built', 2)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Residues<br>sequenced',
                                           'total number of residues sequenced', 3)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Residues<br>in longest<br>fragment',
                                           'total number of residues in longest fragment', 4)
        if process == 'Buccaneer':
            pyrvapi.rvapi_add_table(
                build_table, 'Buccaneer Build Summary', pipeline_tab, 1, 0, 1, 1, False)
            pyrvapi.rvapi_put_horz_theader(
                build_table, 'Run #', 'nth run of cbuccaneer', 0)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Completeness<br>by<br>residues<br>built (%)',
                                           '(Number of unique residues allocated to chain)/(Residues built)', 1)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Completeness<br>by<br>chains<br>built (%)',
                                           '(Number of unique residues allocated to chain)/(Total sequence length provided)', 2)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Chains<br>built',
                                           'total number of chains built', 3)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Fragments<br>built',
                                           'total number of fragments built', 4)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Residues<br>built',
                                           'total number of residues built', 5)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Residues<br>sequenced',
                                           'total number of residues sequenced', 6)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Unique<br>residues<br>allocated',
                                           'total number of unique residues allocated to chain', 7)
            pyrvapi.rvapi_put_horz_theader(build_table, 'Residues<br>in longest<br>fragment',
                                           'total number of residues in longest fragment', 8)
        pyrvapi.rvapi_flush()
        # fill table
        i = 0  # row
        for entries in data:
            j = 0  # column
            for name in entries:
                if j == 0:
                    pyrvapi.rvapi_put_table_string(
                        build_table, str(i + 1), i, j)  # fill in cycle number
                    j += 1  # %0.1f %
                    if name == 'CompByRes':
                        value = float(entries[name].text) * 100
                        pyrvapi.rvapi_put_table_string(
                            build_table, '%0.1f' % value, i, j)  # fill in % value
                    else:
                        pyrvapi.rvapi_put_table_string(
                            build_table, entries[name].text, i, j)  # fill in built values
                else:
                    if name == 'CompByChain':
                        value = float(entries[name].text) * 100
                        pyrvapi.rvapi_put_table_string(
                            build_table, '%0.1f' % value, i, j)  # fill in % value
                    else:
                        pyrvapi.rvapi_put_table_string(
                            build_table, entries[name].text, i, j)  # fill in built values
                j += 1
            i += 1
        pyrvapi.rvapi_flush()
        return data

    def set_refine_results(self):
        '''
        set up the refinement statistics tab
        put together the summary of every run
        '''
        # set tab and sections
        refine_tab = 'refine_tab'
        reference_sec = 'reference_sec'
        pyrvapi.rvapi_add_tab(
            refine_tab, 'Refmac Refinement statistics', False)
        pyrvapi.rvapi_add_section(
            reference_sec, 'Please reference', refine_tab, 0, 0, 1, 1, False)
        reference_text = self.get_reference('Refinement')
        # add references
        r = 0
        for i in range(0, len(reference_text)):
            pyrvapi.rvapi_add_text(str(i + 1) + ') ',
                                   reference_sec, r, 0, 1, 1)
            for j in range(0, len(reference_text[i])):
                pyrvapi.rvapi_add_text(
                    reference_text[i][j], reference_sec, r, 1, 1, 1)
                r += 1
        pyrvapi.rvapi_flush()

        refine_results = [None] * len(self.refine_process)
        ri = 0
        keep_col_ncyc = ['Ncyc', 'Rfact', 'rmsBOND', 'rmsANGL', 'rmsCHIRAL']
        keep_col_fsc = [angstrom_label, 'FSCwork',
                        'SigmaA_Fc1', 'CorrFofcWork']
        columnsIDs = {}
        ncyc_plot_xmax = 0
        # set and fill table for graphs
        #testprocess = self.refine_process[1:2]
        for jobs in self.refine_process:  # testprocess: #
            fix_stars(stdout=jobs.stdout)
            refine_results[ri] = refmac_results.RefmacRefineResultsParser(
                stdout=jobs.stdout)
            if refine_results[ri] is not None:
                for col in refine_results[ri].ncyc_table.columns:
                    if col not in keep_col_ncyc:
                        refine_results[ri].ncyc_table.drop(
                            col, axis=1, inplace=True)
                    else:
                        if col != 'Ncyc':
                            refine_results[ri].ncyc_table.rename_column(
                                col, col + '_run' + str(ri + 1))
                refine_results[ri].ncyc_table.set_index('Ncyc', inplace=True)
                # to fix the weird xmax set by default when xmax of different
                # runs are different
                if refine_results[ri].ncyc_table.shape[0] > ncyc_plot_xmax:
                    # print refine_results[ri].ncyc_table.shape[0]
                    ncyc_plot_xmax = refine_results[ri].ncyc_table.shape[0]
                    # print 'xmax = {0}' .format(ncyc_plot_xmax)

                for col in refine_results[ri].fsc_fom_table.columns:
                    if col not in keep_col_fsc:
                        refine_results[ri].fsc_fom_table.drop(col,
                                                              axis=1,
                                                              inplace=True)
                    else:
                        if col != angstrom_label:  # rename columns add run number
                            refine_results[ri].fsc_fom_table.rename_column(
                                col, col + '_run' + str(ri + 1))
                refine_results[ri].fsc_fom_table.set_index(angstrom_label,
                                                           inplace=True)
                # Set table
                if ri == 0:
                    refine_summary_table = 'refine_summary_table'
                    ref_table_sec = 'refsum_table_section'
                    pyrvapi.rvapi_add_section(
                        ref_table_sec, 'Refinement summary', refine_tab, 1, 0, 1, 1, True)
                    pyrvapi.rvapi_add_table(
                        refine_summary_table, 'Refinement statistics table', ref_table_sec, 0, 0, 1, 1, False)
                    c = 0
                    pyrvapi.rvapi_put_horz_theader(
                        refine_summary_table, 'Run #', 'C' + str(c + 1), c)
                    c += 1
                    for colname in refine_results[ri].results_summary.columns:
                        pyrvapi.rvapi_put_horz_theader(
                            refine_summary_table, colname, 'C' + str(c + 1), c)
                        pyrvapi.rvapi_shape_horz_theader(
                            refine_summary_table, c, '', '', 1, 2)
                        columnsIDs[colname] = c
                        c += 1

                    # last column = weight
                    pyrvapi.rvapi_put_horz_theader(
                        refine_summary_table, 'Weight', 'C' + str(c + 1), c)
                    columnsIDs['Weight'] = c
                    # put start, finish except last column
                    for c in range(len(columnsIDs) - 1):
                        pyrvapi.rvapi_put_table_string(
                            refine_summary_table, 'Start', 0, (c * 2) + 1)
                        pyrvapi.rvapi_put_table_string(
                            refine_summary_table, 'Finish', 0, (c * 2) + 2)

                    #c = 0
                pyrvapi.rvapi_put_table_string(
                    refine_summary_table, str(ri + 1), ri + 1, 0)
                for data in refine_results[ri].results_summary:
                    s1 = refine_results[ri].results_summary[data][0]
                    s2 = refine_results[ri].results_summary[data][1]
                    pyrvapi.rvapi_put_table_string(
                        refine_summary_table, s1, ri + 1, (columnsIDs[data] * 2) - 1)
                    pyrvapi.rvapi_put_table_string(
                        refine_summary_table, s2, ri + 1, (columnsIDs[data] * 2))
                    #c += 1
                    if len(refine_results[ri].weightText) > 1:
                        weightParts = refine_results[ri].weightText.split('E')
                        weightReal = 0.0
                        if len(weightParts) == 2:
                            weightReal = float(
                                weightParts[0]) * (10**int(weightParts[1]))
                            if weightReal > 0:
                                pyrvapi.rvapi_put_table_string(refine_summary_table,
                                                               refine_results[ri].weightText +
                                                               ' (%0.6f)' % weightReal,
                                                               ri + 1,
                                                               (columnsIDs['Weight'] * 2) - 1)
                            else:
                                pyrvapi.rvapi_put_table_string(refine_summary_table,
                                                               refine_results[ri].weightText,
                                                               ri + 1,
                                                               (columnsIDs['Weight'] * 2) - 1)
                pyrvapi.rvapi_flush()
            ri += 1
        # setup graphs
        ncyc_sec = 'sec_cycle'
        pyrvapi.rvapi_add_section(ncyc_sec,
                                  'Refinement summary in graphs ',
                                  refine_tab,
                                  2, 0, 1, 1, False)
        pyrvapi.rvapi_flush()
        #keep_col = keep_col_ncyc[1:]
        #keep_col_final = keep_col_fsc[1:]
        refinement_graphs_id = 'ncyc_graphs_run'
        ncyc_data_id = 'ncyc_data'
        pyrvapi.rvapi_append_loggraph(refinement_graphs_id, ncyc_sec)
        pyrvapi.rvapi_add_graph_data(ncyc_data_id,
                                     refinement_graphs_id,
                                     'Statistics per refinement cycle')
        final_data_id = 'final_data'
        pyrvapi.rvapi_append_loggraph(refinement_graphs_id, ncyc_sec)
        pyrvapi.rvapi_add_graph_data(final_data_id,
                                     refinement_graphs_id,
                                     'Final refinement statistics')

        for ri in range(len(refine_results)):
            refine_results[ri].ncyc_table.set_pyrvapi_graph(
                graph_id=refinement_graphs_id,
                data_id=ncyc_data_id,
                originalXAxis=True,
                step=1)
            refine_results[ri].fsc_fom_table.set_pyrvapi_graph(
                graph_id=refinement_graphs_id,
                data_id=final_data_id,
                originalXAxis=True,
                step=int(len(refine_results[ri].fsc_fom_table) / 7.0),
                ymin=0.0)

        pyrvapi.rvapi_flush()
        return refine_results

        ####
        # this part use the modified version of setup pyrvapi graphs to have different runs in same plot
        # however too messy in one plot if there are many pipeline cycles i.e.25, crowded with legends, can't see much
        # for ri in range(len(refine_results)): #-1, -1, -1): #():len(testprocess)
        #    plotthegraphs(ncyc_data_id,refinement_graphs_id,refine_results[ri].ncyc_table,keep_col,1, ncyc_plot_xmax)
        #    plotthegraphs(final_data_id,refinement_graphs_id, refine_results[ri].fsc_fom_table,keep_col_final,int(len(refine_results[ri].fsc_fom_table) / 7.0 ),None)
        ####
    def set_mrctomtz_results(self):
        '''
        use RefmacPowerSpectrumParser from refmac_results to get power spectrum
        1) Mn(|F|)
        2) Mn(|F|^2)
        3) Var = sqrt(<Fmean**2> - <Fmean>2)
            -> math.sqrt ( Mn|F|^2) - math.pow(Mn|F|, 2))
        '''
        refine_tab = 'refine_tab'
        refinement_graphs_id = 'ncyc_graphs_run'
        mr = None
        mr = refmac_results.RefmacPowerSpectrumParser(
            stdout=self.mrc2mtz_process.stdout)
        if mr is not None:
            if mr.mean_amp_table is not None:
                keep_col_mr = [angstrom_label, 'Mn(|F|)', 'Mn(|F|^2)']
                for col in mr.mean_amp_table.columns:
                    if col not in keep_col_mr:
                        mr.mean_amp_table.drop(col, axis=1, inplace=True)
                mr.mean_amp_table.set_index(angstrom_label, inplace=True)

                # calculate variance
                mr.mean_amp_table['Mn(|F|)^2'] = \
                    mr.mean_amp_table['Mn(|F|)'].apply(
                        lambda x: str(math.pow(float(x), 2)))
                mr.mean_amp_table['Variance'] = \
                    mr.mean_amp_table['Mn(|F|^2)'].astype(float)
                mr.mean_amp_table['Variance'] = \
                    mr.mean_amp_table['Variance'].subtract(
                        mr.mean_amp_table['Mn(|F|)^2'].astype(float))
                mr.mean_amp_table['Variance'] = \
                    mr.mean_amp_table['Variance'].apply(
                        np.sqrt).astype(str)
                mr.mean_amp_table.drop(
                    'Mn(|F|)^2',
                    axis=1,
                    inplace=True)

                mr.mean_amp_table['Log(Mn(|F|))'] = \
                    mr.mean_amp_table['Mn(|F|)'].apply(
                        lambda x: str(math.log(float(x))))
                mr.mean_amp_table['Log(Mn(|F|^2))'] = \
                    mr.mean_amp_table['Mn(|F|^2)'].apply(
                        lambda x: str(math.log(float(x))))
                mr.mean_amp_table['Log(Variance)'] = \
                    mr.mean_amp_table['Variance'].apply(
                        lambda x: str(math.log(float(x))))

                mr.mean_amp_table.drop(
                    'Mn(|F|)',
                    axis=1,
                    inplace=True)
                mr.mean_amp_table.drop(
                    'Mn(|F|^2)',
                    axis=1,
                    inplace=True)
                mr.mean_amp_table.drop(
                    'Variance',
                    axis=1,
                    inplace=True)

                map_data_id = 'map_data'
                pyrvapi.rvapi_append_loggraph(refinement_graphs_id, refine_tab)
                pyrvapi.rvapi_add_graph_data(map_data_id,
                                             refinement_graphs_id,
                                             'Input SF statistics')
                istep = int(len(mr.mean_amp_table) / 7.0)
                if istep == 0:
                    istep = 1
                mr.mean_amp_table.set_pyrvapi_graph(
                    graph_id=refinement_graphs_id,
                    data_id=map_data_id,
                    originalXAxis=True,
                    step=istep)  # int(len(mr.mean_amp_table) / 7.0))


def fix_stars(stdout):
    '''
    Fix ***** in tables
    '''
    error_line = '0.0******  0.00'
    #fixed_line = '0.0  0.00  0.00'
    for line in fileinput.input(stdout, inplace=True):
        if line.find(error_line) != -1:
            fixed_line = re.sub('0.0\*\*\*\*\*\*  0.00',
                                '0.0  0.00  0.00', line)
            print(fixed_line.strip('\n'))
        else:
            print(line.strip('\n'))
    fileinput.close()
    # return re.sub('0.0\*\*\*\*\*\*  0.00', '0.0  0.00  0.00', line)


def plotthegraphs(data_id, graph_id, table_data, plot_id_list=None, step=1, xmax=None):
    '''
    Modified version of set_pyrvapi_graph
    Take multiple runs and plot same column from different runs in same plot
    and rename the label with run #
    '''
    pyrvapi.rvapi_add_graph_dataset('index', data_id, graph_id,
                                    'index', str(table_data.index.name))

    # for i in range(len(table_data.shape)):
    #    print table_data.shape[i]
    for i in range(table_data.shape[0]):
        if np.issubdtype(table_data.index.dtype, np.int_):
            pyrvapi.rvapi_add_graph_int(
                'index',
                data_id,
                graph_id,
                int(i))
        else:
            pyrvapi.rvapi_add_graph_real('index',
                                         data_id,
                                         graph_id,
                                         float(i),
                                         '')
        # print 'Test {0}' .format(i)

    for n, col in enumerate(table_data.columns):
        col_name = col
        for col_i in plot_id_list:
            if col_i in col:
                col_name = col_i
        # print col_name
        if len(plot_id_list) == 1:
            plot_id = plot_id_list[0]
        else:
            plot_id = plot_id_list[n]

        # add dataset
        pyrvapi.rvapi_add_graph_dataset(col,
                                        data_id,
                                        graph_id,
                                        col,
                                        col)
        # add data
        # if xmax is not None:
        #    print 'inside xmax = {0}' .format(xmax)
        #    pyrvapi.rvapi_set_plot_xmax(plot_id, graph_id, xmax)
        for i in range(len(table_data)):
            if table_data[col][i] != 'NaN':
                if np.issubdtype(table_data[col].dtype, np.int):
                    pyrvapi.rvapi_add_graph_int(
                        col,
                        data_id,
                        graph_id,
                        int(table_data[col][i]))
                else:
                    pyrvapi.rvapi_add_graph_real(
                        col,
                        data_id,
                        graph_id,
                        float(table_data[col][i]),
                        '')
                # print "test 2 {0} {1} {2}" .format(table_data.index[i], i, table_data[col][i])
            # add plot and line
        pyrvapi.rvapi_add_graph_plot(plot_id,
                                     graph_id,
                                     col_name,
                                     str(table_data.index.name),  # xaxis label
                                     col_name)  # yaxis label
        pyrvapi.rvapi_set_plot_legend(plot_id, graph_id, 'ne', 'outsideGrid')
        pyrvapi.rvapi_add_plot_line(plot_id,
                                    data_id,
                                    graph_id,
                                    'index',  # +str(ri)+'w',
                                    col)  # +str(n)+'w')
        # if xmax is not None:
        #    print 'inside xmax1 = {0}' .format(xmax)
        #    pyrvapi.rvapi_set_plot_xrange(plot_id, graph_id, 0, xmax)

        # if ymin is not None:
        #    pyrvapi.rvapi_set_plot_ymin(plot_id, graph_id, ymin)
        pyrvapi.rvapi_reset_plot_xticks(plot_id,
                                        graph_id)
        for i in range(0, len(table_data), step):
            if table_data[col][i] != 'NaN':
                if np.issubdtype(table_data.index.dtype, np.int):
                    pyrvapi.rvapi_add_plot_xtick(
                        plot_id,
                        graph_id,
                        i,
                        '%d' % table_data.index[i])
                else:
                    pyrvapi.rvapi_add_plot_xtick(
                        plot_id,
                        graph_id,
                        float(i),
                        '%0.1f' % float(table_data.index[i]))
    pyrvapi.rvapi_flush()


def main(target_dir=None):
    from PyQt4 import QtGui, QtCore, QtWebKit
    if target_dir is None:
        pl_dir = '/y/people/swh514/ccpem_project1/Nautilus_86'
    else:
        pl_dir = target_dir
    pipeline_path = pl_dir + '/task.ccpem'
    rvapi_dir = pl_dir + '/report'
    if os.path.exists(rvapi_dir):
        shutil.rmtree(rvapi_dir)

    # set up viewer
    app = QtGui.QApplication(sys.argv)
    web_window = QtWebKit.QWebView()
    web_window.show()

    ccpem_utils.check_directory_and_make(rvapi_dir)
    os.chdir(pl_dir)
    if os.path.exists(pipeline_path):
        rv = PipelineResultsViewer(pipeline_path=pipeline_path)
        web_window.load(QtCore.QUrl(rv.index))
        app.exec_()


if __name__ == '__main__':
    '''
    usage:
        ccpem-python nautilus_results.py <path_to_nautiluspipeline_stdout>
    or:
        ccpem-python -m ccpem_core.gui.tasks.nautilus.nautilus_results
            ./test_data/nautiluspipeline_stdout.txt
    '''
    if len(sys.argv) == 2:
        pl_dir = sys.argv[1]
        main(target_dir=pl_dir)
    else:
        main()
