#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#
import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.buccaneer import buccaneer_task
from ccpem_core.tasks.nautilus import nautilus_results


class Nautilus(task_utils.CCPEMTask):
    '''
    CCPEM Nautilus task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Nautilus',
        author='Cowtan K',
        version='0.5.1',
        description=(
            'Nautilus performs automated building of RNA/DNA from electron '
            'density.\n'
            'Nautilus does not currently perform refinement - you will need '
            'to refine and recycle for further model building yourself. '
            'N.B. requires CCP4.'),
        short_description=(
            'Automated model building.  Requires CCP4'),
        documentation_link='http://www.ccp4.ac.uk/html/cnautilus.html',
        references=None)

    commands = {'refmac': which(program='refmac5'),
                'cnautilus': which('cnautilus'),
                'sftools': which('sftools'),
                'freerflag': which('freerflag'),
                'libg': which('libg')
                }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
        # No longer need to set atomsf since refmac job defaults to use
        # "source EM MB" instead.
        #        self.atomsf = os.path.join(
        #            os.environ['CLIBD'],
        #            'atomsf_electron.lib')

        super(Nautilus, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            verbose=verbose,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-mapin',
            '--input_map',
            help='''Target input map (mrc format)''',
            type=str,
            metavar='Input map',
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-input_seq',
            '--input_seq',
            help='Input sequence file in any common format (e.g. pir, fasta)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-extend_pdb',
            '--extend_pdb',
            help='Initial PDB model to extend (pdb format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-ncycle',
            '--ncycle',
            help='Number of Nautilus pipeline cycles',
            metavar='Build cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_nau1st',
            '--ncycle_nau1st',
            help='Number of Nautilus cycles in 1st pipeline cycle',
            metavar='1st Nautilus cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_naunth',
            '--ncycle_naunth',
            help='Number of Nautilus cycles in subsequent pipeline cycle',
            metavar='N-th Nautilus cycles',
            type=int,
            default=5)
        #
        parser.add_argument(
            '-ncycle_refmac',
            '--ncycle_refmac',
            help='Number of refmac cycles',
            metavar='Refine cycles',
            type=int,
            default=20)
        #
        parser.add_argument(
            '-map_sharpen',
            '--map_sharpen',
            metavar='Sharpen / blur',
            help=('B-factor to apply to map. Negative B-factor to '
                  'sharpen, positive to blur, zero to leave map as input'),
            type=float,
            default=0)
        #
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Library dictionary file for ligands (lib/cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        # added
        parser.add_argument(
            '-local_refinement_on',
            '--local_refinement_on',
            help='Refine around radius of supplied molecule only',
            metavar='Local refine',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-mask_radius',
            '--mask_radius',
            help='Distance around molecule the map should be cut.',
            type=float,
            default=3.0)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Keywords for advanced options.  Select file or define text',
            type=str,
            metavar='Keywords',
            default='')
        #
        parser.add_argument(
            '-refmac_keywords',
            '--refmac_keywords',
            help='Refmac keywords for advanced options. Select file or define text',
            type=str,
            metavar='Refmac Keywords',
            default='')
        return parser

    def run_pipeline(self, job_id=None, run=True, db_inject=None):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        # Convert map to mtz (refmac)
        # set F columns label suffix
        bfactor = self.args.map_sharpen()
        if bfactor >= 0:
            sharp_array = None
            blur_array = [bfactor]
            if bfactor == 0:
                label_out_suffix = '0'
            else:
                label_out_suffix = 'Blur_{0:.2f}'.format(
                    self.args.map_sharpen.value)
        else:
            sharp_array = [-1 * bfactor]
            blur_array = None
            label_out_suffix = 'Sharp_{0:.2f}'.format(
                -1 * self.args.map_sharpen.value)

        self.process_maptomtz = refmac_task.RefmacMapToMtz(
            command=self.commands['refmac'],
            resolution=self.args.resolution.value,
            mode='Global',
            name='Map to MTZ',
            job_location=self.job_location,
            map_path=self.args.input_map.value,
            blur_array=blur_array,
            sharp_array=sharp_array)
#            atomsf_path=self.atomsf)
        pl = [[self.process_maptomtz.process]]

        # Set MTZ SigF and FOM with SFTools
        self.process_sftools_buccanneer = buccaneer_task.SFToolsBuccaneer(
            command=self.commands['sftools'],
            job_location=self.job_location,
            hklin=self.process_maptomtz.hklout_path,
            name='Set MTZ SigF and SG')
        pl.append([self.process_sftools_buccanneer.process])

        # Create R free flags
        hklout = os.path.join(self.job_location,
                              'nautilus.mtz')
        self.process_free_r_flags = buccaneer_task.FreeRFlags(
            command=self.commands['freerflag'],
            job_location=self.job_location,
            hklin=self.process_sftools_buccanneer.hklout,
            hklout=hklout,
            name='Set Rfree')
        pl.append([self.process_free_r_flags.process])

        # Save seq if sequence string is provided rather than file path
        if (not os.path.exists(self.args.input_seq.value) and
                isinstance(self.args.input_seq.value, str)):
            path = os.path.join(self.job_location,
                                'input.seq')
            f = open(path, 'w')
            f.write(self.args.input_seq.value)
            f.close()
            self.args.input_seq.value = f.name

        # Add optional refmac keywords
        refine_keywords = ''
        # make newligand noexit to prevent refmac from exiting if no ligand dictionary found,
        # very rare unless is a new ligand
        if self.args.lib_in.value:
            refine_keywords += 'MAKE NEWLigand Noexit\n'
        refine_keywords += add_refmac_keywords(self.args.refmac_keywords.value)

        # Run Nautilus pipeline
        # Nautilus->LIBG->{1}->Refmac->repeat...{1}=maptomtz local
        for i in range(1, (self.args.ncycle.value + 1)):
            # 1st cycle Nautilus
            if i == 1:
                naut_int_cycle = self.args.ncycle_nau1st.value
                pdbin_path = self.args.extend_pdb.value
            else:
                naut_int_cycle = self.args.ncycle_naunth.value
                pdbin_path = self.process_refine.pdbout_path

            self.process_nautilus_pipeline = NautilusPipeline(
                command=self.commands['cnautilus'],
                job_location=self.job_location,
                hklin=self.process_free_r_flags.hklout,
                seqin=self.args.input_seq.value,
                label_out_suffix=label_out_suffix,
                ncycle=i,
                internal_cycle=naut_int_cycle,
                resolution=self.args.resolution.value,
                pdbin=pdbin_path,
                pdbout=None,
                name='Nautilus build {0}'.format(str(i)),
                job_title=self.args.job_title.value,
                keywords=self.args.keywords.value)
            pl.append([self.process_nautilus_pipeline.process])

            # Run libg for DNA/RNA restraints
            self.process_libg = refmac_task.LibgRestraints(
                command=self.commands['libg'],
                job_location=self.job_location,
                name='DNA or RNA basepair restraints {0}'.format(str(i)),
                pdb_path=self.process_nautilus_pipeline.pdbout)
            pl.append([self.process_libg.process])

            # Invert sharpening value for refmac
            if self.args.map_sharpen() is None:
                sharp = 0
            else:
                sharp = -1.0 * self.args.map_sharpen()
            # Run RefmacRefine (global/local)
            if self.args.local_refinement_on():
                self.process_maptomtz_local = refmac_task.RefmacMapToMtz(
                    command=self.commands['refmac'],
                    name='Map to MTZ (local) {0}'.format(str(i)),
                    resolution=self.args.resolution.value,
                    mode='Local',
                    job_location=self.job_location,
                    lib_path=self.args.lib_in.value,
                    map_path=self.args.input_map.value,
                    pdb_path=self.process_nautilus_pipeline.pdbout,
                    blur_array=blur_array,
                    sharp_array=sharp_array,
                    mrad=self.args.mask_radius.value)
                pl.append([self.process_maptomtz_local.process])
                # Refinement parameters: Local
                mode = 'Local'
                name = 'Refmac refine (local) {0}'.format(str(i))
                pdb_path_a = self.process_maptomtz_local.pdbout_path
                mtz_path_a = 'masked_fs.mtz'
            else:
                # Refinement parameters: Global
                mode = 'Global'
                name = 'Refmac refine (global) {0}'.format(str(i))
                pdb_path_a = self.process_nautilus_pipeline.pdbout
                mtz_path_a = self.process_free_r_flags.hklout

            self.process_refine = refmac_task.RefmacRefine(
                command=self.commands['refmac'],
                job_location=self.job_location,
                pdb_path=pdb_path_a,
                mtz_path=mtz_path_a,
                resolution=self.args.resolution.value,
                pdbout_path=os.path.join(
                    self.job_location, 'refined{0}.pdb'.format(str(i))),
                mode=mode,
                name=name,
                sharp=sharp,
                ncycle=self.args.ncycle_refmac.value,
                output_hkl=True,
                symmetry_auto=False,
                keywords=refine_keywords,
                libg_restraints=True,
                libg_restraints_path=[self.process_libg.libg_restraints_path])
            pl.append([self.process_refine.process])

        custom_finish = NautilusResultsOnFinish(
            pipeline_path=self.job_location + '/task.ccpem',
            refine_process=self.process_refine)

        if run:
            os.chdir(self.job_location)
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                db_inject=db_inject,
                database_path=self.database_path,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose,
                on_finish_custom=custom_finish)
            self.pipeline.start()

    def validate_args(self):
        # Now do at gui level
        return True


class NautilusPipeline(object):
    '''
    Real time nucleic acid chain tracing (N.B. runs cnautilus)
    '''

    def __init__(self,
                 command,
                 job_location,
                 hklin,
                 seqin,
                 label_out_suffix,
                 ncycle=1,
                 internal_cycle=5,
                 resolution=2.0,
                 pdbin=None,
                 pdbout=None,
                 name=None,
                 job_title=None,
                 keywords=None):
        # counter for overall NautilusPipeline cycles, start with 1
        assert command is not None
        self.job_location = job_location
        self.hklin = hklin
        self.seqin = seqin
        assert os.path.exists(path=self.seqin)
        self.label_out_suffix = label_out_suffix
        self.pdbout = pdbout
        self.ncycle = ncycle
        self.internal_cycle = internal_cycle
        self.resolution = resolution
        self.pdbin = pdbin
        if self.pdbout is None:
            self.pdbout = os.path.join(
                job_location, 'build' + str(ncycle) + '.pdb')
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.job_title = job_title
        self.stdin = None
        self.stdin_extra = None
        self.keywords = keywords
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['-stdin']

    def set_stdin(self):
        pdbin_ref = os.path.join(os.environ['CLIBD'],
                                 'nautilus_lib.pdb')
        # if self.ncycle == 1:
        #    self.stdin_extra = '''colin-phifom PHIem, FOMem
        #    '''
        # else:
        #    self.stdin_extra = '''colin-hl HLACOMB, HLBCOMB, HLCCOMB, HLDCOMB
# colin-fc FWT, PHWT
#'''

        self.stdin = '''
title {0}
pdbin-ref {1}
seqin {2}
mtzin {3}
colin-fo Fout{4},SIGFem
colin-free FreeR_flag
colin-phifom Pout0,FOMem
pdbout {5}
cycles {6}
resolution {7}
anisotropy-correction
xmlout program{8}.xml
'''.format(self.job_title,
           pdbin_ref,
           self.seqin,
           self.hklin,
           self.label_out_suffix,
           self.pdbout,
           self.internal_cycle,
           self.resolution,
           self.ncycle)

        #self.stdin = self.stdin + self.stdin_extra
        # Add optional nautilus keywords
        if isinstance(self.keywords, str):
            if self.keywords != '':
                # Remove trailing white space and new lines
                keywords = self.keywords.strip()
                for line in keywords.split('\n'):
                    self.stdin += line + '\n'
        if self.pdbin is not None:
            self.stdin += 'pdbin {0}\n'.format(self.pdbin)


class NautilusResultsOnFinish(process_manager.CCPEMPipelineCustomFinish):
    '''
    Generate RVAPI results on finish.
    '''

    def __init__(self,
                 pipeline_path,
                 refine_process):
        super(NautilusResultsOnFinish, self).__init__()
        self.pipeline_path = pipeline_path
        self.refine_process = refine_process

    def on_finish(self, parent_pipeline=None):
        # generate RVAPI report
        nautilus_results.PipelineResultsViewer(
            pipeline_path=self.pipeline_path)


def add_refmac_keywords(refkeywords):
    '''
    Add default and additional refmac keywords
    '''
    refine_keywords = ''
# PHOUT
# PNAME nautilus
# DNAME nautilus
#'''.format(labelout_suffix)

    if isinstance(refkeywords, str):
        if refkeywords != '':
            # Remove trailing white space and new lines
            keywords = refkeywords.strip()
            for line in keywords.split('\n'):
                refine_keywords += line + '\n'

    return refine_keywords
