#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
import math, glob
import numpy as np
import mrcfile
try:
    import modeller  # @UnusedImport
    modeller_available = True
    from ccpem_progs.flex_em import flexem
except ImportError:
    modeller_available = False
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.process_manager import job_register
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.flex_em import flexem_results
from ccpem_core.tasks.ribfind import ribfind_task
from ccpem_core import settings


class FlexEM(task_utils.CCPEMTask):
    '''
    CCP-EM FlexEM task.
    '''

    task_info = task_utils.CCPEMTaskInfo(
        name='Flex-EM',
        author='M. Topf, K. Lasker, B. Webb, H. Wolfson, W. Chiu, A. Sali',
        version='1.0',
        description=(
            '''
            Fitting and refinement of atomic structures guided by cryoEM density.
            <br><br>Input a map and an atomic model to be fitted. 
            The map resolution is required as well.<br><br>
            Segments of the atomic model are constrained as rigid bodies 
            during fitting and refinement. By default, the rigid bodies are 
            identified using RIBFIND. The size of rigid bodies identified 
            depends on the Ribfind cutoff. A cutoff of 100 will use secondary 
            structures are rigid bodies. Lower cutoffs (minimum 0) will 
            consider progressively larger rigid bodies. The number of iterations
             required for fitting and refinement depends on how far is the 
             starting atomic model from the target map and the map resolution. 
            Use the plot of Cross correlation score vs iteration (in Results) 
            to select an optimum number of iterations required. The output models
            from each iteration can be found in the job directory. <br><br>
            We recommend using the 'MD' mode for FlexEM runs. Maximum 
            atomic displacements (Angstroms) in each MD step can be adjusted 
            in the advanced options. We recommend 0.39 for larger rigid 
            bodies, 0.15 for secondary structures used as rigid bodies and 
            0.1 for all-atom refinement (empty rigid body file required).<br><br>
            Rigid body file format (two lines):<br>
            10:A 20:A<br>
            30:B 40:B 60:B 70:B<br>
            indicates two rigid bodies, one formed of residues 10 
            to 20 from chain A, and another formed of residues 30 to 40 
            and 60 to 70 of chain B.<br><br>
            
            Keywords can be used to add specific restraints:<br>
            To add helix restraint between residue 10 to 20 of chain A
                use keyword: <br>
                helix 10:A 20:A<br><br>
            To add a distance restraint (say 5 Angstroms) between residue 35 and 40
                use keyword: <br>
                distance 35:A 40:A 5.0<br>
            '''),
        short_description=(
            'Fitting and refinement of atomic structures guided by cryoEM density'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/flex-em/',
        references=None)
    if modeller_available:
        commands = {'flexem_python': ['ccpem-python',
                                      os.path.realpath(flexem.__file__)],
                    'ccpem-ribfind': settings.which(
                        'ccpem-ribfind')}


    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        super(FlexEM, self).__init__(database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        alpha_mode = parser.add_argument_group()
        alpha_mode.add_argument(
            '-alpha_mode',
            '--alpha_mode',
            help='Test mode for refine B and FSC calc',
            metavar='Alpha mode',
            type=bool,
            default=False)
        #
        mode = parser.add_argument_group()
        mode.add_argument(
            '-mode',
            '--mode',
            choices=['MD',
                     'CG'],
            help='Molecular dynamics or conjugate gradient optimisation',
            metavar='Mode',
            type=str,
            default='MD')
        #
        input_pdb = parser.add_argument_group()
        input_pdb.add_argument(
            '-input_pdb',
            '--input_pdb',
            help='Input coordinate file (pdb format)',
            metavar='Input PDB',
            type=str,
            default=None)
        #
        input_map = parser.add_argument_group()
        input_map.add_argument(
            '-input_map',
            '--input_map',
            help='Target input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        #
        map_resolution = parser.add_argument_group()
        map_resolution.add_argument(
            '-map_resolution',
            '--map_resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            default=None)
        #
        iterations = parser.add_argument_group()
        iterations.add_argument(
            '-iterations',
            '--iterations',
            help='''Number of Flex-EM iterations 
                (dependant on the extent of conformational difference)''',
            metavar='Iterations',
            type=int,
            default=3)
        #
        ribfind_cutoff = parser.add_argument_group()
        ribfind_cutoff.add_argument(
            '-ribfind_cutoff',
            '--ribfind_cutoff',
            help=('Set Ribfind cutoff for rigid body elements.  Default '
                  'of 100 corresponds to one rigid body per secondary '
                  'structure element'),
            metavar='Ribfind cutoff',
            type=int,
            default=100)
        #
        create_rigid_body = parser.add_argument_group()
        create_rigid_body.add_argument(
            '-create_rigid_body',
            '--create_rigid_body',
            help=('''Generate rigid bodies based on resolution 
                    (e.g. individual secondary structures for 
                    resolutions better than 7.5)'''),
            metavar='Auto rigid body',
            type=bool,
            default=True)
        #
        input_rigid = parser.add_argument_group()
        input_rigid.add_argument(
            '-input_rigid',
            '--input_rigid',
            help='Rigid body file from Ribfind',
            metavar='Rigid body',
            type=str,
            default=None)
        #
        max_atom_disp = parser.add_argument_group()
        max_atom_disp.add_argument(
            '-max_atom_disp',
            '--max_atom_disp',
            help=('Maximum atom displacement cutoff recommended to use 0.39 for '
                  'large rigid bodies, 0.2 for SSE refinement, 0.10 for '
                  'all-atom refinement'),
            metavar='Atom disp',
            type=float,
            default=0.20)
        #
        phi_psi_restraints = parser.add_argument_group()
        phi_psi_restraints.add_argument(
            '-phi_psi_restraints',
            '--phi_psi_restraints',
            help=('Restrain to starting model phi-psi (see homology '
                  'derived restraints in Modeller)'),
            metavar='Template phi/psi restraint',
            type=bool,
            default=True)
        distance_restraints = parser.add_argument_group()
        distance_restraints.add_argument(
            '-distance_restraints',
            '--distance_restraints',
            help=('Locally restrain to starting model distances (see homology '
                  'derived restraints in Modeller)'),
            metavar='Template distance restraint',
            type=bool,
            default=False)
        
        distance_restraints.add_argument(
            '-max_distance',
            '--max_distance',
            help=('Maximum distance to apply template restraints (see homology '
                  'derived restraints in Modeller)'),
            metavar='maximum distance for restraint',
            type=float,
            default=5.0)
        
        parser.add_argument(
            '-density_weight',
            '--density_weight',
            help=('Factor to scale density with [0.0-1.0]'),
            metavar='density weight',
            type=float,
            default=1.0)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Keywords for advanced options',
            type=str,
            default='')
        #
        test_mode = parser.add_argument_group()
        test_mode.add_argument(
            '-test_mode',
            '--test_mode',
            help='Runs short protocol.  For unit testing only.',
            metavar='Test mode',
            type=bool,
            default=False)

        ####### Refmac Refine Bfactor
        parser.add_argument(
            '-refine_b',
            '--refine_b',
            help='''Refine Bfactors with Refmac''',
            metavar='Refine Bfactor',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-fsc_calc',
            '--fsc_calc',
            help='''Calculate model vs map FSC''',
            metavar='FSC Calc',
            type=bool,
            default=False)

        ####### Half map validation
        parser.add_argument(
            '-validate',
            '--validate',
            help='''Perform validation''',
            metavar='Validate',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help=('Half map 1 of input map, required for cross validation '
                  'post refinement (mrc format)'),
            metavar='Half map 1',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help=('Half map 2 of input map, required for cross validation '
                  'post refinement (mrc format)'),
            type=str,
            metavar='Half map 1',
            default=None)

        return parser

    def validate_args(self):
        '''
        Check arguments before running job.
        '''
        args_correct = True
        warnings = ''
        # Check input PDB
        warnings += self.set_arg_absolute_path(
            arg=self.args.input_pdb)
        warnings += self.set_arg_absolute_path(
            arg=self.args.input_map)
        if not self.args.create_rigid_body():
            warnings += self.set_arg_absolute_path(
                arg=self.args.input_rigid)
        # Get map data, check is cubic
        if self.args.input_map.value is not None:
            if os.path.exists(self.args.input_map.value):
                mrc = mrcfile.open(self.args.input_map.value)
                self.map_data = mrc

        if self.args.map_resolution.value is None:
            warnings += '\nMap resolution not set'
        # Display warnings in parent GUI
        if warnings != '':
            args_correct = False
            print warnings
        return args_correct

    def get_keywords(self):
        self.dict_keywords = {}
        keyword_text = self.args.keywords.value
        if len(keyword_text) > 0:
            keyword_lines = keyword_text.split('\n')
            for line in keyword_lines:
                line_split = line.split()
                if len(line_split) < 3: continue
                if line_split[0] == 'helix':
                    try:
                        self.dict_keywords['helix'].append([line_split[1],line_split[2]])
                    except KeyError: 
                        self.dict_keywords['helix'] = [[line_split[1],line_split[2]]]
                elif line_split[0] == 'strand':
                    try:
                        self.dict_keywords['strand'].append([line_split[1],line_split[2]])
                    except KeyError: 
                        self.dict_keywords['strand'] = [[line_split[1],line_split[2]]]
                elif line_split[0] == 'distance':
                    try:
                        res_distance = float(line_split[3])
                        try:
                            self.dict_keywords['distance'].append([line_split[1],line_split[2],res_distance])
                        except KeyError: 
                            self.dict_keywords['distance'] = [[line_split[1],line_split[2],res_distance]]
                    except ValueError: pass
        
        with open(self.keyword_jsonfile,'w') as kj:
            json.dump(self.dict_keywords,kj)
        
        
    def flexem_job(self):
        #
        flex_em_json_args = {
            'optimization': self.args.mode.value,
            'input_pdb_file': self.args.input_pdb.value,
            'code': 'fit',
            'em_map_file': self.args.input_map.value,
            'map_format': 'MRC',
            'apix': self.map_data.voxel_size.z.item(),#voxx_voxy_voxz[2],  # Voxel size
            'resolution': self.args.map_resolution.value,
            'x': self.map_data.header.origin.x.item(),
            'y': self.map_data.header.origin.y.item(),
            'z': self.map_data.header.origin.z.item(),
            'path': self.args.job_location.value,
            'init_dir': 1,
            'num_of_iter': self.args.iterations.value,
            'rigid_filename': self.args.input_rigid.value,
            'job_location': self.job_location,
            'max_atom_disp': self.args.max_atom_disp.value,
            'phi_psi_restraints': self.args.phi_psi_restraints.value,
            'distance_restraints':  self.args.distance_restraints.value,
            'max_distance': self.args.max_distance.value,
            'keyword_json': self.keyword_jsonfile,
            'density_weight': self.args.density_weight.value,
            'test_mode': self.args.test_mode.value}
        jsonpath = os.path.join(self.job_data, 'flex_em_args.json')
        json.dump(flex_em_json_args,
                  open(jsonpath, 'w'),
                  indent=4,
                  separators=(',', ': '))
        self.flexem_process = process_manager.CCPEMProcess(
            name='Flex-EM',
            command=self.commands['flexem_python'],
            args=[jsonpath],
            location=self.job_location,
            stdin=None)

    def ribfind_job(self, db_inject=None, job_title=None):
        # Set 
        if self.database_path is None:
            path = os.path.dirname(self.job_location)
        else:
            path = os.path.dirname(self.database_path)

        job_id, job_location = job_register.job_register(
            db_inject=db_inject,
            path=path,
            task_name=ribfind_task.Ribfind.task_info.name)

        # Set task args
        args = ribfind_task.Ribfind().args
        args.job_title.value = job_title
        args.input_pdb.value = self.args.input_pdb()
        args_json_string = args.output_args_as_json(
            return_string=True)

        # Set process args
        process_args = ['--no-gui']
        process_args += ["--args_string='{0}'".format(
            args_json_string)]
        process_args += ['--job_location={0}'.format(
            job_location)]
        if self.database_path is not None:
            process_args += ['--project_location={0}'.format(
                os.path.dirname(self.database_path))]
        if job_id is not None:
            process_args += ['--job_id={0}'.format(
                job_id)]

        # Create process
        self.ribfind_process = process_manager.CCPEMProcess(
            name='Ribfind auto run task',
            command=self.commands['ccpem-ribfind'],
            args=process_args,
            location=job_location,
            stdin=None)

    def run_pipeline(self, job_id=None, db_inject=None):
        # Generate rigid body file by running ribfind
        # To do:
        # Wait till finish
        # Check runs from command line
        self.validate_args()
        pl = None
        if self.args.create_rigid_body():
            
            ribfind_job_title = 'Auto run from Flex-EM'
            if job_id is not None:
                ribfind_job_title += ' {0}'.format(
                    job_id)
            self.ribfind_job(db_inject=db_inject,
                             job_title=ribfind_job_title)
            pl = [[self.ribfind_process]]

            # Set rigid body path
            pdb = self.args.input_pdb.value
            pdb_outdir = '.'.join(os.path.basename(pdb).split('.')[:-1])
            rigid_body_path = \
                os.path.join(self.ribfind_process.location,
                             pdb_outdir,'protein')
                
            if self.args.ribfind_cutoff() is None:
                if self.args.map_resolution.value < 7.5:
                    cutoff = 100
                elif self.args.map_resolution.value < 15.0:
                    cutoff = 60
                elif self.args.map_resolution.value < 100.0:
                    cutoff = 30
            else:
                cutoff = int(math.ceil(self.args.ribfind_cutoff() / 10.0)) * 10
            
            self.args.ribfind_cutoff.value = cutoff
            list_cutoffs = []
            rigid_body_files = []
            for rigidclust in xrange(0,101,10):
                rigid_body_files.append(os.path.join(rigid_body_path,
                    pdb_outdir+'_denclust_{}.txt'.format(rigidclust)))
            
            for rf in rigid_body_files:
                basename = '.'.join(os.path.basename(rf).split('.')[:-1])
                basename = basename.split('_')[-1]
                try:
                    list_cutoffs.append(int(basename))
                except TypeError: pass
            list_cutoffs.sort()
            cutoff_array = np.array(list_cutoffs)
            sel_indices = np.searchsorted(cutoff_array,cutoff) 
            if isinstance(sel_indices,int):
                sel_cutoff = ''
                if sel_indices >= len(cutoff_array):
                    sel_indices = -1
                sel_cutoff = cutoff_array[sel_indices]
            else: 
                try: sel_cutoff = cutoff_array[sel_indices[0]]
                except IndexError: sel_cutoff = cutoff_array[-1]
            self.args.input_rigid.value = \
                os.path.join(rigid_body_path,
                             pdb_outdir+'_denclust_{}.txt'.format(sel_cutoff))

        # Directory to store intermediate data
        self.job_data = os.path.join(self.job_location, 'job_data')
        ccpem_utils.check_directory_and_make(self.job_data)
        
        # FlexEM
        self.keyword_jsonfile = os.path.join(self.job_data,
                                         'keywords.json')
        self.get_keywords()
        self.flexem_job()
        if pl is None:
            pl = [[self.flexem_process]]
        else:
            pl.append([self.flexem_process])

        # Custom finish
        if self.args.mode.value == 'CG':
            metadata_file = os.path.join(self.job_location,
                                         '1_CG/metadata.json')
        else:
            metadata_file = os.path.join(self.job_location,
                                         '1_MD/metadata.json')
        custom_finish = FlexEMResultsOnFinish(
            pipeline_path=self.job_location+'/task.ccpem',
            metadata_file=metadata_file)

        if [self.args.refine_b(),
            self.args.validate(),
            self.args.fsc_calc()].count(True) > 0:

            from ccpem_core.tasks.refmac import refmac_task
            # Refmac refine b / fsc
            self.process_maptomtz = refmac_task.RefmacMapToMtz(
                command=self.command,
                resolution=self.args.resolution.value,
                mode='Global',
                name='Map to MTZ',
                job_location=self.job_location,
                map_path=self.args.input_map.value,
                blur_array=None,
                sharp_array=None,
                atomsf_path=refmac_task.Refmac.atomsf)

        # Pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            on_finish_custom=custom_finish,
            title=self.args.job_title.value)
        self.pipeline.start()


class FlexEMResultsOnFinish(process_manager.CCPEMPipelineCustomFinish):
    '''
    Generate RVAPI results on finish.
    '''
    def __init__(self,
                 pipeline_path,
                 metadata_file):
        super(FlexEMResultsOnFinish, self).__init__()
        self.pipeline_path = pipeline_path
        self.metadata_file = metadata_file

    def on_finish(self, parent_pipeline=None):
        flexem_results.FlexEMResultsViewer(
            metadata_file=self.metadata_file,
            pipeline_path=self.pipeline_path)


def main():
    print 'test'
    args = ribfind_task.Ribfind().args
    print args
    args.output_args_as_json(filename='/Users/tom/temp/test.json')
    print args.output_args_as_json(return_string=True)
     
if __name__ == '__main__':
    main()
