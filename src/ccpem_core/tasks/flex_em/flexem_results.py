#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json
import shutil
import pyrvapi
from ccpem_core import ccpem_utils
from ccpem_core import process_manager

class FlexEMResultsViewer(object):
    def __init__(self, metadata_file, pipeline_path):
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=None,
            import_json=pipeline_path)

        self.directory = os.path.join(self.pipeline.location,
                                      'rvapi_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        self.metadata_file = metadata_file
        if self.metadata_file is not None:
            self.metadata_file = os.path.abspath(self.metadata_file)
            self.set_metadata()
        else:
            self.metadata = None
        self.refine_results = None
        self.index = None
        self.set_pyrvapi_page()
        self.set_results()

    def set_results(self):
        self.set_results_summary()
        self.set_results_graphs()

    def set_pyrvapi_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
        # Setup share jsrview
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document('TestRun', self.directory,
                                    'RVAPI Demo 1', 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()

    def set_results_summary(self):
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('Flex EM Results')
        pyrvapi.rvapi_add_tab('tab2', 'Simulation', True)
        pyrvapi.rvapi_add_section('sec1', 'Results', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text('Simulation summary', 'sec1', 0, 0, 1, 1)
        # Set refinement statistics table
        pyrvapi.rvapi_add_table1('sec1/table1', 'Statistics',
                                 1, 0, 1, 1, True)
        # Set vertical
        pyrvapi.rvapi_put_vert_theader('table1', 'Initial', '', 0)
        pyrvapi.rvapi_put_vert_theader('table1', 'Final', '', 1)
        # Set horizontal
        pyrvapi.rvapi_put_horz_theader('table1', 'CCC',
                                       'R value', 0)
        start_ccc = '{0:.3f}'.format(self.metadata['em_density'][0])
        finish_ccc = '{0:.3f}'.format(self.metadata['em_density'][-1])
        pyrvapi.rvapi_put_table_string('table1', start_ccc, 0, 0)
        pyrvapi.rvapi_put_table_string('table1', finish_ccc, 1, 0)
        pyrvapi.rvapi_flush()

    def set_results_graphs(self):
        # Refinement statistics vs cycle
        pyrvapi.rvapi_append_loggraph1('sec1/graphWidget1')
        # Add graph data
        pyrvapi.rvapi_add_graph_data1('graphWidget1/data1',
                                      'Statistics / iteration')
        pyrvapi.rvapi_add_graph_dataset1('graphWidget1/data1/x',
                                         'x',
                                         'Ncycle')
        pyrvapi.rvapi_add_graph_dataset1('graphWidget1/data1/y1',
                                         'CCC',
                                         'CCC')
        # Get data
        for i in range(len(self.metadata['iteration'])):
            pyrvapi.rvapi_add_graph_int1(
                'graphWidget1/data1/x',
                int(self.metadata['iteration'][i]))
            pyrvapi.rvapi_add_graph_real1(
                'graphWidget1/data1/y1',
                float(self.metadata['em_density'][i]),
                '%g')

        # Plot 1 -> R factor
        pyrvapi.rvapi_add_graph_plot1('graphWidget1/plot1',
                                      'CCC',
                                      'Iteration',
                                      'CCC')
        pyrvapi.rvapi_add_plot_line1('graphWidget1/data1/plot1', 'x',
                                     'y1')
#         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
#         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)
        #
        pyrvapi.rvapi_flush()

    def set_metadata(self):
        self.metadata = json.load(fp=open(self.metadata_file, 'r'))

