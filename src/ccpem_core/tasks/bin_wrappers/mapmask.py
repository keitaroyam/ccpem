#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

from ccpem_core import process_manager


class MapMask(object):
    '''
    Python wrapper to MAPMASK
    '''
    command = 'mapmask'
    def __init__(self,
                 job_location,
                 mapin1,
                 mapout=None,
                 fast=None,
                 medium=None,
                 slow=None):
        self.job_location = job_location
        self.mapin1 = mapin1
        if mapout is None:
            mapout = 'mapmask.mrc'
        self.mapout = mapout
        self.fast = fast
        self.medium = medium
        self.slow = slow
        self.name = self.__class__.__name__

        self.set_args()
        self.set_stdin()

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['mapin1', self.mapin1]
        self.args += ['mapout', self.mapout]

    def set_stdin(self):
        self.stdin = 'axis {0} {1} {2}'.format(
            self.fast,
            self.medium,
            self.slow)
        self.stdin += '\nend'
