#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.

from ccpem_core import process_manager


class FFT(object):
    '''
    FFT to convert MTZ to map
    '''
    command = 'fft'
    def __init__(self,
                 job_location,
                 mtz_path,
                 map_nx=None,
                 map_ny=None,
                 map_nz=None,
                 fast=None,
                 medium=None,
                 slow=None,
                 map_path=None,
                 name=None):
        self.job_location = job_location
        self.mtz_path = mtz_path
        if map_path is None:
            map_path = 'fft.mrc'
        self.map_path = map_path
        self.map_nx = map_nx
        self.map_ny = map_ny
        self.map_nz = map_nz
        self.fast = fast
        self.medium = medium
        self.slow = slow
        if name is None:
            self.name = self.__class__.__name__
        else:
            self.name = name
            
        self.set_args()
        self.set_stdin()

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['hklin', self.mtz_path]
        self.args += ['mapout', self.map_path]

    def set_stdin(self):
        self.stdin = 'xyzlim asu'
        self.stdin += '\nscale F1 1.0'
        self.stdin += '\nlabin - '
        self.stdin += '\n   F1=Fout0 PHI=Pout0'
        if all([self.map_nx, self.map_ny, self.map_nz]):
            self.stdin += '\ngrid {0} {1} {2}'.format(self.map_nx,
                                                      self.map_ny,
                                                      self.map_nz)
        if all([self.fast, self.medium, self.slow]):
            self.stdin += '\naxis {0} {1} {2}'.format(self.fast,
                                                      self.medium,
                                                      self.slow)
        self.stdin += '\nend'
