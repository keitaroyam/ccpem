#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os, json
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.cryoEF import extractAngles,extractSubset, PlotOD, \
        cryoEF_results
from ccpem_core.settings import which

class cryoEF(task_utils.CCPEMTask):
    '''
    CCPEM cryoEF task for estimating particle angular spread.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='cryoEF',
        author='Naydenova K, Russo C.J',
        version='1.1',
        description=(''' A method to describe the quality of an orientation 
        distribution in terms of providing uniform resolution in all directions, 
        by a single number - the efficiency.<br><br> 
        The cryoEF program will assist you in determining to what extent this 
        affects the resolution of your 3D reconstruction.The efficiency score, 
        calculated by cryoEF, measures the ability of the distribution to 
        provide uniform information and resolution in all directions of the 
        reconstruction, independent of other factors. This metric allows rapid 
        and rigorous evaluation of specimen preparation methods, assisting 
        structure determination to high resolution with minimal data. 
        Also included is an algorithm for predicting optimal tilt angles for 
        data collection.<br><br>
        To run cryoEF, you will need a list of the particle orientation angles 
        assigned by one of the popular 3D reconstruction programs. The input 
        format is a two column ASCII text file, where the columns are 
        separated by spaces and end in a carriage return. You can extract this 
        from the data file from your 3D reconstruction (e.g. the *.par file 
        containing the PSI angles from a Frealign reconstruction or the *_data.star 
        file from any RELION 3D reconstruction (_rlnAngleRot,_rlnAngleTilt)).<br><br>
        Outputs:<br>
        angles.log: contains <br>
        Efficiency value. As a rough guide, a value of 0.8 - 1 corresponds to 
        a good orientation distribution, providing uniform Fourier space coverage.
         Conversely, values below 0.5 are indicative of a problem with the 
        orientation distribution.<br>
        Fourier space gap alerts.<br>
        Recommended Tilt. This provides suggestions of up to three tilt angles, 
        which may improve the efficiency of the given orientation distribution.<br>
        angles_K.mrc: contains the Fourier space (k-space) information coverage 
        of the orientation distribution.<br>
        angles_R.mrc: point spread function (PSF) corresponding to the geometry 
        of the orientation distribution. Ideally, the PSF should also be spherical, 
        but most likely it will be elongated in some direction. 
        This corresponds to the weakest resolved (most smeared) direction in 
        your 3D reconstruction.
        '''),
        short_description=(
            'Single number estimate for angle spread'),
        documentation_link='https://www.mrc-lmb.cam.ac.uk/crusso/cryoEF/index.html',
        references=None)
    
    commands = {'cryoEF': [which('cryoEF')],
        'extract-angles': ['ccpem-python',os.path.realpath(extractAngles.__file__)],
        'extract-subset': ['ccpem-python',os.path.realpath(extractSubset.__file__)],
        'plotOD': ['ccpem-python',os.path.realpath(PlotOD.__file__)]}
    
    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(cryoEF, self).__init__(
             database_path=database_path,
             args=args,
             args_json=args_json,
             pipeline=pipeline,
             job_location=job_location,
             parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        
        parser.add_argument(
            '-input_selection',
            '--input_selection',
            help='input angles list file or relion star file with orientation angles',
            metavar='Input file options',
            choices=['text file','star file'],
            type=str,
            default='text file')
        #
        angles_file = parser.add_argument_group()
        angles_file.add_argument(
            '-angles_file_path',
            '--angles_file_path',
            help='input text file with orientation angles',
            metavar='Angles text file',
            type=str,
            default=None)
        angles_file.add_argument(
            '-angles_star_file',
            '--angles_star_file',
            help='Input relion star file with orientation angles',
            metavar='Star file with Rot,Tilt angles',
            type=str,
            default=None)
        parser.add_argument(
            '-number_subset',
            '--number_subset',
            help='Number of angles to include (random selection from data)',
            metavar='Number of angles',
            type=int,
            default=1000)
        #Symmetry group
        map_symmetry = parser.add_argument_group()
        map_symmetry.add_argument(
            '-particle_symmetry',
            '--particle_symmetry',
            help='''Symmetry group (eg. C7 or T or I2).'''
                ''' For nomenclature see http://xmipp.cnb.csic.es/twiki/bin/view/Xmipp/Symmetry''',
            metavar='Symmetry group',
            type=str,
            default=None)
        # Particle diameter
        particle_dimensions = parser.add_argument_group()
        particle_dimensions.add_argument(
            '-particle_diameter',
            '--particle_diameter',
            help='Approximate particle diameter (Angstroms)',
            metavar='Approx particle diameter',
            type=float,
            default=200)
        # Particle box size
        particle_dimensions.add_argument(
            '-particle_box_size',
            '--particle_box_size',
            help='''Size of particle box (pixels). '''
                ''' Use lower values (less accurate efficiency estimates) for faster calculations''',
            metavar='Box size (pixels)',
            type=int,
            default=128)
        # Box padding
        particle_dimensions.add_argument(
            '-particle_box_padding',
            '--particle_box_padding',
            help='''Padding for particle box (pixels). ''',
            metavar='Box padding (pixels)',
            type=int,
            default=384)

        #b-factor estimate
        bfactor = parser.add_argument_group()
        bfactor.add_argument(
            '-bfactor',
            '--bfactor',
            help='B-factor estimate of data',
            metavar='B-factor estimate of data',
            type=float,
            default=100)
        #resolution estimate
        resolution = parser.add_argument_group()
        resolution.add_argument(
            '-resolution',
            '--resolution',
            help='Resolution from FSC 0.143',
            metavar='Resolution (FSC 0.143)',
            type=float,
            default=None)
        #angular accuracy
        angular_accuracy = parser.add_argument_group()
        angular_accuracy.add_argument(
            '-angular_accuracy',
            '--angular_accuracy',
            help='Accuracy of orientation angles (deg)',
            metavar='Accuracy of input angles',
            type=float,
            default=1.0)
        #max tilt angle
        max_tilt = parser.add_argument_group()
        max_tilt.add_argument(
            '-max_tilt',
            '--max_tilt',
            help='Maximum tilt angle allowed for the prediction algorithm',
            metavar='Maximum tilt allowed',
            type=float,
            default=45.0)
        return parser

    def edit_argsfile(self,args_file):
        with open(args_file,'r') as f:
            json_args = json.load(f)
            #set ribfind task output
            json_args['angles_file_path'] = self.args.angles_file_path.value
        
        with open(args_file,'w') as f:
            json.dump(json_args,f)

    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        if self.args.input_selection.value != 'text file':
            if self.args.angles_star_file.value != None:
                self.extractAngles = extractAngles(
                    job_location=self.job_location,
                    command=self.commands['extract-angles'],
                    star_file_path=self.args.angles_star_file.value,
                    name='extractAngles')
                angles_file = os.path.join(self.job_location,
                                    '.'.join(os.path.basename(
                                            self.args.angles_star_file.value).split('.')[:-1])+'_angles.dat')
                pl.append([self.extractAngles.process])
                
                self.extractSubset = extractSubset(
                    job_location=self.job_location,
                    command=self.commands['extract-subset'],
                    angles_file_path=angles_file,
                    number_angles = self.args.number_subset.value,
                    name='extractSubset')
                angles_subset_file = os.path.join(self.job_location,
                                            '.'.join(os.path.basename(
                                                angles_file).split('.')[:-1])+'_subset.dat')
                self.args.angles_file_path.value = angles_subset_file
                # Set args
                args_file = os.path.join(self.job_location,
                                 'args.json')
                self.edit_argsfile(args_file)
                pl.append([self.extractSubset.process])
        log_file = os.path.splitext(self.args.angles_file_path.value)[0] + \
                        '.log'
        self.cryoEF_process = cryoEF_wrapper(
            job_location=self.job_location,
            command=self.commands['cryoEF'],
            angles_file_path=self.args.angles_file_path.value,
            particle_symmetry = self.args.particle_symmetry.value,
            particle_diameter = self.args.particle_diameter.value,
            particle_box_size = self.args.particle_box_size.value,
            bfactor = self.args.bfactor.value,
            resolution = self.args.resolution.value,
            angular_accuracy = self.args.angular_accuracy.value,
            max_tilt = self.args.max_tilt.value
            )
        pl.append([self.cryoEF_process.process])
        #plot angles
        self.plotAngles = plotAngles(
                    job_location=self.job_location,
                    command=self.commands['plotOD'],
                    angles_file_path=self.args.angles_file_path.value,
                    name='plotOD')
        pl.append([self.plotAngles.process])
        # Generate results
        results_path = os.path.realpath(cryoEF_results.__file__)
        results_process = process_manager.CCPEMProcess(
            name='cryoEF results',
            command='ccpem-python',
            args=[results_path, os.path.join(self.job_location,'task.ccpem'),
                  self.args.angles_file_path.value],
            location=self.job_location)
        pl.append([results_process])
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class extractAngles():
    '''
    Wrapper for extractAngles.py
    '''
    def __init__(self,
                 job_location,
                command,
                star_file_path,
                output_angles_file=None,
                name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.args = [star_file_path]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location)

class extractSubset():
    '''
    Wrapper for extractSubset.py
    '''
    def __init__(self,
                 job_location,
                command,
                angles_file_path,
                output_subset_file=None,
                number_angles=1000,
                name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.args = [number_angles]
        self.args += [angles_file_path]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location)

class plotAngles():
    '''
    Wrapper for PlotOD.py
    '''
    def __init__(self,
                job_location,
                command,
                angles_file_path,
                name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.args = [angles_file_path]
        
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
        
class cryoEF_wrapper():
    '''
    Wrapper for cryoEF
    '''
    def __init__(self,
                 job_location,
                command,
                angles_file_path,
                particle_symmetry=None,
                particle_diameter = None,
                particle_box_size = None,
                bfactor = None,
                resolution = None,
                angular_accuracy=None,
                max_tilt=None,
                name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.args = ['-f',angles_file_path]
        if particle_symmetry is not None:
            self.args += ['-g',particle_symmetry]
        if particle_diameter is not None:
            self.args += ['-D',particle_diameter]
        if particle_box_size is not None:
            self.args += ['-b',particle_box_size]
        if bfactor is not None:
            self.args += ['-B',bfactor]
        if resolution is not None:
             self.args += ['-r',resolution]
        if angular_accuracy is not None:
            self.args += ['-a',angular_accuracy]
        if max_tilt is not None:
            self.args += ['-m',max_tilt]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)