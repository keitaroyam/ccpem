import sys
import re
import os
import time
import pyrvapi
import fileinput
from collections import OrderedDict
import shutil, json
import numpy as np
import pandas as pd
import math
from jpred_log_parser import JpredLogParse
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
from ccpem_core.pdb_tools.biopy_tools import *

class SetJpredResults():
    def __init__(self,jpred_process,list_chains=[],
                 dssp_dict=None, cootset_instance=None,
                 glob_reportfile=None):
        self.jpred_process = jpred_process
        self.cootset = cootset_instance
        self.list_chains = list_chains
        self.dssp_dict = dssp_dict
        self.add_chain_sections = False
        self.residue_outliers = OrderedDict()
        self.dict_logparse = OrderedDict()
        self.dict_cootdata = OrderedDict()
        self.dict_jsonfiles = OrderedDict()
        self.list_modelids = []
        self.residue_coordinates = {}
        self.dict_outlier_details = {}
        self.dict_results_href = {}
        for jpred_job in jpred_process:
            modelid = jpred_job.name.split(' ')[-1]
            job_location = jpred_job.location
            self.list_modelids.append(modelid)
            jp = JpredLogParse(jpred_job.location)
            self.dict_logparse[modelid] = jp
            #self.residue_outliers[modelid] = {}
            if len(list_chains) == 0:
                for ch in jp.list_chains:
                    if not ch in self.list_chains:
                        self.list_chains.append(ch) 
                self.add_chain_sections = True
            self.dict_jsonfiles[modelid] = os.path.join(job_location,
                                                        modelid+"_jpred.json")
        #jpred_dictseq = jp.dict_seq
        #jpred_dictpred = jp.dict_pred
        self.set_local_data()
        self.glob_reportfile = glob_reportfile
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('*Jpred4* secondary structure prediction from sequence\n')
            self.glob_reportfile.write('=====================================================\n')
            
        self.set_global_results()
        if self.glob_reportfile is not None:
            self.glob_reportfile.write('-----------------------------------------------------\n\n')
            
        self.save_outlier_json()
        self.add_cootdata()
        self.delete_cootdata()
        self.set_local_results()
        
        
    def set_global_results(self):
        '''
        Set summary in the global tab
        '''
        tab_id = 'global_tab'
        section_id = 'jpred_global_sec'
        if len(self.dict_summary) > 0:
            pyrvapi.rvapi_add_section(section_id, 'JPred summary', tab_id, 
                                      0, 0, 1, 1, False)
            self.set_summary_table(tab_id,section_id,"jpred_summary")

    def set_summary_table(self, tab, section, table_id):
        '''
        Set global summary table
        '''
        add_table_to_section(section,table_id)
        gs = global_summary_settings()
        add_vert_headers_to_table(table_id,gs.list_vert_headers,
                                  gs.list_vert_header_tooltips)
        #expand vert header for all models
        list_horz_headers, list_horz_header_tooltips = \
            self.expand_table_header_models(gs.list_horz_headers, 
                                            gs.list_horz_header_tooltips)
        
        add_horz_headers_to_table(table_id,list_horz_headers,
                                  list_horz_header_tooltips)
        
        ct_model = 0
        
        for mid in self.list_modelids:
            #Percent of outliers based on Jpred
            try: data_array = [[self.dict_summary[mid]]]
            #mid not in dict_summary
            except KeyError: continue
            #add for each model
            add_data_to_table(table_id,data_array, start_col=ct_model)
            ct_model += 1
        pyrvapi.rvapi_flush()

    def expand_table_header_models(self, list_headers, list_header_tooltips):
        list_expanded_headers = []
        list_expanded_header_tips = []
        for mid in self.list_modelids:
            ct_h = 0
            for h in xrange(len(list_headers)):
                header = list_headers[h]
                header += "<br>({})".format(mid)
                list_expanded_headers.append(header)
                list_expanded_header_tips.append(list_header_tooltips[h])
        return list_expanded_headers, list_expanded_header_tips

    def set_local_data(self):
        '''
        Get outliers for each model and chain
            check dssp data for the chain
            save results in dict
        '''
        self.dict_summary = {}
        self.dict_outliers = {}
        self.dict_model_len = {}
        for modelid in self.dict_logparse:
            jp = self.dict_logparse[modelid]
            dict_pred = jp.jpred_predictions
            count_chain = 0
            for chain in dict_pred:
                    #check dssp
                    dssp_chain_flag = False
                    
                    if self.dssp_dict != None:
                        if modelid in self.dssp_dict:
                            if chain in self.dssp_dict[modelid]:
                                dssp_chain_flag=True
                                if not chain in self.list_chains:
                                    self.list_chains.append(chain)
                                self.get_outliers_chain(modelid,chain,dict_pred)
        for modelid in self.dict_summary:
            try:
                self.dict_summary[modelid] = round((self.dict_summary[modelid]*100.0) \
                                                /self.dict_model_len[modelid],3)
            except KeyError:
                del self.dict_summary[modelid]
        
    def get_outliers_chain(self,mid,chain,dict_pred):
        list_data = []
        self.dict_outliers[chain] = {}
        
        outlier_settings = residue_outlier_settings()
        measure = 'Jpred Outlier'
        
        for residue_id in dict_pred[chain]:
            residue_num,resname = residue_id.split()
            try:
                self.dict_model_len[mid] += 1
            except KeyError: self.dict_model_len[mid] = 1
            pred,conf,href = dict_pred[chain][residue_id]
            if not mid in self.dict_results_href:
                self.dict_results_href[mid] = {}
            self.dict_results_href[mid][chain] = href
            
            try:
                dssp_sse = self.dssp_dict[mid][chain][residue_num][1]
            except KeyError: continue
            if dssp_sse in ['H','I','G']: dssp_sse = 'H'
            elif dssp_sse in ['E','B']: dssp_sse = 'E'
            
            #if prediction doesnt match with dssp
            if pred != dssp_sse:
                #if dssp is helix/strand and jpred prediction is of high confidence
                if dssp_sse in ['H','E'] and conf >7:
                    try: self.dict_summary[mid] += 1
                    except KeyError: self.dict_summary[mid] = 1
                    
                    residue_details = [residue_id]
                    residue_details.append(pred)
                    residue_details.append(dssp_sse)
                    if len(residue_details) == 3: 
                        list_data.append(residue_details[:])
                    #save outlier residue and outlier type for summary table
                    if not mid in self.residue_outliers:
                        self.residue_outliers[mid] = {}
                    try:
                        self.residue_outliers[mid][chain][str(residue_num)] = \
                                outlier_settings.data_titles[measure]
                    except KeyError:
                        self.residue_outliers[mid][chain] = OrderedDict([
                        (str(residue_num),outlier_settings.data_titles[measure])])
                                
                    #get coordinates for outlier
                    CAx,CAy,CAz = [float(val) for val in \
                                self.dssp_dict[mid][chain][residue_num][-3:] ]
                    #save coordinates
                    if not mid in self.residue_coordinates:
                        self.residue_coordinates[mid] = {}
                    try:
                        self.residue_coordinates[mid][chain][str(residue_num)] = \
                                (CAx,CAy,CAz)
                    except KeyError:
                        self.residue_coordinates[mid][chain] = OrderedDict([
                                (str(residue_num),(CAx,CAy,CAz))])
                    #set coot data dictionary
                    if self.cootset is not None:
                        coot_data_res = str(chain),str(residue_num),str(resname),str(pred),\
                                str(dssp_sse),(CAx,CAy,CAz)
                        try: 
                            self.dict_cootdata[mid]['jpred'].append((coot_data_res))
                        except KeyError:
                            self.dict_cootdata[mid] = {'jpred':[(coot_data_res)]}
        #set dict of outlier details
        if len(list_data) == 0: return
        #save outlier data in dictionary
        if not mid in self.dict_outlier_details:
            self.dict_outlier_details[mid] = {}
        if not chain in self.dict_outlier_details[mid]:
            self.dict_outlier_details[mid][chain] = {}
        self.dict_outlier_details[mid][chain][measure] = list_data[:]
        
    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError: continue
            with open(jsonfile,'w') as jf:
                json.dump(self.dict_outlier_details[mid],jf)
                
    def delete_dictresults(self):
        '''
        Delete results data dict
        '''
        self.dict_outlier_details.clear()
        del self.dict_outlier_details
    
    def set_local_results(self):
        '''
        Set local sections and tables
        '''
        #local_tab = 'local_tab'
        #local_tab_name = "Local Results"
        outlier_settings = residue_outlier_settings()
        count_chain = 0
        
        for chain in self.list_chains:
            chain_section = chain
            chain_name = 'chain '+chain
            
            if hasattr(self, 'dict_outlier_details'):
                if len(self.dict_outlier_details) == 0 and \
                        not self.check_chain_outliers(chain): continue
            if self.add_chain_sections:
                pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                          local_tab, 0, 0, 1, 1, False)
            
            self.set_outlier_sections_tables(chain_section,
                                             outlier_settings)
            
            count_chain += 1
        pyrvapi.rvapi_flush()
        self.delete_dictresults()
        
    def check_chain_outliers(self,chain):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                return True
        return False
    
    def set_outlier_sections_tables(self,chain,outlier_settings):
        measure = 'Jpred Outlier'
        outlier_subsection = \
                "JPred"+''.join(measure.split())+str(chain)
        
        if measure in outlier_settings.data_titles:
            outlier_subsection_name = "JPred: "+ \
                                outlier_settings.data_titles[measure]
            add_subsection(chain,outlier_subsection,
                           outlier_subsection_name)
            add_text_to_section(outlier_subsection,
                                [outlier_settings.data_info[measure]])
            #add separate tables for each model
            for mid in self.list_modelids:
                if mid in self.dict_results_href:
                    try:
                        href = self.dict_results_href[mid][chain]
                    except KeyError: pass
#                     if len(href) > 0:
#                         print "Link to server results for {}: {}".format(mid,href)
#                         href_string = '<br><a href="{}" ' \
#                                     'target="_blank">link to server results:{}</a>'.format(href,mid)
#                         add_text_to_section(outlier_subsection,
#                                 [href_string])
                table_id = 'jpred'+''.join(outlier_settings.data_titles[measure].split()) + \
                            str(chain)+mid
                #print table_id
                table_name = mid #+ ':' +outlier_settings.data_titles[measure]
                add_table_to_section(outlier_subsection,table_id, table_name)
                
                list_horz_headers = outlier_settings.data_headers[measure][:]
                list_horz_headers_tips = outlier_settings.data_tooltips[measure][:]
        
                #add dssp column
                list_horz_headers.append('DSSP')
                list_horz_headers_tips.append('Current secondary structure (by DSSP)')
                #print list_horz_headers
                add_horz_headers_to_table(table_id,list_horz_headers,list_horz_headers_tips)
                
                self.add_outlier_data_to_table(table_id,chain,mid,measure)
        pyrvapi.rvapi_flush()
    
    def add_outlier_data_to_table(self,table_id,chain,mid,measure):
        '''
        Add outlier data from dictionary to table
        '''
        if mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                if measure in self.dict_outlier_details[mid][chain]:
                    list_data = self.dict_outlier_details[mid][chain][measure]
                    add_data_to_table(table_id,list_data)
    
    def add_cootdata(self):
        '''
        Add outliers to coot todo list (coot script) 
        '''
        for mid in self.dict_cootdata:
            if self.cootset is not None:
                self.cootset.add_data_to_cootfile(self.dict_cootdata[mid],
                    modelid=mid, method='jpred')
    
    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata
                

class global_summary_settings() :
    data_keys = ['Jpred Outliers']
    list_horz_headers = ['Percent']
    list_horz_header_tooltips = ['Outlier percent']
    list_vert_headers = data_keys
    list_vert_header_tooltips = [' ',' ',' ']

class residue_outlier_settings() :
    #NOTE: data_keys has same keywords as in log file
    #change this with updates
    data_keys = ['Jpred Outlier']
    data_titles = {'Jpred Outlier':'outlier'}
    data_headers = {
                    'Jpred Outlier':["residue","SS prediction"]
                    }
    data_tooltips = {
                    'Jpred Outlier':["residue","Secondary structure predicted from sequence"]
                    }
    data_info = {'Jpred Outlier':'''DSSP codes: <br>'H': helix, 'E':strand, 'T':turn, 
                                'B': beta bridge, 'G': helix-3, 'I': helix-5,
                                 'S':bend'''}
