import pyrvapi
import re
import os

# Resolution label (equivalent to <4SSQ/LL>
ang_min_one = (ur'Resolution (\u00c5-\u00B9)').encode('utf-8')
angstrom_label = (ur'Resolution (\u00c5)').encode('utf-8')

list_line_colors = ['Lime','Orange','Pink','Black','Gray',
                    'Brown','Magenta','Indigo','Purple',
                    'Blue',
                    'Red','Maroon', 'Lime','Orange','Pink','Black','Gray',
                    'Brown','Magenta','Indigo','Purple',
                    'Blue',
                    'Red','Maroon'
                    ]
list_fill_colors = [
                    'Pink',
                    'Aquamarine','Plum',
                    'Salmon','Orange',
                    'Silver'
                    ]
list_cell_colors = ['blue','green','orange','pink','grey','red','lime',
                    'brown','yellow']

global_tab_name = 'Global results'
global_tab = 'global_tab'
local_tab = 'local_tab'
local_tab_name = "Local Results"

def add_subsection(section_id, subsection_id, subsection_name,show=False):
    pyrvapi.rvapi_add_section1(section_id+'/'+subsection_id, 
                                       subsection_name, 
                                       0, 0, 1, 1, show)
def add_table_to_section(section_id,table_id, table_name='',show=True):
    table_section = section_id+'/'+table_id
    pyrvapi.rvapi_add_table1(table_section,table_name,1, 0, 1, 1, show)

def add_horz_headers_to_table(table_id, list_headers, list_tooltips):
    for l in xrange(len(list_headers)):
        pyrvapi.rvapi_put_horz_theader(table_id,list_headers[l],
                                        list_tooltips[l], l)
def add_vert_headers_to_table(table_id, list_headers, list_tooltips):
    for l in xrange(len(list_headers)):
        pyrvapi.rvapi_put_vert_theader(table_id,list_headers[l],
                                        list_tooltips[l], l)

def add_data_to_table(table_id,data_array,start_col=0,start_row=0):
    for r in xrange(len(data_array)):
        for c in xrange(len(data_array[r])):
            pyrvapi.rvapi_put_table_string(table_id,
                str(data_array[r][c]),r+start_row,c+start_col)

def customize_table_cell(table_id,x,y,cell_color="table-blue",
                         rowspan=1,colspan=1):
    pyrvapi.rvapi_shape_table_cell(table_id,  # tableId
            x,  # row
            y,  # column
            "",  # tooltip
            "",  # cell_css
            "",  #cell_color  ( cell_style)
            rowspan,  # rowSpan
            colspan)  # colSpan
    
    
def add_text_to_section(section_id, list_string,start_row=1):
    #TODO: check how to print strings in lines
    for line in list_string:
        pyrvapi.rvapi_add_text(line, section_id, start_row, 0, 1, 1)
        start_row += 1
    pyrvapi.rvapi_flush()

def add_axis_data(graphwidget_id,data_id,plotlinename,
                  list_val,val_id, OriginalAxisOrder=False,axisdtype='float',
                  dataflag="%g"):

    for l in xrange(len(list_val)):
        if OriginalAxisOrder:
            if axisdtype == 'int':
                pyrvapi.rvapi_add_graph_int1 (
                        graphwidget_id+'/'+data_id+'/'+val_id,
                        l)
            elif axisdtype == 'float':
                pyrvapi.rvapi_add_graph_real1 (
                        graphwidget_id+'/'+data_id+'/'+val_id,
                        float(l),dataflag)
        else:
            if axisdtype == 'int':
                pyrvapi.rvapi_add_graph_int1(
                    graphwidget_id+'/'+data_id+'/'+val_id,
                    list_val[l]
                    )
            elif axisdtype == 'float':
                pyrvapi.rvapi_add_graph_real1(
                    graphwidget_id+'/'+data_id+'/'+val_id,
                    list_val[l],dataflag
                    )

def set_axis_ticks(graphwidget_id,data_id,plotlinename,
                  list_val,plot_id='plot1',
                  axisdtype='float'):
    pyrvapi.rvapi_reset_plot_xticks(plot_id, graphwidget_id)
    for l in xrange(0,len(list_val),2):
        if axisdtype == 'int':
            pyrvapi.rvapi_add_plot_xtick (
                            plot_id,
                            graphwidget_id,
                            l,
                            '%d' % list_val[l])
        elif axisdtype == 'float':
            pyrvapi.rvapi_add_plot_xtick (
                            plot_id,
                            graphwidget_id,
                            float(l),
                            '%0.1f' % list_val[l])

def add_plot_to_graph(graphwidget_id,data_id,plotname,plotlinename,list_x,list_y,
                          x_label, y_label,xtype='float',ytype='float',
                          plot_id='plot1',
                          lnum=1,
                          OriginalXaxisOrder=False,
                          line_fill=False,
                          line_color=pyrvapi.RVAPI_COLOR_Green,
                          fill_color=pyrvapi.RVAPI_COLOR_Gold):
    
    pyrvapi.rvapi_add_graph_dataset1(
                                graphwidget_id+'/'+data_id+'/x',
                                'x',
                                x_label)
    
    add_axis_data(graphwidget_id, data_id, plotlinename, list_x, 'x',
                  OriginalXaxisOrder, axisdtype=xtype,dataflag='')
    
    pyrvapi.rvapi_add_graph_plot1(
                                graphwidget_id+'/'+plot_id,
                                 plotname,
                                 x_label,
                                 y_label)
    
    add_line_to_plot(graphwidget_id,data_id,plotlinename,list_y,
                          ytype='float',
                          plot_id=plot_id,
                          lnum=lnum,
                          line_color=line_color,
                          line_fill=line_fill,
                          fill_color=fill_color)
    if OriginalXaxisOrder:
        set_axis_ticks(graphwidget_id,data_id,plotlinename,
                  list_x,plot_id,xtype)


def add_line_to_plot(graphwidget_id,data_id,plotlinename,list_y,
                            list_x = [],
                          ytype='float',xtype='int',x_label='',
                          plot_id='plot1',
                          lnum=1,
                          #LightCoral, Aquamarine, IndianRed
                          line_color=pyrvapi.RVAPI_COLOR_Green,
                          line_show='', #RVAPI_LINE_Off
                          marker_style=pyrvapi.RVAPI_MARKER_filledCircle,
                          line_width=2.5,
                          line_fill=False,
                          fill_color=pyrvapi.RVAPI_COLOR_Gold
                          ):
    xid = 'x'
    yid = 'y'+str(lnum)
    if len(list_x) > 0:
        xid = 'x'+str(lnum)
         #add dataset
        pyrvapi.rvapi_add_graph_dataset1(
            graphwidget_id+'/'+data_id+'/'+xid,
            xid,
            x_label)
         #add data
        add_axis_data(graphwidget_id, data_id, plotlinename, list_x, xid,
                       False, axisdtype=xtype)
     #add dataset
    pyrvapi.rvapi_add_graph_dataset1(
        graphwidget_id+'/'+data_id+'/'+yid,
        plotlinename,
        plotlinename)
     #add data
    add_axis_data(graphwidget_id, data_id, plotlinename, list_y, yid,
                   False, axisdtype=ytype)
    #add line
    pyrvapi.rvapi_add_plot_line1(
        graphwidget_id+'/'+data_id+'/'+plot_id,
                             xid,
                             yid)
    #set line options
    pyrvapi.rvapi_set_line_options1(graphwidget_id+'/'+data_id+'/'+plot_id+'/'+yid,
                          line_color,
                          line_show,marker_style,
                          line_width, True )
    if line_fill:
        pyrvapi.rvapi_set_line_fill1 ( graphwidget_id+'/'+data_id+'/'+plot_id+'/'+yid,
                        True,True,fill_color,0.2 )