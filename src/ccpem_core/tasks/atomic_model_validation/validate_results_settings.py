import sys
import re
import os
import shutil
import numpy as np
import pandas as pd
import math
from collections import OrderedDict

class residue_summary_table_settings():
    #NOTE: data_keys has same keywords as in molprobity log
    data_titles = ['residue',
                        'molprobity',
                        'cablam',
                        'smoc',
                        'jpred']
    list_horz_headers = ['Residue',"Molprobity <br> outliers",
                         "CaBLAM <br> outliers", "SMOC <br> outliers",
                         "Jpred <br> outliers"]
    list_horz_header_tooltips = ['Residue number',
                            'Outlier types from Molprobity',
                            'Outlier types from CaBLAM',
                            'Outlier based on SMOC z-score',
                            'Outlier based on Jpred SS prediction vs current SS']
    list_vert_headers = []
    list_vert_header_tooltips = []
