import os
import json

import mrcfile

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.smoc.smoc_task import SMOC
from ccpem_core.tasks.refmac.refmac_task import RefmacMapToMtz, RefmacRefine
from ccpem_core.tasks.tempy.smoc import smoc_process
from ccpem_core.process_manager import job_register
from ccpem_core import settings
import validate_results
import subprocess

#class to override stdout headers and footers 
class ccpemprocess_nostdoutheader(process_manager.CCPEMProcess):
    def write_header_stdout(self):
        pass
    def write_footer_stdout(self):
        pass
    
class ReduceRun():
    '''
    Wrapper for reduce run
    '''
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.pdb_path = pdb_path
        self.out_path = os.path.join(self.job_location,'reduce_out.pdb')
        self.set_args()
        #set custom finish
        on_finish_custom = ReduceRunOnFinishCustom()
         # Set process
        assert command is not None
        self.process = ccpemprocess_nostdoutheader(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None,
            stdout=self.out_path,
            on_finish_custom=on_finish_custom)
       # Set args
    def set_args(self):
#         args_string = '-FLIP ' + self.pdb_path + ' 1 > '+self.out_path
#         self.args = [args_string]
        self.args = ['-FLIP']
        self.args = ['-Quiet']
        self.args += [self.pdb_path]
#         self.args += [1]
#         self.args += ['>']
#         self.args += [self.out_path]

class ReduceRunOnFinishCustom(process_manager.CCPEMProcessCustomFinish):
    def __init__(self):
        super(ReduceRunOnFinishCustom, self).__init__()
    def on_finish(self,parent_process=None):
        if parent_process is not None:
            file_stdout = parent_process.stdout
            if not os.path.isfile(file_stdout) or \
                not self.check_reduce_output(file_stdout):
                inp_pdb = parent_process.command[-1]
                if os.path.isfile(inp_pdb):
                    try:
                        shutil.copyfile(inp_pdb,file_stdout)
                    except: pass
    
    def check_reduce_output(self,file_stdout):
        with open(file_stdout,'r') as of:
            for line in of:
                if 'ATOM' in line:
                    return True
        return False
        
    
class ResultsRun(object):
    '''
    Wrapper for results generation.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
         # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            location=self.job_location,
            stdin=None)

class MolprobityRun(object):
    '''
    Wrapper for main molprobity run.
    '''
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.pdb_path = pdb_path
        self.set_args()
         # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
       # Set args
    def set_args(self):
        self.args = [self.pdb_path]
        #self.args += ['use_pdb_header_resolution_cutoffs=True']
        self.args += ['output.percentiles=True']


class CablamRun(object):
    '''
    Wrapper for main molprobity run.
    '''
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.pdb_path = pdb_path
        self.set_args()
         # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
            #stdout='cablam.out')
       # Set args
    def set_args(self):
        self.args = [self.pdb_path]

class GlobScoreWrapper(object):
    '''
    Wrapper for TEMPy GlobScore process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        self.args = [os.path.join(self.job_location,
                                 'args.json')]
        self.args += [False]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

class SMOCWrapper(object):
    '''
    Wrapper for TEMPy SMOC process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        self.args = [os.path.join(self.job_location,
                                 'args.json')]
        self.args += [False]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

# class sf2mapWrapper():
#     '''
#     Wrapper for gemmi sf2map
#     '''
#     def __init__(self,
#                  job_location,
#                  command,
#                  map_nx,
#                  map_ny,
#                  map_nz,
#                  fast,
#                  medium,
#                  slow,
#                  mtz_path,
#                  map_path,
#                  name=None):
#         
#         self.job_location = ccpem_utils.get_path_abs(job_location)
#         self.name = name
#         if self.name is None:
#             self.name = self.__class__.__name__
#         # Set args
#         self.args = ['sf2map','-f','Fout0','-p','Pout0']
#         gridarg = '--grid='+','.join([str(map_nx),str(map_ny),str(map_nz)])
#         self.args += [gridarg]
#         self.args += ['--exact',mtz_path,map_path]
#         # Set process
#         assert command is not None
#         self.process = process_manager.CCPEMProcess(
#             name=self.name,
#             command=command,
#             args=self.args,
#             location=self.job_location,
#             stdin=None)

class BfactWrapper(object):
    '''
    Wrapper for Bfactor analysis.
    '''
    def __init__(self,
                 command,
                 model_path,
                 job_location=None,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        # Set args
        self.args = [model_path]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

# class mapprocess_wrapper():
#     '''
#     Wrapper for TEMPy Map Processing tool.
#     '''
#     def __init__(self,
#                  job_location,
#                  command,
#                  map_path,
#                  list_process,
#                  map_resolution=None,
#                  map_contour=None,
#                  map_apix=None,
#                  map_pad=None,
#                  map_origin=None,
#                  mask_path=None,
#                  out_map=None,
#                  name='MapProcess'):
#         assert map_path is not None
#         self.job_location = ccpem_utils.get_path_abs(job_location)
#         self.name = name
#         if self.name is None:
#             self.name = self.__class__.__name__
#         self.map_path = ccpem_utils.get_path_abs(map_path)
#         # Set args
#         self.args = ['-m', self.map_path]
#         if list_process is not None:
#             list_process_args = ['-l']
#             list_process_args.extend(list_process)
#             self.args += list_process_args
#         if map_resolution is not None:
#             self.args += ['-r', map_resolution]
#         #threshold
#         if map_contour is not None:
#             self.args += ['-t', map_contour]
#         #new apix
#         if map_apix is not None:
#             self.args += ['-p', map_apix]
#         #padding
#         if map_pad is not None:
#             pad_args = ['-pad']
#             pad_args.extend(list(map_pad))
#             if len(map_pad) > 0: self.args += pad_args#' '.join(
#                                                     #[str(p) for p in map_pad])]
#         #new origin
#         if map_origin is not None:
#             origin_args = ['-ori']
#             origin_args.extend(map_origin)
#             if len(map_origin) > 0: self.args += origin_args
#         #mask file
#         if mask_path is not None:
#             self.args += ['-ma', mask_path]
#         
#         if out_map is not None:
#             self.args += ['-out', out_map]
#         
#         assert command is not None
#         self.process = process_manager.CCPEMProcess(
#             name=self.name,
#             command=command,
#             args=self.args,
#             location=self.job_location,
#             stdin=None)

class DSSPRun():
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 output_dssp = 'output.dssp',
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        self.args = [pdb_path, output_dssp]
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None,
            on_finish_custom=None)

#Generate RVAPI Results
class ValidationResultsOnRunning(process_manager.CCPEMPipelineCustomRunning):
    '''
    Generate RVAPI results on finish.
    '''
    def __init__(self,
                 pipeline_path):
        super(ValidationResultsOnRunning, self).__init__()
        self.pipeline_path = pipeline_path

    def on_running(self, parent_pipeline=None):
        #run script to generate results
        results_scriptfile = validate_results.__file__
        run_command = ['ccpem-python',results_scriptfile]
        run_command.append(self.pipeline_path)
        #print run_command
        subprocess.Popen(run_command)

#Generate RVAPI Results
class ValidationResultsOnFinish(process_manager.CCPEMPipelineCustomFinish):
    '''
    Generate RVAPI results on finish.
    '''
    def __init__(self,
                 pipeline_path):
        super(ValidationResultsOnFinish, self).__init__()
        self.pipeline_path = pipeline_path

    def on_part_fail(self, parent_pipeline=None):
        print 'A few jobs failed'
        #generate RVAPI report
        validate_results.PipelineResultsViewer(
                  pipeline_path=self.pipeline_path)
    def on_finish(self, parent_pipeline=None):
        #generate RVAPI report
        validate_results.PipelineResultsViewer(
                  pipeline_path=self.pipeline_path)

