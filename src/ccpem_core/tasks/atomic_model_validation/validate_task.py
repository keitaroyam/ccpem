#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import json, shutil
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.tempy.smoc.smoc_task import SMOC
from ccpem_core.tasks.tempy.difference_map import difference_map_task
from ccpem_core.tasks.tempy.difference_map import difference_map
from ccpem_core.tasks.tempy.scores.scores_task import GlobScore
from ccpem_core.tasks.refmac.refmac_task import RefmacMapToMtz, RefmacRefine, \
    RefmacSfcalcCrd
from ccpem_core.tasks.bin_wrappers import fft
from ccpem_core.tasks.tempy.smoc import smoc_process
from ccpem_core.tasks.tempy.scores import scores_process
from ccpem_core.tasks.atomic_model_validation.bfactor_analysis import analyse_bfactors
from ccpem_core.tasks.model2map.model2map_task import mapprocess_wrapper, \
                sf2mapWrapper, PDBSetCell, fix_map_model_refmac
from ccpem_core.tasks.jpred.jpred_task import JPredRunWrapper, JPredDownloadWrapper
from ccpem_core.tasks.atomic_model_validation import validate_results
from ccpem_core.process_manager import job_register
from ccpem_core import settings
from run_validation_tasks import *
from ccpem_progs.jpred import jpred_submit
from ccpem_core.tasks.jpred import jpred_download
import mrcfile
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
from ccpem_core.map_tools.TEMPy import map_preprocess \
                            , attribute_map_values

class ValidateTask(task_utils.CCPEMTask):
    '''
    Atomic model validation
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Model_validation',
        author='VB. Chen, et al.,Brown A et al., Joesph et al',
        version='',
        description=(
            '''Validate protein atomic model quality and agreement with map data.<br><br>
            Reports outliers based on 
            stereochemistry (Molprobity),<br>
            C-alpha geometry/secondary structure (CaBLAM),<br>
            local fit of atomic model in density (TEMPy SMOC),<br>
            and global density fit by model-map FSC (Refmac)<br><br>
            Refer:<br>
            MolProbity: http://molprobity.biochem.duke.edu/ <br>
            CaBLAM : https://www.phenix-online.org/newsletter/CCN_2013_07.pdf <br>
            '''),
        short_description=(
            'Evaluate atomic model geometry and density fit'),
        documentation_link='http://molprobity.biochem.duke.edu/ ',
                        
        references=None)

    commands = {'molprobity': settings.which(program='molprobity.molprobity'),
                'reduce': settings.which(program='molprobity.reduce'),
                'cablam': settings.which(program='molprobity.cablam'),
                'ccpem-smoc': ['ccpem-python', os.path.realpath(smoc_process.__file__)],
                'ccpem-scores': ['ccpem-python', os.path.realpath(scores_process.__file__)],
                'ccpem-ribfind': settings.which('ccpem-ribfind'),
                'pdbset':  settings.which(program='pdbset'),
                'ccpem-gemmi':settings.which(program='gemmi'),
                'refmac': settings.which(program='refmac5'),
                'dssp': settings.which('mkdssp'),
                'ccpem-jpred':
                ['ccpem-python', os.path.realpath(jpred_submit.__file__)],
                'jpred-download':
                ['ccpem-python', os.path.realpath(jpred_download.__file__)],
                'ccpem-mapprocess':
                ['ccpem-python', os.path.realpath(map_preprocess.__file__)],
                'pdbset':  settings.which(program='pdbset'),
                'bfactor':
                ['ccpem-python',os.path.realpath(analyse_bfactors.__file__)],
                'ccpem-diffmap':
                 ['ccpem-python', os.path.realpath(difference_map.__file__)],
                 'ccpem-mapval':
                 ['ccpem-python', 
                        os.path.realpath(attribute_map_values.__file__)]
                }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(ValidateTask, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)
        
        #self.smoc_task = SMOC()
    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Input pdb'),
            metavar='Input atomic model',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_pdbs',
            '--input_pdbs',
            help=('Input pdb(s). \n'
                  'Ensure same residue numbering in all. \n'
                  'Ensure atomic B-factors are refined/set (for Refmac) \n'
                  'Currently supports single model only (per file)'),
            metavar='Input atomic model',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-fixed_pdbs',
            '--fixed_pdbs',
            help=('Fixed PDBs for Refmac'),
            metavar='Fixed atomic models',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-input_pdb_chains',
            '--input_pdb_chains',
            help='Input PDB chain(s)',
            metavar='Input PDB chain selections',
            type=str,
            nargs='*',
            default=None)
        
        # Refine B-factors
        parser.add_argument(
            '-refine_bfactor',
            '--refine_bfactor',
            help=('Refine atomic B-factors'),
            metavar='Refine atomic B-factors?',
            type=bool,
            default=False)

        # use Refmac to synthesize maps for SMOC and global scores
        parser.add_argument(
            '-use_refmac',
            '--use_refmac',
            help=('Use Refmac to make map from model. \
Ensure atomic B-factors are refined. \
Input map and resolution to activate'),
            metavar='Use Refmac to simulate map from model?',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-sim_maps',
            '--sim_maps',
            help='Synthetic maps from models, to score against map',
            metavar='Input synthetic maps',
            type=str,
            nargs='*',
            default=None)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #input map
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help=('Input map (mrc format). \n'
                  'Ensure model is fitted inside map. \n'
                  'Required for evaluating density fits'),
            metavar='Input map',
            type=str,
            default=None)
        map_path.add_argument(
            '-map_path_edit',
            '--map_path_edit',
            help='Edited map (mrc format)',
            metavar='Edited input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of map (Angstrom) \n'
                  'Required for evaluating density fit'),
            metavar='Map resolution',
            type=float,
            default=None)
        
        num_proc = 1
        num_proc = max(self.get_ncpu()-1,num_proc)
        parser.add_argument(
            '-ncpu',
            '--ncpu',
            help= ('Number of cpus to use \n'
                   '(max cpus - 1, by default)'),
            metavar='Number of cpus',
            type=int,
            default=num_proc)
        
        parser.add_argument(
            '-results_parallel',
            '--results_parallel',
            help= ('''Generate results in parallel.\n'''),
            metavar='Generate results in parallel',
            type=bool,
            default=False)
        
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help=('Half map 1 (mrc format). Can be another map to compare fit against.\
Same dimensions and voxel size as the fullmap recommended'),
            metavar='Half map 1',
            type=str,
            default=None)
        
        parser.add_argument(
            '-half_map1_edit',
            '--half_map1_edit',
            help=('Half map 1 fixed for origin'),
            metavar='Half map1 fixed',
            type=str,
            default=None)
        
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help=('Half map 2 (mrc format).Can be another map to compare fit against.\
Same dimensions and voxel size as the fullmap recommended'),
            type=str,
            metavar='Half map 2',
            default=None)
        
        parser.add_argument(
            '-half_map2_edit',
            '--half_map2_edit',
            help=('Half map 2 fixed for origin'),
            type=str,
            metavar='Half map2 fixed',
            default=None)
        
        parser.add_argument(
            '-modelmap',
            '--modelmap',
            help='Map simulated from model (mrc format)',
            type=str,
            default=None)
        parser.add_argument(
            '-run_molprobity',
            '--run_molprobity',
            help= ('''Check geometry using molprobity'''),
            metavar='Geometry (Molprobity)',
            type=bool,
            default=True)
        parser.add_argument(
            '-run_cablam',
            '--run_cablam',
            help= ('''Check CA geometry using CaBLAM'''),
            metavar='Ca geometry(CaBLAM)',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-run_smoc',
            '--run_smoc',
            help= ('''SMOC: calculate Segment Manders\' Overlap Coefficient  \n''' \
                   ''' Scores residues and outputs per residue plots'''),
            metavar='Local density fit (TEMPy)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_scores',
            '--run_scores',
            help= ('''TEMPy scores: calculate global scores for model fit  \n'''),
            metavar='Global density fit (TEMPy)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_jpred',
            '--run_jpred',
            help= ('''predict secondary structure from sequence.  \n''' \
                   '''Task run on the Jpred server, may take several minutes'''),
            metavar='SSE prediction (JPred)',
            type=bool,
            default=False)
        parser.add_argument(
            '-run_refmac',
            '--run_refmac',
            help= ('''REFMAC: calculate model-map FSC and FSCavg'''),
            metavar='Model-map FSC (Refmac)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_diffmap',
            '--run_diffmap',
            help= ('''TEMPy diffmap: calculate model-map difference\n'''),
            metavar='Difference map (TEMPy)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-run_fdr',
            '--run_fdr',
            help= ('''FDR scores: calculate FDR map values for model\n'''),
            metavar='Fit to signal vs background (Confidence maps)',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-output_dssp',
            '--output_dssp',
            help='File name for dssp output',
            metavar='Output dssp',
            type=str,
            default='output.dssp')
        #set contour
        parser.add_argument(
            '-auto_contour_level',
            '--auto_contour_level',
            help='''Automatically set map contour level. 
                    Unselect to set manually (recommended for masked/segmented maps).''',
            metavar='Auto set contour',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-map_contour_level',
            '--map_contour_level',
            help='''Set map contour level''',
            metavar='Contour level',
            type=float,
            default=None)
        #SMOC srguments
        SMOC().add_basic_arguments(parser)
        SMOC().add_advanced_arguments(parser)
        return parser

    def get_ncpu(self):
        try:
            import multiprocessing
            return multiprocessing.cpu_count()
        except (ImportError, NotImplementedError):
            return 1

    def ribfind_job(self, db_inject=None, job_title=None):
        # Set 
        if self.database_path is None:
            path = os.path.dirname(self.job_location)
        else:
            path = os.path.dirname(self.database_path)

        job_id, job_location = job_register.job_register(
            db_inject=db_inject,
            path=path,
            task_name=ribfind_task.Ribfind.task_info.name)

        # Set task args
        args = ribfind_task.Ribfind().args
        args.job_title.value = job_title
        pdbs = self.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        args.input_pdb.value = pdbs[0]
        args_json_string = args.output_args_as_json(
            return_string=True)

        # Set process args
        process_args = ['--no-gui']
        process_args += ["--args_string='{0}'".format(
            args_json_string)]
        process_args += ['--job_location={0}'.format(
            job_location)]
        if self.database_path is not None:
            process_args += ['--project_location={0}'.format(
                os.path.dirname(self.database_path))]
        if job_id is not None:
            process_args += ['--job_id={0}'.format(
                job_id)]

        # Create process
        self.ribfind_process = process_manager.CCPEMProcess(
            name='Ribfind auto run task',
            command=self.commands['ccpem-ribfind'],
            args=process_args,
            location=job_location,
            stdin=None)

    def copy_argsfile(self,argsfile, new_argsfile, new_map_path=None,
                      new_pdb_path=None):
        
        with open(argsfile,'r') as inpf:
            json_args = json.load(inpf)
        with open(new_argsfile,'w') as outf:
            json_args['job_location'] = os.path.dirname(new_argsfile)
            #set ribfind task output
            json_args['rigid_body_path'] = self.args.rigid_body_path.value
            if new_map_path is not None:
                json_args['map_path'] = new_map_path
                json_args['map_path_edit'] = new_map_path
            if new_pdb_path is not None:
                json_args['input_pdbs'] = new_pdb_path
            json.dump(json_args,outf)
            
    def edit_argsfile(self,args_file, new_map_path=None,
                      contour_level=None):
        with open(args_file,'r') as f:
            json_args = json.load(f)
            #set ribfind task output
            json_args['rigid_body_path'] = self.args.rigid_body_path.value
            #set mapprocess task output
            if self.mapprocess_task is not None:
                mapprocess_job_location = self.mapprocess_task.job_location
                #the processed map is written to the same dir as input map
                #the job location has other output files
                if mapprocess_job_location is not None:
                    processed_map_path = os.path.splitext(
                                        os.path.abspath(self.args.map_path.value))[0] \
                                        +'_processed.mrc'
                    #print processed_map_path
                    if os.path.isfile(processed_map_path):
                        json_args['map_path'] = processed_map_path
                        self.unprocessed_map = self.args.map_path.value
                        self.args.map_path.value = processed_map_path
            if new_map_path is not None:
                json_args['map_path'] = new_map_path
            #add synthetic maps as an argument
            if self.args.use_refmac.value:
                json_args['map_path_edit'] = self.args.map_path_edit.value
                if new_map_path is not None:
                    json_args['map_path_edit'] = new_map_path
                if len(self.args.sim_maps) == len(self.list_pdbs):
                    json_args['sim_maps'] = self.args.sim_maps
            #add user input contour
            if contour_level is not None:
                json_args['map_contour_level'] = contour_level
                json_args['auto_contour_level'] = False
            
        with open(args_file,'w') as f:
            json.dump(json_args,f)
    #translate model to fit to shifted map
    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)

    def check_processed_map(self):
        processed_map_path = os.path.splitext(
                            os.path.abspath(self.args.map_path.value))[0] \
                            +'_processed.mrc'
        #print processed_map_path
        if os.path.isfile(processed_map_path) and \
            self.mapprocess_task is not None:
            self.map_input.value_line.setText(processed_map_path)

    def set_model_lists(self):
        #set input pdbs
        self.list_pdbs = self.args.input_pdbs()
        
        if self.list_pdbs == None or len(self.list_pdbs) == 0:
            if self.args.pdb_path.value != None:
                self.list_pdbs = [self.args.pdb_path.value]
        elif not isinstance(self.list_pdbs, list):
            self.list_pdbs = [self.list_pdbs]
        
        #set input maps
        self.list_maps = []
        if self.args.map_path.value != None and not self.args.map_path.value in self.list_maps:
            self.list_maps.append(self.args.map_path.value)
        if self.args.half_map_1.value != None and not self.args.half_map_1.value in self.list_maps:
            self.list_maps.append(self.args.half_map_1.value)
        if self.args.half_map_2.value != None and not self.args.half_map_2.value in self.list_maps:
            self.list_maps.append(self.args.half_map_2.value)
            
    def set_model_ids(self):
        self.set_model_lists()
        #TODO: set mapids from basenames
        #self.list_mapids = ['map0','hm1','hm2']
#         #set map ids
        ct_map = 0
        self.list_mapids = []
        for map in self.list_maps:
            map_basename = os.path.basename(map)
            try: map_basename = '.'.join(map_basename.split('.')[:-1])
            except IndexError: 
                map_basename = map_basename.split('.')[0]
            if len(map_basename) <= 10:
                mapid = map_basename
            else:
                mapid = map_basename[:5]+'_'+\
                    map_basename[-5:]+'_'+\
                    str(ct_map)
            if not mapid in self.list_mapids:
                self.list_mapids.append(mapid)
            ct_map += 1
        
        self.list_pdbids = []
        #set model ids
        ct_pdb = 0
        for pdb in self.list_pdbs:
            pdb_basename = os.path.basename(pdb)
#             try: pdb_basename = '.'.join(pdb_basename.split('.')[:-1])
#             except IndexError: 
#                 pdb_basename = pdb_basename.split('.')[0]
            pdb_basename = os.path.splitext(pdb_basename)[0]
            if len(pdb_basename) <= 10:
                pdbid = pdb_basename+'_'+\
                    str(ct_pdb)
            else:
                pdbid = pdb_basename[:5]+'_'+\
                    pdb_basename[-5:]+'_'+\
                    str(ct_pdb)
            if not pdbid in self.list_pdbids:
                self.list_pdbids.append(pdbid)
            ct_pdb += 1

    def fix_map_model_refmac(self,mapfile):
        ori_x = ori_y = ori_z = 0.
        fix_map_model = True
        with mrcfile.open(mapfile, mode='r',permissive=True) as mrc:
            map_nx = mrc.header.nx
            map_ny = mrc.header.ny
            map_nz = mrc.header.nz
            map_nxstart = mrc.header.nxstart
            map_nystart = mrc.header.nystart
            map_nzstart = mrc.header.nzstart
            map_mapc = mrc.header.mapc
            map_mapr = mrc.header.mapr
            map_maps = mrc.header.maps
            ori_x = mrc.header.origin.x
            ori_y = mrc.header.origin.y
            ori_z = mrc.header.origin.z
            cella = (mrc.header.cella.x,mrc.header.cella.y,
                     mrc.header.cella.z)
            apix = mrc.voxel_size.item()
            self.map_dim = (map_nx,map_ny,map_nz)
        #fix map and model when nstart is non-zero
        if ori_x == 0. and ori_y == 0. and ori_z == 0.:
            if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
                #fix origin to match nstart
                map_edit = os.path.splitext(
                            os.path.basename(mapfile))[0]+'_fix.mrc'
                self.args.map_path_edit.value = os.path.join(
                                        self.job_location,map_edit)
                shutil.copyfile(mapfile,self.args.map_path_edit.value)
                try: assert os.path.isfile(self.args.map_path_edit.value)
                except AssertionError:
                    return fix_map_model, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)
                #edit float origin
                with mrcfile.open(self.args.map_path_edit.value,'r+') as mrc:
                    mrc.header.origin.x = map_nxstart*apix[0]
                    mrc.header.origin.y = map_nystart*apix[1]
                    mrc.header.origin.z = map_nzstart*apix[2]
                #refmac uses existing nstart values, no need to translate models
#                 ori_x = map_nxstart*apix[0]
#                 ori_y = map_nystart*apix[1]
#                 ori_z = map_nzstart*apix[2]
                fix_map_model = False
        return fix_map_model, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)

    def run_pipeline(self, job_id=None, db_inject=None):
        #set input model and map ids
        self.set_model_ids()
        self.fixed_pdbs = []
        if self.args.map_contour_level.value is not None:
            self.args.auto_contour_level.value = False
        #make dirs for maps and models
        for mid in self.list_pdbids:
            mdir = os.path.join(self.job_location,mid)
            if not os.path.isdir(mdir):
                os.mkdir(mdir)
        #initialize edited data
        self.mapprocess_task = None
        self.args.map_path_edit.value = self.args.map_path.value
        self.args.half_map1_edit.value = self.args.half_map_1.value
        self.args.half_map2_edit.value = self.args.half_map_2.value
        self.args.fixed_pdbs.value = self.list_pdbs
        self.bfact_refined = self.list_pdbs
        fix_map_model = True
        #fix map origin
        self.args.map_path_edit.value, fix_map_model, cella, self.origin_vector, self.map_dim = \
            fix_map_model_refmac(self.args.map_path.value,self.args.map_path_edit.value,
                                 self.job_location)
        trans_vector = (-self.origin_vector[0],
                        -self.origin_vector[1],
                        -self.origin_vector[2])
        self.list_maps[0] = self.args.map_path_edit.value
        
        if self.args.half_map_1.value != None:
            self.args.half_map1_edit.value, fix_map_model, cella_h1, origin_vector_h1, map_dim_h1 = \
            fix_map_model_refmac(self.args.half_map_1.value,self.args.half_map1_edit.value,
                                 self.job_location)
            self.list_maps[1] = self.args.half_map1_edit.value
        if self.args.half_map_2.value != None:
            self.args.half_map2_edit.value, fix_map_model, cella_h2, origin_vector_h2, map_dim_h2 = \
            fix_map_model_refmac(self.args.half_map_2.value,self.args.half_map2_edit.value,
                                 self.job_location)
            self.list_maps[2] = self.args.half_map2_edit.value
        #fix map and models for refmac
        if self.args.use_refmac.value or self.args.run_refmac.value:
            
            self.set_model_ids()
            ##
            self.fixed_pdbs = []
            #fix models if required
            ct_pdb = 0
            for pdb_path in self.list_pdbs:
                pdbid = self.list_pdbids[ct_pdb]
                mdir = os.path.join(self.job_location,pdbid)
                #pdb_edit_path = pdb_path
                pdb_edit_path = os.path.join(mdir,
                                        os.path.splitext(os.path.basename(
                                            pdb_path))[0]+'_fix.pdb')
                #translate model to  nstart
                if self.origin_vector[0] != 0. or self.origin_vector[1] != 0. \
                        or self.origin_vector[2] != 0.:#if nstart non-zero
                    
                    self.fix_model_for_refmac(pdb_path,pdb_edit_path,trans_vector,
                                              remove_charges=True)
                else:
                    #remove charges
                    remove_atomic_charges(pdb_path,
                                              pdb_edit_path)
                ct_pdb += 1
                
                try:
                    self.fixed_pdbs.append(pdb_edit_path)
                except: break
                                              
            #store pdbs fixed for refmac (origin 0)
            if len(self.fixed_pdbs) == len(self.list_pdbs):
                self.args.fixed_pdbs.value = self.fixed_pdbs[:]
                self.bfact_refined = self.fixed_pdbs[:] #initialize to fixed pdbs
#         for mid in self.list_mapids:
#             mdir = os.path.join(self.job_location,mid)
#             if not os.path.isdir(mdir):
#                 os.mkdir(mdir)
        #override any sccc selection
        self.args.use_smoc.value = True
        self.args.use_sccc.value = False
        if self.args.results_parallel.value:
            self.args.ncpu.value = max(1,self.args.ncpu.value-1)
        
        pl1 = []
        #set order in which job types are added to the pipeline
        #bfactor
        self.add_bfactor_process(pl1)
        #run refmac and refine b factors
        if self.args.run_refmac.value:
            self.add_refmac_process(pl1)
        #reduce,molprobity
        if self.args.run_molprobity.value:
            self.add_molprobity_process(pl1)
        #cablam
        if self.args.run_cablam.value:
            self.add_cablam_process(pl1)
        if self.args.refine_bfactor.value:
            pl = []
        else:
            pl = pl1
        #refmac synthetic maps
        if self.args.use_refmac.value:
            self.add_mapsyn_process(pl)
        #scores
        if self.args.run_scores.value:
            self.add_scores_process(pl)
        #smoc
        if self.args.run_smoc.value:
            self.add_smoc_process(pl,job_id=job_id,db_inject=db_inject)
        #tempy diffmap
        if self.args.run_diffmap.value:
            self.add_diffmap_process(pl)
        #jpred
        if self.args.run_jpred.value:
            self.add_jpred_process(pl)
        #set Results
        if not self.args.results_parallel.value:
            if len(pl) > 0:
                self.add_results_process(pl,len(pl))
        if self.args.refine_bfactor.value:
            pl1.extend(pl)
            pl = pl1
            
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
            #on_running_custom=custom_running)
        self.pipeline.start()

    def add_results_process(self,pl,index_pipeline=0):
        results_scriptfile = validate_results.__file__
        run_command = ['ccpem-python',results_scriptfile]
        run_command.append(os.path.join(self.job_location,'task.ccpem'))
        self.process_results = process_manager.CCPEMProcess(
            command=run_command,
            location=self.job_location,
            name='Results')
        #self.add_parallel_process(pl,[self.process_results])
        pl.insert(index_pipeline,[self.process_results])
    
    def add_bfactor_process(self,pl):
        ct_pdb = 0
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.process_bfactor = BfactWrapper(
                self.commands['bfactor'],
                        pdb,
                        job_location=mdir,
                        name='Bfactor '+pdbid,
                        )
            self.add_parallel_process(pl, [self.process_bfactor.process])
            ct_pdb += 1
    
    def add_molprobity_process(self,pl):
        ct_pdb = 0
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.process_reduce = ReduceRun(
                command=self.commands['reduce'],
                        job_location=mdir,
                        pdb_path=pdb,
                        name='Reduce '+pdbid)
            reduce_outpdb = os.path.join(mdir,'reduce_out.pdb')
#             try: 
#                 shutil.copyfile(pdb,reduce_outpdb)
#             except: 
#                 molprobity_inputpdb = pdb
            #also check for reduce error
            #if os.path.isfile(reduce_outpdb):
            molprobity_inputpdb = reduce_outpdb
            self.process_molprobity = MolprobityRun(
                command=self.commands['molprobity'],
                        job_location=mdir,
                        name='Molprobity '+pdbid,
                        pdb_path=molprobity_inputpdb)
            
            self.add_parallel_process(pl, [self.process_reduce.process,
                                           self.process_molprobity.process])
            ct_pdb += 1
    
    def add_cablam_process(self,pl):
        ct_pdb = 0
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.process_cablam = CablamRun(
                command=self.commands['cablam'],
                        job_location=mdir,
                        name='CABLAM '+pdbid,
                        pdb_path=pdb)
            self.process_dssp = DSSPRun(
                command=self.commands['dssp'],
                        job_location=mdir,
                        name='DSSP '+pdbid,
                        pdb_path=pdb,
                        output_dssp=self.args.output_dssp.value)
            self.add_parallel_process(pl, [self.process_dssp.process,
                                           self.process_cablam.process])
            ct_pdb += 1
            
    def add_jpred_process(self,pl):
        ct_pdb = 0
        list_processes = []
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            self.jpred_wrapper = JPredRunWrapper(
                self.commands['ccpem-jpred'],
                pdb,
                job_location=mdir,
                name='Jpred: '+pdbid,
                )
            self.download_wrapper = JPredDownloadWrapper(
                self.commands['jpred-download'],
                job_location=mdir,
                name='Jpred Download: '+pdbid,
                )
            if not self.check_process_type_exists(pl,'DSSP'):
                self.process_dssp = DSSPRun(
                    command=self.commands['dssp'],
                            job_location=mdir,
                            name='DSSP '+pdbid,
                            pdb_path=pdb,
                            output_dssp=self.args.output_dssp.value)
                list_processes.append(self.process_dssp.process)
            #add jpred process
            list_processes.append(self.jpred_wrapper.process)
            #list_processes.append(self.download_wrapper.process)
            self.add_parallel_process(pl,list_processes[:])
            pl.append([self.download_wrapper.process])
            ct_pdb += 1
    
    def check_process_type_exists(self,pl,process_name):
        for st in pl:
            for process in st:
                if process_name in process.name:
                    return True
        return False
    
    def add_parallel_process(self,pl, list_process):
        num_process = len(list_process)
        ct_added_process = 0
        pl_stages = len(pl)
        #pl_stages = min(len(pl),num_process)
        for l in xrange(pl_stages,0,-1):
            if len(pl[-l]) < self.args.ncpu.value and ct_added_process < num_process:
                pl[-l].append(list_process[ct_added_process])
                ct_added_process += 1

        for l in xrange(ct_added_process,num_process):
            pl.append([list_process[l]])
            
    def add_mapsyn_process(self,pl):
        ct_pdb = 0
        fix_map_model = False
        self.args.sim_maps = []
        if not hasattr(self,'origin_vector'):
            #fix map for refmac
            self.args.map_path_edit.value, fix_map_model, cella, self.origin_vector, self.map_dim = \
                fix_map_model_refmac(self.args.map_path.value,self.args.map_edit_path.value,
                                     self.job_location)
            trans_vector = (-self.origin_vector[0],
                            -self.origin_vector[1],
                            -self.origin_vector[2])
            ##if fix_map_model: refmac generates maps with origin 0
            self.fixed_pdbs = []
            #fix models if required
            ct_pdb = 0
            for pdb_path in self.list_pdbs:
                pdbid = self.list_pdbids[ct_pdb]
                mdir = os.path.join(self.job_location,pdbid)
                #pdb_edit_path = pdb_path
                pdb_edit_path = os.path.join(mdir,
                                        os.path.splitext(os.path.basename(
                                            pdb_path))[0]+'_fix.pdb')
                #translate model to nstart 0
                if self.origin_vector[0] != 0. or self.origin_vector[1] != 0. \
                        or self.origin_vector[2] != 0.:
                    
                    self.fix_model_for_refmac(pdb_path,pdb_edit_path,trans_vector,
                                              remove_charges=True)
                else:
                    #remove charges
                    remove_atomic_charges(pdb_path,
                                              pdb_edit_path)
                try:
                    self.fixed_pdbs.append(pdb_edit_path)
                except: break
            #store pdbs fixed for refmac (origin 0)
            if len(self.fixed_pdbs) == len(self.list_pdbs):
                self.args.fixed_pdbs.value = self.fixed_pdbs[:]
            
        #check whether to use fixed and bfact refined pdbs
        if len(self.bfact_refined) == len(self.list_pdbs):
            list_pdbs = self.bfact_refined
        elif len(self.args.fixed_pdbs.value) == len(self.list_pdbs):
            list_pdbs = self.args.fixed_pdbs.value
        else:
            list_pdbs = self.list_pdbs
        ct_pdb = 0
        for pdb_path in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            list_process = []
            pdb_edit_path = pdb_path
            model_map_filename = os.path.splitext(
                    os.path.basename(pdb_path))[0]
            reference_map = os.path.join(
                mdir,
                model_map_filename+'_refmac.mrc')
            modelid = pdbid
#             if len(model_map_filename) > 10:
#                 modelid = model_map_filename[:5]+'_'+model_map_filename[-5:]
#             else:
#                 modelid = model_map_filename
            # Set PDB cryst from mtz
            self.process_pdb_set = PDBSetCell(
                command=self.commands['pdbset'],
                job_location=mdir,
                name='Set PDB cell '+modelid,
                pdb_path=pdb_edit_path,
                map_path=self.args.map_path_edit.value)
            list_process.append(self.process_pdb_set.process)

            self.pdb_set_path = os.path.join(
                mdir,
                'pdbset.pdb')
            # Calculate mtz from pdb 
            self.refmac_sfcalc_crd_process = RefmacSfcalcCrd(
                job_location=mdir,
                pdb_path=self.pdb_set_path,
                name='SfcalcCrd '+modelid,
                lib_in=self.args.lib_in(),
                resolution=self.args.map_resolution.value)
            self.refmac_sfcalc_mtz = os.path.join(
                mdir,
                'sfcalc_from_crd.mtz')
            list_process.append(self.refmac_sfcalc_crd_process.process)

            # Convert calc mtz to map
            #gemmi sf2map
            self.sf2map_process = sf2mapWrapper(
                job_location=mdir,
                command=self.commands['ccpem-gemmi'],
                map_nx=self.map_dim[0],
                map_ny=self.map_dim[1],
                map_nz=self.map_dim[2],
                # For now only use x,y,z
    #                 fast=mrc_axis[int(map_mapc)],
    #                 medium=mrc_axis[int(map_mapr)],
    #                 slow=mrc_axis[int(map_maps)],
                fast='X',
                medium='Y',
                slow='Z',
                mtz_path=self.refmac_sfcalc_mtz,
                map_path=reference_map,
                name = 'sf2map '+ modelid)
            list_process.append(self.sf2map_process.process)
             
            #set origin and remove background
            reference_map_processed = os.path.join(
                    mdir,
                    model_map_filename+'_syn.mrc')
            if not all(o == 0 for o in self.origin_vector):
                self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=mdir,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=reference_map,
                                list_process=['shift_origin','threshold'],
                                map_origin=self.origin_vector,
                                map_contour=0.02,
                                #map_resolution=self.args.map_resolution.value,
                                out_map=reference_map_processed,
                                name='MapProcess '+modelid
                                )
            else:
                self.mapprocess_wrapper = mapprocess_wrapper(
                                job_location=mdir,
                                command=self.commands['ccpem-mapprocess'],
                                map_path=reference_map,
                                list_process=['threshold'],
                                map_origin=self.origin_vector,
                                map_contour=0.02,
                                #map_resolution=self.args.map_resolution.value,
                                out_map=reference_map_processed,
                                name='MapProcess '+modelid
                                )
            list_process.append(self.mapprocess_wrapper.process)
            
            self.args.sim_maps.append(reference_map_processed)
            
            self.add_parallel_process(pl, list_process[:])
            ct_pdb += 1
        # Set args
        args_file = os.path.join(self.job_location,
                                         'args.json')
        self.edit_argsfile(args_file)
        
    def add_smoc_process(self,pl,job_id=None, db_inject=None):
        ct_pdb = 0
        self.mapprocess_task = None
        #using these map ids for now
        #TODO: replace with self.list_mapids
        #list_mapids = ['map0','hm1','hm2']
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            ct_map = 0
            list_process = []
            for map in self.list_maps:
                mapid = self.list_mapids[ct_map]
                job_dir = os.path.join(mdir,mapid)
                if not os.path.isdir(job_dir):
                    os.mkdir(job_dir)
                #SMOC pipeline
                if (self.args.map_resolution.value > 7.5 or self.args.use_sccc.value) \
                    and self.args.rigid_body_path.value is None :
                    ribfind_job_title = 'Auto ribfind run'
                    if job_id is not None:
                        ribfind_job_title += ' {0}'.format(
                            job_id)
                    self.ribfind_job(db_inject=db_inject,
                                     job_title=ribfind_job_title)
                    list_process.append(self.ribfind_process)
                
                    # Set rigid body path
                    pdb_outdir = '.'.join(os.path.basename(pdb).split('.')[:-1])
                    self.args.rigid_body_path.value = \
                        os.path.join(self.ribfind_process.location,
                                     pdb_outdir,'protein')
                # Set args
                args_file = os.path.join(self.job_location,
                                         'args.json')
                new_argsfile = os.path.join(job_dir,
                                         'args.json')
                #self.check_processed_map()
                if not os.path.isfile(new_argsfile):
                    self.copy_argsfile(args_file,new_argsfile, new_map_path=map,
                                   new_pdb_path=[pdb])
                    self.edit_argsfile(new_argsfile, new_map_path=map)
                # Generate process
                if self.args.use_smoc.value:
                    self.smoc_wrapper = SMOCWrapper(
                        command=self.commands['ccpem-smoc'],
                        job_location=job_dir,
                        name='SMOC '+mapid+':'+pdbid)
#                 else:
#                     self.smoc_wrapper = SMOCWrapper(
#                         command=self.commands['ccpem-smoc'],
#                         job_location=job_dir,
#                         name='SCCC '+mapid+':'+pdbid)
                list_process.append(self.smoc_wrapper.process)
                ct_map += 1
            if self.args.use_refmac.value:
                for p in list_process:
                    pl.append([p])
            else:
                self.add_parallel_process(pl, list_process[:])
                
            ct_pdb += 1
    
    def add_diffmap_process(self,pl):
        ct_pdb = 0
        self.mapprocess_task = None
        if self.args.use_refmac.value:
            self.map_or_pdb_selection_edit = 'Map'
        else:
            self.map_or_pdb_selection_edit = 'Model'
        for pdb in self.list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            #check for refmac syn maps
            sim_map = None
            if self.args.sim_maps is not None and \
                            len(self.list_pdbs) == len(self.args.sim_maps):
                sim_map = self.args.sim_maps[ct_pdb]
            ct_map = 0
            list_process = []
            for map in self.list_maps:
                mapid = self.list_mapids[ct_map]
                job_dir = os.path.join(mdir,mapid)
                if not os.path.isdir(job_dir):
                    os.mkdir(job_dir)
                # Generate diffmap process
                self.tempy_difference_map = difference_map_task.TEMPyDifferenceMap(
                    job_location=job_dir,
                    command=self.commands['ccpem-diffmap'],
                    map_path_1=map,
                    map_or_pdb=self.map_or_pdb_selection_edit,
                    map_path_2=sim_map,#synthetic map
                    map_resolution_1=self.args.map_resolution.value,
                    map_resolution_2=self.args.map_resolution.value,
                    pdb_path = pdb,
                    mode='local',
                    noscale=False,
                    nofilt=False,
                    maskfile=None,
                    mpi=False,
                    n_mpi = 2,
                    window_size=None,
                    threshold_fraction=None,
                    dust_filter=False,
                    fracmap=True,
                    dust_prob = 0.1,
                    ref_scale = True)
                list_process.append(self.tempy_difference_map.process)
                # Generate getmapval process
                #model-map
                diff_frac2_map = os.path.join(
                        job_dir,'diff_frac2.mrc')
                self.get_map_val = difference_map_task.GetMapVal(
                    job_location=job_dir,
                    command=self.commands['ccpem-mapval'],
                    map_path_1=diff_frac2_map,
                    map_resolution_1=self.args.map_resolution.value,
                    pdb_path = pdb,
                    name = 'Get difference: '+ pdbid)
                 
                list_process.append(self.get_map_val.process)
                ct_map += 1
            if self.args.use_refmac.value:
                for p in list_process:
                    pl.append([p])
            else:
                self.add_parallel_process(pl, list_process[:])
                 
            ct_pdb += 1
            
    def add_scores_process(self,pl):
        self.mapprocess_task = None
        #using these map ids for now
        #TODO: replace with self.list_mapids
        #list_mapids = ['map0','hm1','hm2']
        ct_map = 0
        list_process = []
        for map in self.list_maps:
            mapid = self.list_mapids[ct_map]
            job_dir = os.path.join(self.job_location,mapid)
            if not os.path.isdir(job_dir):
                os.mkdir(job_dir)
            # Set args
            args_file = os.path.join(self.job_location,
                                     'args.json')
            new_argsfile = os.path.join(job_dir,
                                     'args.json')
            #copy args json
            #if not os.path.isfile(new_argsfile):
            self.copy_argsfile(args_file,new_argsfile, new_map_path=map,
                               new_pdb_path=self.list_pdbs)
            
            if self.args.map_contour_level.value is not None:
                self.edit_argsfile(new_argsfile, new_map_path=map,
                                   contour_level=self.args.map_contour_level.value)
            else:
                self.edit_argsfile(new_argsfile, new_map_path=map)
            # Generate process
            self.scores_wrapper = GlobScoreWrapper(
                command=self.commands['ccpem-scores'],
                job_location=job_dir,
                name='TEMPy scores '+mapid)
            ct_map += 1
            if self.args.use_refmac.value:
                pl.append([self.scores_wrapper.process])
            else:
                self.add_parallel_process(pl, [self.scores_wrapper.process])
            

    def add_refmac_process(self,pl):
#         #check whether to use fixed pdbs
        if len(self.args.fixed_pdbs.value) == len(self.list_pdbs):
            list_pdbs = self.args.fixed_pdbs.value
        else:
            list_pdbs = self.list_pdbs
        ct_pdb = 0
        for pdb in list_pdbs:
            pdbid = self.list_pdbids[ct_pdb]
            mdir = os.path.join(self.job_location,pdbid)
            ct_map = 0
            for map in self.list_maps:
                mapid = self.list_mapids[ct_map]
                # Convert map to mtz
                self.process_maptomtz = RefmacMapToMtz(
                    command=self.commands['refmac'],
                    resolution=self.args.map_resolution.value,
                    mode='Global',
                    name='Map to MTZ '+mapid+':'+pdbid,
                    job_location=mdir,
                    map_path=map,
                    hklout_path=os.path.join(self.job_location,
                                                     mapid+'.mtz')
                    )
                #set PDB cell
                self.process_pdb_set = PDBSetCell(
                    command=self.commands['pdbset'],
                    job_location=mdir,
                    name='Set PDB cell '+mapid+':'+pdbid,
                    pdb_path=pdb,
                    map_path=map)
                start_refmac_pdb_path = self.process_pdb_set.pdbout_path
                # Global refine
                if not self.args.refine_bfactor.value:
                    self.process_refine = RefmacRefine(
                        command=self.commands['refmac'],
                        job_location=mdir,
                        pdb_path=start_refmac_pdb_path,
                        resolution=self.args.map_resolution.value,
                        mtz_path=self.process_maptomtz.hklout_path,
                        mode='Global',
                        name='Refmac '+mapid+':'+pdbid,
                        ncycle=0)
                    
                else:
                    self.process_refine = RefmacRefine(
                        command=self.commands['refmac'],
                        job_location=mdir,
                        pdb_path=start_refmac_pdb_path,
                        resolution=self.args.map_resolution.value,
                        mtz_path=self.process_maptomtz.hklout_path,
                        mode='Local',
                        name='Refmac '+mapid+':'+pdbid,
                        weight_auto=False,
                        keywords='weight auto 1'+'\nrefi bonly',
                        ncycle=10)
                    self.bfact_refined[ct_pdb] = os.path.join(mdir,'refined.pdb')
                    
                self.add_parallel_process(pl, [self.process_maptomtz.process,
                                               self.process_pdb_set.process,
                                           self.process_refine.process])
                ct_map += 1
# 
#             if self.args.half_map_1.value != None:
#                 # Convert half map 1 to mtz; stage
#                 self.process_maptomtz_hm1 = RefmacMapToMtz(
#                     command=self.commands['refmac'],
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Map to MTZ hm1:'+pdbid,
#                     job_location=mdir,
#                     map_path=self.args.half_map_1.value,
#                     hklout_path=os.path.join(self.job_location,
#                                                  'starting_map_hm1.mtz'))
#                 #set PDB cell
#                 self.process_pdb_set = PDBSetCell(
#                     command=self.commands['pdbset'],
#                     job_location=mdir,
#                     name='Set PDB cell hm1:'+pdbid,
#                     pdb_path=pdb,
#                     map_path=self.args.half_map_1.value)
#                 start_refmac_pdb_path = self.process_pdb_set.pdbout_path
#                 self.process_refine_hm1 = RefmacRefine(
#                     command=self.commands['refmac'],
#                     job_location=mdir,
#                     pdb_path=start_refmac_pdb_path,
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Refmac hm1:'+pdbid,
#                     ncycle=0,
#                     mtz_path=self.process_maptomtz_hm1.hklout_path)
#                 self.add_parallel_process(pl, [self.process_maptomtz_hm1.process,
#                                      self.process_pdb_set.process,
#                                      self.process_refine_hm1.process])
#             
#             if self.args.half_map_2.value != None:
#                 # Convert half map 1 to mtz; stage
#                 self.process_maptomtz_hm2 = RefmacMapToMtz(
#                     command=self.commands['refmac'],
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Map to MTZ hm2:'+pdbid,
#                     job_location=mdir,
#                     map_path=self.args.half_map_2.value,
#                     hklout_path=os.path.join(self.job_location,
#                                                  'starting_map_hm2.mtz'))
#                 #set PDB cell
#                 self.process_pdb_set = PDBSetCell(
#                     command=self.commands['pdbset'],
#                     job_location=mdir,
#                     name='Set PDB cell hm2:'+pdbid,
#                     pdb_path=pdb,
#                     map_path=self.args.half_map_2.value)
#                 start_refmac_pdb_path = self.process_pdb_set.pdbout_path
#                 self.process_refine_hm2 = RefmacRefine(
#                     command=self.commands['refmac'],
#                     job_location=mdir,
#                     pdb_path=start_refmac_pdb_path,
#                     resolution=self.args.map_resolution.value,
#                     mode='Global',
#                     name='Refmac hm2:'+pdbid,
#                     ncycle=0,
#                     mtz_path=self.process_maptomtz_hm2.hklout_path)
#                 self.add_parallel_process(pl, [self.process_maptomtz_hm2.process,
#                                      self.process_pdb_set.process,
#                                      self.process_refine_hm2.process])
            ct_pdb += 1

