import sys
import re
import os
from collections import OrderedDict
import json

dictaa_1l_3l = {'P':'PRO','S':'SER','G':'GLY','L': 'LEU','T':'THR',
                'V':'VAL','I':'ILE','M':'MET','C':'CYS','F':'PHE','Y':'TYR',
                'W':'TRP','A':'ALA','K':'LYS','R':'ARG','D':'ASP','E':'GLU',
                'N':'ASN','Q':'GLN','H':'HIS'
                }
dssp_sse_desc = {'H':'helix','E' : 'strand', 'T':'turn', 
                 'B': 'beta bridge', 'G': 'helix-3', 'I': 'helix-5',
                 'S':'bend'}
# Rose et al. 1985
dict_maxacc = {
    'A': 118.1,
'R':256.0,
'N': 165.5,
'D':158.7,
'C':146.1,
'Q':193.2,
'E': 186.2,
'G': 88.1,
'H':202.5,
'I':181.0,
'L':193.1,
'K':225.8,
'M':203.4,
'F':222.8,
'P':146.8,
'S':129.8,
'T':152.5,
'W': 266.3,
'Y':236.8,
'V': 164.5}


class DSSP_LogParser():
    def __init__(self,dssp_process):
        self.dssp_process = dssp_process
        self.list_modelids = []
        self.dict_sses = OrderedDict()
        self.list_chains = []
        for dssp_job in self.dssp_process:
            dssp_outfile = os.path.join(dssp_job.location,'output.dssp')
            modelid = dssp_job.name.split(' ')[-1]
            self.dict_sses[modelid] = OrderedDict()
            self.list_modelids.append(modelid)
            self.parse_dssp_file(dssp_outfile,modelid)
            self.get_neigh_dsspsse()
        
    def parse_dssp_file(self,dsspfile,modelid):
        out_fh = open(dsspfile,'r')
        residue_scores_start = False
        for line in out_fh:
            if residue_scores_start:
                self.parse_dssp_line(line[:-1],modelid)
            if '#' in line and 'RESIDUE' in line:
                residue_scores_start = True
                
    def parse_dssp_line(self,line,modelid):
        line_split = line.split()
        #residue number
        resnum = line_split[1]
        #residue name
        try:
            resname = dictaa_1l_3l[line_split[3]]
        except KeyError: resname = line_split[3]
        #chain ID
        chainID = line_split[2]
        if not chainID in self.list_chains:
            self.list_chains.append(chainID)
        #secondary structure
        dssp_sse = line[16]
        #secondary structure description
        try:
            sse_desc = dssp_sse_desc[dssp_sse]
        except KeyError:
            sse_desc = ' '
        #calculate relative solvant accessibility
        try: 
            dssp_acc = float(line[35:38])
            residue_1l = line_split[3]
            if len(residue_1l) == 1 and residue_1l.islower():
                residue_1l = 'C' # disulfide bridge
            dssp_acc = dssp_acc/dict_maxacc[residue_1l]
        except: dssp_acc = -1.0
        #Ca coordinates
        try:
            x,y,z = ["{:.4f}".format(float(coord)) for coord in line_split[-5:-2]]
        except IndexError: x=y=z = ''
        #save details
        try: self.dict_sses[modelid][chainID][resnum] = [sse_desc,dssp_sse,dssp_acc,resname,
                                                         x,y,z]
        except KeyError:
            self.dict_sses[modelid][chainID] = OrderedDict()
            self.dict_sses[modelid][chainID][resnum] = [sse_desc,dssp_sse,dssp_acc,resname,
                                                        x,y,z]
    
    def get_neigh_dsspsse(self):
        """
        Get secondary structures of 5 consecutive residues
        """
        for modelid in self.dict_sses:
            for chainID in self.dict_sses[modelid]:
                list_dsspsse = []
                for resnum in self.dict_sses[modelid][chainID]:
                    dssp_sse = self.dict_sses[modelid][chainID][resnum][1]
                    if dssp_sse == ' ': dssp_sse = '-'
                    try:
                        if len(list_dsspsse) == 0:
                            list_dsspsse.append(dssp_sse)
                        elif int(resnum) - int(list_dsspsse[-1]) == 1:
                            list_dsspsse.append(dssp_sse)
                        else:
                            list_dsspsse.append('-')
                    except (TypeError,ValueError) as exc:
                        list_dsspsse.append(dssp_sse)
                l=0
                for resnum in self.dict_sses[modelid][chainID]:
                    dssp_sses_5 = ''
                    leftindex = max(0,l-2)
                    rightindex = min(len(list_dsspsse),l+3)
                    if l < 2:
                        for i in xrange(2-l):
                            dssp_sses_5 += '-'
                    dssp_sses_5 += ''.join(
                                    list_dsspsse[leftindex:rightindex])
                    if l > len(list_dsspsse)-3:
                        for i in xrange(l-(len(list_dsspsse)-3)):
                            dssp_sses_5 += '-'
                    self.dict_sses[modelid][chainID][resnum].insert(2,dssp_sses_5)
                    l += 1