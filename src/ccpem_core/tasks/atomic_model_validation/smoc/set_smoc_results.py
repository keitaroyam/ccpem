import sys
import re
import os
import pyrvapi
import shutil
import numpy as np
import pandas as pd
import math
from collections import OrderedDict
from ccpem_core.tasks.atomic_model_validation.pyrvapi_utils import *
from copy import deepcopy
import json

class SetSMOCResults():
    def __init__(self, smoc_process, list_chains=[],
                 pipeline_location=None, cootset_instance=None,
                 glob_reportfile=None):
        self.smoc_process = sorted(smoc_process, 
                                     key=lambda j: j.name.split(' ')[-1][-1]+
                                                    j.name.split(' ')[0][-1])
        self.cootset=cootset_instance
        self.smoc_results = []
        self.smoc_csvs = []#smoc scores per model
        self.smoc_dataframes = OrderedDict()
        #outlier details
        self.dict_outlier_details = OrderedDict()
        #output json files for each model
        self.dict_jsonfiles = OrderedDict()
        self.list_modelids = []
        self.list_pdbids = []
        self.list_mapids = []
        self.residue_outliers = OrderedDict()
        self.residue_coordinates = OrderedDict()
        self.dict_cootdata = OrderedDict()
        self.dict_ca_coord = OrderedDict()
        for smoc_job in smoc_process:
            smoc_result = os.path.join(smoc_job.location,'smoc_score.csv')
            self.smoc_results.append(smoc_result)
            
            if os.path.isfile(smoc_result):
                modelid = smoc_job.name.split(' ')[-1]
                self.smoc_dataframes[modelid] = pd.read_csv(smoc_result,
                                              skipinitialspace=True,
                                              skip_blank_lines=True)
                self.list_modelids.append(modelid)
                self.dict_jsonfiles[modelid] = os.path.join(smoc_job.location,
                                                        modelid+"_smoc.json")
                self.smoc_csvs.append(
                        os.path.join(smoc_job.location,modelid+'_smoc.csv'))
                pdbid = modelid.split(':')[-1]
                if not pdbid in self.list_pdbids: 
                    self.list_pdbids.append(pdbid)
                if pipeline_location is not None:
                    ca_coordfile = os.path.join(pipeline_location,pdbid,'ca_coord.json')
                    if os.path.isfile(ca_coordfile):
                        self.dict_ca_coord[pdbid] = ca_coordfile
                mapid = modelid.split(':')[0]
                if not mapid in self.list_mapids: 
                    self.list_mapids.append(mapid)
                #self.residue_outliers[pdbid] = {}
                self.residue_coordinates[pdbid] = {}
        self.list_chains = list_chains
        self.add_chain_sections = False
        if len(self.list_chains) == 0:
            self.add_chain_sections = True
        
        self.min_val = None
        
        #set data for local outliers
        self.set_local_data()
        self.check_chain_order_cootdata()
        self.add_cootdata()
        self.delete_cootdata()
        self.save_outlier_json()
        #set smoc plots
        self.set_local_results()
        self.delete_dictresults()
        
    def add_cootdata(self):
        if self.cootset is not None:
            for pdbid in self.dict_cootdata:
                self.cootset.add_data_to_cootfile(self.dict_cootdata[pdbid],
                    modelid=pdbid,method='smoc')
                
    def delete_cootdata(self):
        self.dict_cootdata.clear()
        del self.dict_cootdata
    
    def delete_dictplots(self):
        self.dict_pdbdata.clear()
        del self.dict_pdbdata
        self.dict_mapdata.clear()
        del self.dict_mapdata
        
    def delete_dictresults(self):
        self.dict_outlier_details.clear()
        del self.dict_outlier_details

    def set_local_data(self):
#         if not self.add_chain_sections:
#             for chain in self.list_chains:
#                 smoc_subsection = 'smoc'+chain
#                 self.add_subsection_to_chain(chain,smoc_subsection)
        self.dict_pdbdata = OrderedDict()
        self.dict_mapdata = OrderedDict()
        ct_model = 0
        for mid in self.list_modelids:
            if mid in self.smoc_dataframes:
                df = self.smoc_dataframes[mid]
                self.set_results_data(df, mid,self.smoc_csvs[ct_model])
                ct_model += 1
    
    def save_outlier_json(self):
        for mid in self.dict_outlier_details:
            try:
                jsonfile = self.dict_jsonfiles[mid]
            except KeyError: continue
            with open(jsonfile,'w') as jf:
                json.dump(self.dict_outlier_details[mid],jf)


    def set_local_results(self):
        self.set_results_graphs()
        self.delete_dictplots()
    
    def add_section_chain(self, chain_section, chain_name):
        pyrvapi.rvapi_add_section(chain_section, chain_name, 
                                          local_tab, 0, 0, 1, 1, False)
    def add_subsection_to_chain(self,chain,smoc_subsection):
        pyrvapi.rvapi_add_section1(chain+'/'+smoc_subsection, 
                               'SMOC (Segment Manders\' Overlap Coefficient)', 
                               0, 0, 1, 1, False)
                        
    def set_results_data(self,df,mid,mid_csv=None):
        mapid = mid.split(':')[0]
        pdbid = mid.split(':')[-1]
        list_chains = []
        ct_plot = 0
        list_chain_model = []
        if not mid in self.dict_outlier_details:
            self.dict_outlier_details[mid] = {}
        if not pdbid in self.dict_cootdata:
            self.dict_cootdata[pdbid] = {}
        if not 'smoc' in self.dict_cootdata[pdbid]:
            self.dict_cootdata[pdbid]['smoc'] = []
        if not pdbid in self.residue_outliers:
            self.residue_outliers[pdbid] = {}
            self.residue_coordinates[pdbid] = {}
        if mid_csv is not None:
            scoreout = mid_csv
            sco = open(scoreout,'w')
            sco.write('#chain,residue,score,z_local,z_global\n')
        # Get data
        for pdb in df:
            if not '_' in pdb: continue
            #add plots for each chain
            chain = pdb.split('_')[-1]
            
            ct_data_index = 0
            if '.' in chain: 
                chain = chain.split('.')[0]
                pdbname = '.'.join(pdb.split('.')[:-1])
            if chain == '': chain = ' '
            added_smoc_text = False
            #check if ca coordinates are available
            flag_coord_exists = False
            if pdbid in self.dict_ca_coord:
                ca_coord_json = self.dict_ca_coord[pdbid]
                with open(ca_coord_json,'r') as j:
                    dict_pdb_ca_coord = json.load(j)
                flag_coord_exists = True
            else:
                dict_pdb_ca_coord = {}
            
            #add chain sections
            if self.add_chain_sections:
                if not chain in self.list_chains:
                    self.list_chains.append(chain)
                chain_section = chain
                chain_name = 'chain '+chain
                self.add_section_chain(chain_section, chain_name)
            
            if chain in self.list_chains:
                if not chain in self.dict_pdbdata:
                    self.dict_pdbdata[chain] = {}
                if not chain in self.residue_outliers[pdbid]:
                    self.residue_outliers[pdbid][chain] = {}
                    self.residue_coordinates[pdbid][chain] = {}
                if not chain in list_chain_model:
                    dict_results = {}
                    list_chain_model.append(chain)
                    
                ct_res_chain = 0
                pdb_df = df[pdb].dropna(axis=0, how='any')
                if pdb_df[0] == 'resnum': 
                    dict_results['residue'] = []
                    for val in pdb_df[1:]:
                        residue_num = int(float(val))
                        dict_results['residue'].append(residue_num)
                        try:
                            ca_coord = dict_pdb_ca_coord["1"][chain][str(residue_num)][0]
                            res_name = dict_pdb_ca_coord["1"][chain][str(residue_num)][1]
                            try:
                                dict_results['CAx'].append(ca_coord[0])
                                dict_results['CAy'].append(ca_coord[1])
                                dict_results['CAz'].append(ca_coord[2])
                                dict_results['resname'].append(res_name)
                            except KeyError:
                                dict_results['CAx'] = [ca_coord[0]]
                                dict_results['CAy'] = [ca_coord[1]]
                                dict_results['CAz'] = [ca_coord[2]]
                                dict_results['resname'] = [res_name]
                        except: 
                            flag_coord_exists = False
                
                if not flag_coord_exists:
                    if 'CAx' in dict_results: del dict_results['CAx']
                    if 'CAy' in dict_results: del dict_results['CAy']
                    if 'CAz' in dict_results: del dict_results['CAz']
                
#                 if pdb_df[0] == 'resname':
#                     dict_results['resname'] = []
#                     for val in pdb_df[1:]:
#                         dict_results['resname'].append(val)
                
                if pdb_df[0] == 'smoc':
                    dict_results['smoc'] = []
                    for val in pdb_df[1:]:
                        dict_results['smoc'].append(float(val))
                
#                 if pdb_df[0] == 'CAx':
#                     dict_results['CAx'] = []
#                     for val in pdb_df[1:]:
#                         dict_results['CAx'].append(float(val))
#                 
#                 if pdb_df[0] == 'CAy':
#                     dict_results['CAy'] = []
#                     for val in pdb_df[1:]:
#                         dict_results['CAy'].append(float(val))
#                 
#                 if pdb_df[0] == 'CAz':
#                     dict_results['CAz'] = []
#                     for val in pdb_df[1:]:
#                         dict_results['CAz'].append(float(val))
                
                if 'residue' in dict_results and 'smoc' in dict_results:
                    try:
                        assert len(dict_results['residue']) == len(dict_results['smoc'])
                    except AssertionError:
                        print pdbid
                        print len(dict_results['residue']), len(dict_results['smoc'])
                        print dict_results['residue'], dict_results['smoc']
                        continue
                    if self.min_val == None: self.min_val = min(dict_results['smoc'])
                    else: self.min_val = min(self.min_val,min(dict_results['smoc']))
                    added_smoc_text = True
                    #store plot data for each pdb and map
                    if not chain in self.dict_pdbdata: 
                        self.dict_pdbdata[chain] = OrderedDict()
                    if not pdbid in self.dict_pdbdata[chain]:
                        self.dict_pdbdata[chain][pdbid] = OrderedDict()
                    if not mapid in self.dict_pdbdata[chain][pdbid]:
                        self.dict_pdbdata[chain][pdbid][mapid] = OrderedDict()

                    self.dict_pdbdata[chain][pdbid][mapid] = [
                                                dict_results['residue'][:],
                                                dict_results['smoc'][:]]
                    if 'resname' in dict_results:
                        #save outliers in dict
                        for l in xrange(len(dict_results['residue'])):
                            try:
                                self.dict_outlier_details[mid][chain].append([
                                                            str(dict_results['residue'][l]) +' '+
                                                            dict_results['resname'][l],
                                                            dict_results['smoc'][l]])
                            except KeyError:
                                self.dict_outlier_details[mid][chain] = [[
                                                            str(dict_results['residue'][l]) +' '+
                                                            dict_results['resname'][l], 
                                                            dict_results['smoc'][l]]]
                    if not chain in self.dict_mapdata: 
                        self.dict_mapdata[chain] = OrderedDict()
                    if not mapid in self.dict_mapdata[chain]:
                        self.dict_mapdata[chain][mapid] = OrderedDict()
                    if not pdbid in self.dict_mapdata[chain][mapid]:
                        self.dict_mapdata[chain][mapid][pdbid] = OrderedDict()
                    
                    self.dict_mapdata[chain][mapid][pdbid] = [
                                                dict_results['residue'][:],
                                                dict_results['smoc'][:]]
                    
                    if 'resname' in dict_results and 'CAx' in dict_results and \
                        'CAy' in dict_results and 'CAz' in dict_results:
                        
                        smoc_z, list_z_local, list_z_global = self.select_outliers_by_z(dict_results, -2.0)
                        #print len(dict_results['smoc']),len(list_z_local)
                        for l in xrange(len(dict_results['residue'])):
                            #write z scores
                            residue_scores = ','.join([
                                chain,str(dict_results['resname'][l])+
                                '_'+str(dict_results['residue'][l]),
                                "{0:.2f}".format(float(dict_results['smoc'][l])),
                                "{0:.2f}".format(list_z_local[l]),
                                "{0:.2f}".format(list_z_global[l])
                                               ])
                            sco.write(residue_scores+"\n")
                            
                            if smoc_z[l]:
                                self.residue_outliers[pdbid][chain][str(dict_results['residue'][l])] = 'Outlier'
                                self.residue_coordinates[pdbid][chain][str(dict_results['residue'][l])] = \
                                                                                (dict_results['CAx'][l],
                                                                                 dict_results['CAy'][l],
                                                                                 dict_results['CAz'][l])
                                #TODO: use only the full map (map0) for coot outliers
                                if mapid not in self.list_mapids or \
                                    self.list_mapids.index(mapid) > 0: continue
                                try:
                                    self.dict_cootdata[pdbid]['smoc'].append((chain,
                                                            dict_results['residue'][l],
                                                            dict_results['resname'][l],
                                                            dict_results['smoc'][l],
                                                            (dict_results['CAx'][l],
                                                            dict_results['CAy'][l],
                                                            dict_results['CAz'][l])
                                                                ))
                                except IndexError, msg:
                                    print 'Could not set coot data for', pdbid, chain, msg
                                    self.dict_cootdata[pdbid]['smoc'].append((chain,
                                                            dict_results['residue'][l],
                                                            '',
                                                            dict_results['smoc'][l],
                                                            ('',
                                                            '',
                                                            '')
                                                                ))
    #         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
    #         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)
        
        pyrvapi.rvapi_flush()
        sco.close()
        #print self.dict_outlier_details
        
    def check_chain_outliers(self,chain):
        for mid in self.dict_outlier_details:
            if chain in self.dict_outlier_details[mid]:
                return True
        return False

    def check_chain_order_cootdata(self):
        #print 'Chains:', self.list_chains
        for pdbid in self.dict_cootdata:
            list_chain_sort = []
            for chain in self.list_chains:
                for resdata in self.dict_cootdata[pdbid]['smoc']:
                    if resdata[0] in self.list_chains:
                        if resdata[0] == chain:
                            list_chain_sort.append(resdata)
                    else:
                        list_chain_sort.append(resdata)
            self.dict_cootdata[pdbid]['smoc'] = list_chain_sort[:]
            
    def set_results_graphs(self):
        x_label = 'Residue'
        y_label = 'SMOC'
        for chain in self.dict_mapdata:
            section_name = 'smoc'+chain
            #skip chain tab if outliers not found
            if hasattr(self, 'dict_outlier_details'):
                if len(self.dict_outlier_details) > 0 and \
                    not self.check_chain_outliers(chain): continue
            
            self.add_subsection_to_chain(chain,section_name)
            #self.add_smoc_section_text(section_name)
            
            ct_plot = 0
            for mapid in self.list_mapids:
                if not mapid in self.dict_mapdata[chain]: continue
                skip_map = False
                #skip map plot if mult map vs single pdb
                if len(self.dict_mapdata[chain][mapid]) == 1:
                    for pdbid in self.dict_pdbdata[chain]:
                        if mapid in self.dict_pdbdata[chain][pdbid] and \
                                len(self.dict_pdbdata[chain][pdbid]) > 1: 
                            skip_map = True
                            break
                if skip_map: continue
                pyrvapi.rvapi_add_text(mapid+":<br>", section_name, ct_plot, 0, 1, 1)
                pyrvapi.rvapi_flush()
                graphwidget = 'graphWidget'+str(ct_plot)+section_name
                data = 'smocdata'+str(ct_plot)
                dataname = 'SMOC data'
                pyrvapi.rvapi_add_graph(graphwidget, section_name, ct_plot, 0, 1, 1)
                #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
                pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                    dataname)
                plotname = mapid
                plot_id = 'plot'+str(ct_plot)
                ct_pdb = 0
                ct_line = 0
                for pdbid in self.list_pdbids:
                    if not pdbid in self.dict_mapdata[chain][mapid]: continue
                    list_x, list_y = \
                        self.dict_mapdata[chain][mapid][pdbid]
                    plotlinename = pdbid
                    if ct_pdb == 0:
                        add_plot_to_graph(graphwidget,data,plotname,
                                          plotlinename,list_x,list_y,
                                          x_label, y_label,
                                          plot_id=plot_id,
                                          xtype='int',lnum=ct_line+1)
                    else:
                        line_color = getattr(pyrvapi, 
                                             'RVAPI_COLOR_'+list_line_colors[ct_pdb-1])
                        add_line_to_plot(graphwidget,data,plotlinename,list_y,
                                         list_x=list_x,x_label=x_label,
                          ytype='float',
                          plot_id=plot_id,
                          lnum=ct_line+1,
                          line_color=line_color)
                    ct_pdb += 1
                    ct_line += 1
                    pyrvapi.rvapi_flush()
                ct_plot += 1
                
            for pdbid in self.list_pdbids:
                if not pdbid in self.dict_pdbdata[chain]: continue
                #skip pdb plot if single map vs multiple pdbs
                if len(self.dict_pdbdata[chain][pdbid]) == 1: continue
                pyrvapi.rvapi_add_text(pdbid+":<br>", section_name, ct_plot, 0, 1, 1)
                graphwidget = 'graphWidget'+str(ct_plot)+section_name
                data = 'smocdata'+str(ct_plot)
                dataname = 'SMOC data'
                pyrvapi.rvapi_add_graph(graphwidget, section_name, ct_plot, 0, 1, 1)
                #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
                pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                    dataname)
                plotname = pdbid
                plot_id = 'plot'+str(ct_plot)
                ct_map = 0
                ct_line = 0
                for mapid in self.list_mapids:
                    if not mapid in self.dict_pdbdata[chain][pdbid]:
                        continue
                    list_x, list_y = \
                        self.dict_pdbdata[chain][pdbid][mapid]
                    plotlinename = mapid
                    if ct_map == 0:
                        add_plot_to_graph(graphwidget,data,plotname,
                                          plotlinename,list_x,list_y,
                                          x_label, y_label,
                                          plot_id=plot_id,
                                          xtype='int',lnum=ct_line+1)
                    else:
                        line_color = getattr(pyrvapi, 'RVAPI_COLOR_'+list_line_colors[ct_map-1])
                        add_line_to_plot(graphwidget,data,plotlinename,list_y,
                                         list_x=list_x,x_label=x_label,
                          ytype='float',
                          plot_id=plot_id,
                          lnum=ct_line+1,
                          line_color=line_color)
                    ct_map += 1
                    ct_line += 1
                    pyrvapi.rvapi_flush()
                ct_plot += 1

    def select_outliers_by_z(self,dict_results, z_cutoff = -2.5):
        list_smoc = dict_results['smoc']
        array_smoc = np.array(list_smoc)
        list_z_global = self.calc_z_median(list_smoc)
        #print len(list_smoc),len(list_z_global)
        #if coords are available, set z score locally
        if 'CAx' in dict_results and 'CAy' in dict_results and \
            'CAz' in dict_results:
            indi = zip(dict_results['CAx'], dict_results['CAy'], dict_results['CAz'])
            try: 
                from scipy.spatial import cKDTree
                gridtree = cKDTree(indi)
            except ImportError:
                try:
                    from scipy.spatial import KDTree
                    gridtree = KDTree(indi)
                except ImportError: 
                    gridtree = None
            list_z_local = []
            if gridtree is not None:
                list_z = []
                for l in xrange(len(list_smoc)):
                    ca_coord = indi[l]
                    val = list_smoc[l]
                    list_neigh = self.get_indices_sphere(gridtree,ca_coord,dist=8.0)
                    list_smoc_local = array_smoc[list_neigh]
                    z_local = self.calc_z_median(list_smoc_local,val)
                    #res_index = list_neigh.index(l)
                    list_z_local.append(z_local)
                    list_z.append(z_local < z_cutoff)
                
        else:
            list_z_local = [0.]*len(list_z_global) #when no local z is calculated
            z = self.calc_z_median(array_smoc)
            list_z = z < z_cutoff
        #print len(list_z),len(list_z_local),len(list_z_global)
        return list_z, list_z_local, list_z_global
    
    def calc_z(self,list_vals,val=None):
        if val is not None:
            try:
                list_vals.remove(val) #remove val from list
            except: pass
        sum_mapval = np.sum(list_vals)
        mean_mapval = sum_mapval/len(list_vals)
        #median_mapval = np.median(list_vals)
        std_mapval = np.std(list_vals)
        if val is None:
            if std_mapval == 0.:
                z = [0.0]*len(list_vals)
            else:
                z = np.round((np.array(list_vals) - mean_mapval)/std_mapval,2)
        else:
            if std_mapval == 0.:
                z = 0.0
            else:
                z = (val - mean_mapval)/std_mapval
        return z
     
    def calc_z_median(self,list_vals,val=None):
        if val is not None:
            try:
                list_vals.remove(val)
            except: pass
        sum_smoc = np.sum(list_vals)
        median_smoc = np.median(list_vals)
        mad_smoc = np.median(np.absolute(np.array(list_vals)-median_smoc))
        if val is None:
            if mad_smoc == 0.:
                z = [0.0]*len(list_vals)
            else:
                z = np.around((np.array(list_vals) - median_smoc)/mad_smoc,2)
        else:
            if mad_smoc == 0.:
                z = 0.0
            else:
                z = round(((val - median_smoc)/mad_smoc),2)
        return z    

    def get_indices_sphere(self,gridtree,coord,dist=5.0):
        list_points = gridtree.query_ball_point(\
                    [coord[0],coord[1],coord[2]], 
                    dist)
        return list_points
    
    def add_smoc_section_text(self,section_name):
        pyrvapi.rvapi_add_text(
                        '<br><br>',
                        section_name, 0, 0, 1, 1)
        pyrvapi.rvapi_add_text(
            'Local fragment Score based on Manders\' Overlap Coefficient (SMOC)',
                        section_name, 0, 0, 1, 1)
        pyrvapi.rvapi_flush()
        
    def set_smoc_graph(self, section, graphwidget, data, dataname,
                       list_x,list_y,x_label, y_label, plotname,
                       plotlinename):
        pyrvapi.rvapi_add_graph(graphwidget, section, 0, 0, 1, 1)
        #pyrvapi.rvapi_add_loggraph(graphwidget, section, 0, 0, 1, 1)
        pyrvapi.rvapi_add_graph_data1(graphwidget+'/'+data,
                                                dataname)
        add_plot_to_graph(graphwidget,data,plotname,plotlinename,list_x,list_y,
                          x_label, y_label,xtype='int')
        
#     def add_outlier_title_for_residues(self, outlier_title, chain, residue_num):
#         try: 
#             if not outlier_title in self.residue_outliers[chain][residue_num]:
#                 self.residue_outliers[chain][residue_num].append(outlier_title)
#         except KeyError:
#             self.residue_outliers[chain][residue_num] = [outlier_title]


class SetSCCCResults():
    def __init__(self, sccc_process):
        self.sccc_process = sccc_process
        self.sccc_results = os.path.join(sccc_process.location,'sccc_score.txt')
        if os.path.isfile(self.sccc_results):
            self.sccc_dataframe = pd.read_csv(self.sccc_results)
            
    def set_results_summary(self,local_tab, sccc_section):
        pyrvapi.rvapi_add_section(sccc_section, 'Local SCCC scores', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            'Rigid Body Segment Cross Correlation Coefficient (SCCC)',
            sccc_section, 0, 0, 1, 1)

        pyrvapi.rvapi_flush()
    
    def set_results_tables(self, sccc_section):
        build_table = sccc_section+'/table1'
        pyrvapi.rvapi_add_table1(build_table,'SCCC scores', 1, 0, 1, 1, False)
        pyrvapi.rvapi_put_horz_theader('table1','Rigid Bodies','',0)
        ct_pdbs = 1
        for col in self.sccc_dataframe.columns:
            pyrvapi.rvapi_put_horz_theader('table1',col,'',ct_pdbs)
            ct_pdbs += 1
        for i in range(len(self.sccc_dataframe.index)):
            index = self.sccc_dataframe.index[i]
            pyrvapi.rvapi_put_table_string ( 'table1',index,i,0 )
            for j in range(1,ct_pdbs):
                col = self.sccc_dataframe.columns[j-1]
                #print self.sccc_dataframe[col][index]
                try: pyrvapi.rvapi_put_table_string ( 'table1','%0.2f' % self.sccc_dataframe[col][index],i,j )
                except TypeError: sys.exit('SCCC calculation failed')
        pyrvapi.rvapi_flush()
