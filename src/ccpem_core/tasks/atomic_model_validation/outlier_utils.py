import sys
import re
import os
import time
import pyrvapi
import fileinput
import collections
import shutil
import numpy as np
import pandas as pd
import math
try:
    from scipy.spatial import cKDTree as kdtree
except ImportError:
    from scipy.spatial import KDTree as kdtree

from collections import OrderedDict

class cluster_residues():
    
    def __init__(self):
        pass
    
    def get_cluster_outliers(self,list_res,list_coord):
        '''
        Spatially cluster given lists of res and coord
        '''
        array_coords = np.array(list_coord)
        kdtree_coords = kdtree(array_coords, leafsize=20.0)
        list_neigh = kdtree_coords.query_ball_tree(kdtree_coords,r=7.0)
        
        self.check_common_merge(list_neigh)
                    
        list_res_cluster = []
        for cluster in list_neigh:
            res_in_cluster = []
            for c in cluster:
                res_in_cluster.append(list_res[c])
            res_in_cluster.sort()
            list_res_cluster.append(res_in_cluster[:])
        del array_coords
        #print list_res_cluster
        return list_res_cluster

    def check_common_merge(self,list_2d):
        '''
        merge lists/arrays in a list where atleast one element is common
        '''
        l = len(list_2d)
        
        for i in xrange(l):
            if i >= l: break
            j = i + 1
            while j < l:
                if i >= l: break
                if j >= l: break
                
                arr1 = np.array(list_2d[i])
                arr2 = np.array(list_2d[j])
                
                if True in np.in1d(arr1, arr2):
                    #print arr1, arr2
                    list_2d[i] = list(np.union1d(arr1, arr2))
                    list_2d.pop(j)
                    l = l - 1
                    j = j - 1
                    
                j += 1

    def check_seq_neigh_cluster(self,list_res,list_clusters):
        
        flat_list = [item for sublist in list_clusters for item in sublist]
        for res in list_res:
            if not res in flat_list:
                self.add_res_cluster_byseq(str(res), list_clusters)
        self.check_common_merge(list_clusters)
        
        list_clusters.sort(key=len,reverse=True)
                
    def add_res_cluster_byseq(self,res,list_clusters):
        '''
        Add residue to clusters based on sequence distance
        '''
        flag_added = False
        for cl in list_clusters:
            for res_cl in cl:
                try:
                    # 4 residue distance in seq
                    if abs(int(res_cl) - int(res)) < 4:
                        if not res in cl: cl.append(res)
                        flag_added = True
                        break
                except TypeError:
                    pass
        if not flag_added: list_clusters.append([res])

    def merge_list_clusters(self,list_clusters):
        '''
        Merge clusters with one residue into one cluster at the end
        '''
        list_singles = []
        ct_pop = 0
        for l in xrange(len(list_clusters)):
            #print l, ct_pop, len(list_clusters)
            lc = list_clusters[l-ct_pop]
            if len(lc) == 1:
                #convert to int for sorting
                try: list_singles.append(int(lc[0]))
                except TypeError:
                    list_singles.append(lc[0])
                list_clusters.pop(l-ct_pop)
                ct_pop += 1
        list_singles.sort()
        list_singles = [str(item) for item in list_singles]
        list_clusters.append(list_singles)
