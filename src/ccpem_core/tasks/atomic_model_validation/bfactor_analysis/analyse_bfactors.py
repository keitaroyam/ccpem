import sys,os
from ccpem_core.model_tools.gemmi_utils import *


def main():
    
    model_path = sys.argv[1]
    assert os.path.exists(model_path)
    modelid = os.path.splitext(os.path.basename(model_path))[0]
    get_bfactors(model_path,out_json='bfact.json')
    #calc_bfact_deviation(model_path,out_json='bfact_deviation.json')
    get_residue_ca_coordinates(model_path,out_json='ca_coord.json')

if __name__ == '__main__':
    sys.exit(main())