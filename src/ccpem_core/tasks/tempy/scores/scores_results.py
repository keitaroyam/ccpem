#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
import shutil
import pyrvapi
from ccpem_core import ccpem_utils
try:
    from bokeh.plotting import figure as bokeh_figure, output_file as bokeh_output_file, save as bokeh_save
except ImportError:
    bokeh = None

class GlobScoreResultsViewer1(object):
    def __init__(self, smoc_dataframe, directory):
        self.directory = os.path.join(directory,
                                      'bokeh_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        # Metadata contained in pandas dataframe
        self.smoc_dataframe = smoc_dataframe

        # Set results
        if bokeh is not None:
            self.index = None
            self.set_bokeh_page()
            self.set_results()
    
    def set_bokeh_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
    
    def set_results(self):
        self.set_results_summary()
        self.set_results_graphs()
        
    def set_results_summary(self):
        # Setup refine_results (summary, graphs and output files)
        pass

    def set_results_graphs(self):
        
        # output to static HTML file
        bokeh_output_file(os.path.join(self.directory, 'index.html'),
                          mode="inline")
        
        x_label = 'Residue'
        y_label = 'SMOC'
        # create a new plot with a title and axis labels
        p = bokeh_figure(title="SMOC score", 
                         x_axis_label=x_label, 
                         y_axis_label=y_label,
                         tools="pan,box_zoom,reset,save")
        
        # Get data
        for pdb in self.smoc_dataframe.columns.levels[0]:

            pdb_df = self.smoc_dataframe[pdb].dropna(axis=0, how='any')

            p.line(pdb_df['residue'].tolist(),pdb_df['smoc'].tolist(),legend=pdb,
                   line_width=3,line_color="green")
            p.circle(pdb_df['residue'].tolist(),pdb_df['smoc'].tolist(), 
                       legend=pdb,fill_color="green")
                
        bokeh_save(p)
        
        
class GlobScoreResultsViewer(object):
    def __init__(self, pd_dataframe, directory):
        self.directory = os.path.join(directory,
                                      'rvapi_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        # Metadata contained in pandas dataframe
        self.sccc_dataframe = pd_dataframe

        # Set results
        self.index = None
        self.set_pyrvapi_page()
        self.set_results()

    def set_results(self):
        self.set_results_summary()
        self.set_results_tables()

    def set_pyrvapi_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
        # Setup share jsrview
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document('TestRun', self.directory,
                                    'RVAPI Demo 1', 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()

    def set_results_summary(self):
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('TEMPy GlobScore results')
        pyrvapi.rvapi_add_tab('tab2', 'TEMPy Scores', True)
        pyrvapi.rvapi_add_section('sec1', 'Results', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            '',
            'sec1', 0, 0, 1, 1)

        # See flex-em results for example of setting a datatable here.
        pyrvapi.rvapi_flush()
    
    def set_results_tables(self):
        build_table = 'sec1/table1'
        pyrvapi.rvapi_add_table1(build_table,'TEMPy scores', 1, 0, 1, 1, False)
        pyrvapi.rvapi_put_horz_theader('table1','Atomic model','',0)
        dict_scores_tooltip = {'overlap_map':'fraction of overlap w.r.t map',
                               'overlap_model':'fraction of overlap w.r.t model',
                               'correlation': 'cross correlation coefficient',
                               'mi': 'mutual information',
                               'local_correlation': 'cross correlation coefficent'\
                                ' calculated on the volume of overlap',
                                'local_mi': 'mutual information score'\
                                ' calculated on the volume of overlap',
                                'ccc_ov': 'combined local_correlation and overlap_model score',
                                'mi_ov': 'combined mutual information and overlap_model score'}
        num_models = len(self.sccc_dataframe.index)
        ct_sc = len(self.sccc_dataframe.columns)
        ct_col = 0
        for j in range(1,ct_sc+1):
            if j == 4: continue#skip MI
            if num_models < 3:
                if j > ct_sc-2: continue #skip combined overlap scores
            col = self.sccc_dataframe.columns[j-1]
            ct_col += 1
            pyrvapi.rvapi_put_horz_theader('table1',col,dict_scores_tooltip[col],ct_col)
            
            
        
        for i in range(len(self.sccc_dataframe.index)):
            index = self.sccc_dataframe.index[i]
            pyrvapi.rvapi_put_table_string ( 'table1',index,i,0 )
            ct_col = 0
            for j in range(1,ct_sc+1):
                #print i,index,j,self.sccc_dataframe.columns[j-1]
                if j == 4: continue#skip MI
                if num_models < 3:
                    if j > ct_sc-2: continue #skip combined overlap scores
                col = self.sccc_dataframe.columns[j-1]
                ct_col += 1
                pyrvapi.rvapi_put_table_string ( 'table1','%0.3f' % round(self.sccc_dataframe[col][index],3),i,ct_col)
        pyrvapi.rvapi_flush()
