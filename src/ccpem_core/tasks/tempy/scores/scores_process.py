#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

'''
Run TEMPy:Global scores.
    Take input list of structures and score against single map.
    Output csv table of results
'''

import sys
import os
import json
import pandas as pd
from ccpem_core.TEMPy.MapParser import MapParser
from ccpem_core.TEMPy.StructureParser import PDBParser
from TEMPy.StructureBlurrer import StructureBlurrer
from ccpem_core.TEMPy.ScoringFunctions import ScoringFunctions
from ccpem_core import ccpem_utils
from ccpem_core.tasks.tempy.scores import scores_results
from collections import OrderedDict
from TEMPy.mapprocess import Filter
mrcfile_import=True
try:
    import mrcfile
except ImportError:
    mrcfile_import = False

def main(json_path, verbose=True,set_results=True):
    # Process arguments from json parameter file
    args = json.load(open(json_path, 'r'))
    map_path = args['map_path']
    map_resolution =  args['map_resolution']
    pdb_path_list = args['input_pdbs']
    job_location = args['job_location']
    map_level = None
    if not args['auto_contour_level']:
        map_level = args['map_contour_level']
    syn_map_list = []
    if args['use_refmac']:
        if 'sim_maps' in args:
            simmap = True
            for smap in args['sim_maps']:
                if not os.path.exists(smap):
                    simmap = False
                    break
            if simmap: syn_map_list = args['sim_maps']
        if 'map_path_edit' in args:
            if os.path.exists(args['map_path_edit']):
                map_path = args['map_path_edit']
    
    if job_location is None:
        job_location = os.getcwd() 

    if not isinstance(pdb_path_list, list):
        pdb_path_list = [pdb_path_list]

    if verbose:
        ccpem_utils.print_sub_header('Process TEMPy scores')
    # Run sccc score
    scores_df = process_global_scores(
        map_path=map_path,
        map_resolution=map_resolution,
        map_level = map_level,
        pdb_path_list=pdb_path_list,
        syn_map_list=syn_map_list,
        directory=job_location)
        
    # Save scores as csv
    csv_path = os.path.join(job_location,
                            'global_scores.csv')
    if verbose:
        ccpem_utils.print_sub_header('Save raw scores')
        ccpem_utils.print_sub_sub_header(csv_path)
    with open(csv_path, 'w') as path:
        scores_df.to_csv(path,sep=';')
            
    # Set jsrview
    if set_results and not set_results == 'False':
        scores_results.GlobScoreResultsViewer(
            scores_df,
            directory=job_location)


def process_global_scores(map_path,
                        map_resolution,
                        map_level,
                        pdb_path_list,
                        syn_map_list,
                        directory):
    em_map = read_mapfile(map_path)
    #em_map = MapParser.readMRC(map_path)
    if map_resolution > 4.0: t = 2.0
    else: t=2.5
    if map_level is None:
        map_level = map_contour(em_map, t)
        print 'Calculated map contour level is', map_level
    # Get score for each structure in list
    dict_table = {}
    dict_scores = {}
    list_pdbs = []
    l = 0
    if map_resolution > 10.0: t = 2.5
    elif map_resolution > 6.0: t = 2.0
    else: t = 1.5
    for pdb_path in pdb_path_list:
        pdb_id = os.path.basename(pdb_path)
        list_pdbs.append(pdb_id)
        if len(syn_map_list) == 0:
            pName,modelmap = blur_model(pdb_path, map_resolution, em_map)
            model_level = model_contour(modelmap, t)
        else:
            synmapfile = syn_map_list[l]
            modelmap = read_mapfile(synmapfile)
            pName = os.path.splitext(pdb_id)[0]
            #level for Refmac map
            model_level = 0.15
            print 'Calculated model contour level is', model_level
        #calculate scores
        get_global_scores(em_map,modelmap,map_level,model_level,dict_scores,pName)
        l += 1
        
    sc = ScoringFunctions()
    
    dict_scores['mi_ov'] = sc.scale_median(
                            dict_scores['overlap_model'],
                            dict_scores['local_mi'])
    dict_scores['ccc_ov'] = sc.scale_median(
                            dict_scores['overlap_model'],
                            dict_scores['local_correlation'])

    order = ['overlap_map','overlap_model','correlation','mi','local_correlation',
             'local_mi','ccc_ov','mi_ov']
    dict_scores_ordered = OrderedDict()
    for k in order:
        dict_scores_ordered[k] = dict_scores[k]
    df = pd.DataFrame(dict_scores_ordered,index = list_pdbs)
    return df

def read_mapfile(map_path):
    if mrcfile_import:
        mrcobj=mrcfile.open(map_path,mode='r',permissive=True)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
        emmap.set_apix_tempy()
    else:
        mrcobj=MapParser.readMRC(map_path)
        emmap = Filter(mrcobj)
        emmap.fix_origin()
    return emmap

#calculate map contour
def map_contour(emmap,t=-1.):
    c1 = 0.0
    if t != -1.0:
        #print 'calculating map contour'
        zeropeak,ave,sigma1 = emmap.peak_density()
        if not zeropeak is None: c1 = zeropeak+(t*sigma1)
    return c1
#calculate model contour
def model_contour(modelmap,t=-1.):
    c2 = 0.0
    if t != -1.0:
        c2 = t*modelmap.std()#0.0
    #print 'Calculated model contour: ', c2
    return c2

def blur_model(p,res=4.0,emmap=False):
    pName = os.path.basename(p).split('.')[0]
    structure_instance=PDBParser.read_PDB_file(pName,p,hetatm=False,water=False)
    blurrer = StructureBlurrer()
    if res is None: sys.exit('Map resolution required..')
    #emmap = blurrer.gaussian_blur(structure_instance, res,densMap=emmap_1,normalise=True)
    modelmap = blurrer.gaussian_blur_real_space(structure_instance, res,sigma_coeff=0.225,densMap=emmap,normalise=True) 
    return pName,modelmap

def get_global_scores(emmap1,emmap2,c1,c2,dict_scores_hits,Name2):
    sc = ScoringFunctions()
    print Name2
    #OVR
    overlap_map, overlap_model = sc.calculate_overlap_scores(emmap1,emmap2,c1,c2)
    print 'Fraction of map overlapping with model: ', overlap_map
    print 'Fraction of model overlapping with map: ', overlap_model
    if overlap_model < 0.02:
        print "Maps do not overlap: ", Name2
        ccc_mask = ccc = -1.0
        mi_mask = mi = 0.0
    else:
        #CCC
        try:
            ccc_mask,ovr = sc.CCC_map(emmap1,emmap2,c1,c2,3,meanDist=True)
            
            if ovr < 0.0: ovr = 0.0
        except:
            print 'ccc and overlap score calculation failed for', Name2
            ovr = 0.0
        #SCCC
        print 'Local correlation score: ', ccc_mask
        if ccc_mask < -1.0 or ccc_mask > 1.0:
            ccc_mask = -1.0
        #LMI
        try:
            mi_mask = sc.MI(emmap1,emmap2,c1,c2,3)
            print 'Local Mutual information score: ', mi_mask
            if mi_mask < 0.0: mi_mask = 0.0
        except:
            print 'MI score calculation failed for', Name2
            mi_mask = 0.0
        
        #CCC
        try:
            ccc,overlap = sc.CCC_map(emmap1,emmap2,c1,c2,2,meanDist=True)
        except:
            print 'ccc score calculation failed for', Name2
        #SCCC
        print 'Correlation score: ', ccc
        if ccc < -1.0 or ccc > 1.0:
            ccc = -1.0
            
          #NMI
        try:
            mi = sc.MI(emmap1,emmap2,c1,c2,1,None,None,False)
            print 'Mutual information score:', mi
            if mi < 0.0: mi = 0.0
        except:
            print 'MI score calculation failed for', Name2
            mi = 0.0
        
    try: dict_scores_hits['local_correlation'].append(ccc_mask)
    except KeyError: dict_scores_hits['local_correlation'] = [ccc_mask]
    try: dict_scores_hits['local_mi'].append(mi_mask)
    except KeyError: dict_scores_hits['local_mi'] = [mi_mask] 
    try: dict_scores_hits['overlap_map'].append(overlap_map)
    except KeyError: dict_scores_hits['overlap_map'] = [overlap_map]
    try: dict_scores_hits['overlap_model'].append(overlap_model)
    except KeyError: dict_scores_hits['overlap_model'] = [overlap_model]
    try: dict_scores_hits['correlation'].append(ccc)
    except KeyError: dict_scores_hits['correlation'] = [ccc]
    try: dict_scores_hits['mi'].append(mi)
    except KeyError: dict_scores_hits['mi'] = [mi]


def import_scores_from_csv(path):
    return pd.read_csv(path, index_col=0)


if __name__ == '__main__':
    if len(sys.argv) <= 1:
        main(json_path='./test_data/unittest_args.json')
#         print 'Please supply json parameter file'
    elif len(sys.argv) > 2:
        main(json_path=sys.argv[1], set_results=sys.argv[2])
    else:
        main(json_path=sys.argv[1])
