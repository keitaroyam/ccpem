#
#     Copyright (C) 2017 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os,sys
import shutil
import pyrvapi
from ccpem_core import ccpem_utils
bokeh_flag = 1
try:
    from bokeh.plotting import figure as bokeh_figure, output_file as bokeh_output_file, save as bokeh_save
except ImportError:
    bokeh_flag = 0

class SMOCResultsViewer1(object):
    def __init__(self, smoc_dataframe, directory):
        self.directory = os.path.join(directory,
                                      'bokeh_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        # Metadata contained in pandas dataframe
        self.smoc_dataframe = smoc_dataframe

        # Set results
        if bokeh_flag == 1:
            self.index = None
            self.set_bokeh_page()
            self.set_results()
    
    def set_bokeh_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
    
    def set_results(self):
        self.set_results_summary()
        self.set_results_graphs()
        
    def set_results_summary(self):
        # Setup refine_results (summary, graphs and output files)
        pass

    def set_results_graphs(self):
        
        # output to static HTML file
        bokeh_output_file(os.path.join(self.directory, 'index.html'),
                          mode="inline")
        
        x_label = 'Residue'
        y_label = 'SMOC'
        # create a new plot with a title and axis labels
        p = bokeh_figure(title="SMOC score", 
                         x_axis_label=x_label, 
                         y_axis_label=y_label,
                         tools="pan,box_zoom,reset,save")
        
        # Get data
        for pdb in self.smoc_dataframe.columns.levels[0]:

            pdb_df = self.smoc_dataframe[pdb].dropna(axis=0, how='any')

            p.line(pdb_df['residue'].tolist(),pdb_df['smoc'].tolist(),legend=pdb,
                   line_width=3,line_color="green")
            p.circle(pdb_df['residue'].tolist(),pdb_df['smoc'].tolist(), 
                       legend=pdb,fill_color="green")
                
        bokeh_save(p)
        

class SMOCResultsViewer(object):
    def __init__(self, smoc_dataframe, directory, set_results=True):
        self.directory = os.path.join(directory,
                                      'rvapi_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        # Metadata contained in pandas dataframe
        self.smoc_dataframe = smoc_dataframe

        # Set results
        self.index = None
        if set_results:
            self.set_pyrvapi_page()
            self.set_results_summary()
            self.set_results_graphs()

    def set_pyrvapi_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
        # Setup share jsrview
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document('TestRun', self.directory,
                                    'RVAPI Demo 1', 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()

    def set_results_summary(self,section_name='sec1'):
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('SMOC results')
        pyrvapi.rvapi_add_tab('tab2', 'SMOC Scores', True)
        pyrvapi.rvapi_add_section(section_name, 'Results', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            'Local fragment Score based on Manders\' Overlap Coefficient (SMOC)',
            'sec1', 0, 0, 1, 1)

        # See flex-em results for example of setting a datatable here.
        pyrvapi.rvapi_flush()

    def set_results_graphs(self,section_name='sec1'):
        # Refinement statistics vs cycle
        pyrvapi.rvapi_append_loggraph1(section_name+'/graphWidget1')
        # Add graph data
        pyrvapi.rvapi_add_graph_data1('graphWidget1/data1',
                                      'SMOC Scores')
        x_label = 'Residue'
        
        # Set plot
        y_label = 'SMOC'

        list_chains = []
        ct_plot = 0
        dict_chain = {}
        # Get data
        for pdb in self.smoc_dataframe.columns.levels[0]:
            #print pdb
            #add plots for each chain
            chain = pdb.split('_')[-1]
            if chain == '': chain = ' '
            if not chain in list_chains:
                list_chains.append(chain)
                ct_plot += 1
                dict_chain[chain] = ct_plot
                pyrvapi.rvapi_add_graph_plot1(
                    'graphWidget1/plot'+`ct_plot`,
                     chain,
                     x_label,
                     y_label)
            
            pyrvapi.rvapi_add_graph_dataset1(
                'graphWidget1/data1/x'+pdb,
                'x',
                x_label)
            pyrvapi.rvapi_add_graph_dataset1(
                'graphWidget1/data1/y1'+pdb,
                pdb,
                pdb)

            pdb_df = self.smoc_dataframe[pdb].dropna(axis=0, how='any')
        
            for index, row in pdb_df.iterrows():
                pyrvapi.rvapi_add_graph_int1(
                    'graphWidget1/data1/x'+pdb,
                    int(row['resnum']))
                pyrvapi.rvapi_add_graph_real1(
                    'graphWidget1/data1/y1'+pdb,
                    float(row['smoc']),
                    '%g')
                #print row['resnum'], row['smoc']

            pyrvapi.rvapi_add_plot_line1('graphWidget1/data1/plot'+`dict_chain[chain]`,
                                         'x'+pdb,
                                         'y1'+pdb)
#         pyrvapi.rvapi_set_plot_xmin('plot1', 'graphWidget1', 0.0)
#         pyrvapi.rvapi_set_plot_ymin('plot1', 'graphWidget1', 0.0)
        #
        pyrvapi.rvapi_flush()
        
class SCCCResultsViewer(object):
    
    def __init__(self, pd_dataframe, directory, set_results=True):
        self.directory = os.path.join(directory,
                                      'rvapi_data')
        if os.path.exists(self.directory):
            shutil.rmtree(self.directory)
        ccpem_utils.check_directory_and_make(self.directory)

        # Metadata contained in pandas dataframe
        self.sccc_dataframe = pd_dataframe

        # Set results
        self.index = None
        if set_results:
            self.set_pyrvapi_page()
            self.set_results_summary()
            self.set_results_tables()

    def set_pyrvapi_page(self):
        # Setup index.html
        self.index = os.path.join(self.directory, 'index.html')
        # Setup share jsrview
        ccp4 = os.environ['CCPEM']
        share_jsrview = os.path.join(ccp4, 'share', 'jsrview')
        # Setup pages
        pyrvapi.rvapi_init_document('TestRun', self.directory,
                                    'RVAPI Demo 1', 1, 4, share_jsrview,
                                    None, None, None, None)
        pyrvapi.rvapi_flush()

    def set_results_summary(self):
        # Setup refine_results (summary, graphs and output files)
        pyrvapi.rvapi_add_header('SCCC results')
        pyrvapi.rvapi_add_tab('tab2', 'SCCC Scores', True)
        pyrvapi.rvapi_add_section('sec1', 'Results', 'tab2', 0, 0, 1, 1,
                                  True)
        pyrvapi.rvapi_add_text(
            'Rigid Body Segment Cross Correlation Coefficient (SCCC)',
            'sec1', 0, 0, 1, 1)

        # See flex-em results for example of setting a datatable here.
        pyrvapi.rvapi_flush()
    
    def set_results_tables(self):
        build_table = 'sec1/table1'
        pyrvapi.rvapi_add_table1(build_table,'SCCC scores', 1, 0, 1, 1, False)
        pyrvapi.rvapi_put_horz_theader('table1','Rigid Bodies','',0)
        ct_pdbs = 1
        for col in self.sccc_dataframe.columns:
            pyrvapi.rvapi_put_horz_theader('table1',col,'',ct_pdbs)
            ct_pdbs += 1
        for i in range(len(self.sccc_dataframe.index)):
            index = self.sccc_dataframe.index[i]
            pyrvapi.rvapi_put_table_string ( 'table1',index,i,0 )
            for j in range(1,ct_pdbs):
                col = self.sccc_dataframe.columns[j-1]
                #print self.sccc_dataframe[col][index]
                try: pyrvapi.rvapi_put_table_string ( 'table1','%0.2f' % self.sccc_dataframe[col][index],i,j )
                except TypeError: sys.exit('SCCC calculation failed')
        pyrvapi.rvapi_flush()
