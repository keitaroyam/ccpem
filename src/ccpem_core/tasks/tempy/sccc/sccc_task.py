#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.TEMPy.Examples import get_sccc

class SCCC(task_utils.CCPEMTask):
    '''
    CCPEM / TEMPy difference map wrapper.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='TEMPySCCC',
        author='M. Topf, A. Joseph',
        version='1.1',
        description=(
            'SCCC local scoring using TEMPy library'),
        short_description=(
            '[BETA TEST] Local fragment Score based on Cross Correlation '
            'Coefficient'),
        documentation_link='http://topf-group.ismb.lon.ac.uk/TEMPY.html',
        references=None)

    commands = {'ccpem-python':
        ['ccpem-python', os.path.realpath(get_sccc.__file__)]}

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(SCCC, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input map (mrc format)',
            metavar='Input map',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-pdb_path',
            '--pdb_path',
            help='Input pdb path',
            metavar='Input PDB',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-rigid_body_path',
            '--rigid_body_path',
            help='Input rigid body file',
            metavar='Input rigid body',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-map_resolution',
            '--map_resolution',
            help=('Resolution of map (Angstrom)'),
            metavar='Resolution map',
            type=float,
            default=None)
        #
        parser.add_argument(
            '-score_name',
            '--score_name',
            help=('File name for score output'),
            metavar='Score name',
            type=str,
            default='sccc_score.txt')
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        # Set args job location
        self.args.job_location.value = self.job_location
        filename = os.path.join(self.job_location, 'args.json')
        self.args.output_args_as_json(
            filename=filename)

        # Generate process
        self.sccc_wrapper = SCCCWrapper(
            command=self.commands['ccpem-python'],
            job_location=self.job_location,
            name='SCCC score')

        pl = [[self.sccc_wrapper.process]]
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

class SCCCWrapper(object):
    '''
    Wrapper for TEMPy SCCC process.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__

        # Set args
        self.args = os.path.join(self.job_location,
                                 'args.json')

        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
