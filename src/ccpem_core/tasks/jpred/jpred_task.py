
import os,sys,re
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
import json
from ccpem_progs.jpred import jpred_submit
import subprocess
import glob
import time
from Bio import SeqIO
import types
import urllib
import tarfile
from ccpem_core.process_manager import job_register
from ccpem_core import settings

class JpredTask:
    '''
    JPred Secondary Structure Prediction
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Jpred',
        author='Drozdetskiy A et al.',
        version='4',
        description=(
            '''Predict secondary structure from sequence.<br>
            Uses a neural network based Jnet algorithm for prediction.<br>
            Mean prediction accuracy : >82%
            '''),
        short_description=(
            'Predict secondary structure from sequence'),
        documentation_link='http://www.compbio.dundee.ac.uk/jpred4/about.shtml',
                        
        references=None)

    commands = {'ccpem-jpred':
        ['ccpem-python', os.path.realpath(jpred_submit.__file__)]}
    
    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(JpredTask, self).__init__(
            database_path=database_path,
            args=args,
            args_json=args_json,
            pipeline=pipeline,
            job_location=job_location,
            parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Input pdb'),
            metavar='Input PDB',
            type=str,
            default=None)
        
        parser.add_argument(
            '-input_pdbs',
            '--input_pdbs',
            help=('Input pdb(s) \n'
                  'ensure same residue numbering in all \n'
                  'ensure atomic B-factors are refined/set (for Refmac) \n'
                  'currently supports single model only (per file)'),
            metavar='Input PDB',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-input_pdb_chains',
            '--input_pdb_chains',
            help='Input PDB chain(s)',
            metavar='Input PDB chain selections',
            type=str,
            nargs='*',
            default=None)

    
    def run_pipeline(self, job_id=None, db_inject=None):
        
        pl = []
        pdbs = self.args.input_pdbs()
        if not isinstance(pdbs, list):
            pdbs = [pdbs]
        list_processes = []
        for pdbfile in pdbs:
            pdbname = '.'.join(os.path.basename(pdbfile).split('.')[:-1])
            self.jpred_wrapper = JPredWrapper(
                self.commands['ccpem-jpred'],
                pdbfile,
                job_location=self.job_location,
                name='Jpred: '+pdbname,
                )
            list_processes.append(self.jpred_wrapper.process)
        
        
    def add_parallel_process(self,pl, list_process):
        num_process = len(list_process)
        ct_added_process = 0
        pl_stages = min(len(pl),num_process)
        for l in xrange(pl_stages,0,-1):
            if len(pl[-l]) < self.args.ncpu.value and \
                                ct_added_process < num_process: 
                pl[-l].append(list_process[ct_added_process])
                ct_added_process += 1

        for l in xrange(ct_added_process,num_process):
            pl.append([list_process[l]])
        
class JPredRunWrapper():
    '''
    Wrapper for JPred job submission.
    '''
    def __init__(self,
                 command,
                 pdb_path,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        # Set args
        self.args = [pdb_path]
        self.args += [self.job_location]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
        
class JPredDownloadWrapper():
    '''
    Wrapper for JPred job download.
    '''
    def __init__(self,
                 command,
                 job_location,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        # Set args
        self.args = [self.job_location]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
def main():
    pdbfile = sys.argv[1]
    if len(sys.argv) > 2:
        job_location = sys.argv[2]
    else: job_location = None
    #jp = JpredRun(pdbfile=pdbfile,job_location=job_location)

if __name__ == "__main__":
    sys.exit(main())
    