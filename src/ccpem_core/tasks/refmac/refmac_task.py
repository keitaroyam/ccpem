#
#     Copyright (C) 2015 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

# ProSMART CCP4 implementation details
# http://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/prosmart/documentation.html#refmac
# http://www.ccp4.ac.uk/newsletters/newsletter48/articles/ProSmart/an_overview_of_prosmart.html

import os
import shutil
import copy

import mrcfile

from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core.tasks.refmac import refmac_results
from ccpem_core import ccpem_utils
from ccpem_core.settings import which
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core import settings

# From Garib:
# source em mb # generate electron form factors from X-ray. (Preferred)
# source em    # use electron from factors (Peng et al). In this case atomsf
#                should be changed to atomsf_electron.lib
refmac_source = 'source EM MB'

class Refmac(task_utils.CCPEMTask):
    '''
    CCPEM Refmac task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='Refmac5',
        author='Brown A, Long F, Nicholls RA, Long F, Toots J, Murshudov G',
        version='5.8.0103',
        description=(
            'Macromolecular refinement program.  REFMAC is a program '
            'designed for REFinement of MACromolecular structures. It uses '
            'maximum likelihood and some elements of Bayesian '
            'statistics.  N.B. requires CCP4.'),
        short_description=(
            'Atomic structure refinement.'),
        documentation_link='https://www2.mrc-lmb.cam.ac.uk/groups/murshudov/content/refmac/refmac_keywords.html',
        references=None)

    commands = {
        'refmac': settings.which(program='refmac5'),
        'sfcheck': settings.which(program='sfcheck'),
        'pdbset':  settings.which(program='pdbset'),
        'libg': settings.which(program='libg'),
        'ccpem-python':  settings.which(program='ccpem-python')
    }


    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 verbose=False,
                 parent=None):
#        self.atomsf = os.path.join(
#            os.environ['CLIBD'],
#           'atomsf_electron.lib')
#        assert os.path.exists(self.atomsf)
        self.atomsf = None # Garib prefer to use 'source EM MB' rather than atomsf - so switching atomsf off

        super(Refmac, self).__init__(database_path=database_path,
                                     args=args,
                                     args_json=args_json,
                                     pipeline=pipeline,
                                     job_location=job_location,
                                     verbose=verbose,
                                     parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-dc_mode',
            '--dc_mode',
            help='''Refmac divide and conquer for multiple job submission
(in testing)''',
            type=bool,
            metavar='Multi PDBs/Maps',
            default=False)
        #
        parser.add_argument(
            '-dc_proto',
            '--dc_proto',
            help='''Refmac divide and conquer for multiple job submission
(in testing)''',
            type=bool,
            metavar='Refmac DAC Proto',
            default=True)
        #
        parser.add_argument(
            '-libg',
            '--libg',
            help='''Use LIBG to generate restraints for nucleic acids''',
            metavar='Nucleic acids',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-libg_selection',
            '--libg_selection',
            help='Specify nucleotide ranges for libg (please refer to libg documentation). If you ignore this field, all nucleic acids will be selected for restraint generation',
            metavar='Nucleotide range (ignore to select all)',
            type=str,
            default='')
        #
        parser.add_argument(
            '-input_map',
            '--input_map',
            help='''Target input map (mrc format)''',
            type=str,
            metavar='Input map',
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-resolution',
            '--resolution',
            help='''Resolution of input map (Angstrom)''',
            metavar='Resolution',
            type=float,
            nargs='*',
            default=None)

        parser.add_argument(
            '-use_cluster',
            '--use_cluster',
            help=('Submit jobs to cluster rather then run on local CPU'),
            type=bool,
            metavar='Submit to cluster',
            default=False)

        parser.add_argument(
            '-numberCPUsOrNodes',
            '--numberCPUsOrNodes',
            help=('Number of CPUs or cluster nodes for parallel execution'),
            type=int,
            metavar='# of CPUs or nodes',
            default=1)

        parser.add_argument(
            '-timeQuant',
            '--timeQuant',
            help=('Waiting time before polling Refmac5 jobs - for quick unittesting only'),
            type=int,
            metavar='Time Quant',
            default=60)

        parser.add_argument(
            '-validate_on',
            '--validate_on',
            help='''Perform check for model overfitting''',
            metavar='Validate',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-vector_difference_map',
            '--vector_difference_map',
            help='Output vector difference map (use observed phases)',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-half1',
            '--half_map_1',
            help=('Half map 1 of input map, required for cross validation '
                  'post refinement (mrc format)'),
            metavar='Half map 1',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-half2',
            '--half_map_2',
            help=('Half map 2 of input map, required for cross validation '
                  'post refinement (mrc format)'),
            type=str,
            metavar='Half map 2',
            default=None)
        #
        parser.add_argument(
            '-mapref',
            '--reference_map',
            help='Reference map for half map 1 vs half map 2 calculation (mrc format)',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-start_pdb',
            '--start_pdb',
            help='Input coordinate file (pdb/cif format)',
            type=str,
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Library dictionary file for ligands (lib/cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-find_in_map',
            '--find_in_map',
            help='Fit starting model into map if necessary (uses Molrep)',
            metavar='Find in map',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-external_restraints_on',
            '--external_restraints_on',
            help='Use external restraints',
            metavar='Use restraints',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-restraints_files',
            '--restraints_files',
            help='External restraints files',
            type=str,
            metavar='Restraints',
            nargs='*',
            default=None)
        #
        parser.add_argument(
            '-external_weight_scales',
            '--external_weight_scales',
            help=('External restraints weight. Increasing this weight '
                  'increases the influence of external restraints during '
                  'refinement'),
            type=float,
            nargs='*',
            metavar='Weight',
            default=10.0)
        #
        parser.add_argument(
            '-external_weight_gmwts',
            '--external_weight_gmwts',
            help=(
'Geman-McClure parameter, which controls robustness to outliers. Increasing '
'this value reduces the influence of outliers (i.e. restraints that are very '
'different from the current interatomic distance)'),
            type=float,
            nargs='*',
            metavar='GMWT',
            default=0.02)
        #
        parser.add_argument(
            '-restraints_file',
            '--restraints_file',
            help='External restraints files',
            type=str,
            metavar='Restraints',
            default=None)
        #
        parser.add_argument(
            '-external_weight_scale',
            '--external_weight_scale',
            help=('External restraints weight. Increasing this weight '
                  'increases the influence of external restraints during '
                  'refinement'),
            type=float,
            metavar='Weight',
            default=10.0)
        #
        parser.add_argument(
            '-external_weight_gmwt',
            '--external_weight_gmwt',
            help=(
'Geman-McClure parameter, which controls robustness to outliers. Increasing '
'this value reduces the influence of outliers (i.e. restraints that are very '
'different from the current interatomic distance)'),
            type=float,
            metavar='GMWT',
            default=0.02)
#
        parser.add_argument(
            '-external_weight_dmx',
            '--external_weight_dmx',
            help=('Maximum restraint interatomic distance'),
            type=float,
            metavar='Dmax',
            default=4.2)
        #
        parser.add_argument(
            '-external_main_only',
            '--external_main_only',
            help=(
'Discards any side-chain restraints that may be present in the external '
'restraints file'),
            type=bool,
            metavar='Main chain',
            default=False)
        #
        parser.add_argument(
            '-local_refinement_on',
            '--local_refinement_on',
            help='Refine around radius of supplied molecule only',
            metavar='Local refine',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-mask_radius',
            '--mask_radius',
            help='Distance around molecule the map should be cut',
            type=float,
            default=3.0)
        #
        parser.add_argument(
            '-ncycle',
            '--ncycle',
            help='Number of refmac cycles',
            metavar='Refmac cycles',
            type=int,
            default=20)
        #
        parser.add_argument(
            '-weight',
            '--weight',
            help=('Relative weight of experimental restraints w.r.t. '
                  'geometric restraints.  Smaller values result in stricter '
                  'geometric restraints'),
            metavar='Weight',
            type=float,
            default=0.0001)
#
        parser.add_argument(
            '-weight_auto',
            '--weight_auto',
            help=('Use auto-weighting by REFMAC5 (recommended)'),
            metavar='Auto weight',
            type=bool,
            default=True)
#
        parser.add_argument(
            '-symmetry_auto',
            '--symmetry_auto',
            help='Automatically detect and apply symmetry (recommended)',
            metavar='Auto symmetry',
            choices=['None',
                     'Local',
                     'Global'
                     ],
            type=str,
            default='Local')

        #
        parser.add_argument(
            '-map_sharpen',
            '--map_sharpen',
            metavar='Sharpen / blur',
            help=('B-factor to apply to map. Negative B-factor to '
                  'sharpen, positive to blur, zero to leave map as '
                  'input'),
            type=float,
            default=None)
        #
        parser.add_argument(
            '-set_bfactor_on',
            '--set_bfactor_on',
            help='Reset all atomic B-factors to given value on input',
            metavar='Set B-factor',
            type=bool,
            default=True)
        #
        parser.add_argument(
            '-set_bfactor',
            '--set_bfactor',
            help='Set all input atomic B-factors to this value',
            metavar='B-factor',
            type=float,
            default=40.0)
        #
        parser.add_argument(
            '-jelly_body',
            '--jelly_body',
            help='Use jelly body restraints',
            type=bool,
            metavar='Jelly body',
            default=True)
        #
        parser.add_argument(
            '-hydrogens',
            '--hydrogens',
            help='Add hydrogen atoms during refinement (may improve clash score)',
            metavar='Add hydrogens',
            type=bool,
            default=False)
        #
        parser.add_argument(
            '-n_monomers',
            '--n_monomers',
            help='''Number of monomers to find''',
            metavar='Number to find',
            type=int,
            default=1)
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Keywords for advanced options.  Select file or define text',
            type=str,
            default='')
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None, run=True):
        '''
        Generate job classes and process.  Run=false for reloading.
        '''
        if self.args.dc_mode():
#            print 'Refmac DC - Oleg to add...'
#           if run:
#                process = process_manager.CCPEMProcess(
#                    name='Test_process',
#                    command='ccpem-python',
#                    args=['-m', 'ccpem_core.gui_ext.tasks.refmac.dummy_dac'],
#                    location=self.job_location,
#                    stdin=None)
#                pl = [[process]]
#                self.pipeline = process_manager.CCPEMPipeline(
#                    pipeline=pl,
#                    args_path=self.args.jsonfile,
#                    location=self.job_location,
#                    database_path=self.database_path,
#                    taskname=self.task_info.name,
#                    title=self.args.job_title.value,
#                    verbose=True)
#                self.pipeline.start()
#            return

            if isinstance(self.args.start_pdb.value, list) and \
               isinstance(self.args.input_map.value, list) and \
               isinstance(self.args.resolution.value, list):

                list_length = len(self.args.start_pdb.value)
                if all(len(lst) != list_length for lst in [self.args.start_pdb.value, self.args.input_map.value, self.args.resolution.value]):
                    print 'At least one list of parameters has different length; check your input'
                    return

                option_l_FileName = os.path.join(self.job_location,'dac_pdbs_maps.txt')

                option_l_File = open(option_l_FileName, 'w')

                for i in range(len(self.args.start_pdb.value)):
                    option_l_File.write('%s %s %0.2f\n' %
                                       (os.path.abspath(self.args.start_pdb.value[i]),
                                       os.path.abspath(self.args.input_map.value[i]),
                                       self.args.resolution.value[i]) )
                option_l_File.close()

                if self.args.ncycle.value:
                    if self.args.ncycle.value >= 0:
                        ncyc = self.args.ncycle.value
                    else:
                        print 'Number of refinement cycles is negative; setting 0'
                        ncyc = 0
                else:
                    print "Number of refinement cycles is not defined; using 0"
                    ncyc = 0

                if self.args.numberCPUsOrNodes.value:
                    if self.args.numberCPUsOrNodes.value >= 1:
                        numberCPUsOrNodes = self.args.numberCPUsOrNodes.value
                    else:
                        print "Number of CPUs is less than 1; using 1"
                        numberCPUsOrNodes = 1
                else:
                    print "Number of CPUs is not defined; using 1"
                    numberCPUsOrNodes = 1

                if self.args.timeQuant.value:
                    if self.args.timeQuant.value >= 1:
                        timeQuant = self.args.timeQuant.value
                    else:
                        timeQuant = 60
                else:
                    timeQuant = 60

                if self.args.map_sharpen() is None:
                    sharp = 0
                else:
                    sharp = -1.0*self.args.map_sharpen()
                self.process_DaC = DivideAndConquer(
                  command=[self.commands['ccpem-python'],
                           '-m', 'ccpem_core.tasks.refmac.dummy_dac'],
                  name='Refine chain by chain',
                  job_location=self.job_location,
                  option_l = option_l_FileName,
                  option_cluster = self.args.use_cluster.value,
                  option_nCPUsOrNodes = numberCPUsOrNodes,
                  option_ncyc = ncyc,
                  option_timeQuant = timeQuant,
                  weight=self.args.weight.value,
                  weight_auto=self.args.weight_auto.value,
                  sharp=sharp,
                  set_bfactor=self.args.set_bfactor_on.value,
                  atom_bfactor=self.args.set_bfactor.value,
                  jelly_body_on=self.args.jelly_body.value,
                  hydrogens=self.args.hydrogens.value,
                  keywords=self.args.keywords.value,
                  lib_in=self.args.lib_in.value)

                pl = [[self.process_DaC.process]]

                #XXXX
                print 'Add results'
                if run:
                    self.pipeline = process_manager.CCPEMPipeline(
                        pipeline=pl,
                        args_path=self.args.jsonfile,
                        location=self.job_location,
                        database_path=self.database_path,
                        taskname=self.task_info.name,
                        title=self.args.job_title.value,
                        verbose=self.verbose)
                    self.pipeline.start()
                return

        # Convert map to mtz
        self.process_maptomtz = RefmacMapToMtz(
            command=self.commands['refmac'],
            resolution=self.args.resolution.value,
            mode='Global',
            name='Map to MTZ',
            job_location=self.job_location,
            map_path=self.args.input_map.value,
            blur_array=[self.args.map_sharpen.value],
            sharp_array=None,
            atomsf_path=self.atomsf)
        pl = [[self.process_maptomtz.process]]

        # DNA/RNA restraints using libg
#$LIBG -p $fin -o ${name}/basepair.restraints -w bp >>& ${logf}
#$LIBG -p $fin -o ${name}/stack.restraints -w sp >>& ${logf}
#$LIBG -p $fin -o ${name}/pucker.restraints -w pu >>& ${logf}
        self.libg_restraints = False
        self.libg_restraints_path = []
        if self.args.libg.value:
            if self.args.libg_selection.value:
                if len(self.args.libg_selection.value.strip()) > 0:
                    self.process_libg = LibgRestraints(
                        command=self.commands['libg'],
                        job_location=self.job_location,
                        name='DNA or RNA basepair restraints',
                        pdb_path=self.args.start_pdb.value,
                        u = self.args.libg_selection.value.strip())
                else:
                    self.process_libg = LibgRestraints(
                        command=self.commands['libg'],
                        job_location=self.job_location,
                        name='DNA or RNA basepair restraints',
                        pdb_path=self.args.start_pdb.value)
            else:
                self.process_libg = LibgRestraints(
                    command=self.commands['libg'],
                    job_location=self.job_location,
                    name='DNA or RNA basepair restraints',
                    pdb_path=self.args.start_pdb.value)
            self.libg_restraints = True
            self.libg_restraints_path = [self.process_libg.libg_restraints_path]
            pl.append([self.process_libg.process])

        # Set cell parameters and scale of model
        self.process_set_unit_cell = SetModelCell(
            job_location=self.job_location,
            name='Set model unit cell',
            pdb_path=self.args.start_pdb.value,
            map_path=self.args.input_map.value)
        pl.append([self.process_set_unit_cell.process])

        # Find PDB in map
        if self.args.find_in_map.value:
            self.process_find_in_map = MolRepFindInMap(
                 job_location=self.job_location,
                 mtz_path=self.process_maptomtz.hklout_path,
                 pdb_path=os.path.join(self.job_location,
                                       'starting_model.pdb'),
                 name='Find PDB in map',
                 resolution=self.args.resolution.value,
                 nmon=self.args.n_monomers.value,
                 f_labin='Fout0')
            pl.append([self.process_find_in_map.process])
            self.start_refmac_pdb_path = \
                self.process_find_in_map.pdbout_path
        else:
            self.start_refmac_pdb_path = self.process_set_unit_cell.cifout_path

        if self.args.local_refinement_on():
            # Local refine
            # Get local structure factors.  N.B. this must come after find in
            # map
            self.process_maptomtz_local = RefmacMapToMtz(
                command=self.commands['refmac'],
                name='Map to MTZ (local)',
                resolution=self.args.resolution.value,
                mode='Local',
                job_location=self.job_location,
                lib_path=self.args.lib_in.value,
                map_path=self.args.input_map.value,
                pdb_path=self.start_refmac_pdb_path,
                blur_array=[self.args.map_sharpen.value],
                sharp_array=None,
                atomsf_path=self.atomsf)
            pl.append([self.process_maptomtz_local.process])

            # Refinement parameters: Local
            mode = 'Local'
            name='Refmac refine (local)'
            pdb_path=self.process_maptomtz_local.pdbout_path
            mtz_path = 'masked_fs.mtz'
        else:
            # Refinement parameters: Local
            mode = 'Global'
            name = 'Refmac refine (global)'
            pdb_path = self.start_refmac_pdb_path
            mtz_path = 'starting_map.mtz'

        #  Refine
        # Invert sharpening value for refmac
        if self.args.map_sharpen() is None:
            sharp = 0
        else:
            sharp = -1.0*self.args.map_sharpen()
        self.process_refine = RefmacRefine(
            command=self.commands['refmac'],
            job_location=self.job_location,
            pdb_path=pdb_path,
            resolution=self.args.resolution.value,
            mtz_path=mtz_path,
            mode=mode,
            name=name,
            weight=self.args.weight.value,
            weight_auto=self.args.weight_auto.value,
            symmetry_auto=self.args.symmetry_auto.value,
            sharp=sharp,
            set_bfactor=self.args.set_bfactor_on.value,
            atom_bfactor=self.args.set_bfactor.value,
            atomsf_path=self.atomsf,
            ncycle=self.args.ncycle.value,
            external_restraints_on=self.args.external_restraints_on.value,
            external_restraints_path=self.args.restraints_files(),
            external_weight_scale=self.args.external_weight_scales(),
            external_weight_gmwt=self.args.external_weight_gmwts(),
            external_weight_dmax=self.args.external_weight_dmx.value,
            external_main_chain=self.args.external_main_only.value,
            lib_path=self.args.lib_in.value,
            jelly_body_on=self.args.jelly_body.value,
            hydrogens=self.args.hydrogens.value,
            keywords=self.args.keywords.value,
            libg_restraints=self.libg_restraints,
            libg_restraints_path=self.libg_restraints_path)
            #
        pl.append([self.process_refine.process])

        # Validation
        if self.args.validate_on.value:
            # Shake structure
            self.process_shake = PDBSetShake(
                name='Shake refined structure',
                job_location=self.job_location,
                pdb_path=self.process_refine.pdbout_path)
            pl.append([self.process_shake.process])

            # Convert half map 1 to mtz; stage
            self.process_maptomtz_hm1 = RefmacMapToMtz(
                command=self.commands['refmac'],
                resolution=self.args.resolution.value,
                refmap_path = self.args.reference_map.value,
                mode=mode,
                lib_path=self.args.lib_in.value,
                name='Map to MTZ (HM1)',
                job_location=self.job_location,
                pdb_path=self.process_shake.pdbout_path,
                pdbout_path='shifted_local_hm1.pdb',
                map_path=self.args.half_map_1.value,
                hklout_path=os.path.join(self.job_location,
                                         'starting_map_hm1.mtz'),
                blur_array=[self.args.map_sharpen.value],
                sharp_array=None,
                atomsf_path=self.atomsf)
            pl.append([self.process_maptomtz_hm1.process])

            # Refine shaken structure vs half map 1
            self.process_refine_hm1 = copy.deepcopy(
                self.process_refine)

            if self.args.local_refinement_on():
                self.process_refine_hm1.pdb_path = \
                    self.process_maptomtz_hm1.pdbout_path
                self.process_refine_hm1.mtz_path = 'masked_fs.mtz'
            else:
                self.process_refine_hm1.pdb_path = \
                    self.process_shake.pdbout_path
                self.process_refine_hm1.mtz_path = \
                    self.process_maptomtz_hm1.hklout_path

            self.process_refine_hm1.pdbout_path = os.path.join(
                self.job_location,
                'refined_hm1.pdb')
            self.process_refine_hm1.name = 'Refmac refine (HM1)'
            self.process_refine_hm1.set_process()
            pl.append([self.process_refine_hm1.process])

            # Convert half map 2 to mtz
            self.process_maptomtz_hm2 = RefmacMapToMtz(
                command=self.commands['refmac'],
                resolution=self.args.resolution.value,
                refmap_path = self.args.reference_map.value,
                lib_path=self.args.lib_in.value,
                mode=mode,
                name='Map to MTZ (HM2)',
                job_location=self.job_location,
                map_path=self.args.half_map_2.value,
                pdb_path=self.process_refine_hm1.pdbout_path,
                pdbout_path='shifted_local_hm2.pdb',
                hklout_path=os.path.join(self.job_location,
                                         'starting_map_hm2.mtz'),
                blur_array=[self.args.map_sharpen.value],
                sharp_array=None,
                atomsf_path=self.atomsf)
            pl.append([self.process_maptomtz_hm2.process])

            # Calculate FSC of model refined against half map 1
            # vs half map 2
            self.process_fsc_hm2 = copy.deepcopy(
                self.process_refine_hm1)

            # Set to 0 cycles to only calculate stats and do not reset
            # bfactors
            self.process_fsc_hm2.ncycle=0
            self.process_fsc_hm2.set_bfactor=False

            if self.args.local_refinement_on():
                self.process_fsc_hm2.pdb_path = \
                    self.process_maptomtz_hm2.pdbout_path
                self.process_fsc_hm2.mtz_path = 'masked_fs.mtz'
            else:
                self.process_fsc_hm2.pdb_path = \
                    self.process_refine_hm1.pdbout_path
                self.process_fsc_hm2.mtz_path = \
                    self.process_maptomtz_hm2.hklout_path

            self.process_fsc_hm2.pdbout_path = os.path.join(
                self.job_location,
                'refined_hm2.pdb')
            self.process_fsc_hm2.name = 'Refmac stats (HM2)'
            self.process_fsc_hm2.set_process()
            pl.append([self.process_fsc_hm2.process])

            # Calculate FSC of two half maps
            self.process_fsc_hm1_vs_hm2 = RefmacFSC(
                command=self.commands['refmac'],
                name='Refmac FSC (HM1 vs HM2)',
                job_location=self.job_location,
                map1_path=self.args.half_map_1.value,
                map2_path=self.args.half_map_2.value,
                resolution=self.args.resolution.value,
                refmap_path=self.args.reference_map.value,
                atomsf_path=self.atomsf)
            pl.append([self.process_fsc_hm1_vs_hm2.process])

        # Generate results
        refmac_results_path = os.path.realpath(refmac_results.__file__)
        refmac_results_process = process_manager.CCPEMProcess(
            name='Refmac results',
            command=self.commands['ccpem-python'],
            args=[refmac_results_path, self.job_location, mode],
            location=self.job_location)
        pl.append([refmac_results_process])

        if run:
            self.pipeline = process_manager.CCPEMPipeline(
                pipeline=pl,
                job_id=job_id,
                args_path=self.args.jsonfile,
                location=self.job_location,
                database_path=self.database_path,
                db_inject=db_inject,
                taskname=self.task_info.name,
                title=self.args.job_title.value,
                verbose=self.verbose)
            self.pipeline.start()


class SetModelCell(object):
    '''
    Set input PDB cell and scale cards to input map.
    '''
    def __init__(self,
                 job_location,
                 pdb_path,
                 map_path,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
        self.map_path = ccpem_utils.get_path_abs(map_path)
        self.pdbout_path = os.path.join(self.job_location, 'starting_model.pdb')
        self.cifout_path = os.path.join(self.job_location, 'starting_model.cif')
        #
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        #
        command = which('ccpem-python')
        self.args = [
            '-m', 'ccpem_core.model_tools.command_line',
            '--model', self.pdb_path,
            '--set_cell_from_map', self.map_path,
            '--output_pdb',
            '--output_cif',
            '--output_name', os.path.splitext(self.cifout_path)[0]
        ]
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location)


class LibgRestraints(object):
    '''
    Generates DNA/RNA restraints using libg; later they will be used by Refmac.
    '''
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 name=None,
                 w='',
                 u=''):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.pdb_path = pdb_path
        self.w = w
        self.u = u

        self.libg_u_path =  os.path.join(self.job_location,
                                        'libg_u.txt')
        if len(self.w) > 0:
            self.libg_restraints_path =  os.path.join(self.job_location,
                                            'libg_restraints_%s.txt' % self.w)
        else:
            self.libg_restraints_path =  os.path.join(self.job_location,
                                            'libg_restraints.txt')
        #
        self.set_stdin()
        self.set_args()
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['-p', self.pdb_path]
        self.args += ['-o', self.libg_restraints_path]
        if len('w') > 0:
            self.args += ['-w', self.w]
        if len('u') > 0:
            uFile = open(self.libg_u_path, 'w')
            uFile.write(self.u)
            uFile.close()
            self.args += ['-u', self.libg_u_path]

    def set_stdin(self):
        self.stdin = '\n'

class RefmacMapToMtz(object):
    '''
    Refmac process to convert map to mtz
    '''
    modes = ['Global',  'Local']

    def __init__(self,
                 command,
                 job_location,
                 map_path,
                 resolution=None,
                 mode='Global',
                 name=None,
                 pdb_path=None,
                 pdbout_path='shifted_local.pdb',
                 lib_path=None,
                 refmap_path=None,
                 hklout_path=None,
                 mrad=3.0,
                 atomsf_path=None,
                 blur_array=None,
                 sharp_array=None,
                 on_finish_custom=None):
        self.command = command
        self.job_location = job_location
        self.map_path = map_path
        self.resolution = resolution
        self.refmap_path = refmap_path
        self.mode = mode
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__ + self.mode
        assert self.mode in self.modes
        self.lib_path = lib_path
        self.pdb_path = pdb_path
        if self.mode == 'Local':
            assert self.pdb_path is not None
        self.pdbout_path = os.path.join(
            self.job_location,
            pdbout_path)
        self.mrad = mrad
        self.atomsf_path = atomsf_path
        self.blur_array = blur_array
#         if self.blur_array == [0.0]:
#             self.blur_array = None
        self.sharp_array = sharp_array
#         if self.sharp_array == [0.0]:
#             self.sharp_array = None
        self.stdin = None
        self.hklout_path = hklout_path
        if self.hklout_path is None:
            self.hklout_path = os.path.join(self.job_location,
                                            'starting_map.mtz')
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin,
            on_finish_custom=on_finish_custom)

    def set_args(self):
        self.args = ['mapin', self.map_path]
        self.args += ['hklout', self.hklout_path]
        if self.refmap_path is not None:
            self.args += ['mapref', self.refmap_path]
        if self.atomsf_path is not None:
            self.atomsf_path = ccpem_utils.get_path_abs(self.atomsf_path)
            self.args += ['atomsf', self.atomsf_path]
        if self.lib_path is not None:
            self.args += ['libin', self.lib_path]
        if self.pdb_path is not None:
            self.args += ['xyzin', self.pdb_path]
            self.args += ['xyzout', self.pdbout_path]

    def set_stdin(self):
        self.stdin = 'mode sfcalc'
        self.stdin += '\n' + refmac_source
        if self.resolution is not None:
            self.stdin += '\nreso {0}'.format(self.resolution)
        if self.mode == 'Local':
            self.stdin += '\nsfcalc mrad {0}'.format(self.mrad)
            self.stdin += '\nsfcalc shift'
        if self.blur_array is not None and len(self.blur_array) > 0:
            blur_str = '\nsfcalc blur'
            for b in self.blur_array:
                if b is not None:
                    blur_str += ' {0}'.format(b)
            self.stdin += blur_str
        if self.sharp_array is not None and len(self.sharp_array) > 0:
            if self.sharp_array[0] is not None:
                sharp_str = '\nsfcalc sharp'
                for b in self.sharp_array:
                    if b is not None:
                        sharp_str += ' {0}'.format(b)
                self.stdin += sharp_str
        self.stdin += '\nend'

class MolRepFindInMap(object):
    '''
    Rigid body search to dock PDB into map using Molrep
    '''
    def __init__(self,
                 job_location,
                 mtz_path,
                 pdb_path,
                 resolution,
                 name=None,
                 nmon=1.0,
                 f_labin='Fout0'):
        self.command = which('molrep')
        self.job_location = job_location
        self.mtz_path = mtz_path
        self.pdb_path = pdb_path
        self.nmon = nmon
        self.resolution = resolution
        self.f_labin = f_labin
        self.stdin = None
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.pdbout_path = os.path.join(self.job_location,
                                        'in_map.pdb')
        #
        self.set_args()
        self.set_stdin()
        # Move molrep.pdb to in_map.pdb
        custom_finish = FindInMapCustomFinish(job_location=self.job_location,
                                              pdbout_path=self.pdbout_path)
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin,
            on_finish_custom=custom_finish)

    def set_args(self):
        self.args = ['-f', self.mtz_path]
        self.args += ['-m', self.pdb_path]
        self.args += ['-i']

    def set_stdin(self):
        self.stdin = 'labin F={0} PH=Pout0'.format(self.f_labin)
        self.stdin += '\nnmon {0}'.format(self.nmon)
        self.stdin += '\nresmax {0}'.format(self.resolution)
        self.stdin += '\nscore n'
        self.stdin += '\nnp 5'
        self.stdin += '\nstick n'


class FindInMapCustomFinish(process_manager.CCPEMProcessCustomFinish):
    '''
    Find in map custom finish to mv 'molrep.pdb' to 'in_map.pdb.
    '''
    def __init__(self,
                 job_location,
                 pdbout_path):
        super(FindInMapCustomFinish, self).__init__()
        self.job_location = job_location
        self.pdbout_path = pdbout_path

    def on_finish(self, parent_process=None):
        molrep_pdb = os.path.join(self.job_location,
                                  'molrep.pdb')
        if os.path.exists(molrep_pdb):
            shutil.move(molrep_pdb,
                        self.pdbout_path)


class RefmacRefine(object):
    '''
    Refmac refinement job.  Global or local refinement.
    '''
    modes = ['Global',  'Local']

    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 resolution,
                 mtz_path,
                 mode='Global',
                 pdbout_path='refined.pdb',
                 name=None,
                 atomsf_path=None,
                 sharp=None,
                 lib_path=None,
                 ncycle=20,
                 set_bfactor=False,
                 atom_bfactor=None,
                 jelly_body_on=True,
                 jelly_body_sigma=0.01,
                 jelly_body_dmax=4.2,
                 hydrogens=False,
                 external_restraints_on=False,
                 external_restraints_path=None,
                 external_weight_scale=None,
                 external_weight_dmax=4.2,
                 external_weight_gmwt=None,
                 external_main_chain=False,
                 vector_diff_map=False,
                 weight=0.0001,
                 weight_auto=True,
                 symmetry_auto='Local',
                 keywords=None,
                 libg_restraints=False,
                 libg_restraints_path=None,
                 output_hkl=False):
        self.command = command
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.resolution = resolution
        self.mode = mode
        assert self.mode in self.modes
        self.mtz_path = mtz_path
        self.pdbout_path = os.path.join(self.job_location,
                                        pdbout_path)
        self.cifout_path = os.path.splitext(self.pdbout_path)[0] + '.mmcif'
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__ + self.mode
        self.mtz_path = os.path.join(self.job_location,
                                     self.mtz_path)
        self.atomsf_path = atomsf_path
        self.sharp = sharp
        self.lib_path = lib_path
        self.ncycle = ncycle
        self.set_bfactor = set_bfactor
        self.atom_bfactor = atom_bfactor
        self.weight = weight
        self.weight_auto = weight_auto
        self.symmetry_auto = symmetry_auto
        self.jelly_body_on = jelly_body_on
        self.jelly_body_sigma = jelly_body_sigma
        self.jelly_body_dmax = jelly_body_dmax
        self.hydrogens = hydrogens
        self.external_restraints_on = external_restraints_on
        self.external_restraints_path = external_restraints_path
        if self.external_restraints_path is not None:
            if type(self.external_restraints_path) is list:
                absPathList = []
                for path in self.external_restraints_path:
                    absPathList.append(ccpem_utils.get_path_abs(
                    path))
                self.external_restraints_path = absPathList
        self.external_weight_scale = external_weight_scale
        self.external_weight_dmax = external_weight_dmax
        self.external_weight_gmwt = external_weight_gmwt
        self.external_main_chain = external_main_chain
        self.vector_diff_map = vector_diff_map
        self.keywords = keywords
        self.stdin = None
        self.libg_restraints=libg_restraints
        self.libg_restraints_path=libg_restraints_path
        self.output_hkl = output_hkl
        #
        self.set_process()

    def set_process(self):
        self.set_args()
        self.set_stdin()
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        self.args += ['hklin', self.mtz_path]
        if self.lib_path is not None:
            self.args += ['libin', self.lib_path]
        self.args += ['xyzout', self.pdbout_path]
        if self.atomsf_path is not None:
            self.atomsf_path = ccpem_utils.get_path_abs(self.atomsf_path)
            self.args += ['atomsf', self.atomsf_path]
        if self.output_hkl:
            self.hklout_path = os.path.join(self.job_location,'refine.mtz')
            self.args += ['hklout', self.hklout_path]

    def set_stdin(self):
        self.stdin = 'labin FP=Fout0 PHIB=Pout0'
        if self.mode == 'Local':
            self.stdin += '\n@shifts.txt'
        if self.hydrogens:
            self.stdin += '\nmake hydr all'
            self.stdin += '\nhout no' # do NOT write hydrogens out
        else:
            self.stdin += '\nmake hydr no'
        self.stdin += '\nsolvent no'
        self.stdin += '\n' + refmac_source
        # Keyword to fix smooth gauss error
#         out += '\nscale lsscale function lsq'
        if self.vector_diff_map:
            self.stdin += '\nmapc vector'
        if self.sharp is not None:
            if self.sharp != 0:
                self.stdin += '\nrefine sharp {0}'.format(self.sharp)
        self.stdin += '\nncycle {0}'.format(self.ncycle)
        if self.weight_auto:
            self.stdin += '\nweight auto'
        else:
            if self.weight is not None:
                self.stdin += '\nweight matrix {0}'.format(self.weight)
        if self.symmetry_auto == 'Local':
            self.stdin += '\nncsr local'
        elif self.symmetry_auto == 'Global':
            self.stdin += '\nncsr global'
        if self.set_bfactor and self.atom_bfactor is not None:
            self.stdin += '\nBFACtor SET {0}'.format(self.atom_bfactor)
        self.stdin += '\nreso {0}'.format(self.resolution)
        if self.jelly_body_on:
            if self.jelly_body_on:
                self.stdin += '\nridge dist sigma {0}'.format(
                    self.jelly_body_sigma)
                self.stdin += '\nridge dist dmax {0}'.format(
                    self.jelly_body_dmax)
        if self.libg_restraints:
            for fileName in self.libg_restraints_path:
                self.stdin += '\n@' + fileName

# External restraints
        if self.external_restraints_on:

            if (len(self.external_weight_scale) == len(self.external_weight_gmwt)) and (len(self.external_weight_scale) == len(self.external_restraints_path)):
                if self.external_main_chain:
                    self.stdin += '\nEXTERNAL USE MAIN'
                self.stdin += '\nEXTERNAL DMAX {0}'.format(
                    self.external_weight_dmax)

                for erp, ews, ewg in zip(self.external_restraints_path, self.external_weight_scale, self.external_weight_gmwt):
                    self.stdin += '\nEXTERNAL WEIGHT SCALE {0}'.format(ews)
                    self.stdin += '\nEXTERNAL WEIGHT GMWT {0}'.format(ewg)
                    self.stdin += '\n@' + erp

            else:
                print 'Something wrong with external restraints, they will not be used'
                print self.external_restraints_path
                print self.external_weight_scale
                print self.external_weight_gmwt
# end of external restraints

            self.stdin += '\nMONI DIST 1000000'
        if self.keywords is not None:
            self.stdin += '\n' + self.keywords
        self.stdin += '\nend'



class RefmacSfcalcCrd(object):
    '''
    Calculate sfcalc (mtz) from coordinate input.
    '''
    command = 'refmac5'
    def __init__(self,
                 job_location,
                 pdb_path,
                 lib_in=None,
                 resolution=None,
                 name=None,
                 command = None):
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.lib_in = lib_in
        self.resolution = resolution
        if name is None:
            self.name = self.__class__.__name__
        else:
            self.name = name
        if command is not None:
            self.command = command
        self.set_args()
        self.set_stdin()

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        if self.lib_in is not None:
            self.args += ['lib_in', self.lib_in]

    def set_stdin(self):
        self.stdin = 'mode sfcalc'
        self.stdin += '\nsfcalc cr2f'
        self.stdin += '\n' + refmac_source
        self.stdin += '\nresol {0}'.format(self.resolution)
        self.stdin += '\nend'


class PDBSetShake(object):
    '''
    PDBSet process to shake structure for cross validation.
    '''
    def __init__(self,
                 job_location,
                 pdb_path,
                 pdbout_path=None,
                 name=None,
                 shift=0.5):
        command = which('pdbset')
        assert command is not None
        self.job_location = job_location
        self.pdb_path = pdb_path
        self.pdbout_path = pdbout_path
        if self.pdbout_path is None:
            self.pdbout_path = os.path.join(self.job_location,
                                            'shaked.pdb')
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.stdin = None
        #
        self.set_args()
        self.set_stdin(shift)
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        self.args += ['xyzout', self.pdbout_path]

    def set_stdin(self, shift):
        self.stdin = '\nnoise {}\nend'.format(shift)


class RefmacFSC(object):
    '''
    Refmac process to shift back coordinates after local refinement
    '''
    def __init__(self,
                 command,
                 job_location,
                 map1_path,
                 map2_path,
                 resolution=None,
                 refmap_path=None,
                 name=None,
                 atomsf_path=None):
        self.command = command
        self.job_location = job_location
        self.map1_path = map1_path
        self.map2_path = map2_path
        self.resolution = resolution
        self.refmap_path = refmap_path
        self.atomsf_path = atomsf_path
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.stdin = None
        #
        self.set_args()
        self.set_stdin()
        #
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['mapin1', self.map1_path]
        self.args += ['mapin2', self.map2_path]
        if self.refmap_path is not None:
            self.args += ['mapref', self.refmap_path]
        if self.atomsf_path is not None:
            self.atomsf_path = ccpem_utils.get_path_abs(self.atomsf_path)
            self.args += ['atomsf', self.atomsf_path]

    def set_stdin(self):
        self.stdin = '\nmode sfcalc'
        self.stdin += '\nsfcalc fsc'
        if self.resolution is not None:
            self.stdin += '\nRESO {0}'.format(self.resolution)
        self.stdin += '\nend'


class DivideAndConquer(object):
    '''
    Divide and Conquer script for refinement of large complexes (several PDBs and several maps)
    '''
    def __init__(self,
                 command,
                 job_location,
                 option_l = None,
                 option_cluster = False,
                 option_nCPUsOrNodes = 1,
                 option_ncyc = None,
                 option_timeQuant = None,
                 name = None,
                 lib_in = None,
                 sharp = None,
                 weight  = None,
                 weight_auto = True,
                 set_bfactor  = None,
                 atom_bfactor = None,
                 jelly_body_on = None,
                 jelly_body_sigma = 0.01,
                 jelly_body_dmax = 4.2,
                 hydrogens=False,
                 keywords  = None ):
        self.command = command
        self.job_location = job_location

        self.option_l = option_l
        self.option_nCPUsOrNodes = option_nCPUsOrNodes
        self.option_cluster = option_cluster
        self.option_ncyc = option_ncyc
        self.option_timeQuant = option_timeQuant

        self.lib_in = lib_in
        self.sharp = sharp
        self.weight = weight
        self.weight_auto = weight_auto
        self.set_bfactor  = set_bfactor
        self.atom_bfactor = atom_bfactor
        self.jelly_body_on = jelly_body_on
        self.jelly_body_sigma = jelly_body_sigma
        self.jelly_body_dmax = jelly_body_dmax
        self.hydrogens = hydrogens
        self.keywords  = keywords

        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.stdin = None
        #
        self.set_args()
        self.set_stdin()
        #

        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=self.command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def set_args(self):
        self.args = ['-l', self.option_l]
        self.args += ['-i']
        if self.option_cluster:
            self.args += ['-cluster', self.option_nCPUsOrNodes]
        else:
            self.args += ['-cpu', self.option_nCPUsOrNodes]
        if self.option_ncyc is not None:
            self.args += ['-ncyc', self.option_ncyc]
        if self.option_timeQuant is not None:
            self.args += ['-time', self.option_timeQuant]
        if self.lib_in is not None:
            self.args += ['-lib_in', self.lib_in]



    def set_stdin(self):
        self.stdin = 'labin FP=Fout0 PHIB=Pout0'
        if self.hydrogens:
            self.stdin += '\nmake hydr all'
            self.stdin += '\nhout no' # do NOT write hydrogens out
        else:
            self.stdin += '\nmake hydr no'
        self.stdin += '\nsolvent no'
        self.stdin += '\nmake segid yes'
        self.stdin += '\n@shifts.txt'
        if self.sharp is not None:
            if self.sharp != 0:
                self.stdin += '\nrefine sharp {0}'.format(self.sharp)
        if self.weight_auto:
            self.stdin += '\nweight auto'
        else:
            if self.weight is not None:
                self.stdin += '\nweight matrix {0}'.format(self.weight)
        if self.set_bfactor and self.atom_bfactor is not None:
            self.stdin += '\nBFACtor SET {0}'.format(self.atom_bfactor)
        if self.jelly_body_on:
            if self.jelly_body_on:
                self.stdin += '\nridge dist sigma {0}'.format(
                    self.jelly_body_sigma)
                self.stdin += '\nridge dist dmax {0}'.format(
                    self.jelly_body_dmax)
            self.stdin += '\nMONI DIST 1000000'
        if self.keywords is not None:
            self.stdin += '\n' + self.keywords
        self.stdin += '\nend\n'
