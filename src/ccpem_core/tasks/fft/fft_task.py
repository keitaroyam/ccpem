#
#     Copyright (C) 2018 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#

import os
import shutil
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import settings
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks import bin_wrappers


class MTZtoMRC(task_utils.CCPEMTask):
    '''
    CCPEM FFT task.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='MRCtoMTZ',
        author='Lynn F. Ten Eyck',
        version='1',
        description=(
            '<p>Wrapper which converts mtz file to mrc using CCP4 FFT '
            'function N.B. requires CCP4.</p>'
            '<p>Full documentation:</p>'
            '<p>http://www.bin_wrappers.ac.uk/html/fft.html</p>'),
        short_description=(
            'Convert MTZ to MRC.  Requires CCP4'),
        documentation_link='http://www.bin_wrappers.ac.uk/html/fft.html',
        references=None)
    commands = {
        'fft': settings.which(program='fft'),
        }

    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
            super(MTZtoMRC, self).__init__(
                database_path=database_path,
                args=args,
                args_json=args_json,
                pipeline=pipeline,
                job_location=job_location,
                parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        parser.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-input_mtz',
            '--input_mtz',
            help='''Input mtz structure factor files''',
            metavar='Input mtz',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-amplitude_label',
            '--amplitude_label',
            help='''Column label for amplitudes''',
            metavar='Amplitude label',
            type=str,
            default=None)
        #
        parser.add_argument(
            '-phase_label',
            '--phase_label',
            help='''Column label for phases''',
            metavar='Phase label',
            type=str,
            default=None)
        #
        
        #
        parser.add_argument(
            '-keywords',
            '--keywords',
            help='Keywords for advanced options.  Select file or define text',
            type=str,
            metavar='Keywords',
            default='')
        #
        return parser

    def run_pipeline(self, job_id=None, db_inject=None):
        # Generate processes

        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            db_inject=db_inject,
            location=self.job_location,
            database_path=self.database_path,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()
