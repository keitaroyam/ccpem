#
#     Copyright (C) 2016 CCP-EM
#
#     This code is distributed under the terms and conditions of the
#     CCP-EM Program Suite Licence Agreement as a CCP-EM Application.
#     A copy of the CCP-EM licence can be obtained by writing to the
#     CCP-EM Secretary, RAL Laboratory, Harwell, OX11 0FA, UK.
#


import os
from ccpem_core.ccpem_utils import ccpem_argparser
from ccpem_core import ccpem_utils
from ccpem_core import process_manager
from ccpem_core.tasks import task_utils
from ccpem_core.tasks.refmac import refmac_task
from ccpem_core.tasks.bin_wrappers import fft
from ccpem_core.map_tools.TEMPy import map_preprocess
from ccpem_core.model_tools.gemmi_utils import shift_coordinates, remove_atomic_charges
import mrcfile
from ccpem_core import settings
import shutil,json
from model2map_utils import set_cubic_map_grid, set_map_grid

class Model2Map(task_utils.CCPEMTask):
    '''
    CCPEM tool for atomic model to map conversion.
    '''
    task_info = task_utils.CCPEMTaskInfo(
        name='model2map',
        author='G. Murshudov, R. Nicholas, T.Burnley, C.Palmer, A.Joseph',
        version='1.0',
        description=(
            '''
            Tool for atomic model to MRC map conversion using pdbset (CCP4), 
            Refmac5 (CCP4), gemmi, mrcfile and TEMPy mapprocess. 
            '''
                    ),
        short_description=(
            'Model to map coversion'),
        documentation_link='',
        references=None)

    commands = {
        'refmac': settings.which(program='refmac5'),
        'pdbset':  settings.which(program='pdbset'),
        'ccpem-gemmi':settings.which(program='gemmi'),
        'ccpem-mapprocess':
        ['ccpem-python', os.path.realpath(map_preprocess.__file__)]}
    
    def __init__(self,
                 database_path=None,
                 args=None,
                 args_json=None,
                 pipeline=None,
                 job_location=None,
                 parent=None):
        #
        super(Model2Map, self).__init__(
             database_path=database_path,
             args=args,
             args_json=args_json,
             pipeline=pipeline,
             job_location=job_location,
             parent=parent)

    def parser(self):
        parser = ccpem_argparser.ccpemArgParser()
        #
        job_title = parser.add_argument_group()
        job_title.add_argument(
            '-job_title',
            '--job_title',
            help='Short description of job',
            metavar='Job title',
            type=str,
            default=None)
        #
        job_location = parser.add_argument_group()
        job_location.add_argument(
            '-job_location',
            '--job_location',
            help='Directory to run job',
            metavar='Job location',
            type=str,
            default=None)
        #
        pdb_path = parser.add_argument_group()
        pdb_path.add_argument(
            '-pdb_path',
            '--pdb_path',
            help=('Synthetic map will be created and difference maps '
                  'calculated using this'),
            metavar='Input PDB',
            type=str,
            default=None)
        #
        map_resolution = parser.add_argument_group()
        map_resolution.add_argument(
            '-map_resolution',
            '--map_resolution',
            help='''Resolution of synthetic map (Angstrom)''',
            metavar='Resolution of map',
            type=float,
            default=None)
        #ligand dictionary for Refmac
        parser.add_argument(
            '-lib_in',
            '--lib_in',
            help='Ligand dictionary for Refmac (cif format)',
            metavar='Ligand dictionary',
            type=str,
            default=None)
        #
        map_apix = parser.add_argument_group()
        map_apix.add_argument(
            '-map_apix',
            '--map_apix',
            help='''Grid spacing for synthetic map (Angstrom)''',
            metavar='Grid spacing',
            type=float,
            default=None)
        #
        map_path = parser.add_argument_group()
        map_path.add_argument(
            '-map_path',
            '--map_path',
            help='Input reference map (mrc format)',
            metavar='Input reference map',
            type=str,
            default=None)
        
        map_path.add_argument(
            '-map_edit_path',
            '--map_edit_path',
            help='Edited map (mrc format)',
            metavar='Edited input map',
            type=str,
            default=None)
                
        return parser
    
    def edit_argsfile(self,args_file):
        with open(args_file,'r') as f:
            json_args = json.load(f)
        #set map and pdb paths if fixed for origin
        json_args['pdb_path'] = self.pdb_edit_path
                    
        with open(args_file,'w') as f:
            json.dump(json_args,f)
    
    def fix_model_for_refmac(self,pdb_path,pdb_edit_path,trans_vector,
                             remove_charges=False):
        shift_coordinates(pdb_path,pdb_edit_path,trans_vector,remove_charges)
    
    def run_pipeline(self, job_id=None, db_inject=None):
        pl = []
        fix_map_model = True
        self.map_edit_path = None
        #use Refmac for model map conversion
        #if a target map is given
        if self.args.map_path.value != None and \
                os.path.isfile(self.args.map_path.value):
            self.args.map_edit_path.value = self.args.map_path.value
            self.args.map_edit_path.value, fix_map_model, self.cella, (ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz) = \
                fix_map_model_refmac(self.args.map_path.value,self.args.map_edit_path.value,
                                     self.job_location)
#                     #reset map path
#                     self.args.map_path.value = self.map_edit_path
        else:
            #find map grid origin and dimensions
            #voxel size = resolution/3
            if self.args.map_apix.value == None or self.args.map_apix.value == 0.0:
                apix = self.args.map_resolution.value/3.0
            else:
                apix = self.args.map_apix.value
            #add edge to the box bounding atom coordinates
            edge = max(30.0,3*self.args.map_resolution.value)
            ori,map_n = set_map_grid(self.args.pdb_path.value,apix,edge=edge)
            map_nx,map_ny,map_nz = map_n
            max_dim = max(map_n)
            ori_x,ori_y,ori_z = ori
            self.cella = (map_nx*apix,map_ny*apix,map_nz*apix)
            #cella = (max_dim*apix,max_dim*apix,max_dim*apix)
            
        ##self.pdb_edit_path = self.args.pdb_path.value
        self.pdb_edit_path = os.path.join(self.job_location,
                                os.path.splitext(os.path.basename(
                                    self.args.pdb_path.value))[0]+'_fix.pdb')
        #translate model to origin 0 if nstart is 0
        if fix_map_model and not all(o == 0. for o in (ori_x,ori_y,ori_z)):
            trans_vector = [-ori_x,
                            -ori_y,
                            -ori_z]
            self.fix_model_for_refmac(self.args.pdb_path.value,
                                      self.pdb_edit_path,trans_vector,
                                      remove_charges=True)
        else:
            #remove charges
            remove_atomic_charges(self.args.pdb_path.value,
                                      self.pdb_edit_path)
#             #reset pdb path
#             self.args.pdb_path.value = self.pdb_edit_path
#             args_file = os.path.join(self.job_location,
#                          'args.json')
#             self.edit_argsfile(args_file)
            
        model_map_filename = os.path.splitext(
                    os.path.basename(self.args.pdb_path.value))[0]
        self.reference_map = os.path.join(
            self.job_location,
            model_map_filename+'_refmac.mrc')
        # Set PDB cryst from mtz
        self.process_pdb_set = PDBSetCell(
            command=self.commands['pdbset'],
            job_location=self.job_location,
            name='Set PDB cell',
            pdb_path=self.pdb_edit_path,
            cella = self.cella,
            map_path=self.args.map_edit_path.value)
        pl = [[self.process_pdb_set.process]]

        self.pdb_set_path = os.path.join(
            self.job_location,
            'pdbset.pdb')

        # Calculate mtz from pdb 
        self.refmac_sfcalc_crd_process = refmac_task.RefmacSfcalcCrd(
            job_location=self.job_location,
            pdb_path=self.pdb_set_path,
            lib_in=self.args.lib_in(),
            resolution=self.args.map_resolution.value,
            command=self.commands['refmac'])
        self.refmac_sfcalc_mtz = os.path.join(
            self.job_location,
            'sfcalc_from_crd.mtz')
        pl.append([self.refmac_sfcalc_crd_process.process])
        #gemmi sf2map
        self.sf2map_process = sf2mapWrapper(
            job_location=self.job_location,
            command=self.commands['ccpem-gemmi'],
            map_nx=map_nx,
            map_ny=map_ny,
            map_nz=map_nz,
            # For now only use x,y,z
#                 fast=mrc_axis[int(map_mapc)],
#                 medium=mrc_axis[int(map_mapr)],
#                 slow=mrc_axis[int(map_maps)],
            fast='X',
            medium='Y',
            slow='Z',
            mtz_path=self.refmac_sfcalc_mtz,
            map_path=self.reference_map)
        pl.append([self.sf2map_process.process])
        
# #         # Convert calc mtz to map
#         self.fft_process = fft.FFT(
#             job_location=self.job_location,
#             map_nx=map_nx,
#             map_ny=map_ny,
#             map_nz=map_nz,
#             # For now only use x,y,z
# #                 fast=mrc_axis[int(map_mapc)],
# #                 medium=mrc_axis[int(map_mapr)],
# #                 slow=mrc_axis[int(map_maps)],
#             fast='X',
#             medium='Y',
#             slow='Z',
#             mtz_path=self.refmac_sfcalc_mtz,
#             map_path=self.reference_map)
#         pl.append([self.fft_process.process])
        
        #set origin and remove background
        self.reference_map_processed = os.path.join(
                self.job_location,
                model_map_filename+'_syn.mrc')
        if not all(o == 0. for o in (ori_x,ori_y,ori_z)):
            self.mapprocess_wrapper = mapprocess_wrapper(
                            job_location=self.job_location,
                            command=self.commands['ccpem-mapprocess'],
                            map_path=self.reference_map,
                            list_process=['shift_origin','threshold'],
                            map_origin=(ori_x,ori_y,ori_z),
                            map_contour=0.09,
                            out_map=self.reference_map_processed
                            )
        else:
            self.mapprocess_wrapper = mapprocess_wrapper(
                            job_location=self.job_location,
                            command=self.commands['ccpem-mapprocess'],
                            map_path=self.reference_map,
                            list_process=['threshold'],
                            map_origin=(ori_x,ori_y,ori_z),
                            map_contour=0.09,
                            out_map=self.reference_map_processed
                            )
        pl.append([self.mapprocess_wrapper.process])
        
        self.reference_map = self.reference_map_processed
        # pipeline
        self.pipeline = process_manager.CCPEMPipeline(
            pipeline=pl,
            job_id=job_id,
            args_path=self.args.jsonfile,
            location=self.job_location,
            database_path=self.database_path,
            db_inject=db_inject,
            taskname=self.task_info.name,
            title=self.args.job_title.value)
        self.pipeline.start()

def fix_map_model_refmac(mapfile,mapeditfile,job_location):
    ori_x = ori_y = ori_z = 0.
    fix_map_model = True
    with mrcfile.open(mapfile, mode='r',permissive=True) as mrc:
        map_nx = mrc.header.nx
        map_ny = mrc.header.ny
        map_nz = mrc.header.nz
        map_nxstart = mrc.header.nxstart
        map_nystart = mrc.header.nystart
        map_nzstart = mrc.header.nzstart
        map_mapc = mrc.header.mapc
        map_mapr = mrc.header.mapr
        map_maps = mrc.header.maps
        ori_x = mrc.header.origin.x
        ori_y = mrc.header.origin.y
        ori_z = mrc.header.origin.z
        cella = (mrc.header.cella.x,mrc.header.cella.y,
                 mrc.header.cella.z)
        apix = mrc.voxel_size.item()
        map_dim = (map_nx,map_ny,map_nz)
    #fix map and model when nstart is non-zero
    if ori_x == 0. and ori_y == 0. and ori_z == 0.:
        if map_nxstart != 0 or map_nystart != 0 or map_nzstart != 0:
            #set nstart to 0 and fix origin (for Refmac5)
            map_edit = os.path.splitext(
                        os.path.basename(mapfile))[0]+'_fix.mrc'
            mapeditfile = os.path.join(job_location,map_edit)
            shutil.copyfile(mapfile,mapeditfile)
            try: assert os.path.isfile(mapeditfile)
            except AssertionError:
                mapeditfile,fix_map_model, cella,(ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)
            #edit float origin
            with mrcfile.open(mapeditfile,'r+') as mrc:
                mrc.header.origin.x = map_nxstart*apix[0]
                mrc.header.origin.y = map_nystart*apix[1]
                mrc.header.origin.z = map_nzstart*apix[2]
                
            ori_x = map_nxstart*apix[0]
            ori_y = map_nystart*apix[1]
            ori_z = map_nzstart*apix[2]
            
            
    return mapeditfile,fix_map_model, cella,(ori_x,ori_y,ori_z), (map_nx,map_ny,map_nz)


class PDBSetCell(object):
    '''
    Set input PDB cell and scale cards to input dimensions.
    '''
    def __init__(self,
                 command,
                 job_location,
                 pdb_path,
                 map_path=None,
                 cella=None,
                 name=None):
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.pdb_path = ccpem_utils.get_path_abs(pdb_path)
        self.pdbout_path = os.path.join(self.job_location,
                                        'pdbset.pdb')
        self.cell_info = None
        self.stdin = None
        #
        if map_path is None:
            self.cell_info = [cella[0],
                              cella[1],
                              cella[2],
                              90.0,
                              90.0,
                              90.0]
                                  
        else:
            self.map_path = ccpem_utils.get_path_abs(map_path)
            self.get_cell_parameters()
        self.set_stdin()
        self.set_args()
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=self.stdin)

    def get_cell_parameters(self):
        if os.path.exists(path=self.map_path):
            with mrcfile.open(self.map_path, header_only=True) as map:
                self.cell_info = [map.header.cella.x,
                                  map.header.cella.y,
                                  map.header.cella.z,
                                  map.header.cellb.alpha,
                                  map.header.cellb.beta,
                                  map.header.cellb.gamma]

    def set_args(self):
        self.args = ['xyzin', self.pdb_path]
        self.args += ['xyzout', self.pdbout_path]

    def set_stdin(self):
        if self.cell_info is not None:
            self.stdin = 'CELL '
            for p in self.cell_info:
                self.stdin += ' {0}'.format(p)
            #self.stdin += "\nEXCLUDE HEADERS" #exclude header records
            self.stdin += "\nSPACEGROUP P1"
            self.stdin += '\nend'

class sf2mapWrapper():
    '''
    Wrapper for gemmi sf2map
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_nx,
                 map_ny,
                 map_nz,
                 fast,
                 medium,
                 slow,
                 mtz_path,
                 map_path,
                 name=None):
        
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        # Set args
        self.args = ['sf2map','-f','Fout0','-p','Pout0']
        gridarg = '--grid='+','.join([str(map_nx),str(map_ny),str(map_nz)])
        self.args += [gridarg]
        self.args += ['--exact',mtz_path,map_path]
        # Set process
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)

class mapprocess_wrapper():
    '''
    Wrapper for TEMPy Map Processing tool.
    '''
    def __init__(self,
                 job_location,
                 command,
                 map_path,
                 list_process,
                 map_resolution=None,
                 map_contour=None,
                 map_apix=None,
                 map_pad=None,
                 map_origin=None,
                 mask_path=None,
                 out_map=None,
                 name='MapProcess'):
        assert map_path is not None
        self.job_location = ccpem_utils.get_path_abs(job_location)
        self.name = name
        if self.name is None:
            self.name = self.__class__.__name__
        self.map_path = ccpem_utils.get_path_abs(map_path)
        # Set args
        self.args = ['-m', self.map_path]
        if list_process is not None:
            list_process_args = ['-l']
            list_process_args.extend(list_process)
            self.args += list_process_args
        if map_resolution is not None:
            self.args += ['-r', map_resolution]
        #threshold
        if map_contour is not None:
            self.args += ['-t', map_contour]
        #new apix
        if map_apix is not None:
            self.args += ['-p', map_apix]
        #padding
        if map_pad is not None:
            pad_args = ['-pad']
            pad_args.extend(list(map_pad))
            if len(map_pad) > 0: self.args += pad_args#' '.join(
                                                    #[str(p) for p in map_pad])]
        #new origin
        if map_origin is not None:
            origin_args = ['-ori']
            origin_args.extend(map_origin)
            if len(map_origin) > 0: self.args += origin_args
        #mask file
        if mask_path is not None:
            self.args += ['-ma', mask_path]
        
        if out_map is not None:
            self.args += ['-out', out_map]
        
        assert command is not None
        self.process = process_manager.CCPEMProcess(
            name=self.name,
            command=command,
            args=self.args,
            location=self.job_location,
            stdin=None)
